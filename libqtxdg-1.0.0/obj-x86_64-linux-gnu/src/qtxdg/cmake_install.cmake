# Install script for directory: /home/debian/lxqt/libqtxdg/libqtxdg-3.8.0/src/qtxdg

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "RelWithDebInfo")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

# Set default install directory permissions.
if(NOT DEFINED CMAKE_OBJDUMP)
  set(CMAKE_OBJDUMP "/usr/bin/objdump")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xRuntimex" OR NOT CMAKE_INSTALL_COMPONENT)
  foreach(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/x86_64-linux-gnu/libQt5Xdg.so.3.8.0"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/x86_64-linux-gnu/libQt5Xdg.so.3"
      )
    if(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      file(RPATH_CHECK
           FILE "${file}"
           RPATH "")
    endif()
  endforeach()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/x86_64-linux-gnu" TYPE SHARED_LIBRARY FILES
    "/home/debian/lxqt/libqtxdg/libqtxdg-3.8.0/obj-x86_64-linux-gnu/src/qtxdg/libQt5Xdg.so.3.8.0"
    "/home/debian/lxqt/libqtxdg/libqtxdg-3.8.0/obj-x86_64-linux-gnu/src/qtxdg/libQt5Xdg.so.3"
    )
  foreach(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/x86_64-linux-gnu/libQt5Xdg.so.3.8.0"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/x86_64-linux-gnu/libQt5Xdg.so.3"
      )
    if(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      file(RPATH_CHANGE
           FILE "${file}"
           OLD_RPATH "/home/debian/lxqt/libqtxdg/libqtxdg-3.8.0/obj-x86_64-linux-gnu/src/xdgiconloader:"
           NEW_RPATH "")
      if(CMAKE_INSTALL_DO_STRIP)
        execute_process(COMMAND "/usr/bin/strip" "${file}")
      endif()
    endif()
  endforeach()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xRuntimex" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/x86_64-linux-gnu/libQt5Xdg.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/x86_64-linux-gnu/libQt5Xdg.so")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/x86_64-linux-gnu/libQt5Xdg.so"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/x86_64-linux-gnu" TYPE SHARED_LIBRARY FILES "/home/debian/lxqt/libqtxdg/libqtxdg-3.8.0/obj-x86_64-linux-gnu/src/qtxdg/libQt5Xdg.so")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/x86_64-linux-gnu/libQt5Xdg.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/x86_64-linux-gnu/libQt5Xdg.so")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/x86_64-linux-gnu/libQt5Xdg.so"
         OLD_RPATH "/home/debian/lxqt/libqtxdg/libqtxdg-3.8.0/obj-x86_64-linux-gnu/src/xdgiconloader:"
         NEW_RPATH "")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/x86_64-linux-gnu/libQt5Xdg.so")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xDevelx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/qt5xdg" TYPE FILE FILES
    "/home/debian/lxqt/libqtxdg/libqtxdg-3.8.0/src/qtxdg/xdgaction.h"
    "/home/debian/lxqt/libqtxdg/libqtxdg-3.8.0/src/qtxdg/xdgdesktopfile.h"
    "/home/debian/lxqt/libqtxdg/libqtxdg-3.8.0/src/qtxdg/xdgdirs.h"
    "/home/debian/lxqt/libqtxdg/libqtxdg-3.8.0/src/qtxdg/xdgicon.h"
    "/home/debian/lxqt/libqtxdg/libqtxdg-3.8.0/src/qtxdg/xdgmenu.h"
    "/home/debian/lxqt/libqtxdg/libqtxdg-3.8.0/src/qtxdg/xdgmenuwidget.h"
    "/home/debian/lxqt/libqtxdg/libqtxdg-3.8.0/src/qtxdg/xmlhelper.h"
    "/home/debian/lxqt/libqtxdg/libqtxdg-3.8.0/src/qtxdg/xdgautostart.h"
    "/home/debian/lxqt/libqtxdg/libqtxdg-3.8.0/src/qtxdg/xdgmacros.h"
    "/home/debian/lxqt/libqtxdg/libqtxdg-3.8.0/src/qtxdg/xdgmimetype.h"
    "/home/debian/lxqt/libqtxdg/libqtxdg-3.8.0/src/qtxdg/xdgmimeapps.h"
    "/home/debian/lxqt/libqtxdg/libqtxdg-3.8.0/src/qtxdg/xdgdefaultapps.h"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xDevelx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/qt5xdg" TYPE FILE FILES
    "/home/debian/lxqt/libqtxdg/libqtxdg-3.8.0/obj-x86_64-linux-gnu/InTreeBuild/include/qt5xdg/XdgAction"
    "/home/debian/lxqt/libqtxdg/libqtxdg-3.8.0/obj-x86_64-linux-gnu/InTreeBuild/include/qt5xdg/XdgDesktopFile"
    "/home/debian/lxqt/libqtxdg/libqtxdg-3.8.0/obj-x86_64-linux-gnu/InTreeBuild/include/qt5xdg/XdgDirs"
    "/home/debian/lxqt/libqtxdg/libqtxdg-3.8.0/obj-x86_64-linux-gnu/InTreeBuild/include/qt5xdg/XdgIcon"
    "/home/debian/lxqt/libqtxdg/libqtxdg-3.8.0/obj-x86_64-linux-gnu/InTreeBuild/include/qt5xdg/XdgMenu"
    "/home/debian/lxqt/libqtxdg/libqtxdg-3.8.0/obj-x86_64-linux-gnu/InTreeBuild/include/qt5xdg/XdgMenuWidget"
    "/home/debian/lxqt/libqtxdg/libqtxdg-3.8.0/obj-x86_64-linux-gnu/InTreeBuild/include/qt5xdg/XmlHelper"
    "/home/debian/lxqt/libqtxdg/libqtxdg-3.8.0/obj-x86_64-linux-gnu/InTreeBuild/include/qt5xdg/XdgAutoStart"
    "/home/debian/lxqt/libqtxdg/libqtxdg-3.8.0/obj-x86_64-linux-gnu/InTreeBuild/include/qt5xdg/XdgMimeType"
    "/home/debian/lxqt/libqtxdg/libqtxdg-3.8.0/obj-x86_64-linux-gnu/InTreeBuild/include/qt5xdg/XdgMimeApps"
    "/home/debian/lxqt/libqtxdg/libqtxdg-3.8.0/obj-x86_64-linux-gnu/InTreeBuild/include/qt5xdg/XdgDefaultApps"
    )
endif()

