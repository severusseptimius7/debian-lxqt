/********************************************************************************
** Form generated from reading UI file 'userdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_USERDIALOG_H
#define UI_USERDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_UserDialog
{
public:
    QVBoxLayout *verticalLayout;
    QTabWidget *tabWidget;
    QWidget *tab;
    QFormLayout *formLayout_2;
    QLabel *label;
    QLineEdit *loginName;
    QLabel *label_3;
    QLineEdit *fullName;
    QLabel *label_2;
    QSpinBox *uid;
    QLabel *label_5;
    QComboBox *mainGroup;
    QWidget *tab_3;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_6;
    QListWidget *groupList;
    QWidget *tab_2;
    QFormLayout *formLayout;
    QLabel *label_4;
    QComboBox *loginShell;
    QLineEdit *homeDir;
    QLabel *label_7;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *UserDialog)
    {
        if (UserDialog->objectName().isEmpty())
            UserDialog->setObjectName(QString::fromUtf8("UserDialog"));
        UserDialog->resize(403, 293);
        verticalLayout = new QVBoxLayout(UserDialog);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        tabWidget = new QTabWidget(UserDialog);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        formLayout_2 = new QFormLayout(tab);
        formLayout_2->setObjectName(QString::fromUtf8("formLayout_2"));
        formLayout_2->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        label = new QLabel(tab);
        label->setObjectName(QString::fromUtf8("label"));

        formLayout_2->setWidget(0, QFormLayout::LabelRole, label);

        loginName = new QLineEdit(tab);
        loginName->setObjectName(QString::fromUtf8("loginName"));

        formLayout_2->setWidget(0, QFormLayout::FieldRole, loginName);

        label_3 = new QLabel(tab);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        formLayout_2->setWidget(1, QFormLayout::LabelRole, label_3);

        fullName = new QLineEdit(tab);
        fullName->setObjectName(QString::fromUtf8("fullName"));

        formLayout_2->setWidget(1, QFormLayout::FieldRole, fullName);

        label_2 = new QLabel(tab);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        formLayout_2->setWidget(3, QFormLayout::LabelRole, label_2);

        uid = new QSpinBox(tab);
        uid->setObjectName(QString::fromUtf8("uid"));
        uid->setMaximum(32768);

        formLayout_2->setWidget(3, QFormLayout::FieldRole, uid);

        label_5 = new QLabel(tab);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        formLayout_2->setWidget(4, QFormLayout::LabelRole, label_5);

        mainGroup = new QComboBox(tab);
        mainGroup->setObjectName(QString::fromUtf8("mainGroup"));
        mainGroup->setEditable(true);

        formLayout_2->setWidget(4, QFormLayout::FieldRole, mainGroup);

        tabWidget->addTab(tab, QString());
        tab_3 = new QWidget();
        tab_3->setObjectName(QString::fromUtf8("tab_3"));
        verticalLayout_2 = new QVBoxLayout(tab_3);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        label_6 = new QLabel(tab_3);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        verticalLayout_2->addWidget(label_6);

        groupList = new QListWidget(tab_3);
        groupList->setObjectName(QString::fromUtf8("groupList"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(1);
        sizePolicy.setHeightForWidth(groupList->sizePolicy().hasHeightForWidth());
        groupList->setSizePolicy(sizePolicy);

        verticalLayout_2->addWidget(groupList);

        tabWidget->addTab(tab_3, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QString::fromUtf8("tab_2"));
        formLayout = new QFormLayout(tab_2);
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        label_4 = new QLabel(tab_2);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        formLayout->setWidget(0, QFormLayout::LabelRole, label_4);

        loginShell = new QComboBox(tab_2);
        loginShell->setObjectName(QString::fromUtf8("loginShell"));
        loginShell->setEditable(true);

        formLayout->setWidget(0, QFormLayout::FieldRole, loginShell);

        homeDir = new QLineEdit(tab_2);
        homeDir->setObjectName(QString::fromUtf8("homeDir"));

        formLayout->setWidget(1, QFormLayout::FieldRole, homeDir);

        label_7 = new QLabel(tab_2);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        formLayout->setWidget(1, QFormLayout::LabelRole, label_7);

        tabWidget->addTab(tab_2, QString());

        verticalLayout->addWidget(tabWidget);

        buttonBox = new QDialogButtonBox(UserDialog);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        verticalLayout->addWidget(buttonBox);


        retranslateUi(UserDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), UserDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), UserDialog, SLOT(reject()));

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(UserDialog);
    } // setupUi

    void retranslateUi(QDialog *UserDialog)
    {
        UserDialog->setWindowTitle(QCoreApplication::translate("UserDialog", "User Settings", nullptr));
        label->setText(QCoreApplication::translate("UserDialog", "Login name:", nullptr));
        label_3->setText(QCoreApplication::translate("UserDialog", "Full name:", nullptr));
        label_2->setText(QCoreApplication::translate("UserDialog", "User ID:", nullptr));
        uid->setSpecialValueText(QCoreApplication::translate("UserDialog", "Default", nullptr));
        label_5->setText(QCoreApplication::translate("UserDialog", "Main group:", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab), QCoreApplication::translate("UserDialog", "General", nullptr));
        label_6->setText(QCoreApplication::translate("UserDialog", "The user belongs to the following groups:", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_3), QCoreApplication::translate("UserDialog", "Groups", nullptr));
        label_4->setText(QCoreApplication::translate("UserDialog", "Login shell:", nullptr));
        label_7->setText(QCoreApplication::translate("UserDialog", "Home directory:", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QCoreApplication::translate("UserDialog", "Advanced", nullptr));
    } // retranslateUi

};

namespace Ui {
    class UserDialog: public Ui_UserDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_USERDIALOG_H
