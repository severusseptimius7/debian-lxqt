/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QTreeWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionAdd;
    QAction *actionDelete;
    QAction *actionProperties;
    QAction *actionRefresh;
    QAction *actionChangePasswd;
    QWidget *centralwidget;
    QVBoxLayout *verticalLayout;
    QTabWidget *tabWidget;
    QWidget *tab;
    QVBoxLayout *verticalLayout_2;
    QTreeWidget *userList;
    QCheckBox *showSystemUsers;
    QWidget *tab_2;
    QVBoxLayout *verticalLayout_3;
    QTreeWidget *groupList;
    QToolBar *toolBar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *UserGroupSettingsWindow)
    {
        if (UserGroupSettingsWindow->objectName().isEmpty())
            UserGroupSettingsWindow->setObjectName(QString::fromUtf8("UserGroupSettingsWindow"));
        UserGroupSettingsWindow->resize(676, 362);
        actionAdd = new QAction(UserGroupSettingsWindow);
        actionAdd->setObjectName(QString::fromUtf8("actionAdd"));
        QIcon icon;
        QString iconThemeName = QString::fromUtf8("list-add");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon = QIcon::fromTheme(iconThemeName);
        } else {
            icon.addFile(QString::fromUtf8("."), QSize(), QIcon::Normal, QIcon::Off);
        }
        actionAdd->setIcon(icon);
        actionDelete = new QAction(UserGroupSettingsWindow);
        actionDelete->setObjectName(QString::fromUtf8("actionDelete"));
        QIcon icon1;
        iconThemeName = QString::fromUtf8("list-remove");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon1 = QIcon::fromTheme(iconThemeName);
        } else {
            icon1.addFile(QString::fromUtf8("."), QSize(), QIcon::Normal, QIcon::Off);
        }
        actionDelete->setIcon(icon1);
        actionProperties = new QAction(UserGroupSettingsWindow);
        actionProperties->setObjectName(QString::fromUtf8("actionProperties"));
        QIcon icon2;
        iconThemeName = QString::fromUtf8("document-properties");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon2 = QIcon::fromTheme(iconThemeName);
        } else {
            icon2.addFile(QString::fromUtf8("."), QSize(), QIcon::Normal, QIcon::Off);
        }
        actionProperties->setIcon(icon2);
        actionRefresh = new QAction(UserGroupSettingsWindow);
        actionRefresh->setObjectName(QString::fromUtf8("actionRefresh"));
        QIcon icon3;
        iconThemeName = QString::fromUtf8("view-refresh");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon3 = QIcon::fromTheme(iconThemeName);
        } else {
            icon3.addFile(QString::fromUtf8("."), QSize(), QIcon::Normal, QIcon::Off);
        }
        actionRefresh->setIcon(icon3);
        actionChangePasswd = new QAction(UserGroupSettingsWindow);
        actionChangePasswd->setObjectName(QString::fromUtf8("actionChangePasswd"));
        QIcon icon4;
        iconThemeName = QString::fromUtf8("dialog-password");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon4 = QIcon::fromTheme(iconThemeName);
        } else {
            icon4.addFile(QString::fromUtf8("."), QSize(), QIcon::Normal, QIcon::Off);
        }
        actionChangePasswd->setIcon(icon4);
        centralwidget = new QWidget(UserGroupSettingsWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        verticalLayout = new QVBoxLayout(centralwidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        tabWidget = new QTabWidget(centralwidget);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tabWidget->setDocumentMode(true);
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        verticalLayout_2 = new QVBoxLayout(tab);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        userList = new QTreeWidget(tab);
        userList->setObjectName(QString::fromUtf8("userList"));
        userList->setRootIsDecorated(false);
        userList->setItemsExpandable(false);
        userList->setSortingEnabled(true);
        userList->setExpandsOnDoubleClick(false);

        verticalLayout_2->addWidget(userList);

        showSystemUsers = new QCheckBox(tab);
        showSystemUsers->setObjectName(QString::fromUtf8("showSystemUsers"));

        verticalLayout_2->addWidget(showSystemUsers);

        tabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QString::fromUtf8("tab_2"));
        verticalLayout_3 = new QVBoxLayout(tab_2);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        groupList = new QTreeWidget(tab_2);
        groupList->setObjectName(QString::fromUtf8("groupList"));
        groupList->setRootIsDecorated(false);
        groupList->setItemsExpandable(false);
        groupList->setSortingEnabled(true);

        verticalLayout_3->addWidget(groupList);

        tabWidget->addTab(tab_2, QString());

        verticalLayout->addWidget(tabWidget);

        UserGroupSettingsWindow->setCentralWidget(centralwidget);
        toolBar = new QToolBar(UserGroupSettingsWindow);
        toolBar->setObjectName(QString::fromUtf8("toolBar"));
        toolBar->setMovable(false);
        toolBar->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
        UserGroupSettingsWindow->addToolBar(Qt::TopToolBarArea, toolBar);
        statusbar = new QStatusBar(UserGroupSettingsWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        UserGroupSettingsWindow->setStatusBar(statusbar);

        toolBar->addAction(actionAdd);
        toolBar->addAction(actionDelete);
        toolBar->addAction(actionProperties);
        toolBar->addAction(actionChangePasswd);
        toolBar->addAction(actionRefresh);

        retranslateUi(UserGroupSettingsWindow);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(UserGroupSettingsWindow);
    } // setupUi

    void retranslateUi(QMainWindow *UserGroupSettingsWindow)
    {
        UserGroupSettingsWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "User and Group Settings", nullptr));
        actionAdd->setText(QCoreApplication::translate("MainWindow", "Add", nullptr));
#if QT_CONFIG(tooltip)
        actionAdd->setToolTip(QCoreApplication::translate("MainWindow", "Add new users or groups", nullptr));
#endif // QT_CONFIG(tooltip)
        actionDelete->setText(QCoreApplication::translate("MainWindow", "Delete", nullptr));
#if QT_CONFIG(tooltip)
        actionDelete->setToolTip(QCoreApplication::translate("MainWindow", "Delete selected item", nullptr));
#endif // QT_CONFIG(tooltip)
        actionProperties->setText(QCoreApplication::translate("MainWindow", "Properties", nullptr));
#if QT_CONFIG(tooltip)
        actionProperties->setToolTip(QCoreApplication::translate("MainWindow", "Edit properties of the selected item", nullptr));
#endif // QT_CONFIG(tooltip)
        actionRefresh->setText(QCoreApplication::translate("MainWindow", "Refresh", nullptr));
#if QT_CONFIG(tooltip)
        actionRefresh->setToolTip(QCoreApplication::translate("MainWindow", "Refresh the lists", nullptr));
#endif // QT_CONFIG(tooltip)
        actionChangePasswd->setText(QCoreApplication::translate("MainWindow", "Change Password", nullptr));
#if QT_CONFIG(tooltip)
        actionChangePasswd->setToolTip(QCoreApplication::translate("MainWindow", "Change password for the selected user or group", nullptr));
#endif // QT_CONFIG(tooltip)
        QTreeWidgetItem *___qtreewidgetitem = userList->headerItem();
        ___qtreewidgetitem->setText(4, QCoreApplication::translate("MainWindow", "Home Directory", nullptr));
        ___qtreewidgetitem->setText(3, QCoreApplication::translate("MainWindow", "Group", nullptr));
        ___qtreewidgetitem->setText(2, QCoreApplication::translate("MainWindow", "Full Name", nullptr));
        ___qtreewidgetitem->setText(1, QCoreApplication::translate("MainWindow", "User ID", nullptr));
        ___qtreewidgetitem->setText(0, QCoreApplication::translate("MainWindow", "Login Name", nullptr));
        showSystemUsers->setText(QCoreApplication::translate("MainWindow", "Show system users (for advanced users only)", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab), QCoreApplication::translate("MainWindow", "&Users", nullptr));
        QTreeWidgetItem *___qtreewidgetitem1 = groupList->headerItem();
        ___qtreewidgetitem1->setText(2, QCoreApplication::translate("MainWindow", "Members", nullptr));
        ___qtreewidgetitem1->setText(1, QCoreApplication::translate("MainWindow", "Group ID", nullptr));
        ___qtreewidgetitem1->setText(0, QCoreApplication::translate("MainWindow", "Name", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QCoreApplication::translate("MainWindow", "&Groups", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
