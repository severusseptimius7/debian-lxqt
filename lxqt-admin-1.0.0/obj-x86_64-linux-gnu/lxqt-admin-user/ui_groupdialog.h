/********************************************************************************
** Form generated from reading UI file 'groupdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_GROUPDIALOG_H
#define UI_GROUPDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QSpinBox>

QT_BEGIN_NAMESPACE

class Ui_GroupDialog
{
public:
    QFormLayout *formLayout;
    QLabel *label;
    QLineEdit *groupName;
    QSpinBox *gid;
    QLabel *label_2;
    QLabel *label_3;
    QListWidget *userList;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *GroupDialog)
    {
        if (GroupDialog->objectName().isEmpty())
            GroupDialog->setObjectName(QString::fromUtf8("GroupDialog"));
        GroupDialog->resize(400, 360);
        formLayout = new QFormLayout(GroupDialog);
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        label = new QLabel(GroupDialog);
        label->setObjectName(QString::fromUtf8("label"));

        formLayout->setWidget(0, QFormLayout::LabelRole, label);

        groupName = new QLineEdit(GroupDialog);
        groupName->setObjectName(QString::fromUtf8("groupName"));

        formLayout->setWidget(0, QFormLayout::FieldRole, groupName);

        gid = new QSpinBox(GroupDialog);
        gid->setObjectName(QString::fromUtf8("gid"));
        gid->setMaximum(32768);

        formLayout->setWidget(1, QFormLayout::FieldRole, gid);

        label_2 = new QLabel(GroupDialog);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        formLayout->setWidget(1, QFormLayout::LabelRole, label_2);

        label_3 = new QLabel(GroupDialog);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        formLayout->setWidget(2, QFormLayout::SpanningRole, label_3);

        userList = new QListWidget(GroupDialog);
        userList->setObjectName(QString::fromUtf8("userList"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(1);
        sizePolicy.setHeightForWidth(userList->sizePolicy().hasHeightForWidth());
        userList->setSizePolicy(sizePolicy);

        formLayout->setWidget(3, QFormLayout::SpanningRole, userList);

        buttonBox = new QDialogButtonBox(GroupDialog);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        formLayout->setWidget(4, QFormLayout::SpanningRole, buttonBox);


        retranslateUi(GroupDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), GroupDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), GroupDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(GroupDialog);
    } // setupUi

    void retranslateUi(QDialog *GroupDialog)
    {
        GroupDialog->setWindowTitle(QCoreApplication::translate("GroupDialog", "Group Settings", nullptr));
        label->setText(QCoreApplication::translate("GroupDialog", "Group name:", nullptr));
        gid->setSpecialValueText(QCoreApplication::translate("GroupDialog", "Default", nullptr));
        label_2->setText(QCoreApplication::translate("GroupDialog", "Group ID:", nullptr));
        label_3->setText(QCoreApplication::translate("GroupDialog", "Users belong to this group:", nullptr));
    } // retranslateUi

};

namespace Ui {
    class GroupDialog: public Ui_GroupDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_GROUPDIALOG_H
