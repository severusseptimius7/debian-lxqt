# Install script for directory: /home/debian/lxqt/lxqt-admin/lxqt-admin-1.0.0/lxqt-admin-time

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "RelWithDebInfo")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

# Set default install directory permissions.
if(NOT DEFINED CMAKE_OBJDUMP)
  set(CMAKE_OBJDUMP "/usr/bin/objdump")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xRuntimex" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/share/lxqt/translations/lxqt-admin-time/lxqt-admin-time_ar.qm;/usr/share/lxqt/translations/lxqt-admin-time/lxqt-admin-time_arn.qm;/usr/share/lxqt/translations/lxqt-admin-time/lxqt-admin-time_ast.qm;/usr/share/lxqt/translations/lxqt-admin-time/lxqt-admin-time_bg.qm;/usr/share/lxqt/translations/lxqt-admin-time/lxqt-admin-time_ca.qm;/usr/share/lxqt/translations/lxqt-admin-time/lxqt-admin-time_cs.qm;/usr/share/lxqt/translations/lxqt-admin-time/lxqt-admin-time_cy.qm;/usr/share/lxqt/translations/lxqt-admin-time/lxqt-admin-time_da.qm;/usr/share/lxqt/translations/lxqt-admin-time/lxqt-admin-time_de.qm;/usr/share/lxqt/translations/lxqt-admin-time/lxqt-admin-time_el.qm;/usr/share/lxqt/translations/lxqt-admin-time/lxqt-admin-time_en_GB.qm;/usr/share/lxqt/translations/lxqt-admin-time/lxqt-admin-time_es.qm;/usr/share/lxqt/translations/lxqt-admin-time/lxqt-admin-time_et.qm;/usr/share/lxqt/translations/lxqt-admin-time/lxqt-admin-time_fr.qm;/usr/share/lxqt/translations/lxqt-admin-time/lxqt-admin-time_gl.qm;/usr/share/lxqt/translations/lxqt-admin-time/lxqt-admin-time_he.qm;/usr/share/lxqt/translations/lxqt-admin-time/lxqt-admin-time_hr.qm;/usr/share/lxqt/translations/lxqt-admin-time/lxqt-admin-time_hu.qm;/usr/share/lxqt/translations/lxqt-admin-time/lxqt-admin-time_id.qm;/usr/share/lxqt/translations/lxqt-admin-time/lxqt-admin-time_it.qm;/usr/share/lxqt/translations/lxqt-admin-time/lxqt-admin-time_ja.qm;/usr/share/lxqt/translations/lxqt-admin-time/lxqt-admin-time_ko.qm;/usr/share/lxqt/translations/lxqt-admin-time/lxqt-admin-time_lt.qm;/usr/share/lxqt/translations/lxqt-admin-time/lxqt-admin-time_lv.qm;/usr/share/lxqt/translations/lxqt-admin-time/lxqt-admin-time_nb_NO.qm;/usr/share/lxqt/translations/lxqt-admin-time/lxqt-admin-time_nl.qm;/usr/share/lxqt/translations/lxqt-admin-time/lxqt-admin-time_pl.qm;/usr/share/lxqt/translations/lxqt-admin-time/lxqt-admin-time_pt.qm;/usr/share/lxqt/translations/lxqt-admin-time/lxqt-admin-time_pt_BR.qm;/usr/share/lxqt/translations/lxqt-admin-time/lxqt-admin-time_ru.qm;/usr/share/lxqt/translations/lxqt-admin-time/lxqt-admin-time_si.qm;/usr/share/lxqt/translations/lxqt-admin-time/lxqt-admin-time_sk.qm;/usr/share/lxqt/translations/lxqt-admin-time/lxqt-admin-time_tr.qm;/usr/share/lxqt/translations/lxqt-admin-time/lxqt-admin-time_uk.qm;/usr/share/lxqt/translations/lxqt-admin-time/lxqt-admin-time_vi.qm;/usr/share/lxqt/translations/lxqt-admin-time/lxqt-admin-time_zh_CN.qm;/usr/share/lxqt/translations/lxqt-admin-time/lxqt-admin-time_zh_TW.qm")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/usr/share/lxqt/translations/lxqt-admin-time" TYPE FILE FILES
    "/home/debian/lxqt/lxqt-admin/lxqt-admin-1.0.0/obj-x86_64-linux-gnu/lxqt-admin-time/lxqt-admin-time_ar.qm"
    "/home/debian/lxqt/lxqt-admin/lxqt-admin-1.0.0/obj-x86_64-linux-gnu/lxqt-admin-time/lxqt-admin-time_arn.qm"
    "/home/debian/lxqt/lxqt-admin/lxqt-admin-1.0.0/obj-x86_64-linux-gnu/lxqt-admin-time/lxqt-admin-time_ast.qm"
    "/home/debian/lxqt/lxqt-admin/lxqt-admin-1.0.0/obj-x86_64-linux-gnu/lxqt-admin-time/lxqt-admin-time_bg.qm"
    "/home/debian/lxqt/lxqt-admin/lxqt-admin-1.0.0/obj-x86_64-linux-gnu/lxqt-admin-time/lxqt-admin-time_ca.qm"
    "/home/debian/lxqt/lxqt-admin/lxqt-admin-1.0.0/obj-x86_64-linux-gnu/lxqt-admin-time/lxqt-admin-time_cs.qm"
    "/home/debian/lxqt/lxqt-admin/lxqt-admin-1.0.0/obj-x86_64-linux-gnu/lxqt-admin-time/lxqt-admin-time_cy.qm"
    "/home/debian/lxqt/lxqt-admin/lxqt-admin-1.0.0/obj-x86_64-linux-gnu/lxqt-admin-time/lxqt-admin-time_da.qm"
    "/home/debian/lxqt/lxqt-admin/lxqt-admin-1.0.0/obj-x86_64-linux-gnu/lxqt-admin-time/lxqt-admin-time_de.qm"
    "/home/debian/lxqt/lxqt-admin/lxqt-admin-1.0.0/obj-x86_64-linux-gnu/lxqt-admin-time/lxqt-admin-time_el.qm"
    "/home/debian/lxqt/lxqt-admin/lxqt-admin-1.0.0/obj-x86_64-linux-gnu/lxqt-admin-time/lxqt-admin-time_en_GB.qm"
    "/home/debian/lxqt/lxqt-admin/lxqt-admin-1.0.0/obj-x86_64-linux-gnu/lxqt-admin-time/lxqt-admin-time_es.qm"
    "/home/debian/lxqt/lxqt-admin/lxqt-admin-1.0.0/obj-x86_64-linux-gnu/lxqt-admin-time/lxqt-admin-time_et.qm"
    "/home/debian/lxqt/lxqt-admin/lxqt-admin-1.0.0/obj-x86_64-linux-gnu/lxqt-admin-time/lxqt-admin-time_fr.qm"
    "/home/debian/lxqt/lxqt-admin/lxqt-admin-1.0.0/obj-x86_64-linux-gnu/lxqt-admin-time/lxqt-admin-time_gl.qm"
    "/home/debian/lxqt/lxqt-admin/lxqt-admin-1.0.0/obj-x86_64-linux-gnu/lxqt-admin-time/lxqt-admin-time_he.qm"
    "/home/debian/lxqt/lxqt-admin/lxqt-admin-1.0.0/obj-x86_64-linux-gnu/lxqt-admin-time/lxqt-admin-time_hr.qm"
    "/home/debian/lxqt/lxqt-admin/lxqt-admin-1.0.0/obj-x86_64-linux-gnu/lxqt-admin-time/lxqt-admin-time_hu.qm"
    "/home/debian/lxqt/lxqt-admin/lxqt-admin-1.0.0/obj-x86_64-linux-gnu/lxqt-admin-time/lxqt-admin-time_id.qm"
    "/home/debian/lxqt/lxqt-admin/lxqt-admin-1.0.0/obj-x86_64-linux-gnu/lxqt-admin-time/lxqt-admin-time_it.qm"
    "/home/debian/lxqt/lxqt-admin/lxqt-admin-1.0.0/obj-x86_64-linux-gnu/lxqt-admin-time/lxqt-admin-time_ja.qm"
    "/home/debian/lxqt/lxqt-admin/lxqt-admin-1.0.0/obj-x86_64-linux-gnu/lxqt-admin-time/lxqt-admin-time_ko.qm"
    "/home/debian/lxqt/lxqt-admin/lxqt-admin-1.0.0/obj-x86_64-linux-gnu/lxqt-admin-time/lxqt-admin-time_lt.qm"
    "/home/debian/lxqt/lxqt-admin/lxqt-admin-1.0.0/obj-x86_64-linux-gnu/lxqt-admin-time/lxqt-admin-time_lv.qm"
    "/home/debian/lxqt/lxqt-admin/lxqt-admin-1.0.0/obj-x86_64-linux-gnu/lxqt-admin-time/lxqt-admin-time_nb_NO.qm"
    "/home/debian/lxqt/lxqt-admin/lxqt-admin-1.0.0/obj-x86_64-linux-gnu/lxqt-admin-time/lxqt-admin-time_nl.qm"
    "/home/debian/lxqt/lxqt-admin/lxqt-admin-1.0.0/obj-x86_64-linux-gnu/lxqt-admin-time/lxqt-admin-time_pl.qm"
    "/home/debian/lxqt/lxqt-admin/lxqt-admin-1.0.0/obj-x86_64-linux-gnu/lxqt-admin-time/lxqt-admin-time_pt.qm"
    "/home/debian/lxqt/lxqt-admin/lxqt-admin-1.0.0/obj-x86_64-linux-gnu/lxqt-admin-time/lxqt-admin-time_pt_BR.qm"
    "/home/debian/lxqt/lxqt-admin/lxqt-admin-1.0.0/obj-x86_64-linux-gnu/lxqt-admin-time/lxqt-admin-time_ru.qm"
    "/home/debian/lxqt/lxqt-admin/lxqt-admin-1.0.0/obj-x86_64-linux-gnu/lxqt-admin-time/lxqt-admin-time_si.qm"
    "/home/debian/lxqt/lxqt-admin/lxqt-admin-1.0.0/obj-x86_64-linux-gnu/lxqt-admin-time/lxqt-admin-time_sk.qm"
    "/home/debian/lxqt/lxqt-admin/lxqt-admin-1.0.0/obj-x86_64-linux-gnu/lxqt-admin-time/lxqt-admin-time_tr.qm"
    "/home/debian/lxqt/lxqt-admin/lxqt-admin-1.0.0/obj-x86_64-linux-gnu/lxqt-admin-time/lxqt-admin-time_uk.qm"
    "/home/debian/lxqt/lxqt-admin/lxqt-admin-1.0.0/obj-x86_64-linux-gnu/lxqt-admin-time/lxqt-admin-time_vi.qm"
    "/home/debian/lxqt/lxqt-admin/lxqt-admin-1.0.0/obj-x86_64-linux-gnu/lxqt-admin-time/lxqt-admin-time_zh_CN.qm"
    "/home/debian/lxqt/lxqt-admin/lxqt-admin-1.0.0/obj-x86_64-linux-gnu/lxqt-admin-time/lxqt-admin-time_zh_TW.qm"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/lxqt-admin-time" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/lxqt-admin-time")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/lxqt-admin-time"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "/home/debian/lxqt/lxqt-admin/lxqt-admin-1.0.0/obj-x86_64-linux-gnu/lxqt-admin-time/lxqt-admin-time")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/lxqt-admin-time" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/lxqt-admin-time")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/lxqt-admin-time")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/share/applications/lxqt-admin-time.desktop")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/usr/share/applications" TYPE FILE FILES "/home/debian/lxqt/lxqt-admin/lxqt-admin-1.0.0/obj-x86_64-linux-gnu/lxqt-admin-time/lxqt-admin-time.desktop")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE PROGRAM RENAME "lxqt-admin-time-helper" FILES "/home/debian/lxqt/lxqt-admin/lxqt-admin-1.0.0/lxqt-admin-time/lxqt-admin-time-helper.nosystemd")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/share/polkit-1/actions/org.lxqt.lxqt-admin-time.policy")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/usr/share/polkit-1/actions" TYPE FILE RENAME "org.lxqt.lxqt-admin-time.policy" FILES "/home/debian/lxqt/lxqt-admin/lxqt-admin-1.0.0/obj-x86_64-linux-gnu/lxqt-admin-time/org.lxqt.lxqt-admin-time.policy.nosd")
endif()

