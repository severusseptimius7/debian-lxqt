/********************************************************************************
** Form generated from reading UI file 'timezone.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TIMEZONE_H
#define UI_TIMEZONE_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Timezone
{
public:
    QVBoxLayout *verticalLayout;
    QFormLayout *formLayout;
    QLabel *label_2;
    QLabel *label_timezone;
    QLineEdit *edit_filter;
    QListWidget *list_zones;

    void setupUi(QWidget *Timezone)
    {
        if (Timezone->objectName().isEmpty())
            Timezone->setObjectName(QString::fromUtf8("Timezone"));
        Timezone->resize(400, 300);
        verticalLayout = new QVBoxLayout(Timezone);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        formLayout = new QFormLayout();
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        label_2 = new QLabel(Timezone);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        formLayout->setWidget(0, QFormLayout::LabelRole, label_2);

        label_timezone = new QLabel(Timezone);
        label_timezone->setObjectName(QString::fromUtf8("label_timezone"));

        formLayout->setWidget(0, QFormLayout::FieldRole, label_timezone);


        verticalLayout->addLayout(formLayout);

        edit_filter = new QLineEdit(Timezone);
        edit_filter->setObjectName(QString::fromUtf8("edit_filter"));
        edit_filter->setClearButtonEnabled(true);

        verticalLayout->addWidget(edit_filter);

        list_zones = new QListWidget(Timezone);
        list_zones->setObjectName(QString::fromUtf8("list_zones"));

        verticalLayout->addWidget(list_zones);


        retranslateUi(Timezone);

        QMetaObject::connectSlotsByName(Timezone);
    } // setupUi

    void retranslateUi(QWidget *Timezone)
    {
        label_2->setText(QCoreApplication::translate("Timezone", "Your current timezone:", nullptr));
        edit_filter->setPlaceholderText(QCoreApplication::translate("Timezone", "Filter", nullptr));
        (void)Timezone;
    } // retranslateUi

};

namespace Ui {
    class Timezone: public Ui_Timezone {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TIMEZONE_H
