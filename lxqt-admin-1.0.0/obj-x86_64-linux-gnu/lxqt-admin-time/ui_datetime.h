/********************************************************************************
** Form generated from reading UI file 'datetime.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DATETIME_H
#define UI_DATETIME_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCalendarWidget>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTimeEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_DateTime
{
public:
    QVBoxLayout *verticalLayout;
    QLabel *label_time;
    QTimeEdit *edit_time;
    QLabel *label_2;
    QCalendarWidget *calendar;
    QCheckBox *ntp;
    QCheckBox *localRTC;
    QSpacerItem *verticalSpacer;

    void setupUi(QWidget *DateTime)
    {
        if (DateTime->objectName().isEmpty())
            DateTime->setObjectName(QString::fromUtf8("DateTime"));
        DateTime->resize(365, 323);
        DateTime->setMinimumSize(QSize(50, 50));
        verticalLayout = new QVBoxLayout(DateTime);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        label_time = new QLabel(DateTime);
        label_time->setObjectName(QString::fromUtf8("label_time"));

        verticalLayout->addWidget(label_time);

        edit_time = new QTimeEdit(DateTime);
        edit_time->setObjectName(QString::fromUtf8("edit_time"));
        edit_time->setMaximumSize(QSize(400, 16777215));
        edit_time->setCurrentSection(QDateTimeEdit::HourSection);
        edit_time->setTime(QTime(0, 0, 0));

        verticalLayout->addWidget(edit_time);

        label_2 = new QLabel(DateTime);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        verticalLayout->addWidget(label_2);

        calendar = new QCalendarWidget(DateTime);
        calendar->setObjectName(QString::fromUtf8("calendar"));
        calendar->setMaximumSize(QSize(400, 173));

        verticalLayout->addWidget(calendar);

        ntp = new QCheckBox(DateTime);
        ntp->setObjectName(QString::fromUtf8("ntp"));

        verticalLayout->addWidget(ntp);

        localRTC = new QCheckBox(DateTime);
        localRTC->setObjectName(QString::fromUtf8("localRTC"));

        verticalLayout->addWidget(localRTC);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        verticalLayout->setStretch(3, 1);

        retranslateUi(DateTime);
        QObject::connect(ntp, SIGNAL(toggled(bool)), calendar, SLOT(setDisabled(bool)));
        QObject::connect(ntp, SIGNAL(toggled(bool)), edit_time, SLOT(setDisabled(bool)));

        QMetaObject::connectSlotsByName(DateTime);
    } // setupUi

    void retranslateUi(QWidget *DateTime)
    {
        label_time->setText(QCoreApplication::translate("DateTime", "Time:", nullptr));
        edit_time->setDisplayFormat(QCoreApplication::translate("DateTime", "HH:mm:ss", nullptr));
        label_2->setText(QCoreApplication::translate("DateTime", "Date:", nullptr));
        ntp->setText(QCoreApplication::translate("DateTime", "Enable network time synchronization (NTP)", nullptr));
        localRTC->setText(QCoreApplication::translate("DateTime", "RTC is in local time", nullptr));
        (void)DateTime;
    } // retranslateUi

};

namespace Ui {
    class DateTime: public Ui_DateTime {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DATETIME_H
