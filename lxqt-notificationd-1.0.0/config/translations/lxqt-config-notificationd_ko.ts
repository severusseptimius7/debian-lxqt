<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ko">
<context>
    <name>AdvancedSettings</name>
    <message>
        <location filename="../advancedsettings.ui" line="67"/>
        <source>Sizes</source>
        <translation>크기</translation>
    </message>
    <message>
        <location filename="../advancedsettings.ui" line="75"/>
        <source>Width:</source>
        <translation>너비:</translation>
    </message>
    <message>
        <location filename="../advancedsettings.ui" line="82"/>
        <location filename="../advancedsettings.ui" line="106"/>
        <source> px</source>
        <translation> 픽셀</translation>
    </message>
    <message>
        <location filename="../advancedsettings.ui" line="99"/>
        <source>Spacing:</source>
        <translation>간격:</translation>
    </message>
    <message>
        <location filename="../advancedsettings.ui" line="118"/>
        <source>Duration</source>
        <translation>지속시간</translation>
    </message>
    <message>
        <location filename="../advancedsettings.ui" line="130"/>
        <source>Some notifications set their own on-screen duration.</source>
        <translation>일부 알림은 자체 화면 지속시간을 설정합니다.</translation>
    </message>
    <message>
        <location filename="../advancedsettings.ui" line="142"/>
        <source>Default duration:</source>
        <translation>기본 지속시간:</translation>
    </message>
    <message>
        <location filename="../advancedsettings.ui" line="149"/>
        <source> sec</source>
        <translation> 초</translation>
    </message>
    <message>
        <location filename="../advancedsettings.ui" line="177"/>
        <source>Screen</source>
        <translation>화면</translation>
    </message>
    <message>
        <location filename="../advancedsettings.ui" line="183"/>
        <source>When unchecked the notification will always show on primary screen</source>
        <translation>선택하지 않으면 알림이 항상 기본 화면에 표시됩니다</translation>
    </message>
    <message>
        <location filename="../advancedsettings.ui" line="186"/>
        <source>Show notifications on screen with the mouse</source>
        <translation>마우스로 화면에 알림 표시</translation>
    </message>
    <message>
        <location filename="../advancedsettings.ui" line="196"/>
        <source>Do Not Disturb</source>
        <translation>방해하지 않음</translation>
    </message>
    <message>
        <location filename="../advancedsettings.ui" line="202"/>
        <source>Only save notifications</source>
        <translation>알림만 저장</translation>
    </message>
    <message>
        <location filename="../advancedsettings.ui" line="17"/>
        <source>Unattended Notifications</source>
        <translation>확인되지 않은 알림</translation>
    </message>
    <message>
        <location filename="../advancedsettings.ui" line="25"/>
        <source>How many to save:</source>
        <translation>저장할 수 있는 횟수:</translation>
    </message>
    <message>
        <location filename="../advancedsettings.ui" line="39"/>
        <location filename="../advancedsettings.ui" line="49"/>
        <source>Application name is on the top of notification.</source>
        <translation>응용프로그램 이름은 알림 상단에 있습니다.</translation>
    </message>
    <message>
        <location filename="../advancedsettings.ui" line="42"/>
        <source>Ignore these applications:</source>
        <translation>다음 응용프로그램 무시:</translation>
    </message>
    <message>
        <location filename="../advancedsettings.ui" line="52"/>
        <source>app1,app2,app3</source>
        <translation>앱1,앱2,앱3</translation>
    </message>
</context>
<context>
    <name>BasicSettings</name>
    <message>
        <location filename="../basicsettings.ui" line="23"/>
        <source>Basic Settings</source>
        <translation>기본 설정</translation>
    </message>
    <message>
        <location filename="../basicsettings.ui" line="30"/>
        <source>Position on screen</source>
        <translation>화면에서의 위치</translation>
    </message>
    <message>
        <location filename="../basicsettings.cpp" line="61"/>
        <source>&lt;b&gt;Warning:&lt;/b&gt; notifications daemon is slow to respond.
Keep trying to connect…</source>
        <translation>&lt;b&gt;경고:&lt;/b&gt; 알림 데몬의 응답 속도가 느립니다.
계속 연결을 시도하십시오…</translation>
    </message>
    <message>
        <location filename="../basicsettings.cpp" line="70"/>
        <source>&lt;b&gt;Warning:&lt;/b&gt; No notifications daemon is running.
A fallback will be used.</source>
        <translation>&lt;b&gt;경고:&lt;/b&gt; 실행 중인 알림 데몬이 없습니다.
대체가 사용됩니다.</translation>
    </message>
    <message>
        <location filename="../basicsettings.cpp" line="73"/>
        <source>&lt;b&gt;Warning:&lt;/b&gt; A third-party notifications daemon (%1) is running.
These settings won&apos;t have any effect on it!</source>
        <translation>&lt;b&gt;경고:&lt;/b&gt; 타사 알림 데몬(%1)이 실행 중입니다.
이 설정은 아무런 영향을 미치지 않습니다!</translation>
    </message>
    <message>
        <location filename="../basicsettings.cpp" line="129"/>
        <source>Notification demo </source>
        <translation>알림 데모 </translation>
    </message>
    <message>
        <location filename="../basicsettings.cpp" line="130"/>
        <source>This is a test notification.
 All notifications will now appear here on LXQt.</source>
        <translation>테스트 알림입니다.
 이제 모든 알림이 LXQt에 표시됩니다.</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.cpp" line="41"/>
        <source>Desktop Notifications</source>
        <translation>바탕화면 알림</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="44"/>
        <source>Basic Settings</source>
        <translation>기본 설정</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="48"/>
        <source>Advanced Settings</source>
        <translation>고급 설정</translation>
    </message>
</context>
</TS>
