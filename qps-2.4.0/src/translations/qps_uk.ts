<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="uk">
<context>
    <name>AllFields</name>
    <message>
        <location filename="../details.cpp" line="653"/>
        <source>Field</source>
        <translation>Поле</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="653"/>
        <source>Field name</source>
        <translation>Назва поля</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="654"/>
        <source>Description</source>
        <translation>Опис</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="654"/>
        <source>Field description</source>
        <translation>Опис поля</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="655"/>
        <source>Value</source>
        <translation>Значення</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="655"/>
        <source>Field value</source>
        <translation>Значення поля</translation>
    </message>
</context>
<context>
    <name>Boxvar</name>
    <message>
        <location filename="../prefs.cpp" line="59"/>
        <source>Exit on closing</source>
        <translation>Вийти під час закриття</translation>
    </message>
    <message>
        <location filename="../prefs.cpp" line="60"/>
        <source>Remember Position</source>
        <translation>Запам&apos;ятовувати позицію</translation>
    </message>
    <message>
        <location filename="../prefs.cpp" line="68"/>
        <source>Host Name Lookup</source>
        <translation>Визначення імені комп&apos;ютера</translation>
    </message>
    <message>
        <location filename="../prefs.cpp" line="69"/>
        <source>Service Name Lookup</source>
        <translation>Визначення імені служби</translation>
    </message>
    <message>
        <location filename="../prefs.cpp" line="75"/>
        <source>Disclosure Triangles</source>
        <translation>Трикутники розкриття</translation>
    </message>
    <message>
        <location filename="../prefs.cpp" line="76"/>
        <source>Branch Lines</source>
        <translation>Лінії відгалужень</translation>
    </message>
    <message>
        <location filename="../prefs.cpp" line="82"/>
        <source>Auto Save Settings on Exit</source>
        <translation>Автоматичне збереження налаштувань при виході</translation>
    </message>
    <message>
        <location filename="../prefs.cpp" line="83"/>
        <source>Selection: Copy PIDs to Clipboard</source>
        <translation>Вибір: скопіювати PID-и до буферу обміну</translation>
    </message>
    <message>
        <location filename="../prefs.cpp" line="85"/>
        <source>Normalize NICE</source>
        <translation>Нормалізувати NICE</translation>
    </message>
    <message>
        <location filename="../prefs.cpp" line="86"/>
        <source>Use pmap for Map Names</source>
        <translation>Використовувати pmap для мапи імен</translation>
    </message>
</context>
<context>
    <name>Cbgroup</name>
    <message>
        <location filename="../prefs.cpp" line="106"/>
        <source>General</source>
        <translation>Загальні</translation>
    </message>
</context>
<context>
    <name>Command</name>
    <message>
        <location filename="../command.cpp" line="208"/>
        <source>The command:

</source>
        <translation>Команда:

</translation>
    </message>
    <message>
        <location filename="../command.cpp" line="212"/>
        <source>

failed with the error:

</source>
        <translation>

завершення з помилкою:

</translation>
    </message>
    <message>
        <location filename="../command.cpp" line="215"/>
        <source>Too many processes</source>
        <translation>Забагато процесів</translation>
    </message>
    <message>
        <location filename="../command.cpp" line="218"/>
        <source>Unknown error</source>
        <translation>Невідома помилка</translation>
    </message>
    <message>
        <location filename="../command.cpp" line="228"/>
        <source>

could not be executed because it was not found,
or you did not have execute permission.</source>
        <translation>

не виконано, оскільки не було знайдено,
або ви не маєте прав на виконання.</translation>
    </message>
    <message>
        <location filename="../command.cpp" line="233"/>
        <source>

exited with status </source>
        <translation>

завершення зі статусом </translation>
    </message>
    <message>
        <location filename="../command.cpp" line="237"/>
        <source>Command Failed</source>
        <translation>Помилка виконання команди</translation>
    </message>
</context>
<context>
    <name>CommandDialog</name>
    <message>
        <location filename="../commanddialog.cpp" line="47"/>
        <source>Edit Commands 0.1 alpha</source>
        <translation>Редактор команд 0.1 alpha</translation>
    </message>
    <message>
        <location filename="../commanddialog.cpp" line="63"/>
        <source>Name:</source>
        <translation>Назва:</translation>
    </message>
    <message>
        <location filename="../commanddialog.cpp" line="79"/>
        <source>Popup</source>
        <translation>Спливне вікно</translation>
    </message>
    <message>
        <location filename="../commanddialog.cpp" line="84"/>
        <source>Command Line:</source>
        <translation>Командний рядок:</translation>
    </message>
    <message>
        <location filename="../commanddialog.cpp" line="95"/>
        <source>Substitutions:
%p	PID
%c	COMMAND
%C	CMDLINE
%u	USER
%%	%

</source>
        <translation>Заміщення:
%p	PID
%c	COMMAND
%C	CMDLINE
%u	USER
%%	%

</translation>
    </message>
    <message>
        <location filename="../commanddialog.cpp" line="109"/>
        <source>New...</source>
        <translation>Новий...</translation>
    </message>
    <message>
        <location filename="../commanddialog.cpp" line="111"/>
        <location filename="../commanddialog.cpp" line="229"/>
        <source>Add...</source>
        <translation>Додати...</translation>
    </message>
    <message>
        <location filename="../commanddialog.cpp" line="113"/>
        <source>Delete</source>
        <translation>Видалити</translation>
    </message>
    <message>
        <location filename="../commanddialog.cpp" line="115"/>
        <source>Close</source>
        <translation>Закрити</translation>
    </message>
</context>
<context>
    <name>ControlBar</name>
    <message>
        <location filename="../misc.cpp" line="817"/>
        <source>Linear</source>
        <translation>Лінійний</translation>
    </message>
    <message>
        <location filename="../misc.cpp" line="820"/>
        <source>Tree</source>
        <translation>Ієрархічний</translation>
    </message>
    <message>
        <location filename="../misc.cpp" line="829"/>
        <source>Thread</source>
        <translation>Потік</translation>
    </message>
    <message>
        <location filename="../misc.cpp" line="836"/>
        <source>All Processes</source>
        <translation>Всі процеси</translation>
    </message>
    <message>
        <location filename="../misc.cpp" line="837"/>
        <source>Your Processes</source>
        <translation>Ваші процеси</translation>
    </message>
    <message>
        <location filename="../misc.cpp" line="838"/>
        <source>Non-Root Processes</source>
        <translation>Не Root процеси</translation>
    </message>
    <message>
        <location filename="../misc.cpp" line="839"/>
        <source>Running Processes</source>
        <translation>Запущені процеси</translation>
    </message>
    <message>
        <location filename="../misc.cpp" line="855"/>
        <source>Pause (Ctrl+Space)</source>
        <translation>Пауза (Ctrl+Space)</translation>
    </message>
</context>
<context>
    <name>Details</name>
    <message>
        <location filename="../details.cpp" line="36"/>
        <source>Process %1 ( %2 ) - details</source>
        <translation>Процес %1 ( %2 ) - детально</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="45"/>
        <source>Files</source>
        <translation>Файли</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="54"/>
        <source>Sockets</source>
        <translation>Сокети</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="59"/>
        <source>Memory Maps</source>
        <translation>Мапи пам&apos;яті</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="61"/>
        <source>Environment</source>
        <translation>Середовище</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="62"/>
        <source>All Fields</source>
        <translation>Усі поля</translation>
    </message>
</context>
<context>
    <name>Environ</name>
    <message>
        <location filename="../details.cpp" line="605"/>
        <source>Variable</source>
        <translation>Змінна середовища</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="605"/>
        <source>Variable name</source>
        <translation>Назва змінної</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="606"/>
        <source>Value</source>
        <translation>Значення</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="606"/>
        <source>Variable value</source>
        <translation>Значення змінної</translation>
    </message>
</context>
<context>
    <name>EventDialog</name>
    <message>
        <location filename="../watchdog.ui" line="23"/>
        <source>Watchdog 0.1 alpha</source>
        <translation>Вартівник 0.1 alpha</translation>
    </message>
    <message>
        <location filename="../watchdog.ui" line="117"/>
        <source>Eventcat</source>
        <translation>Eventcat</translation>
    </message>
    <message>
        <location filename="../watchdog.ui" line="162"/>
        <source>Select condition</source>
        <translation>Вибір умови</translation>
    </message>
    <message>
        <location filename="../watchdog.ui" line="170"/>
        <source>labelDescrition</source>
        <translation>labelDescrition</translation>
    </message>
    <message>
        <location filename="../watchdog.ui" line="193"/>
        <source>Enable</source>
        <translation>Увімкнути</translation>
    </message>
    <message>
        <location filename="../watchdog.ui" line="219"/>
        <source>process name</source>
        <translation>назва процесу</translation>
    </message>
    <message>
        <location filename="../watchdog.ui" line="229"/>
        <source>cpu</source>
        <translation>цп</translation>
    </message>
    <message>
        <location filename="../watchdog.ui" line="248"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="../watchdog.ui" line="280"/>
        <source>include already running process</source>
        <translation>врахувати запущений процес</translation>
    </message>
    <message>
        <location filename="../watchdog.ui" line="309"/>
        <source>run command</source>
        <translation>запустити команду</translation>
    </message>
    <message>
        <location filename="../watchdog.ui" line="342"/>
        <source>show Message</source>
        <translation>показати повідомлення</translation>
    </message>
    <message>
        <location filename="../watchdog.ui" line="367"/>
        <source>Help (Not yet. just concept)</source>
        <translation>Довідка (в розробці)</translation>
    </message>
    <message>
        <location filename="../watchdog.ui" line="394"/>
        <source>%p : pid
%c : command</source>
        <translation>%p : pid
%c : команда</translation>
    </message>
    <message>
        <location filename="../watchdog.ui" line="425"/>
        <source>New</source>
        <translation>Новий</translation>
    </message>
    <message>
        <location filename="../watchdog.ui" line="432"/>
        <source>Add</source>
        <translation>Додати</translation>
    </message>
    <message>
        <location filename="../watchdog.ui" line="439"/>
        <source>Delete</source>
        <translation>Видалити</translation>
    </message>
    <message>
        <location filename="../watchdog.ui" line="446"/>
        <source>Close</source>
        <translation>Закрити</translation>
    </message>
</context>
<context>
    <name>ExecWindow</name>
    <message>
        <location filename="../message.ui" line="13"/>
        <source>Qps</source>
        <translation>Qps</translation>
    </message>
    <message>
        <location filename="../message.ui" line="27"/>
        <source>Ok</source>
        <translation>Гаразд</translation>
    </message>
    <message>
        <location filename="../execwindow.cpp" line="42"/>
        <source>Qps Watchdog</source>
        <translation>Qps Вартівник</translation>
    </message>
    <message>
        <location filename="../execwindow.cpp" line="94"/>
        <source>%1 exit with code %2</source>
        <translation>%1 закінчилась з кодом %2</translation>
    </message>
    <message>
        <location filename="../execwindow.cpp" line="96"/>
        <source>Close</source>
        <translation>Закрити</translation>
    </message>
    <message>
        <location filename="../execwindow.cpp" line="102"/>
        <source>%1 [running]</source>
        <translation>%1 [запущений]</translation>
    </message>
    <message>
        <location filename="../execwindow.cpp" line="103"/>
        <source>terminate command</source>
        <translation>перервати команду</translation>
    </message>
    <message>
        <location filename="../execwindow.cpp" line="112"/>
        <source>Error %1 : [%2] Maybe command not found</source>
        <translation>Помилка %1 : [%2] Можливо, команду не знайдено</translation>
    </message>
</context>
<context>
    <name>FieldSelect</name>
    <message>
        <location filename="../fieldsel.cpp" line="34"/>
        <source>Select Custom Fields </source>
        <translation>Вибрати користувацькі поля </translation>
    </message>
    <message>
        <location filename="../fieldsel.cpp" line="75"/>
        <source>Close</source>
        <translation>Закрити</translation>
    </message>
</context>
<context>
    <name>Files</name>
    <message>
        <location filename="../details.cpp" line="521"/>
        <source>Fd</source>
        <translation>Fd</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="521"/>
        <source>File descriptor</source>
        <translation>Дескриптор файлу</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="523"/>
        <source>Mode</source>
        <translation>Режим</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="523"/>
        <source>Open mode</source>
        <translation>Режим відкриття</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="525"/>
        <source>Name</source>
        <translation>Назва</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="525"/>
        <source>File name (if available)</source>
        <translation>Назва файлу (якщо доступна)</translation>
    </message>
</context>
<context>
    <name>IntervalDialog</name>
    <message>
        <location filename="../dialogs.cpp" line="43"/>
        <source>Change Update Period</source>
        <translation>Змінити частоту оновлення</translation>
    </message>
    <message>
        <location filename="../dialogs.cpp" line="49"/>
        <source>New Update Period</source>
        <translation>Нова частота оновлення</translation>
    </message>
    <message>
        <location filename="../dialogs.cpp" line="85"/>
        <source>Cancel</source>
        <translation>Скасувати</translation>
    </message>
    <message>
        <location filename="../dialogs.cpp" line="87"/>
        <source>OK</source>
        <translation>Гаразд</translation>
    </message>
    <message>
        <location filename="../dialogs.cpp" line="121"/>
        <source>No UPDATE</source>
        <translation>Не ОНОВЛЮВАТИ</translation>
    </message>
    <message>
        <location filename="../dialogs.cpp" line="139"/>
        <source>Invalid value</source>
        <translation>Хибне значення</translation>
    </message>
</context>
<context>
    <name>ListModel</name>
    <message>
        <location filename="../listmodel.cpp" line="66"/>
        <source>Event Category</source>
        <translation>Категорія події</translation>
    </message>
    <message>
        <location filename="../listmodel.cpp" line="68"/>
        <source>Enable</source>
        <translation>Увімкнути</translation>
    </message>
</context>
<context>
    <name>Maps</name>
    <message>
        <location filename="../details.cpp" line="392"/>
        <source>Address Range</source>
        <translation>Діапазон адрес</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="392"/>
        <source>Mapped addresses (hex) )</source>
        <translation>Показані адреси (шістнадцяткове) )</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="393"/>
        <source>Size</source>
        <translation>Розмір</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="393"/>
        <source>Kbytes mapped (dec)</source>
        <translation>Кілобайт показано (десяткове)</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="394"/>
        <source>Perm</source>
        <translation>Дозволи</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="394"/>
        <source>Permission flags</source>
        <translation>Прапори дозволів</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="395"/>
        <source>Offset</source>
        <translation>Зсув</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="395"/>
        <source>File offset at start of mapping (hex)</source>
        <translation>Зсув початку показу у файлі (шістнадцяткове)</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="396"/>
        <source>Device</source>
        <translation>Пристрій</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="396"/>
        <source>Major,Minor device numbers (dec)</source>
        <translation>Основні та другорядні номери пристроїв (десяткові)</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="397"/>
        <source>Inode</source>
        <translation>I-вузол</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="397"/>
        <source>Inode number (dec)</source>
        <translation>Номер I-вузла (десяткове)</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="398"/>
        <source>File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="398"/>
        <source>File name (if available)</source>
        <translation>Назва файлу (якщо доступна)</translation>
    </message>
</context>
<context>
    <name>PermissionDialog</name>
    <message>
        <location filename="../dialogs.cpp" line="264"/>
        <source>Permission</source>
        <translation>Дозволи</translation>
    </message>
    <message>
        <location filename="../dialogs.cpp" line="273"/>
        <source>Root password</source>
        <translation>Пароль суперкористувача</translation>
    </message>
    <message>
        <location filename="../dialogs.cpp" line="280"/>
        <source>Cancel</source>
        <translation>Скасувати</translation>
    </message>
    <message>
        <location filename="../dialogs.cpp" line="283"/>
        <source>OK</source>
        <translation>Гаразд</translation>
    </message>
</context>
<context>
    <name>Preferences</name>
    <message>
        <location filename="../prefs.cpp" line="150"/>
        <source>Preferences</source>
        <translation>Параметри</translation>
    </message>
    <message>
        <location filename="../prefs.cpp" line="157"/>
        <source>Setting</source>
        <translation>Налаштування</translation>
    </message>
    <message>
        <location filename="../prefs.cpp" line="191"/>
        <source>%CPU divided by</source>
        <translation>%ЦП поділено</translation>
    </message>
    <message>
        <location filename="../prefs.cpp" line="198"/>
        <source>Total cpu: %1</source>
        <translation>Усього ЦП: %1</translation>
    </message>
    <message>
        <location filename="../prefs.cpp" line="199"/>
        <source>Single cpu: 1</source>
        <translation>Єдиний ЦП: 1</translation>
    </message>
    <message>
        <location filename="../prefs.cpp" line="205"/>
        <source>default</source>
        <translation>типово</translation>
    </message>
    <message>
        <location filename="../prefs.cpp" line="206"/>
        <source>for developer</source>
        <translation>для розробників</translation>
    </message>
    <message>
        <location filename="../prefs.cpp" line="240"/>
        <source>Appearance</source>
        <translation>Зовнішній вигляд</translation>
    </message>
</context>
<context>
    <name>Proc</name>
    <message>
        <location filename="../proc.cpp" line="2876"/>
        <source>PID</source>
        <translation>PID</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2876"/>
        <source>Process ID</source>
        <translation>ID процесу</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2878"/>
        <source>TGID</source>
        <translation>TGID</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2878"/>
        <source>Task group ID ( parent of threads )</source>
        <translation>ID група завдань ( початок потоків )</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2880"/>
        <source>PPID</source>
        <translation>PPID</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2880"/>
        <source>Parent process ID</source>
        <translation>ID батьківського потоку</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2882"/>
        <source>PGID</source>
        <translation>PGID</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2882"/>
        <source>Process group ID</source>
        <translation>ID групи процесів</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2884"/>
        <source>SID</source>
        <translation>SID</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2884"/>
        <source>Session ID</source>
        <translation>ID сеансу</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2886"/>
        <source>TTY</source>
        <translation>TTY</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2886"/>
        <source>Terminal</source>
        <translation>Термінал</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2888"/>
        <source>TPGID</source>
        <translation>TPGID</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2888"/>
        <source>Process group ID of tty owner</source>
        <translation>ID групи процесів власника терміналу</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2890"/>
        <source>USER</source>
        <translation>КОРИСТУВАЧ</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2890"/>
        <source>Owner (*=suid root, +=suid a user)</source>
        <translation>Власник (*=suid суперкористувач, +=suid користувач)</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2892"/>
        <source>GROUP</source>
        <translation>ГРУПА</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2892"/>
        <source>Group name (*=sgid other)</source>
        <translation>Назва групи (*=sgid інше)</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2894"/>
        <source>UID</source>
        <translation>UID</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2894"/>
        <source>Real user ID</source>
        <translation>Справжній ID користувача</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2896"/>
        <source>EUID</source>
        <translation>EUID</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2896"/>
        <source>Effective user ID</source>
        <translation>Ефективний ID користувача</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2898"/>
        <source>SUID</source>
        <translation>SUID</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2898"/>
        <source>Saved user ID (Posix)</source>
        <translation>Збережений ID користувача (Posix)</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2900"/>
        <source>FSUID</source>
        <translation>FSUID</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2900"/>
        <source>File system user ID</source>
        <translation>ID користувача файлової системи</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2902"/>
        <source>GID</source>
        <translation>GID</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2902"/>
        <source>Real group ID</source>
        <translation>Справжній ID групи</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2904"/>
        <source>EGID</source>
        <translation>EGID</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2904"/>
        <source>Effective group ID</source>
        <translation>Ефективний ID групи</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2906"/>
        <source>SGID</source>
        <translation>SGID</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2906"/>
        <source>Saved group ID (Posix)</source>
        <translation>Збереженй ID групи (Posix)</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2908"/>
        <source>FSGID</source>
        <translation>FSGID</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2908"/>
        <source>File system group ID</source>
        <translation>ID групи з доступом до файлової системи</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2910"/>
        <source>PRI</source>
        <translation>PRI</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2910"/>
        <source>Dynamic priority</source>
        <translation>Динамічний пріоритет</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2912"/>
        <source>NICE</source>
        <translation>NICE</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2912"/>
        <source>Scheduling favour (higher -&gt; less cpu time)</source>
        <translation>Перевага у черзі (більше -&gt; менше процесорного часу)</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2914"/>
        <source>NLWP</source>
        <translation>NLWP</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2914"/>
        <source>Number of tasks(threads) in task group</source>
        <translation>Кількість завдань (потоків) у групі завдань</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2916"/>
        <source>PLCY</source>
        <translation>PLCY</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2916"/>
        <source>Scheduling policy</source>
        <translation>Політика черги</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2918"/>
        <source>RPRI</source>
        <translation>RPRI</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2918"/>
        <source>Realtime priority (0-99, more is better)</source>
        <translation>Пріоритет реального часу (0-99, більше — краще)</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2920"/>
        <source>TMS</source>
        <translation>TMS</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2920"/>
        <source>Time slice in milliseconds</source>
        <translation>Відтинок часу в мілісекундах</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2922"/>
        <source>%SAVG</source>
        <translation>%SAVG</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2922"/>
        <source>Percentage average sleep time (-1 -&gt; N/A)</source>
        <translation>Середній відсоток часу сну (-1 -&gt; Н/Д)</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2924"/>
        <source>CPUSET</source>
        <translation>CPUSET</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2924"/>
        <source>Affinity CPU mask (0 -&gt; API not supported)</source>
        <translation>Маска CPU спорідненості (0 -&gt; API не підтримується)</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2926"/>
        <source>MAJFLT</source>
        <translation>MAJFLT</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2926"/>
        <source>Number of major faults (disk access)</source>
        <translation>Кількість значних відмов (доступ до накопичувача)</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2928"/>
        <source>MINFLT</source>
        <translation>MINFLT</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2928"/>
        <source>Number of minor faults (no disk access)</source>
        <translation>Кількість незначних відмов (крім доступу до накопичувача)</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2931"/>
        <source>VSIZE</source>
        <translation>VSIZE</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2931"/>
        <source>Virtual image size of process</source>
        <translation>Розмір віртуального образу процесу</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2933"/>
        <source>RSS</source>
        <translation>RSS</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2933"/>
        <source>Resident set size</source>
        <translation>Розмір резидентної частини</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2935"/>
        <source>MEM</source>
        <translation>MEM</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2935"/>
        <source>memory usage (RSS-SHARE)</source>
        <translation>використовування пам&apos;яті (RSS-SHARE)</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2937"/>
        <source>TRS</source>
        <translation>TRS</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2937"/>
        <source>Text(code) resident set size</source>
        <translation>Розмір тексту(коду) резидентної частини</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2939"/>
        <source>DRS</source>
        <translation>DRS</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2939"/>
        <source>Data resident set size(malloc+global variable)</source>
        <translation>Розмір даних резидентної частини (виділена пам&apos;ять + глобальні змінні)</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2941"/>
        <source>STACK</source>
        <translation>STACK</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2941"/>
        <source>Stack size</source>
        <translation>Розмір стеку</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2943"/>
        <source>SHARE</source>
        <translation>SHARE</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2943"/>
        <source>Shared memory with other libs</source>
        <translation>Обсяг пам&apos;яті, розділеної з іншими бібліотеками</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2945"/>
        <source>SWAP</source>
        <translation>SWAP</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2945"/>
        <source>Kbytes on swap device</source>
        <translation>кБайт у пристрої підкачки</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2947"/>
        <source>IO_R</source>
        <translation>IO_R</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2947"/>
        <source>io read (file)</source>
        <translation>прочитано вводу/виводу (файли)</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2949"/>
        <source>IO_W</source>
        <translation>IO_W</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2949"/>
        <source>io write (file)</source>
        <translation>записано вводу/виводу (файли)</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2951"/>
        <source>DT</source>
        <translation>DT</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2951"/>
        <source>Number of dirty (non-written) pages</source>
        <translation>Кількість «брудних» (не записаних) сторінок</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2953"/>
        <source>STAT</source>
        <translation>STAT</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2953"/>
        <source>State of the process </source>
        <translation>Стан процесів </translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2955"/>
        <source>FLAGS</source>
        <translation>FLAGS</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2955"/>
        <source>Process flags (hex)</source>
        <translation>Прапори процесу (шістнадцяткове)</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2957"/>
        <source>WCHAN</source>
        <translation>WCHAN</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2957"/>
        <source>Kernel function where process is sleeping</source>
        <translation>Функція ядра через яку спить процес</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2959"/>
        <source>%WCPU</source>
        <translation>%WCPU</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2959"/>
        <source>Weighted percentage of CPU (30 s average)</source>
        <translation>Відсоток завантаження ЦП (в середньому за 30 сек)</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2961"/>
        <source>%CPU</source>
        <translation>%CPU</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2961"/>
        <source>Percentage of CPU used since last update</source>
        <translation>Відсоток завантаження ЦП з моменту останнього оновлення</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2963"/>
        <source>%MEM</source>
        <translation>%MEM</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2963"/>
        <source>Percentage of memory used (RSS/total mem)</source>
        <translation>Відсоток використання пам&apos;яті (резидентна частина/загальна кількість)</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2965"/>
        <source>START</source>
        <translation>START</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2965"/>
        <source>Time process started</source>
        <translation>Час запуску процесу</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2967"/>
        <source>TIME</source>
        <translation>TIME</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2967"/>
        <source>Total CPU time used since start</source>
        <translation>Всього використано часу ЦП від запуску</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2969"/>
        <source>CPU</source>
        <translation>CPU</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2969"/>
        <source>CPU the process is executing on (SMP system)</source>
        <translation>ЦП, на якому виконується процес (системи SMP)</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2971"/>
        <source>the process name</source>
        <translation>назва процесу</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2971"/>
        <source>Process Name</source>
        <translation>Назва процесу</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2975"/>
        <source>CWD</source>
        <translation>CWD</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2975"/>
        <source>Current working directory</source>
        <translation>Поточний робочий каталог</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2976"/>
        <source>ROOT</source>
        <translation>ROOT</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2976"/>
        <source>Root directory of process</source>
        <translation>Кореневий каталог процесу</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2979"/>
        <source>COMMAND_LINE</source>
        <translation>COMMAND_LINE</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2979"/>
        <source>Command line that started the process</source>
        <translation>Командний рядок запуску процесу</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../qps.cpp" line="1846"/>
        <source>Show</source>
        <translation>Показати</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="1849"/>
        <source>Hide</source>
        <translation>Сховати</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="1853"/>
        <source>Quit</source>
        <translation>Вийти</translation>
    </message>
</context>
<context>
    <name>Qps</name>
    <message>
        <location filename="../qps.cpp" line="198"/>
        <source>header_popup</source>
        <translation>header_popup</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="199"/>
        <source>Remove Field</source>
        <translation>Вилучити поле</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="200"/>
        <source>Add Field</source>
        <translation>Додати поле</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="207"/>
        <source>View</source>
        <translation>Перегляд</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="208"/>
        <source>Process</source>
        <translation>Процес</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="209"/>
        <source>Log</source>
        <translation>Журнал</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="214"/>
        <source>Fields</source>
        <translation>Поля</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="215"/>
        <source>Custom Fields</source>
        <translation>Користувацькі поля</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="217"/>
        <source>Basic Fields </source>
        <translation>Основні поля </translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="219"/>
        <source>Jobs Fields </source>
        <translation>Поля завдань </translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="221"/>
        <source>Memory Fields </source>
        <translation>Поля пам&apos;яті </translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="224"/>
        <source>Scheduling Fields </source>
        <translation>Поля черги </translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="241"/>
        <source>Select Custom Fields...</source>
        <translation>Вибрати власні поля...</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="203"/>
        <source>Command</source>
        <translation>Команда</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="255"/>
        <source>Options</source>
        <translation>Опції</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="256"/>
        <source>Update Period...</source>
        <translation>Повторюваність оновлення...</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="265"/>
        <source>Show Status bar</source>
        <translation>Показати панель стану</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="274"/>
        <source>Preferences...</source>
        <translation>Параметри...</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="280"/>
        <source>Help</source>
        <translation>Довідка</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="282"/>
        <location filename="../qps.cpp" line="1894"/>
        <source>About</source>
        <translation>Про застосунок</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="370"/>
        <source>Detail</source>
        <translation>Подробиці</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="411"/>
        <source>test</source>
        <translation>тест</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="412"/>
        <source>Copied to Clipboard</source>
        <translation>Скопійовано до буфера обміну</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="464"/>
        <source>context popup</source>
        <translation>контекстне спливне вікно</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="465"/>
        <source>Renice...</source>
        <translation>Зміна пріоритету процесу...</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="466"/>
        <source>Scheduling...</source>
        <translation>Планування...</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="468"/>
        <source>Terminate</source>
        <translation>Перервати</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="470"/>
        <source>Hangup</source>
        <translation>Розірвати</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="471"/>
        <source>Kill</source>
        <translation>Знищити</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="472"/>
        <source>Stop</source>
        <translation>Зупинити</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="474"/>
        <source>Continue</source>
        <translation>Продовжити</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="478"/>
        <source>SIGINT (interrupt)</source>
        <translation>SIGINT (припинити)</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="480"/>
        <source>SIGCONT (continue)</source>
        <translation>SIGCONT (продовжити)</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="482"/>
        <source>SIGSTOP (stop)</source>
        <translation>SIGSTOP (зупинити)</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="484"/>
        <source>SIGQUIT (quit)</source>
        <translation>SIGQUIT (вийти)</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="486"/>
        <source>SIGILL (illegal instruction)</source>
        <translation>SIGILL (заборонена інструкція)</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="488"/>
        <source>SIGABRT (abort)</source>
        <translation>SIGABRT (перервати)</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="490"/>
        <source>SIGFPE (floating point exception)</source>
        <translation>SIGFPE (вилучення через рухому кому)</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="492"/>
        <source>SIGSEGV (segmentation violation)</source>
        <translation>SIGSEGV (порушення сегментації)</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="494"/>
        <source>SIGPIPE (broken pipe)</source>
        <translation>SIGPIPE (розірваний канал)</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="496"/>
        <source>SIGALRM (timer signal)</source>
        <translation>SIGALRM (сигнал таймера)</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="498"/>
        <source>SIGUSR1 (user-defined 1)</source>
        <translation>SIGUSR1 (користувацький сигнал 1)</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="500"/>
        <source>SIGUSR2 (user-defined 2)</source>
        <translation>SIGUSR2 (користувацький сигнал 2)</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="502"/>
        <source>SIGCHLD (child death)</source>
        <translation>SIGCHLD (завершення дочірнього процесу)</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="504"/>
        <source>SIGTSTP (stop from tty)</source>
        <translation>SIGTSTP (зупинення з терміналу)</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="506"/>
        <source>SIGTTIN (tty input)</source>
        <translation>SIGTTIN (читання з терміналу)</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="508"/>
        <source>SIGTTOU (tty output)</source>
        <translation>SIGTTOU (запис на термінал)</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="515"/>
        <source>View Details</source>
        <translation>Перегляд подробиць</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="781"/>
        <source>Show File Path</source>
        <translation>Показати шлях файлу</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="787"/>
        <source>Show Graph</source>
        <translation>Показати графік</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="804"/>
        <source>Include Child Times</source>
        <translation>Врахувати час дочірніх процесів</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="998"/>
        <source>WatchDog</source>
        <translation>Вартівник</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="1000"/>
        <source>Edit Commands...</source>
        <translation>Змінити команди...</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="1017"/>
        <source>Quit</source>
        <translation>Вийти</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="1336"/>
        <location filename="../qps.cpp" line="1367"/>
        <location filename="../qps.cpp" line="1374"/>
        <location filename="../qps.cpp" line="1395"/>
        <location filename="../qps.cpp" line="1429"/>
        <location filename="../qps.cpp" line="1524"/>
        <source>Permission denied</source>
        <translation>У доступі відмовлено</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="1337"/>
        <source>You do not have permission to renice the selected processes. Only the process owner and the super-user are allowed to do that.</source>
        <translation>Ви не маєте дозволу змінювати пріоритет обраних процесів. Це можуть робити лише власник процесу та суперкористувач.</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="1368"/>
        <source>You do not have permission to renice process %1 (%2).Only the process owner and the super-user are allowed to do that.</source>
        <translation>Ви не маєте дозволу змінювати пріоритет процесу %1 ( %2). Це дозволено лише власнику процесу та суперкористувачу.</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="1375"/>
        <source>Only the super-user may lower the nice value of a process.</source>
        <translation>Тільки суперкористувач може знизити значення пріоритету процесу.</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="1396"/>
        <source>Only the super-user may change the scheduling policy and static priority.</source>
        <translation>Тільки суперкористувач може змінювати політику планування та статичний пріоритет.</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="1430"/>
        <source>You do not have permission to change the scheduling and/or priority of process %1 (%2). Only the super-user may do that.</source>
        <translation>Ви не маєте дозволу на зміну планування та/або пріоритету процесу %1 ( %2). Це може робити тільки суперкористувач.</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="1491"/>
        <source>Do you really want to terminate the selected process(es)?</source>
        <translation>Ви дійсно бажаєте припинити обраний(і) процес(и)?</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="1493"/>
        <source>Do you really want to hang up the selected process(es)?</source>
        <translation>Ви дійсно бажаєте розірвати обраний(і) процес(и)?</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="1495"/>
        <source>Do you really want to kill the selected process(es)?</source>
        <translation>Ви дійсно бажаєте знищити обраний(і) процес(и)?</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="1504"/>
        <source>Question</source>
        <translation>Запитання</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="1525"/>
        <source>You do not have permission to send a signal to process %1 (%2). Only the super-user and the owner of the process may send signals to it.</source>
        <translation>Ви не маєте дозволу надсилати сигнал до процесу %1 (%2). Лише суперкористувач та власник можуть надсилати йому сигнали.</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="1910"/>
        <source>&lt;center&gt;&lt;h2&gt; Qps %1&lt;/center&gt;</source>
        <translation>&lt;center&gt;&lt;h2&gt; Qps %1&lt;/center&gt;</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="1912"/>
        <source>&lt;h2&gt; Qps %1 - A Visual Process Manager &lt;/h2&gt; %2 using Qt library %3&lt;br&gt;&lt;br&gt;&lt;b&gt;Source: &lt;/b&gt;&lt;a href=&quot;https://github.com/lxqt/qps&quot;&gt;https://github.com/lxqt/qps/&lt;/a&gt;&lt;br&gt;&lt;b&gt;Bugtracker: &lt;/b&gt;&lt;a href=&quot;https://github.com/lxqt/qps/issues&quot;&gt;https://github.com/lxqt/qps/issues&lt;/a&gt;&lt;br&gt;</source>
        <translation>&lt;h2&gt; Qps %1 - наочний менеджер процесів &lt;/h2&gt; %2 використовує бібліотеку Qt %3&lt;br&gt;&lt;br&gt;&lt;b&gt;Джерельний код: &lt;/b&gt;&lt;a href=&quot;https://github.com/lxqt/qps&quot;&gt;https://github.com/lxqt/qps/&lt;/a&gt;&lt;br&gt;&lt;b&gt;Відстеження помилок: &lt;/b&gt;&lt;a href=&quot;https://github.com/lxqt/qps/issues&quot;&gt;https://github.com/lxqt/qps/issues&lt;/a&gt;&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="1932"/>
        <source>&lt;b&gt;Original Qps by&lt;/b&gt;&lt;br&gt;Mattias Engdegård (f91-men@nada.kth.se)&lt;br&gt;&lt;br&gt;&lt;b&gt;Contributors&lt;/b&gt;&lt;br&gt;Olivier.Daudel@u-paris10.fr&lt;br&gt;jsanchez@todounix.homeip.net &lt;br&gt;daehyun.yang@gmail.com &lt;br&gt;Luís Pereira (luis.artur.pereira@gmail.com)&lt;br&gt;Alf Gaida (agaida@siduction.org)&lt;br&gt;Paulo Lieuthier (paulolieuthier@gmail.com)&lt;br&gt;Jerome Leclanche (jerome@leclan.ch)&lt;br&gt;</source>
        <translation>&lt;b&gt;Первісний Qps написаний&lt;/b&gt;&lt;br&gt;Mattias Engdegård (f91-men@nada.kth.se)&lt;br&gt;&lt;br&gt;&lt;b&gt;Учасники проєкту&lt;/b&gt;&lt;br&gt;Olivier.Daudel@u-paris10.fr&lt;br&gt;jsanchez@todounix.homeip.net &lt;br&gt;daehyun.yang@gmail.com &lt;br&gt;Luís Pereira (luis.artur.pereira@gmail.com)&lt;br&gt;Alf Gaida (agaida@siduction.org)&lt;br&gt;Paulo Lieuthier (paulolieuthier@gmail.com)&lt;br&gt;Jerome Leclanche (jerome@leclan.ch)&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="1970"/>
        <source>&lt;H1&gt;QPS Help&lt;/H1&gt;Updated: May 24 2005&lt;BR&gt;&lt;A HREF=&quot;http://kldp.net/projects/qps&quot;&gt;http://kldp.net/projects/qps&lt;/A&gt;&lt;HR&gt;&lt;table style=&quot;text-align: center; width: 100%;&quot; border=&quot;1&quot; cellpadding=&quot;1&quot; cellspacing=&quot;0&quot;&gt;  &lt;tbody&gt;    &lt;tr&gt;      &lt;td style=&quot;vertical-align: top; background-color: rgb(204, 204, 204);&quot;&gt;Quit      &lt;/td&gt;      &lt;td &gt;&amp;nbsp; CTRL + q , CTRL + x      &lt;/td&gt;    &lt;/tr&gt;    &lt;tr&gt;      &lt;td style=&quot;vertical-align: top; background-color: rgb(204, 204, 204);&quot;&gt;Update      &lt;/td&gt;      &lt;td&gt;&amp;nbsp;Space , Enter       &lt;/td&gt;    &lt;/tr&gt;    &lt;tr&gt;&lt;td&gt; process Terminate &lt;/td&gt;  &lt;td&gt; ALT + T , DELETE &lt;/td&gt; &lt;/tr&gt;    &lt;tr&gt;&lt;td&gt; process Kill &lt;/td&gt;  &lt;td&gt; ALT + K  &lt;/td&gt; &lt;/tr&gt;  &lt;/tbody&gt;&lt;/table&gt;</source>
        <translation>&lt;H1&gt;QPS Довідка&lt;/H1&gt;Оновлено: 24 травня 2005&lt;BR&gt;&lt;A HREF=&quot;http://kldp.net/projects/qps&quot;&gt;http://kldp.net/projects/qps&lt;/A&gt;&lt;HR&gt;&lt;table style=&quot;text-align: center; width: 100%;&quot; border=&quot;1&quot; cellpadding=&quot;1&quot; cellspacing=&quot;0&quot;&gt;  &lt;tbody&gt;    &lt;tr&gt;      &lt;td style=&quot;vertical-align: top; background-color: rgb(204, 204, 204);&quot;&gt;Вийти      &lt;/td&gt;      &lt;td &gt;&amp;nbsp; CTRL + q , CTRL + x      &lt;/td&gt;    &lt;/tr&gt;    &lt;tr&gt;      &lt;td style=&quot;vertical-align: top; background-color: rgb(204, 204, 204);&quot;&gt;Оновити      &lt;/td&gt;      &lt;td&gt;&amp;nbsp;Space , Enter       &lt;/td&gt;    &lt;/tr&gt;    &lt;tr&gt;&lt;td&gt; перервати процес &lt;/td&gt;  &lt;td&gt; ALT + T , DELETE &lt;/td&gt; &lt;/tr&gt;    &lt;tr&gt;&lt;td&gt; знищити процес &lt;/td&gt;  &lt;td&gt; ALT + K  &lt;/td&gt; &lt;/tr&gt;  &lt;/tbody&gt;&lt;/table&gt;</translation>
    </message>
</context>
<context>
    <name>SchedDialog</name>
    <message>
        <location filename="../dialogs.cpp" line="292"/>
        <source>Change scheduling</source>
        <translation>Змінити планування</translation>
    </message>
    <message>
        <location filename="../dialogs.cpp" line="296"/>
        <source>Scheduling Policy</source>
        <translation>Політика планування</translation>
    </message>
    <message>
        <location filename="../dialogs.cpp" line="298"/>
        <source>SCHED_OTHER (time-sharing)</source>
        <translation>SCHED_OTHER (планувальник з розподілом часу)</translation>
    </message>
    <message>
        <location filename="../dialogs.cpp" line="299"/>
        <source>SCHED_FIFO (real-time)</source>
        <translation>SCHED_FIFO (планувальник реального часу)</translation>
    </message>
    <message>
        <location filename="../dialogs.cpp" line="300"/>
        <source>SCHED_RR (real-time)</source>
        <translation>SCHED_RR (планувальник реального часу)</translation>
    </message>
    <message>
        <location filename="../dialogs.cpp" line="314"/>
        <source>OK</source>
        <translation>Гаразд</translation>
    </message>
    <message>
        <location filename="../dialogs.cpp" line="316"/>
        <source>Cancel</source>
        <translation>Скасувати</translation>
    </message>
    <message>
        <location filename="../dialogs.cpp" line="346"/>
        <source>Priority (1-99):</source>
        <translation>Пріоритет (1-99):</translation>
    </message>
    <message>
        <location filename="../dialogs.cpp" line="379"/>
        <source>Invalid Input</source>
        <translation>Хибний ввід</translation>
    </message>
    <message>
        <location filename="../dialogs.cpp" line="380"/>
        <source>The priority must be in the range 1..99</source>
        <translation>Пріоритет має бути в проміжку 1..99</translation>
    </message>
</context>
<context>
    <name>SearchBox</name>
    <message>
        <location filename="../misc.cpp" line="765"/>
        <source>PID, COMMAND, USER...</source>
        <translation>PID, COMMAND, USER...</translation>
    </message>
    <message>
        <location filename="../misc.cpp" line="766"/>
        <source>Filter</source>
        <translation>Фільтр</translation>
    </message>
</context>
<context>
    <name>SliderDialog</name>
    <message>
        <location filename="../dialogs.cpp" line="179"/>
        <source>Renice Process</source>
        <translation>Зміна пріоритету процесу</translation>
    </message>
    <message>
        <location filename="../dialogs.cpp" line="185"/>
        <source>New nice value:</source>
        <translation>Нове значення пріоритету:</translation>
    </message>
    <message>
        <location filename="../dialogs.cpp" line="225"/>
        <source>Cancel</source>
        <translation>Скасувати</translation>
    </message>
    <message>
        <location filename="../dialogs.cpp" line="229"/>
        <source>OK</source>
        <translation>Гаразд</translation>
    </message>
</context>
<context>
    <name>Sockets</name>
    <message>
        <location filename="../details.cpp" line="147"/>
        <source>Fd</source>
        <translation>Fd</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="147"/>
        <source>File descriptor</source>
        <translation>Дескриптор файлу</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="148"/>
        <source>Proto</source>
        <translation>Proto</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="148"/>
        <source>Protocol (TCP or UDP)</source>
        <translation>Протокол (TCP або UDP)</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="149"/>
        <source>Recv-Q</source>
        <translation>Recv-Q</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="149"/>
        <source>Bytes in receive queue</source>
        <translation>Байт у черзі отримання</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="150"/>
        <source>Send-Q</source>
        <translation>Send-Q</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="150"/>
        <source>Bytes in send queue</source>
        <translation>Байт у черзі надсилання</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="151"/>
        <source>Local Addr</source>
        <translation>Локальна адреса</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="151"/>
        <source>Local IP address</source>
        <translation>Локальна IP адреса</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="152"/>
        <location filename="../details.cpp" line="154"/>
        <source>Port</source>
        <translation>Порт</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="152"/>
        <source>Local port</source>
        <translation>Локальний порт</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="153"/>
        <source>Remote Addr</source>
        <translation>Віддалена адреса</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="153"/>
        <source>Remote IP address</source>
        <translation>Віддалена IP-адреса</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="154"/>
        <source>Remote port</source>
        <translation>Віддалений порт</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="155"/>
        <source>State</source>
        <translation>Стан</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="155"/>
        <source>Connection state</source>
        <translation>Стан з&apos;єднання</translation>
    </message>
</context>
<context>
    <name>StatusBar</name>
    <message>
        <location filename="../misc.cpp" line="807"/>
        <source>Process count: %1</source>
        <translation>Кількість процесів: %1</translation>
    </message>
</context>
<context>
    <name>TBloon</name>
    <message>
        <location filename="../misc.cpp" line="347"/>
        <source> This is unstable Alpha feature
 You maybe see a SEGFAULT...</source>
        <translation> Це нестабільна можливість Альфа версії
 Спробуйте SEGFAULT...</translation>
    </message>
</context>
<context>
    <name>TFrame</name>
    <message>
        <location filename="../misc.cpp" line="426"/>
        <source>this is Tframe widget</source>
        <translation>це віджет Tframe</translation>
    </message>
</context>
<context>
    <name>UFrame</name>
    <message>
        <location filename="../misc.cpp" line="615"/>
        <source>title</source>
        <translation>заголовок</translation>
    </message>
</context>
<context>
    <name>WatchdogDialog</name>
    <message>
        <location filename="../watchdogdialog.cpp" line="193"/>
        <source>if process start</source>
        <translation>якщо процес запущено</translation>
    </message>
    <message>
        <location filename="../watchdogdialog.cpp" line="194"/>
        <source>if process finish</source>
        <translation>якщо процес завершено</translation>
    </message>
    <message>
        <location filename="../watchdogdialog.cpp" line="217"/>
        <source>select condition</source>
        <translation>вибрати умову</translation>
    </message>
</context>
</TS>
