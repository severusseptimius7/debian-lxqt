<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ko">
<context>
    <name>AllFields</name>
    <message>
        <location filename="../details.cpp" line="653"/>
        <source>Field</source>
        <translation>필드</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="653"/>
        <source>Field name</source>
        <translation>필드 이름</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="654"/>
        <source>Description</source>
        <translation>설명</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="654"/>
        <source>Field description</source>
        <translation>필드 설명</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="655"/>
        <source>Value</source>
        <translation>값</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="655"/>
        <source>Field value</source>
        <translation>필드 값</translation>
    </message>
</context>
<context>
    <name>Boxvar</name>
    <message>
        <location filename="../prefs.cpp" line="59"/>
        <source>Exit on closing</source>
        <translation>닫을 때 끝내기</translation>
    </message>
    <message>
        <location filename="../prefs.cpp" line="60"/>
        <source>Remember Position</source>
        <translation>위치 기억</translation>
    </message>
    <message>
        <location filename="../prefs.cpp" line="68"/>
        <source>Host Name Lookup</source>
        <translation>호스트 이름 조회</translation>
    </message>
    <message>
        <location filename="../prefs.cpp" line="69"/>
        <source>Service Name Lookup</source>
        <translation>서비스 이름 조회</translation>
    </message>
    <message>
        <location filename="../prefs.cpp" line="75"/>
        <source>Disclosure Triangles</source>
        <translation>펼침 삼각형</translation>
    </message>
    <message>
        <location filename="../prefs.cpp" line="76"/>
        <source>Branch Lines</source>
        <translation>분기 라인</translation>
    </message>
    <message>
        <location filename="../prefs.cpp" line="82"/>
        <source>Auto Save Settings on Exit</source>
        <translation>종료 시 설정 자동 저장</translation>
    </message>
    <message>
        <location filename="../prefs.cpp" line="83"/>
        <source>Selection: Copy PIDs to Clipboard</source>
        <translation>선택: 클립보드에 PID 복사</translation>
    </message>
    <message>
        <location filename="../prefs.cpp" line="85"/>
        <source>Normalize NICE</source>
        <translation>NICE 정규화</translation>
    </message>
    <message>
        <location filename="../prefs.cpp" line="86"/>
        <source>Use pmap for Map Names</source>
        <translation>맵 이름에 pmap 사용</translation>
    </message>
</context>
<context>
    <name>Cbgroup</name>
    <message>
        <location filename="../prefs.cpp" line="106"/>
        <source>General</source>
        <translation>일반</translation>
    </message>
</context>
<context>
    <name>Command</name>
    <message>
        <location filename="../command.cpp" line="208"/>
        <source>The command:

</source>
        <translation>명령:

</translation>
    </message>
    <message>
        <location filename="../command.cpp" line="212"/>
        <source>

failed with the error:

</source>
        <translation>

오류로 실패:

</translation>
    </message>
    <message>
        <location filename="../command.cpp" line="215"/>
        <source>Too many processes</source>
        <translation>너무 많은 프로세스</translation>
    </message>
    <message>
        <location filename="../command.cpp" line="218"/>
        <source>Unknown error</source>
        <translation>알 수 없는 오류</translation>
    </message>
    <message>
        <location filename="../command.cpp" line="228"/>
        <source>

could not be executed because it was not found,
or you did not have execute permission.</source>
        <translation>

찾을 수 없거나 실행 권한이 없기 때문에 
실행할 수 없습니다.</translation>
    </message>
    <message>
        <location filename="../command.cpp" line="233"/>
        <source>

exited with status </source>
        <translation>

상태로 종료 </translation>
    </message>
    <message>
        <location filename="../command.cpp" line="237"/>
        <source>Command Failed</source>
        <translation>명령 실패</translation>
    </message>
</context>
<context>
    <name>CommandDialog</name>
    <message>
        <location filename="../commanddialog.cpp" line="47"/>
        <source>Edit Commands 0.1 alpha</source>
        <translation>명령 0.1 알파 편집</translation>
    </message>
    <message>
        <location filename="../commanddialog.cpp" line="63"/>
        <source>Name:</source>
        <translation>이름:</translation>
    </message>
    <message>
        <location filename="../commanddialog.cpp" line="79"/>
        <source>Popup</source>
        <translation>팝업</translation>
    </message>
    <message>
        <location filename="../commanddialog.cpp" line="84"/>
        <source>Command Line:</source>
        <translation>명령줄:</translation>
    </message>
    <message>
        <location filename="../commanddialog.cpp" line="95"/>
        <source>Substitutions:
%p	PID
%c	COMMAND
%C	CMDLINE
%u	USER
%%	%

</source>
        <translation>대체:
%p	PID
%c	COMMAND
%C	CMDLINE
%u	USER
%%	%

</translation>
    </message>
    <message>
        <location filename="../commanddialog.cpp" line="109"/>
        <source>New...</source>
        <translation>새로 만들기...</translation>
    </message>
    <message>
        <location filename="../commanddialog.cpp" line="111"/>
        <location filename="../commanddialog.cpp" line="229"/>
        <source>Add...</source>
        <translation>추가...</translation>
    </message>
    <message>
        <location filename="../commanddialog.cpp" line="113"/>
        <source>Delete</source>
        <translation>삭제</translation>
    </message>
    <message>
        <location filename="../commanddialog.cpp" line="115"/>
        <source>Close</source>
        <translation>닫기</translation>
    </message>
</context>
<context>
    <name>ControlBar</name>
    <message>
        <location filename="../misc.cpp" line="817"/>
        <source>Linear</source>
        <translation>직선</translation>
    </message>
    <message>
        <location filename="../misc.cpp" line="820"/>
        <source>Tree</source>
        <translation>트리</translation>
    </message>
    <message>
        <location filename="../misc.cpp" line="829"/>
        <source>Thread</source>
        <translation>스레드</translation>
    </message>
    <message>
        <location filename="../misc.cpp" line="836"/>
        <source>All Processes</source>
        <translation>모든 프로세스</translation>
    </message>
    <message>
        <location filename="../misc.cpp" line="837"/>
        <source>Your Processes</source>
        <translation>사용자 프로세스</translation>
    </message>
    <message>
        <location filename="../misc.cpp" line="838"/>
        <source>Non-Root Processes</source>
        <translation>루트가 아닌 프로세스</translation>
    </message>
    <message>
        <location filename="../misc.cpp" line="839"/>
        <source>Running Processes</source>
        <translation>프로세스 실행 중</translation>
    </message>
    <message>
        <location filename="../misc.cpp" line="855"/>
        <source>Pause (Ctrl+Space)</source>
        <translation>일시 중지 (Ctrl+Space)</translation>
    </message>
</context>
<context>
    <name>Details</name>
    <message>
        <location filename="../details.cpp" line="36"/>
        <source>Process %1 ( %2 ) - details</source>
        <translation>프로세스 %1 ( %2 ) - 세부 사항</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="45"/>
        <source>Files</source>
        <translation>파일</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="54"/>
        <source>Sockets</source>
        <translation>소켓</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="59"/>
        <source>Memory Maps</source>
        <translation>메모리 맵</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="61"/>
        <source>Environment</source>
        <translation>환경</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="62"/>
        <source>All Fields</source>
        <translation>모든 필드</translation>
    </message>
</context>
<context>
    <name>Environ</name>
    <message>
        <location filename="../details.cpp" line="605"/>
        <source>Variable</source>
        <translation>변수</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="605"/>
        <source>Variable name</source>
        <translation>변수 이름</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="606"/>
        <source>Value</source>
        <translation>값</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="606"/>
        <source>Variable value</source>
        <translation>변수 값</translation>
    </message>
</context>
<context>
    <name>EventDialog</name>
    <message>
        <location filename="../watchdog.ui" line="23"/>
        <source>Watchdog 0.1 alpha</source>
        <translation>워치독 0.1 알파</translation>
    </message>
    <message>
        <location filename="../watchdog.ui" line="117"/>
        <source>Eventcat</source>
        <translation>이벤트캣</translation>
    </message>
    <message>
        <location filename="../watchdog.ui" line="162"/>
        <source>Select condition</source>
        <translation>조건 선택</translation>
    </message>
    <message>
        <location filename="../watchdog.ui" line="170"/>
        <source>labelDescrition</source>
        <translation>레이블설명</translation>
    </message>
    <message>
        <location filename="../watchdog.ui" line="193"/>
        <source>Enable</source>
        <translation>활성화</translation>
    </message>
    <message>
        <location filename="../watchdog.ui" line="219"/>
        <source>process name</source>
        <translation>프로세스 이름</translation>
    </message>
    <message>
        <location filename="../watchdog.ui" line="229"/>
        <source>cpu</source>
        <translation>CPU</translation>
    </message>
    <message>
        <location filename="../watchdog.ui" line="248"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="../watchdog.ui" line="280"/>
        <source>include already running process</source>
        <translation>이미 실행 중인 프로세스 포함</translation>
    </message>
    <message>
        <location filename="../watchdog.ui" line="309"/>
        <source>run command</source>
        <translation>명령 실행</translation>
    </message>
    <message>
        <location filename="../watchdog.ui" line="342"/>
        <source>show Message</source>
        <translation>메시지 표시</translation>
    </message>
    <message>
        <location filename="../watchdog.ui" line="367"/>
        <source>Help (Not yet. just concept)</source>
        <translation>도움말 (아직 아님. 그냥 컨셉)</translation>
    </message>
    <message>
        <location filename="../watchdog.ui" line="394"/>
        <source>%p : pid
%c : command</source>
        <translation>%p : pid
%c : 명령</translation>
    </message>
    <message>
        <location filename="../watchdog.ui" line="425"/>
        <source>New</source>
        <translation>새로 만들기</translation>
    </message>
    <message>
        <location filename="../watchdog.ui" line="432"/>
        <source>Add</source>
        <translation>추가</translation>
    </message>
    <message>
        <location filename="../watchdog.ui" line="439"/>
        <source>Delete</source>
        <translation>삭제</translation>
    </message>
    <message>
        <location filename="../watchdog.ui" line="446"/>
        <source>Close</source>
        <translation>닫기</translation>
    </message>
</context>
<context>
    <name>ExecWindow</name>
    <message>
        <location filename="../message.ui" line="13"/>
        <source>Qps</source>
        <translation>Qps</translation>
    </message>
    <message>
        <location filename="../message.ui" line="27"/>
        <source>Ok</source>
        <translation>확인</translation>
    </message>
    <message>
        <location filename="../execwindow.cpp" line="42"/>
        <source>Qps Watchdog</source>
        <translation>Qps 워치독</translation>
    </message>
    <message>
        <location filename="../execwindow.cpp" line="94"/>
        <source>%1 exit with code %2</source>
        <translation>코드 %2가 있는 %1 종료</translation>
    </message>
    <message>
        <location filename="../execwindow.cpp" line="96"/>
        <source>Close</source>
        <translation>닫기</translation>
    </message>
    <message>
        <location filename="../execwindow.cpp" line="102"/>
        <source>%1 [running]</source>
        <translation>%1 [실행 중]</translation>
    </message>
    <message>
        <location filename="../execwindow.cpp" line="103"/>
        <source>terminate command</source>
        <translation>명령 종료</translation>
    </message>
    <message>
        <location filename="../execwindow.cpp" line="112"/>
        <source>Error %1 : [%2] Maybe command not found</source>
        <translation>오류 %1: [%2] 명령을 찾을 수 없습니다</translation>
    </message>
</context>
<context>
    <name>FieldSelect</name>
    <message>
        <location filename="../fieldsel.cpp" line="34"/>
        <source>Select Custom Fields </source>
        <translation>사용자 지정 필드 선택 </translation>
    </message>
    <message>
        <location filename="../fieldsel.cpp" line="75"/>
        <source>Close</source>
        <translation>닫기</translation>
    </message>
</context>
<context>
    <name>Files</name>
    <message>
        <location filename="../details.cpp" line="521"/>
        <source>Fd</source>
        <translation>Fd</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="521"/>
        <source>File descriptor</source>
        <translation>파일 설명자</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="523"/>
        <source>Mode</source>
        <translation>모드</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="523"/>
        <source>Open mode</source>
        <translation>모드 열기</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="525"/>
        <source>Name</source>
        <translation>이름</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="525"/>
        <source>File name (if available)</source>
        <translation>파일 이름(사용 가능한 경우)</translation>
    </message>
</context>
<context>
    <name>IntervalDialog</name>
    <message>
        <location filename="../dialogs.cpp" line="43"/>
        <source>Change Update Period</source>
        <translation>업데이트 기간 변경</translation>
    </message>
    <message>
        <location filename="../dialogs.cpp" line="49"/>
        <source>New Update Period</source>
        <translation>새 업데이트 기간</translation>
    </message>
    <message>
        <location filename="../dialogs.cpp" line="85"/>
        <source>Cancel</source>
        <translation>취소</translation>
    </message>
    <message>
        <location filename="../dialogs.cpp" line="87"/>
        <source>OK</source>
        <translation>확인</translation>
    </message>
    <message>
        <location filename="../dialogs.cpp" line="121"/>
        <source>No UPDATE</source>
        <translation>업데이트 없음</translation>
    </message>
    <message>
        <location filename="../dialogs.cpp" line="139"/>
        <source>Invalid value</source>
        <translation>잘못된 값</translation>
    </message>
</context>
<context>
    <name>ListModel</name>
    <message>
        <location filename="../listmodel.cpp" line="66"/>
        <source>Event Category</source>
        <translation>이벤트 범주</translation>
    </message>
    <message>
        <location filename="../listmodel.cpp" line="68"/>
        <source>Enable</source>
        <translation>활성화</translation>
    </message>
</context>
<context>
    <name>Maps</name>
    <message>
        <location filename="../details.cpp" line="392"/>
        <source>Address Range</source>
        <translation>주소 범위</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="392"/>
        <source>Mapped addresses (hex) )</source>
        <translation>매핑된 주소 (16진수) )</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="393"/>
        <source>Size</source>
        <translation>크기</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="393"/>
        <source>Kbytes mapped (dec)</source>
        <translation>매핑된 Kb (dec)</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="394"/>
        <source>Perm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../details.cpp" line="394"/>
        <source>Permission flags</source>
        <translation>퍼미션 플래그</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="395"/>
        <source>Offset</source>
        <translation>오프셋</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="395"/>
        <source>File offset at start of mapping (hex)</source>
        <translation>매핑 시작 시 파일 오프셋 (16진수)</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="396"/>
        <source>Device</source>
        <translation>장치</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="396"/>
        <source>Major,Minor device numbers (dec)</source>
        <translation>메이저, 마이너 장치 번호 (dec)</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="397"/>
        <source>Inode</source>
        <translation>아이노드</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="397"/>
        <source>Inode number (dec)</source>
        <translation>아이노드 번호 (dec)</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="398"/>
        <source>File</source>
        <translation>파일</translation>
    </message>
    <message>
        <location filename="../details.cpp" line="398"/>
        <source>File name (if available)</source>
        <translation>파일 이름 (사용 가능한 경우)</translation>
    </message>
</context>
<context>
    <name>PermissionDialog</name>
    <message>
        <location filename="../dialogs.cpp" line="264"/>
        <source>Permission</source>
        <translation>퍼미션</translation>
    </message>
    <message>
        <location filename="../dialogs.cpp" line="273"/>
        <source>Root password</source>
        <translation>루트 비밀번호</translation>
    </message>
    <message>
        <location filename="../dialogs.cpp" line="280"/>
        <source>Cancel</source>
        <translation>취소</translation>
    </message>
    <message>
        <location filename="../dialogs.cpp" line="283"/>
        <source>OK</source>
        <translation>확인</translation>
    </message>
</context>
<context>
    <name>Preferences</name>
    <message>
        <location filename="../prefs.cpp" line="150"/>
        <source>Preferences</source>
        <translation>기본 설정</translation>
    </message>
    <message>
        <location filename="../prefs.cpp" line="157"/>
        <source>Setting</source>
        <translation>설정</translation>
    </message>
    <message>
        <location filename="../prefs.cpp" line="191"/>
        <source>%CPU divided by</source>
        <translation>%CPU로 나눈 값</translation>
    </message>
    <message>
        <location filename="../prefs.cpp" line="198"/>
        <source>Total cpu: %1</source>
        <translation>전체 CPU: %1</translation>
    </message>
    <message>
        <location filename="../prefs.cpp" line="199"/>
        <source>Single cpu: 1</source>
        <translation>단일 CPU: 1</translation>
    </message>
    <message>
        <location filename="../prefs.cpp" line="205"/>
        <source>default</source>
        <translation>기본값</translation>
    </message>
    <message>
        <location filename="../prefs.cpp" line="206"/>
        <source>for developer</source>
        <translation>개발자용</translation>
    </message>
    <message>
        <location filename="../prefs.cpp" line="240"/>
        <source>Appearance</source>
        <translation>모양새</translation>
    </message>
</context>
<context>
    <name>Proc</name>
    <message>
        <location filename="../proc.cpp" line="2876"/>
        <source>PID</source>
        <translation>PID</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2876"/>
        <source>Process ID</source>
        <translation>프로세스 ID</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2878"/>
        <source>TGID</source>
        <translation>TGID</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2878"/>
        <source>Task group ID ( parent of threads )</source>
        <translation>작업 그룹 ID ( 스레드의 상위 )</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2880"/>
        <source>PPID</source>
        <translation>PPID</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2880"/>
        <source>Parent process ID</source>
        <translation>상위 프로세스 ID</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2882"/>
        <source>PGID</source>
        <translation>PGID</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2882"/>
        <source>Process group ID</source>
        <translation>프로세스 그룹 ID</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2884"/>
        <source>SID</source>
        <translation>SID</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2884"/>
        <source>Session ID</source>
        <translation>세션 ID</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2886"/>
        <source>TTY</source>
        <translation>TTY</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2886"/>
        <source>Terminal</source>
        <translation>터미널</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2888"/>
        <source>TPGID</source>
        <translation>TPGID</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2888"/>
        <source>Process group ID of tty owner</source>
        <translation>tty 소유자의 프로세스 그룹 ID</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2890"/>
        <source>USER</source>
        <translation>사용자</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2890"/>
        <source>Owner (*=suid root, +=suid a user)</source>
        <translation>소유자 (*=suid 루트, +=suid 사용자)</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2892"/>
        <source>GROUP</source>
        <translation>그룹</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2892"/>
        <source>Group name (*=sgid other)</source>
        <translation>그룹 이름 (*=sgid 기타)</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2894"/>
        <source>UID</source>
        <translation>UID</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2894"/>
        <source>Real user ID</source>
        <translation>실제 사용자 ID</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2896"/>
        <source>EUID</source>
        <translation>EUID</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2896"/>
        <source>Effective user ID</source>
        <translation>유효 사용자 ID</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2898"/>
        <source>SUID</source>
        <translation>SUID</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2898"/>
        <source>Saved user ID (Posix)</source>
        <translation>저장된 사용자 ID (Posix)</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2900"/>
        <source>FSUID</source>
        <translation>FSUID</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2900"/>
        <source>File system user ID</source>
        <translation>파일 시스템 사용자 ID</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2902"/>
        <source>GID</source>
        <translation>GID</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2902"/>
        <source>Real group ID</source>
        <translation>실제 그룹 ID</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2904"/>
        <source>EGID</source>
        <translation>EGID</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2904"/>
        <source>Effective group ID</source>
        <translation>유효 그룹 ID</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2906"/>
        <source>SGID</source>
        <translation>SGID</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2906"/>
        <source>Saved group ID (Posix)</source>
        <translation>저장된 그룹 ID (Posix)</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2908"/>
        <source>FSGID</source>
        <translation>FSGID</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2908"/>
        <source>File system group ID</source>
        <translation>파일 시스템 그룹 ID</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2910"/>
        <source>PRI</source>
        <translation>PRI</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2910"/>
        <source>Dynamic priority</source>
        <translation>동적 우선순위</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2912"/>
        <source>NICE</source>
        <translation>NICE</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2912"/>
        <source>Scheduling favour (higher -&gt; less cpu time)</source>
        <translation>스케줄링 선호 (높은 CPU 시간 -&gt; 적은 CPU 시간)</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2914"/>
        <source>NLWP</source>
        <translation>NLWP</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2914"/>
        <source>Number of tasks(threads) in task group</source>
        <translation>작업 그룹의 작업(스레드) 수</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2916"/>
        <source>PLCY</source>
        <translation>PLCY</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2916"/>
        <source>Scheduling policy</source>
        <translation>스케줄링 정책</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2918"/>
        <source>RPRI</source>
        <translation>RPRI</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2918"/>
        <source>Realtime priority (0-99, more is better)</source>
        <translation>실시간 우선순위 (0-99, 많을수록 좋음)</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2920"/>
        <source>TMS</source>
        <translation>TMS</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2920"/>
        <source>Time slice in milliseconds</source>
        <translation>밀리초 단위로 시간 슬라이스</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2922"/>
        <source>%SAVG</source>
        <translation>%SAVG</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2922"/>
        <source>Percentage average sleep time (-1 -&gt; N/A)</source>
        <translation>평균 유휴 시간 비율 (-1 -&gt; 없음)</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2924"/>
        <source>CPUSET</source>
        <translation>CPUSET</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2924"/>
        <source>Affinity CPU mask (0 -&gt; API not supported)</source>
        <translation>선호도 CPU 마스크 (0 -&gt; API가 지원되지 않음)</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2926"/>
        <source>MAJFLT</source>
        <translation>MAJFLT</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2926"/>
        <source>Number of major faults (disk access)</source>
        <translation>주요 오류 수 (디스크 액세스)</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2928"/>
        <source>MINFLT</source>
        <translation>MINFLT</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2928"/>
        <source>Number of minor faults (no disk access)</source>
        <translation>사소한 오류 수 (디스크 액세스 없음)</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2931"/>
        <source>VSIZE</source>
        <translation>VSIZE</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2931"/>
        <source>Virtual image size of process</source>
        <translation>프로세스의 가상 이미지 크기</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2933"/>
        <source>RSS</source>
        <translation>RSS</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2933"/>
        <source>Resident set size</source>
        <translation>레지던트 세트 크기</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2935"/>
        <source>MEM</source>
        <translation>MEM</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2935"/>
        <source>memory usage (RSS-SHARE)</source>
        <translation>메모리 사용량 (RSS-SHARE)</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2937"/>
        <source>TRS</source>
        <translation>TRS</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2937"/>
        <source>Text(code) resident set size</source>
        <translation>텍스트(코드) 상주 세트 크기</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2939"/>
        <source>DRS</source>
        <translation>DRS</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2939"/>
        <source>Data resident set size(malloc+global variable)</source>
        <translation>데이터 상주 세트 크기(malloc+전역 변수)</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2941"/>
        <source>STACK</source>
        <translation>STACK</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2941"/>
        <source>Stack size</source>
        <translation>스택 크기</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2943"/>
        <source>SHARE</source>
        <translation>공유</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2943"/>
        <source>Shared memory with other libs</source>
        <translation>다른 라이브러리와 메모리 공유됨</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2945"/>
        <source>SWAP</source>
        <translation>스왑</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2945"/>
        <source>Kbytes on swap device</source>
        <translation>스왑 장치의 Kb</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2947"/>
        <source>IO_R</source>
        <translation>IO_R</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2947"/>
        <source>io read (file)</source>
        <translation>IO 읽기 (파일)</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2949"/>
        <source>IO_W</source>
        <translation>IO_W</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2949"/>
        <source>io write (file)</source>
        <translation>IO 쓰기 (파일)</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2951"/>
        <source>DT</source>
        <translation>DT</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2951"/>
        <source>Number of dirty (non-written) pages</source>
        <translation>더티(기록되지 않은) 페이지 수</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2953"/>
        <source>STAT</source>
        <translation>상태</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2953"/>
        <source>State of the process </source>
        <translation>프로세스 상태 </translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2955"/>
        <source>FLAGS</source>
        <translation>플래그</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2955"/>
        <source>Process flags (hex)</source>
        <translation>프로세스 플래그 (16진수)</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2957"/>
        <source>WCHAN</source>
        <translation>WCHAN</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2957"/>
        <source>Kernel function where process is sleeping</source>
        <translation>프로세스가 유휴 상태인 커널 함수</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2959"/>
        <source>%WCPU</source>
        <translation>%WCPU</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2959"/>
        <source>Weighted percentage of CPU (30 s average)</source>
        <translation>CPU의 가중치 비율 (30초 평균)</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2961"/>
        <source>%CPU</source>
        <translation>%CPU</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2961"/>
        <source>Percentage of CPU used since last update</source>
        <translation>마지막 업데이트 이후 사용된 CPU 비율</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2963"/>
        <source>%MEM</source>
        <translation>%MEM</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2963"/>
        <source>Percentage of memory used (RSS/total mem)</source>
        <translation>사용된 메모리 비율 (RSS/총 메모리)</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2965"/>
        <source>START</source>
        <translation>시작</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2965"/>
        <source>Time process started</source>
        <translation>시간 프로세스가 시작되었습니다</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2967"/>
        <source>TIME</source>
        <translation>시간</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2967"/>
        <source>Total CPU time used since start</source>
        <translation>시작 이후 사용한 총 CPU 시간</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2969"/>
        <source>CPU</source>
        <translation>CPU</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2969"/>
        <source>CPU the process is executing on (SMP system)</source>
        <translation>프로세스가 실행 중인 CPU (SMP 시스템)</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2971"/>
        <source>the process name</source>
        <translation>프로세스 이름</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2971"/>
        <source>Process Name</source>
        <translation>프로세스 이름</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2975"/>
        <source>CWD</source>
        <translation>CWD</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2975"/>
        <source>Current working directory</source>
        <translation>현재 작업 디렉터리</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2976"/>
        <source>ROOT</source>
        <translation>루트</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2976"/>
        <source>Root directory of process</source>
        <translation>프로세스의 루트 디렉토리</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2979"/>
        <source>COMMAND_LINE</source>
        <translation>명령_줄</translation>
    </message>
    <message>
        <location filename="../proc.cpp" line="2979"/>
        <source>Command line that started the process</source>
        <translation>프로세스를 시작한 명령줄</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../qps.cpp" line="1846"/>
        <source>Show</source>
        <translation>표시</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="1849"/>
        <source>Hide</source>
        <translation>숨김</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="1853"/>
        <source>Quit</source>
        <translation>종료</translation>
    </message>
</context>
<context>
    <name>Qps</name>
    <message>
        <location filename="../qps.cpp" line="198"/>
        <source>header_popup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="199"/>
        <source>Remove Field</source>
        <translation>필드 제거</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="200"/>
        <source>Add Field</source>
        <translation>필드 추가</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="207"/>
        <source>View</source>
        <translation>보기</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="208"/>
        <source>Process</source>
        <translation>프로세스</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="209"/>
        <source>Log</source>
        <translation>로그</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="214"/>
        <source>Fields</source>
        <translation>필드</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="215"/>
        <source>Custom Fields</source>
        <translation>사용자 지정 필드</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="217"/>
        <source>Basic Fields </source>
        <translation>기본 필드 </translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="219"/>
        <source>Jobs Fields </source>
        <translation>작업 필드 </translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="221"/>
        <source>Memory Fields </source>
        <translation>메모리 필드 </translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="224"/>
        <source>Scheduling Fields </source>
        <translation>스케줄링 필드 </translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="241"/>
        <source>Select Custom Fields...</source>
        <translation>사용자 지정 필드 선택...</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="203"/>
        <source>Command</source>
        <translation>명령</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="255"/>
        <source>Options</source>
        <translation>옵션</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="256"/>
        <source>Update Period...</source>
        <translation>업데이트 기간...</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="265"/>
        <source>Show Status bar</source>
        <translation>상태 표시줄 표시</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="274"/>
        <source>Preferences...</source>
        <translation>기본 설정...</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="280"/>
        <source>Help</source>
        <translation>도움말</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="282"/>
        <location filename="../qps.cpp" line="1894"/>
        <source>About</source>
        <translation>정보</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="370"/>
        <source>Detail</source>
        <translation>세부 사항</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="411"/>
        <source>test</source>
        <translation>테스트</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="412"/>
        <source>Copied to Clipboard</source>
        <translation>클립보드에 복사됨</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="464"/>
        <source>context popup</source>
        <translation>컨텍스트 팝업</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="465"/>
        <source>Renice...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="466"/>
        <source>Scheduling...</source>
        <translation>스케줄링...</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="468"/>
        <source>Terminate</source>
        <translation>종결</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="470"/>
        <source>Hangup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="471"/>
        <source>Kill</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="472"/>
        <source>Stop</source>
        <translation>중지</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="474"/>
        <source>Continue</source>
        <translation>계속하기</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="478"/>
        <source>SIGINT (interrupt)</source>
        <translation>SIGINT (개입중단)</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="480"/>
        <source>SIGCONT (continue)</source>
        <translation>SIGCONT (계속하기)</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="482"/>
        <source>SIGSTOP (stop)</source>
        <translation>SIGSTOP (중지)</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="484"/>
        <source>SIGQUIT (quit)</source>
        <translation>SIGQUIT (종료)</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="486"/>
        <source>SIGILL (illegal instruction)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="488"/>
        <source>SIGABRT (abort)</source>
        <translation>SIGABRT (중단)</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="490"/>
        <source>SIGFPE (floating point exception)</source>
        <translation>SIGFPE (부동 소수점 예외)</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="492"/>
        <source>SIGSEGV (segmentation violation)</source>
        <translation>SIGSEGV (세분화 위반)</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="494"/>
        <source>SIGPIPE (broken pipe)</source>
        <translation>SIGPIPE (깨진 파이프)</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="496"/>
        <source>SIGALRM (timer signal)</source>
        <translation>SIGALRM (타이머 신호)</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="498"/>
        <source>SIGUSR1 (user-defined 1)</source>
        <translation>SIGUSR1 (사용자정의됨 1)</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="500"/>
        <source>SIGUSR2 (user-defined 2)</source>
        <translation>SIGUSR2 (사용자정의됨 2)</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="502"/>
        <source>SIGCHLD (child death)</source>
        <translation>SIGCHLD (하위 죽음)</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="504"/>
        <source>SIGTSTP (stop from tty)</source>
        <translation>SIGTSTP (tty에서 중지)</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="506"/>
        <source>SIGTTIN (tty input)</source>
        <translation>SIGTTIN (tty 입력)</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="508"/>
        <source>SIGTTOU (tty output)</source>
        <translation>SIGTTOU (tty 출력)</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="515"/>
        <source>View Details</source>
        <translation>세부 사항 보기</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="781"/>
        <source>Show File Path</source>
        <translation>파일 경로 표시</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="787"/>
        <source>Show Graph</source>
        <translation>그래프 표시</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="804"/>
        <source>Include Child Times</source>
        <translation>하위 시간 포함</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="998"/>
        <source>WatchDog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="1000"/>
        <source>Edit Commands...</source>
        <translation>명령 편집...</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="1017"/>
        <source>Quit</source>
        <translation>종료</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="1336"/>
        <location filename="../qps.cpp" line="1367"/>
        <location filename="../qps.cpp" line="1374"/>
        <location filename="../qps.cpp" line="1395"/>
        <location filename="../qps.cpp" line="1429"/>
        <location filename="../qps.cpp" line="1524"/>
        <source>Permission denied</source>
        <translation>사용 권한 거부됨</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="1337"/>
        <source>You do not have permission to renice the selected processes. Only the process owner and the super-user are allowed to do that.</source>
        <translation>선택한 프로세스를 다시 수행할 수 있는 권한이 없습니다. 프로세스 소유자와 슈퍼 유저만이 그렇게 할 수 있습니다.</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="1368"/>
        <source>You do not have permission to renice process %1 (%2).Only the process owner and the super-user are allowed to do that.</source>
        <translation>%1 (%2) 프로세스를 다시 시작할 수 있는 권한이 없습니다.프로세스 소유자와 슈퍼 유저만이 그렇게 할 수 있습니다.</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="1375"/>
        <source>Only the super-user may lower the nice value of a process.</source>
        <translation>슈퍼 유저만이 프로세스의 nice 값을 낮출 수 있습니다.</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="1396"/>
        <source>Only the super-user may change the scheduling policy and static priority.</source>
        <translation>슈퍼 유저만이 스케줄링 정책과 정적 우선순위를 변경할 수 있습니다.</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="1430"/>
        <source>You do not have permission to change the scheduling and/or priority of process %1 (%2). Only the super-user may do that.</source>
        <translation>%1 (%2) 프로세스의 스케줄링 및/또는 우선 순위를 변경할 권한이 없습니다. 그것은 슈퍼 유저만이 할 수 있습니다.</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="1491"/>
        <source>Do you really want to terminate the selected process(es)?</source>
        <translation>선택한 프로세스를 종료하시겠습니까?</translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="1493"/>
        <source>Do you really want to hang up the selected process(es)?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="1495"/>
        <source>Do you really want to kill the selected process(es)?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="1504"/>
        <source>Question</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="1525"/>
        <source>You do not have permission to send a signal to process %1 (%2). Only the super-user and the owner of the process may send signals to it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="1910"/>
        <source>&lt;center&gt;&lt;h2&gt; Qps %1&lt;/center&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="1912"/>
        <source>&lt;h2&gt; Qps %1 - A Visual Process Manager &lt;/h2&gt; %2 using Qt library %3&lt;br&gt;&lt;br&gt;&lt;b&gt;Source: &lt;/b&gt;&lt;a href=&quot;https://github.com/lxqt/qps&quot;&gt;https://github.com/lxqt/qps/&lt;/a&gt;&lt;br&gt;&lt;b&gt;Bugtracker: &lt;/b&gt;&lt;a href=&quot;https://github.com/lxqt/qps/issues&quot;&gt;https://github.com/lxqt/qps/issues&lt;/a&gt;&lt;br&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="1932"/>
        <source>&lt;b&gt;Original Qps by&lt;/b&gt;&lt;br&gt;Mattias Engdegård (f91-men@nada.kth.se)&lt;br&gt;&lt;br&gt;&lt;b&gt;Contributors&lt;/b&gt;&lt;br&gt;Olivier.Daudel@u-paris10.fr&lt;br&gt;jsanchez@todounix.homeip.net &lt;br&gt;daehyun.yang@gmail.com &lt;br&gt;Luís Pereira (luis.artur.pereira@gmail.com)&lt;br&gt;Alf Gaida (agaida@siduction.org)&lt;br&gt;Paulo Lieuthier (paulolieuthier@gmail.com)&lt;br&gt;Jerome Leclanche (jerome@leclan.ch)&lt;br&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qps.cpp" line="1970"/>
        <source>&lt;H1&gt;QPS Help&lt;/H1&gt;Updated: May 24 2005&lt;BR&gt;&lt;A HREF=&quot;http://kldp.net/projects/qps&quot;&gt;http://kldp.net/projects/qps&lt;/A&gt;&lt;HR&gt;&lt;table style=&quot;text-align: center; width: 100%;&quot; border=&quot;1&quot; cellpadding=&quot;1&quot; cellspacing=&quot;0&quot;&gt;  &lt;tbody&gt;    &lt;tr&gt;      &lt;td style=&quot;vertical-align: top; background-color: rgb(204, 204, 204);&quot;&gt;Quit      &lt;/td&gt;      &lt;td &gt;&amp;nbsp; CTRL + q , CTRL + x      &lt;/td&gt;    &lt;/tr&gt;    &lt;tr&gt;      &lt;td style=&quot;vertical-align: top; background-color: rgb(204, 204, 204);&quot;&gt;Update      &lt;/td&gt;      &lt;td&gt;&amp;nbsp;Space , Enter       &lt;/td&gt;    &lt;/tr&gt;    &lt;tr&gt;&lt;td&gt; process Terminate &lt;/td&gt;  &lt;td&gt; ALT + T , DELETE &lt;/td&gt; &lt;/tr&gt;    &lt;tr&gt;&lt;td&gt; process Kill &lt;/td&gt;  &lt;td&gt; ALT + K  &lt;/td&gt; &lt;/tr&gt;  &lt;/tbody&gt;&lt;/table&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SchedDialog</name>
    <message>
        <location filename="../dialogs.cpp" line="292"/>
        <source>Change scheduling</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs.cpp" line="296"/>
        <source>Scheduling Policy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs.cpp" line="298"/>
        <source>SCHED_OTHER (time-sharing)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs.cpp" line="299"/>
        <source>SCHED_FIFO (real-time)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs.cpp" line="300"/>
        <source>SCHED_RR (real-time)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs.cpp" line="314"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs.cpp" line="316"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs.cpp" line="346"/>
        <source>Priority (1-99):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs.cpp" line="379"/>
        <source>Invalid Input</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs.cpp" line="380"/>
        <source>The priority must be in the range 1..99</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SearchBox</name>
    <message>
        <location filename="../misc.cpp" line="765"/>
        <source>PID, COMMAND, USER...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../misc.cpp" line="766"/>
        <source>Filter</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SliderDialog</name>
    <message>
        <location filename="../dialogs.cpp" line="179"/>
        <source>Renice Process</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs.cpp" line="185"/>
        <source>New nice value:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs.cpp" line="225"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs.cpp" line="229"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Sockets</name>
    <message>
        <location filename="../details.cpp" line="147"/>
        <source>Fd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../details.cpp" line="147"/>
        <source>File descriptor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../details.cpp" line="148"/>
        <source>Proto</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../details.cpp" line="148"/>
        <source>Protocol (TCP or UDP)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../details.cpp" line="149"/>
        <source>Recv-Q</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../details.cpp" line="149"/>
        <source>Bytes in receive queue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../details.cpp" line="150"/>
        <source>Send-Q</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../details.cpp" line="150"/>
        <source>Bytes in send queue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../details.cpp" line="151"/>
        <source>Local Addr</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../details.cpp" line="151"/>
        <source>Local IP address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../details.cpp" line="152"/>
        <location filename="../details.cpp" line="154"/>
        <source>Port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../details.cpp" line="152"/>
        <source>Local port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../details.cpp" line="153"/>
        <source>Remote Addr</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../details.cpp" line="153"/>
        <source>Remote IP address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../details.cpp" line="154"/>
        <source>Remote port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../details.cpp" line="155"/>
        <source>State</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../details.cpp" line="155"/>
        <source>Connection state</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StatusBar</name>
    <message>
        <location filename="../misc.cpp" line="807"/>
        <source>Process count: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TBloon</name>
    <message>
        <location filename="../misc.cpp" line="347"/>
        <source> This is unstable Alpha feature
 You maybe see a SEGFAULT...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TFrame</name>
    <message>
        <location filename="../misc.cpp" line="426"/>
        <source>this is Tframe widget</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UFrame</name>
    <message>
        <location filename="../misc.cpp" line="615"/>
        <source>title</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WatchdogDialog</name>
    <message>
        <location filename="../watchdogdialog.cpp" line="193"/>
        <source>if process start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../watchdogdialog.cpp" line="194"/>
        <source>if process finish</source>
        <translation>프로세스가 끝나면</translation>
    </message>
    <message>
        <location filename="../watchdogdialog.cpp" line="217"/>
        <source>select condition</source>
        <translation>조건 선택</translation>
    </message>
</context>
</TS>
