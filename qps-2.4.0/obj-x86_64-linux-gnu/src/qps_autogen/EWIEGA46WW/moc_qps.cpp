/****************************************************************************
** Meta object code from reading C++ file 'qps.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.15.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../../../src/qps.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qps.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.15.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Qps_t {
    QByteArrayData data[56];
    char stringdata0[670];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Qps_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Qps_t qt_meta_stringdata_Qps = {
    {
QT_MOC_LITERAL(0, 0, 3), // "Qps"
QT_MOC_LITERAL(1, 4, 16), // "clicked_trayicon"
QT_MOC_LITERAL(2, 21, 0), // ""
QT_MOC_LITERAL(3, 22, 33), // "QSystemTrayIcon::ActivationRe..."
QT_MOC_LITERAL(4, 56, 8), // "sig_term"
QT_MOC_LITERAL(5, 65, 7), // "sig_hup"
QT_MOC_LITERAL(6, 73, 8), // "sig_stop"
QT_MOC_LITERAL(7, 82, 8), // "sig_cont"
QT_MOC_LITERAL(8, 91, 8), // "sig_kill"
QT_MOC_LITERAL(9, 100, 11), // "signal_menu"
QT_MOC_LITERAL(10, 112, 8), // "QAction*"
QT_MOC_LITERAL(11, 121, 11), // "run_command"
QT_MOC_LITERAL(12, 133, 5), // "about"
QT_MOC_LITERAL(13, 139, 7), // "license"
QT_MOC_LITERAL(14, 147, 11), // "menu_update"
QT_MOC_LITERAL(15, 159, 16), // "menu_toggle_path"
QT_MOC_LITERAL(16, 176, 19), // "menu_toggle_infobar"
QT_MOC_LITERAL(17, 196, 19), // "menu_toggle_ctrlbar"
QT_MOC_LITERAL(18, 216, 21), // "menu_toggle_statusbar"
QT_MOC_LITERAL(19, 238, 17), // "menu_toggle_cumul"
QT_MOC_LITERAL(20, 256, 10), // "menu_prefs"
QT_MOC_LITERAL(21, 267, 11), // "menu_renice"
QT_MOC_LITERAL(22, 279, 10), // "menu_sched"
QT_MOC_LITERAL(23, 290, 13), // "Action_Detail"
QT_MOC_LITERAL(24, 304, 11), // "menu_parent"
QT_MOC_LITERAL(25, 316, 13), // "menu_children"
QT_MOC_LITERAL(26, 330, 12), // "menu_dynasty"
QT_MOC_LITERAL(27, 343, 11), // "menu_custom"
QT_MOC_LITERAL(28, 355, 17), // "menu_remove_field"
QT_MOC_LITERAL(29, 373, 13), // "menu_edit_cmd"
QT_MOC_LITERAL(30, 387, 8), // "mig_menu"
QT_MOC_LITERAL(31, 396, 2), // "id"
QT_MOC_LITERAL(32, 399, 17), // "make_command_menu"
QT_MOC_LITERAL(33, 417, 9), // "view_menu"
QT_MOC_LITERAL(34, 427, 9), // "save_quit"
QT_MOC_LITERAL(35, 437, 15), // "add_fields_menu"
QT_MOC_LITERAL(36, 453, 3), // "act"
QT_MOC_LITERAL(37, 457, 15), // "show_popup_menu"
QT_MOC_LITERAL(38, 473, 1), // "p"
QT_MOC_LITERAL(39, 475, 20), // "context_heading_menu"
QT_MOC_LITERAL(40, 496, 3), // "col"
QT_MOC_LITERAL(41, 500, 11), // "field_added"
QT_MOC_LITERAL(42, 512, 5), // "index"
QT_MOC_LITERAL(43, 518, 15), // "fromContextMenu"
QT_MOC_LITERAL(44, 534, 13), // "field_removed"
QT_MOC_LITERAL(45, 548, 14), // "set_table_mode"
QT_MOC_LITERAL(46, 563, 8), // "treemode"
QT_MOC_LITERAL(47, 572, 12), // "open_details"
QT_MOC_LITERAL(48, 585, 3), // "row"
QT_MOC_LITERAL(49, 589, 13), // "config_change"
QT_MOC_LITERAL(50, 603, 12), // "update_timer"
QT_MOC_LITERAL(51, 616, 7), // "refresh"
QT_MOC_LITERAL(52, 624, 10), // "showWindow"
QT_MOC_LITERAL(53, 635, 10), // "test_popup"
QT_MOC_LITERAL(54, 646, 4), // "link"
QT_MOC_LITERAL(55, 651, 18) // "update_menu_status"

    },
    "Qps\0clicked_trayicon\0\0"
    "QSystemTrayIcon::ActivationReason\0"
    "sig_term\0sig_hup\0sig_stop\0sig_cont\0"
    "sig_kill\0signal_menu\0QAction*\0run_command\0"
    "about\0license\0menu_update\0menu_toggle_path\0"
    "menu_toggle_infobar\0menu_toggle_ctrlbar\0"
    "menu_toggle_statusbar\0menu_toggle_cumul\0"
    "menu_prefs\0menu_renice\0menu_sched\0"
    "Action_Detail\0menu_parent\0menu_children\0"
    "menu_dynasty\0menu_custom\0menu_remove_field\0"
    "menu_edit_cmd\0mig_menu\0id\0make_command_menu\0"
    "view_menu\0save_quit\0add_fields_menu\0"
    "act\0show_popup_menu\0p\0context_heading_menu\0"
    "col\0field_added\0index\0fromContextMenu\0"
    "field_removed\0set_table_mode\0treemode\0"
    "open_details\0row\0config_change\0"
    "update_timer\0refresh\0showWindow\0"
    "test_popup\0link\0update_menu_status"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Qps[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      44,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,  234,    2, 0x0a /* Public */,
       4,    0,  237,    2, 0x0a /* Public */,
       5,    0,  238,    2, 0x0a /* Public */,
       6,    0,  239,    2, 0x0a /* Public */,
       7,    0,  240,    2, 0x0a /* Public */,
       8,    0,  241,    2, 0x0a /* Public */,
       9,    1,  242,    2, 0x0a /* Public */,
      11,    1,  245,    2, 0x0a /* Public */,
      12,    0,  248,    2, 0x0a /* Public */,
      13,    0,  249,    2, 0x0a /* Public */,
      14,    0,  250,    2, 0x0a /* Public */,
      15,    0,  251,    2, 0x0a /* Public */,
      16,    0,  252,    2, 0x0a /* Public */,
      17,    0,  253,    2, 0x0a /* Public */,
      18,    0,  254,    2, 0x0a /* Public */,
      19,    0,  255,    2, 0x0a /* Public */,
      20,    0,  256,    2, 0x0a /* Public */,
      21,    0,  257,    2, 0x0a /* Public */,
      22,    0,  258,    2, 0x0a /* Public */,
      23,    0,  259,    2, 0x0a /* Public */,
      24,    0,  260,    2, 0x0a /* Public */,
      25,    0,  261,    2, 0x0a /* Public */,
      26,    0,  262,    2, 0x0a /* Public */,
      27,    0,  263,    2, 0x0a /* Public */,
      28,    0,  264,    2, 0x0a /* Public */,
      29,    0,  265,    2, 0x0a /* Public */,
      30,    1,  266,    2, 0x0a /* Public */,
      32,    0,  269,    2, 0x0a /* Public */,
      33,    1,  270,    2, 0x0a /* Public */,
      34,    0,  273,    2, 0x0a /* Public */,
      35,    1,  274,    2, 0x0a /* Public */,
      37,    1,  277,    2, 0x0a /* Public */,
      39,    2,  280,    2, 0x0a /* Public */,
      41,    2,  285,    2, 0x0a /* Public */,
      41,    1,  290,    2, 0x2a /* Public | MethodCloned */,
      44,    1,  293,    2, 0x0a /* Public */,
      45,    1,  296,    2, 0x0a /* Public */,
      47,    1,  299,    2, 0x0a /* Public */,
      49,    0,  302,    2, 0x0a /* Public */,
      50,    0,  303,    2, 0x0a /* Public */,
      51,    0,  304,    2, 0x0a /* Public */,
      52,    0,  305,    2, 0x0a /* Public */,
      53,    1,  306,    2, 0x0a /* Public */,
      55,    0,  309,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 10,    2,
    QMetaType::Void, 0x80000000 | 10,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   31,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 10,    2,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 10,   36,
    QMetaType::Void, QMetaType::QPoint,   38,
    QMetaType::Void, QMetaType::QPoint, QMetaType::Int,   38,   40,
    QMetaType::Void, QMetaType::Int, QMetaType::Bool,   42,   43,
    QMetaType::Void, QMetaType::Int,   42,
    QMetaType::Void, QMetaType::Int,   42,
    QMetaType::Void, QMetaType::Bool,   46,
    QMetaType::Void, QMetaType::Int,   48,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QUrl,   54,
    QMetaType::Void,

       0        // eod
};

void Qps::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<Qps *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->clicked_trayicon((*reinterpret_cast< QSystemTrayIcon::ActivationReason(*)>(_a[1]))); break;
        case 1: _t->sig_term(); break;
        case 2: _t->sig_hup(); break;
        case 3: _t->sig_stop(); break;
        case 4: _t->sig_cont(); break;
        case 5: _t->sig_kill(); break;
        case 6: _t->signal_menu((*reinterpret_cast< QAction*(*)>(_a[1]))); break;
        case 7: _t->run_command((*reinterpret_cast< QAction*(*)>(_a[1]))); break;
        case 8: _t->about(); break;
        case 9: _t->license(); break;
        case 10: _t->menu_update(); break;
        case 11: _t->menu_toggle_path(); break;
        case 12: _t->menu_toggle_infobar(); break;
        case 13: _t->menu_toggle_ctrlbar(); break;
        case 14: _t->menu_toggle_statusbar(); break;
        case 15: _t->menu_toggle_cumul(); break;
        case 16: _t->menu_prefs(); break;
        case 17: _t->menu_renice(); break;
        case 18: _t->menu_sched(); break;
        case 19: _t->Action_Detail(); break;
        case 20: _t->menu_parent(); break;
        case 21: _t->menu_children(); break;
        case 22: _t->menu_dynasty(); break;
        case 23: _t->menu_custom(); break;
        case 24: _t->menu_remove_field(); break;
        case 25: _t->menu_edit_cmd(); break;
        case 26: _t->mig_menu((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 27: _t->make_command_menu(); break;
        case 28: _t->view_menu((*reinterpret_cast< QAction*(*)>(_a[1]))); break;
        case 29: _t->save_quit(); break;
        case 30: _t->add_fields_menu((*reinterpret_cast< QAction*(*)>(_a[1]))); break;
        case 31: _t->show_popup_menu((*reinterpret_cast< QPoint(*)>(_a[1]))); break;
        case 32: _t->context_heading_menu((*reinterpret_cast< QPoint(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 33: _t->field_added((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        case 34: _t->field_added((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 35: _t->field_removed((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 36: _t->set_table_mode((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 37: _t->open_details((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 38: _t->config_change(); break;
        case 39: _t->update_timer(); break;
        case 40: _t->refresh(); break;
        case 41: _t->showWindow(); break;
        case 42: _t->test_popup((*reinterpret_cast< const QUrl(*)>(_a[1]))); break;
        case 43: _t->update_menu_status(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 6:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QAction* >(); break;
            }
            break;
        case 7:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QAction* >(); break;
            }
            break;
        case 28:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QAction* >(); break;
            }
            break;
        case 30:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QAction* >(); break;
            }
            break;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject Qps::staticMetaObject = { {
    QMetaObject::SuperData::link<QWidget::staticMetaObject>(),
    qt_meta_stringdata_Qps.data,
    qt_meta_data_Qps,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Qps::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Qps::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Qps.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int Qps::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 44)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 44;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 44)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 44;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
