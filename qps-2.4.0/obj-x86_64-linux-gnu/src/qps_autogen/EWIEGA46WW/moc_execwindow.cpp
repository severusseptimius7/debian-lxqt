/****************************************************************************
** Meta object code from reading C++ file 'execwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.15.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../../../src/execwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'execwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.15.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_ExecWindow_t {
    QByteArrayData data[11];
    char stringdata0[124];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ExecWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ExecWindow_t qt_meta_stringdata_ExecWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "ExecWindow"
QT_MOC_LITERAL(1, 11, 11), // "cmd_started"
QT_MOC_LITERAL(2, 23, 0), // ""
QT_MOC_LITERAL(3, 24, 12), // "cmd_finished"
QT_MOC_LITERAL(4, 37, 8), // "exitCode"
QT_MOC_LITERAL(5, 46, 20), // "QProcess::ExitStatus"
QT_MOC_LITERAL(6, 67, 10), // "exitStatus"
QT_MOC_LITERAL(7, 78, 9), // "cmd_error"
QT_MOC_LITERAL(8, 88, 22), // "QProcess::ProcessError"
QT_MOC_LITERAL(9, 111, 5), // "error"
QT_MOC_LITERAL(10, 117, 6) // "cmd_ok"

    },
    "ExecWindow\0cmd_started\0\0cmd_finished\0"
    "exitCode\0QProcess::ExitStatus\0exitStatus\0"
    "cmd_error\0QProcess::ProcessError\0error\0"
    "cmd_ok"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ExecWindow[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   34,    2, 0x09 /* Protected */,
       3,    2,   35,    2, 0x09 /* Protected */,
       7,    1,   40,    2, 0x09 /* Protected */,
      10,    0,   43,    2, 0x09 /* Protected */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int, 0x80000000 | 5,    4,    6,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void,

       0        // eod
};

void ExecWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<ExecWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->cmd_started(); break;
        case 1: _t->cmd_finished((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QProcess::ExitStatus(*)>(_a[2]))); break;
        case 2: _t->cmd_error((*reinterpret_cast< QProcess::ProcessError(*)>(_a[1]))); break;
        case 3: _t->cmd_ok(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject ExecWindow::staticMetaObject = { {
    QMetaObject::SuperData::link<QWidget::staticMetaObject>(),
    qt_meta_stringdata_ExecWindow.data,
    qt_meta_data_ExecWindow,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *ExecWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ExecWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_ExecWindow.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int ExecWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 4)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 4;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
