/****************************************************************************
** Meta object code from reading C++ file 'watchdogdialog.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.15.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../../../src/watchdogdialog.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'watchdogdialog.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.15.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_WatchdogDialog_t {
    QByteArrayData data[13];
    char stringdata0[105];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_WatchdogDialog_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_WatchdogDialog_t qt_meta_stringdata_WatchdogDialog = {
    {
QT_MOC_LITERAL(0, 0, 14), // "WatchdogDialog"
QT_MOC_LITERAL(1, 15, 4), // "_new"
QT_MOC_LITERAL(2, 20, 0), // ""
QT_MOC_LITERAL(3, 21, 5), // "apply"
QT_MOC_LITERAL(4, 27, 3), // "add"
QT_MOC_LITERAL(5, 31, 3), // "del"
QT_MOC_LITERAL(6, 35, 11), // "condChanged"
QT_MOC_LITERAL(7, 47, 3), // "str"
QT_MOC_LITERAL(8, 51, 7), // "Changed"
QT_MOC_LITERAL(9, 59, 12), // "comboChanged"
QT_MOC_LITERAL(10, 72, 16), // "eventcat_slected"
QT_MOC_LITERAL(11, 89, 11), // "QModelIndex"
QT_MOC_LITERAL(12, 101, 3) // "idx"

    },
    "WatchdogDialog\0_new\0\0apply\0add\0del\0"
    "condChanged\0str\0Changed\0comboChanged\0"
    "eventcat_slected\0QModelIndex\0idx"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_WatchdogDialog[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   54,    2, 0x09 /* Protected */,
       3,    0,   55,    2, 0x09 /* Protected */,
       4,    0,   56,    2, 0x09 /* Protected */,
       5,    0,   57,    2, 0x09 /* Protected */,
       6,    1,   58,    2, 0x09 /* Protected */,
       8,    1,   61,    2, 0x09 /* Protected */,
       9,    1,   64,    2, 0x09 /* Protected */,
      10,    1,   67,    2, 0x09 /* Protected */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    7,
    QMetaType::Void, QMetaType::QString,    7,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, 0x80000000 | 11,   12,

       0        // eod
};

void WatchdogDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<WatchdogDialog *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->_new(); break;
        case 1: _t->apply(); break;
        case 2: _t->add(); break;
        case 3: _t->del(); break;
        case 4: _t->condChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 5: _t->Changed((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 6: _t->comboChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: _t->eventcat_slected((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject WatchdogDialog::staticMetaObject = { {
    QMetaObject::SuperData::link<QDialog::staticMetaObject>(),
    qt_meta_stringdata_WatchdogDialog.data,
    qt_meta_data_WatchdogDialog,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *WatchdogDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *WatchdogDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_WatchdogDialog.stringdata0))
        return static_cast<void*>(this);
    return QDialog::qt_metacast(_clname);
}

int WatchdogDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 8)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 8;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
