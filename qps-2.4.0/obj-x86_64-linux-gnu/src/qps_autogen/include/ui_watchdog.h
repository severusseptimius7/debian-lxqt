/********************************************************************************
** Form generated from reading UI file 'watchdog.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WATCHDOG_H
#define UI_WATCHDOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListView>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QTableView>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_EventDialog
{
public:
    QVBoxLayout *vboxLayout;
    QListView *listView;
    QTableView *tableView;
    QGroupBox *groupBox_2;
    QVBoxLayout *vboxLayout1;
    QHBoxLayout *hboxLayout;
    QComboBox *comboBox;
    QLabel *labelDesc;
    QSpacerItem *spacerItem;
    QCheckBox *checkBox;
    QHBoxLayout *hboxLayout1;
    QLabel *label_procname;
    QLineEdit *proc_name;
    QLabel *label_cpu;
    QSpinBox *spinBox;
    QSpacerItem *spacerItem1;
    QCheckBox *checkBox_alreadyrun;
    QHBoxLayout *hboxLayout2;
    QLabel *label_3;
    QLineEdit *command;
    QHBoxLayout *hboxLayout3;
    QCheckBox *checkShowMsg;
    QLineEdit *message;
    QGroupBox *groupBox;
    QVBoxLayout *vboxLayout2;
    QLabel *label_2;
    QHBoxLayout *hboxLayout4;
    QPushButton *newButton;
    QPushButton *addButton;
    QPushButton *delButton;
    QPushButton *closeButton;

    void setupUi(QDialog *EventDialog)
    {
        if (EventDialog->objectName().isEmpty())
            EventDialog->setObjectName(QString::fromUtf8("EventDialog"));
        EventDialog->resize(680, 533);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(EventDialog->sizePolicy().hasHeightForWidth());
        EventDialog->setSizePolicy(sizePolicy);
        EventDialog->setFocusPolicy(Qt::WheelFocus);
        EventDialog->setStyleSheet(QString::fromUtf8("QPushButtonX {\n"
"	color: rgb(244, 244, 244);\n"
"	border-image: url(:/icon/vista.png);\n"
"}\n"
"QWidget {\n"
"}\n"
"QFrame, QLabel, QToolTip {\n"
" }\n"
" "));
        vboxLayout = new QVBoxLayout(EventDialog);
        vboxLayout->setSpacing(6);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        vboxLayout->setContentsMargins(9, 9, 9, 9);
        listView = new QListView(EventDialog);
        listView->setObjectName(QString::fromUtf8("listView"));
        sizePolicy.setHeightForWidth(listView->sizePolicy().hasHeightForWidth());
        listView->setSizePolicy(sizePolicy);
        listView->setMinimumSize(QSize(8, 1));
        listView->setMaximumSize(QSize(16777215, 51));

        vboxLayout->addWidget(listView);

        tableView = new QTableView(EventDialog);
        tableView->setObjectName(QString::fromUtf8("tableView"));
        tableView->setEnabled(true);
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(tableView->sizePolicy().hasHeightForWidth());
        tableView->setSizePolicy(sizePolicy1);
        tableView->setFocusPolicy(Qt::NoFocus);
        tableView->setContextMenuPolicy(Qt::NoContextMenu);
        tableView->setAlternatingRowColors(true);
        tableView->setSelectionMode(QAbstractItemView::SingleSelection);
        tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
        tableView->setShowGrid(false);
        tableView->setGridStyle(Qt::NoPen);

        vboxLayout->addWidget(tableView);

        groupBox_2 = new QGroupBox(EventDialog);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        QSizePolicy sizePolicy2(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(groupBox_2->sizePolicy().hasHeightForWidth());
        groupBox_2->setSizePolicy(sizePolicy2);
        vboxLayout1 = new QVBoxLayout(groupBox_2);
        vboxLayout1->setSpacing(6);
        vboxLayout1->setObjectName(QString::fromUtf8("vboxLayout1"));
        vboxLayout1->setContentsMargins(9, 9, 9, 9);
        hboxLayout = new QHBoxLayout();
        hboxLayout->setSpacing(6);
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        hboxLayout->setContentsMargins(0, 0, 0, 0);
        comboBox = new QComboBox(groupBox_2);
        comboBox->addItem(QString());
        comboBox->setObjectName(QString::fromUtf8("comboBox"));
        sizePolicy.setHeightForWidth(comboBox->sizePolicy().hasHeightForWidth());
        comboBox->setSizePolicy(sizePolicy);

        hboxLayout->addWidget(comboBox);

        labelDesc = new QLabel(groupBox_2);
        labelDesc->setObjectName(QString::fromUtf8("labelDesc"));

        hboxLayout->addWidget(labelDesc);

        spacerItem = new QSpacerItem(36, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout->addItem(spacerItem);

        checkBox = new QCheckBox(groupBox_2);
        checkBox->setObjectName(QString::fromUtf8("checkBox"));
        checkBox->setEnabled(false);

        hboxLayout->addWidget(checkBox);


        vboxLayout1->addLayout(hboxLayout);

        hboxLayout1 = new QHBoxLayout();
        hboxLayout1->setSpacing(6);
        hboxLayout1->setObjectName(QString::fromUtf8("hboxLayout1"));
        hboxLayout1->setContentsMargins(0, 0, 0, 0);
        label_procname = new QLabel(groupBox_2);
        label_procname->setObjectName(QString::fromUtf8("label_procname"));

        hboxLayout1->addWidget(label_procname);

        proc_name = new QLineEdit(groupBox_2);
        proc_name->setObjectName(QString::fromUtf8("proc_name"));

        hboxLayout1->addWidget(proc_name);

        label_cpu = new QLabel(groupBox_2);
        label_cpu->setObjectName(QString::fromUtf8("label_cpu"));

        hboxLayout1->addWidget(label_cpu);

        spinBox = new QSpinBox(groupBox_2);
        spinBox->setObjectName(QString::fromUtf8("spinBox"));
        spinBox->setValue(90);

        hboxLayout1->addWidget(spinBox);

        spacerItem1 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout1->addItem(spacerItem1);

        checkBox_alreadyrun = new QCheckBox(groupBox_2);
        checkBox_alreadyrun->setObjectName(QString::fromUtf8("checkBox_alreadyrun"));
        checkBox_alreadyrun->setEnabled(false);
        QFont font;
        checkBox_alreadyrun->setFont(font);
        checkBox_alreadyrun->setChecked(true);

        hboxLayout1->addWidget(checkBox_alreadyrun);


        vboxLayout1->addLayout(hboxLayout1);

        hboxLayout2 = new QHBoxLayout();
        hboxLayout2->setSpacing(6);
        hboxLayout2->setObjectName(QString::fromUtf8("hboxLayout2"));
        hboxLayout2->setContentsMargins(0, 0, 0, 0);
        label_3 = new QLabel(groupBox_2);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        hboxLayout2->addWidget(label_3);

        command = new QLineEdit(groupBox_2);
        command->setObjectName(QString::fromUtf8("command"));

        hboxLayout2->addWidget(command);


        vboxLayout1->addLayout(hboxLayout2);

        hboxLayout3 = new QHBoxLayout();
        hboxLayout3->setSpacing(6);
        hboxLayout3->setObjectName(QString::fromUtf8("hboxLayout3"));
        hboxLayout3->setContentsMargins(0, 0, 0, 0);
        checkShowMsg = new QCheckBox(groupBox_2);
        checkShowMsg->setObjectName(QString::fromUtf8("checkShowMsg"));

        hboxLayout3->addWidget(checkShowMsg);

        message = new QLineEdit(groupBox_2);
        message->setObjectName(QString::fromUtf8("message"));

        hboxLayout3->addWidget(message);


        vboxLayout1->addLayout(hboxLayout3);


        vboxLayout->addWidget(groupBox_2);

        groupBox = new QGroupBox(EventDialog);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        sizePolicy2.setHeightForWidth(groupBox->sizePolicy().hasHeightForWidth());
        groupBox->setSizePolicy(sizePolicy2);
        vboxLayout2 = new QVBoxLayout(groupBox);
        vboxLayout2->setSpacing(4);
        vboxLayout2->setObjectName(QString::fromUtf8("vboxLayout2"));
        vboxLayout2->setContentsMargins(9, 9, 9, 9);
        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        sizePolicy.setHeightForWidth(label_2->sizePolicy().hasHeightForWidth());
        label_2->setSizePolicy(sizePolicy);

        vboxLayout2->addWidget(label_2);


        vboxLayout->addWidget(groupBox);

        hboxLayout4 = new QHBoxLayout();
        hboxLayout4->setSpacing(6);
        hboxLayout4->setObjectName(QString::fromUtf8("hboxLayout4"));
        hboxLayout4->setContentsMargins(0, 0, 0, 0);
        newButton = new QPushButton(EventDialog);
        newButton->setObjectName(QString::fromUtf8("newButton"));

        hboxLayout4->addWidget(newButton);

        addButton = new QPushButton(EventDialog);
        addButton->setObjectName(QString::fromUtf8("addButton"));

        hboxLayout4->addWidget(addButton);

        delButton = new QPushButton(EventDialog);
        delButton->setObjectName(QString::fromUtf8("delButton"));

        hboxLayout4->addWidget(delButton);

        closeButton = new QPushButton(EventDialog);
        closeButton->setObjectName(QString::fromUtf8("closeButton"));

        hboxLayout4->addWidget(closeButton);


        vboxLayout->addLayout(hboxLayout4);


        retranslateUi(EventDialog);

        QMetaObject::connectSlotsByName(EventDialog);
    } // setupUi

    void retranslateUi(QDialog *EventDialog)
    {
        EventDialog->setWindowTitle(QCoreApplication::translate("EventDialog", "Watchdog 0.1 alpha", nullptr));
        groupBox_2->setTitle(QCoreApplication::translate("EventDialog", "Eventcat", nullptr));
        comboBox->setItemText(0, QCoreApplication::translate("EventDialog", "Select condition", nullptr));

        labelDesc->setText(QCoreApplication::translate("EventDialog", "labelDescrition", nullptr));
        checkBox->setText(QCoreApplication::translate("EventDialog", "Enable", nullptr));
        label_procname->setText(QCoreApplication::translate("EventDialog", "process name", nullptr));
        label_cpu->setText(QCoreApplication::translate("EventDialog", "cpu", nullptr));
#if QT_CONFIG(tooltip)
        spinBox->setToolTip(QString());
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(statustip)
        spinBox->setStatusTip(QString());
#endif // QT_CONFIG(statustip)
#if QT_CONFIG(whatsthis)
        spinBox->setWhatsThis(QString());
#endif // QT_CONFIG(whatsthis)
#if QT_CONFIG(accessibility)
        spinBox->setAccessibleName(QString());
#endif // QT_CONFIG(accessibility)
        spinBox->setSuffix(QCoreApplication::translate("EventDialog", "%", nullptr));
        spinBox->setPrefix(QString());
        checkBox_alreadyrun->setText(QCoreApplication::translate("EventDialog", "include already running process", nullptr));
        label_3->setText(QCoreApplication::translate("EventDialog", "run command", nullptr));
#if QT_CONFIG(tooltip)
        command->setToolTip(QString());
#endif // QT_CONFIG(tooltip)
        checkShowMsg->setText(QCoreApplication::translate("EventDialog", "show Message", nullptr));
        message->setText(QString());
        groupBox->setTitle(QCoreApplication::translate("EventDialog", "Help (Not yet. just concept)", nullptr));
        label_2->setText(QCoreApplication::translate("EventDialog", "%p : pid\n"
"%c : command", nullptr));
        newButton->setStyleSheet(QString());
        newButton->setText(QCoreApplication::translate("EventDialog", "New", nullptr));
        addButton->setText(QCoreApplication::translate("EventDialog", "Add", nullptr));
        delButton->setText(QCoreApplication::translate("EventDialog", "Delete", nullptr));
        closeButton->setText(QCoreApplication::translate("EventDialog", "Close", nullptr));
    } // retranslateUi

};

namespace Ui {
    class EventDialog: public Ui_EventDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WATCHDOG_H
