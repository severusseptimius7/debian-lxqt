/********************************************************************************
** Form generated from reading UI file 'lxqtaboutdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LXQTABOUTDIALOG_H
#define UI_LXQTABOUTDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_about
{
public:
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout_2;
    QLabel *iconLabel;
    QLabel *nameLabel;
    QTabWidget *tabWidget;
    QWidget *tab;
    QVBoxLayout *verticalLayout_2;
    QTextBrowser *aboutBrowser;
    QWidget *tab_3;
    QVBoxLayout *verticalLayout_4;
    QTextBrowser *autorsBrowser;
    QWidget *tab_4;
    QVBoxLayout *verticalLayout_5;
    QTextBrowser *thanksBrowser;
    QWidget *translationsTab;
    QVBoxLayout *verticalLayout;
    QTextBrowser *translationsBrowser;
    QWidget *tab_2;
    QVBoxLayout *verticalLayout_6;
    QTextBrowser *techBrowser;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QPushButton *techCopyToClipboardButton;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *about)
    {
        if (about->objectName().isEmpty())
            about->setObjectName(QString::fromUtf8("about"));
        about->resize(633, 416);
        verticalLayout_3 = new QVBoxLayout(about);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(21);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(-1, -1, -1, 16);
        iconLabel = new QLabel(about);
        iconLabel->setObjectName(QString::fromUtf8("iconLabel"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(iconLabel->sizePolicy().hasHeightForWidth());
        iconLabel->setSizePolicy(sizePolicy);
        iconLabel->setSizeIncrement(QSize(0, 0));
        iconLabel->setText(QString::fromUtf8(""));

        horizontalLayout_2->addWidget(iconLabel);

        nameLabel = new QLabel(about);
        nameLabel->setObjectName(QString::fromUtf8("nameLabel"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(1);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(nameLabel->sizePolicy().hasHeightForWidth());
        nameLabel->setSizePolicy(sizePolicy1);
        nameLabel->setText(QString::fromUtf8("<h1>LXQt</h1><p>Version: %1</p>"));

        horizontalLayout_2->addWidget(nameLabel);


        verticalLayout_3->addLayout(horizontalLayout_2);

        tabWidget = new QTabWidget(about);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        verticalLayout_2 = new QVBoxLayout(tab);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        aboutBrowser = new QTextBrowser(tab);
        aboutBrowser->setObjectName(QString::fromUtf8("aboutBrowser"));
        aboutBrowser->setFrameShape(QFrame::NoFrame);
        aboutBrowser->setOpenExternalLinks(true);

        verticalLayout_2->addWidget(aboutBrowser);

        tabWidget->addTab(tab, QString());
        tab_3 = new QWidget();
        tab_3->setObjectName(QString::fromUtf8("tab_3"));
        verticalLayout_4 = new QVBoxLayout(tab_3);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        autorsBrowser = new QTextBrowser(tab_3);
        autorsBrowser->setObjectName(QString::fromUtf8("autorsBrowser"));
        autorsBrowser->setFrameShape(QFrame::NoFrame);
        autorsBrowser->setOpenExternalLinks(true);

        verticalLayout_4->addWidget(autorsBrowser);

        tabWidget->addTab(tab_3, QString());
        tab_4 = new QWidget();
        tab_4->setObjectName(QString::fromUtf8("tab_4"));
        verticalLayout_5 = new QVBoxLayout(tab_4);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        thanksBrowser = new QTextBrowser(tab_4);
        thanksBrowser->setObjectName(QString::fromUtf8("thanksBrowser"));
        thanksBrowser->setFrameShape(QFrame::NoFrame);
        thanksBrowser->setOpenExternalLinks(true);

        verticalLayout_5->addWidget(thanksBrowser);

        tabWidget->addTab(tab_4, QString());
        translationsTab = new QWidget();
        translationsTab->setObjectName(QString::fromUtf8("translationsTab"));
        verticalLayout = new QVBoxLayout(translationsTab);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        translationsBrowser = new QTextBrowser(translationsTab);
        translationsBrowser->setObjectName(QString::fromUtf8("translationsBrowser"));
        translationsBrowser->setFrameShape(QFrame::NoFrame);
        translationsBrowser->setOpenExternalLinks(true);

        verticalLayout->addWidget(translationsBrowser);

        tabWidget->addTab(translationsTab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QString::fromUtf8("tab_2"));
        verticalLayout_6 = new QVBoxLayout(tab_2);
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        techBrowser = new QTextBrowser(tab_2);
        techBrowser->setObjectName(QString::fromUtf8("techBrowser"));
        techBrowser->setFrameShape(QFrame::NoFrame);
        techBrowser->setOpenExternalLinks(true);

        verticalLayout_6->addWidget(techBrowser);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        techCopyToClipboardButton = new QPushButton(tab_2);
        techCopyToClipboardButton->setObjectName(QString::fromUtf8("techCopyToClipboardButton"));

        horizontalLayout->addWidget(techCopyToClipboardButton);


        verticalLayout_6->addLayout(horizontalLayout);

        tabWidget->addTab(tab_2, QString());

        verticalLayout_3->addWidget(tabWidget);

        buttonBox = new QDialogButtonBox(about);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setStandardButtons(QDialogButtonBox::Close);

        verticalLayout_3->addWidget(buttonBox);


        retranslateUi(about);
        QObject::connect(buttonBox, SIGNAL(rejected()), about, SLOT(reject()));

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(about);
    } // setupUi

    void retranslateUi(QDialog *about)
    {
        about->setWindowTitle(QCoreApplication::translate("about", "About LXQt", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab), QCoreApplication::translate("about", "About", "About dialog, Tab title "));
        tabWidget->setTabText(tabWidget->indexOf(tab_3), QCoreApplication::translate("about", "Authors", "About dialog, Tab title"));
        tabWidget->setTabText(tabWidget->indexOf(tab_4), QCoreApplication::translate("about", "Thanks", "About dialog, Tab title"));
        tabWidget->setTabText(tabWidget->indexOf(translationsTab), QCoreApplication::translate("about", "Translations", "About dialog, Tab title"));
        techCopyToClipboardButton->setText(QCoreApplication::translate("about", "Copy to clipboard", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QCoreApplication::translate("about", "Technical Info", "About dialog, Tab title"));
    } // retranslateUi

};

namespace Ui {
    class about: public Ui_about {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LXQTABOUTDIALOG_H
