# Install script for directory: /home/debian/lxqt/lxqt-about/lxqt-about-1.0.0

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "RelWithDebInfo")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

# Set default install directory permissions.
if(NOT DEFINED CMAKE_OBJDUMP)
  set(CMAKE_OBJDUMP "/usr/bin/objdump")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xRuntimex" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/share/lxqt/translations/lxqt-about/lxqt-about_ar.qm;/usr/share/lxqt/translations/lxqt-about/lxqt-about_arn.qm;/usr/share/lxqt/translations/lxqt-about/lxqt-about_ast.qm;/usr/share/lxqt/translations/lxqt-about/lxqt-about_bg.qm;/usr/share/lxqt/translations/lxqt-about/lxqt-about_ca.qm;/usr/share/lxqt/translations/lxqt-about/lxqt-about_cs.qm;/usr/share/lxqt/translations/lxqt-about/lxqt-about_cy.qm;/usr/share/lxqt/translations/lxqt-about/lxqt-about_da.qm;/usr/share/lxqt/translations/lxqt-about/lxqt-about_de.qm;/usr/share/lxqt/translations/lxqt-about/lxqt-about_el.qm;/usr/share/lxqt/translations/lxqt-about/lxqt-about_en_GB.qm;/usr/share/lxqt/translations/lxqt-about/lxqt-about_eo.qm;/usr/share/lxqt/translations/lxqt-about/lxqt-about_es.qm;/usr/share/lxqt/translations/lxqt-about/lxqt-about_es_VE.qm;/usr/share/lxqt/translations/lxqt-about/lxqt-about_et.qm;/usr/share/lxqt/translations/lxqt-about/lxqt-about_eu.qm;/usr/share/lxqt/translations/lxqt-about/lxqt-about_fa.qm;/usr/share/lxqt/translations/lxqt-about/lxqt-about_fi.qm;/usr/share/lxqt/translations/lxqt-about/lxqt-about_fr.qm;/usr/share/lxqt/translations/lxqt-about/lxqt-about_gl.qm;/usr/share/lxqt/translations/lxqt-about/lxqt-about_he.qm;/usr/share/lxqt/translations/lxqt-about/lxqt-about_hr.qm;/usr/share/lxqt/translations/lxqt-about/lxqt-about_hu.qm;/usr/share/lxqt/translations/lxqt-about/lxqt-about_ia.qm;/usr/share/lxqt/translations/lxqt-about/lxqt-about_id.qm;/usr/share/lxqt/translations/lxqt-about/lxqt-about_it.qm;/usr/share/lxqt/translations/lxqt-about/lxqt-about_ja.qm;/usr/share/lxqt/translations/lxqt-about/lxqt-about_ko.qm;/usr/share/lxqt/translations/lxqt-about/lxqt-about_lt.qm;/usr/share/lxqt/translations/lxqt-about/lxqt-about_lv.qm;/usr/share/lxqt/translations/lxqt-about/lxqt-about_nb_NO.qm;/usr/share/lxqt/translations/lxqt-about/lxqt-about_nl.qm;/usr/share/lxqt/translations/lxqt-about/lxqt-about_oc.qm;/usr/share/lxqt/translations/lxqt-about/lxqt-about_pl.qm;/usr/share/lxqt/translations/lxqt-about/lxqt-about_pt.qm;/usr/share/lxqt/translations/lxqt-about/lxqt-about_pt_BR.qm;/usr/share/lxqt/translations/lxqt-about/lxqt-about_ro_RO.qm;/usr/share/lxqt/translations/lxqt-about/lxqt-about_ru.qm;/usr/share/lxqt/translations/lxqt-about/lxqt-about_si.qm;/usr/share/lxqt/translations/lxqt-about/lxqt-about_sk_SK.qm;/usr/share/lxqt/translations/lxqt-about/lxqt-about_sl.qm;/usr/share/lxqt/translations/lxqt-about/lxqt-about_sr@latin.qm;/usr/share/lxqt/translations/lxqt-about/lxqt-about_sr_RS.qm;/usr/share/lxqt/translations/lxqt-about/lxqt-about_sv.qm;/usr/share/lxqt/translations/lxqt-about/lxqt-about_th_TH.qm;/usr/share/lxqt/translations/lxqt-about/lxqt-about_tr.qm;/usr/share/lxqt/translations/lxqt-about/lxqt-about_uk.qm;/usr/share/lxqt/translations/lxqt-about/lxqt-about_vi.qm;/usr/share/lxqt/translations/lxqt-about/lxqt-about_zh_CN.qm;/usr/share/lxqt/translations/lxqt-about/lxqt-about_zh_TW.qm")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/usr/share/lxqt/translations/lxqt-about" TYPE FILE FILES
    "/home/debian/lxqt/lxqt-about/lxqt-about-1.0.0/obj-x86_64-linux-gnu/lxqt-about_ar.qm"
    "/home/debian/lxqt/lxqt-about/lxqt-about-1.0.0/obj-x86_64-linux-gnu/lxqt-about_arn.qm"
    "/home/debian/lxqt/lxqt-about/lxqt-about-1.0.0/obj-x86_64-linux-gnu/lxqt-about_ast.qm"
    "/home/debian/lxqt/lxqt-about/lxqt-about-1.0.0/obj-x86_64-linux-gnu/lxqt-about_bg.qm"
    "/home/debian/lxqt/lxqt-about/lxqt-about-1.0.0/obj-x86_64-linux-gnu/lxqt-about_ca.qm"
    "/home/debian/lxqt/lxqt-about/lxqt-about-1.0.0/obj-x86_64-linux-gnu/lxqt-about_cs.qm"
    "/home/debian/lxqt/lxqt-about/lxqt-about-1.0.0/obj-x86_64-linux-gnu/lxqt-about_cy.qm"
    "/home/debian/lxqt/lxqt-about/lxqt-about-1.0.0/obj-x86_64-linux-gnu/lxqt-about_da.qm"
    "/home/debian/lxqt/lxqt-about/lxqt-about-1.0.0/obj-x86_64-linux-gnu/lxqt-about_de.qm"
    "/home/debian/lxqt/lxqt-about/lxqt-about-1.0.0/obj-x86_64-linux-gnu/lxqt-about_el.qm"
    "/home/debian/lxqt/lxqt-about/lxqt-about-1.0.0/obj-x86_64-linux-gnu/lxqt-about_en_GB.qm"
    "/home/debian/lxqt/lxqt-about/lxqt-about-1.0.0/obj-x86_64-linux-gnu/lxqt-about_eo.qm"
    "/home/debian/lxqt/lxqt-about/lxqt-about-1.0.0/obj-x86_64-linux-gnu/lxqt-about_es.qm"
    "/home/debian/lxqt/lxqt-about/lxqt-about-1.0.0/obj-x86_64-linux-gnu/lxqt-about_es_VE.qm"
    "/home/debian/lxqt/lxqt-about/lxqt-about-1.0.0/obj-x86_64-linux-gnu/lxqt-about_et.qm"
    "/home/debian/lxqt/lxqt-about/lxqt-about-1.0.0/obj-x86_64-linux-gnu/lxqt-about_eu.qm"
    "/home/debian/lxqt/lxqt-about/lxqt-about-1.0.0/obj-x86_64-linux-gnu/lxqt-about_fa.qm"
    "/home/debian/lxqt/lxqt-about/lxqt-about-1.0.0/obj-x86_64-linux-gnu/lxqt-about_fi.qm"
    "/home/debian/lxqt/lxqt-about/lxqt-about-1.0.0/obj-x86_64-linux-gnu/lxqt-about_fr.qm"
    "/home/debian/lxqt/lxqt-about/lxqt-about-1.0.0/obj-x86_64-linux-gnu/lxqt-about_gl.qm"
    "/home/debian/lxqt/lxqt-about/lxqt-about-1.0.0/obj-x86_64-linux-gnu/lxqt-about_he.qm"
    "/home/debian/lxqt/lxqt-about/lxqt-about-1.0.0/obj-x86_64-linux-gnu/lxqt-about_hr.qm"
    "/home/debian/lxqt/lxqt-about/lxqt-about-1.0.0/obj-x86_64-linux-gnu/lxqt-about_hu.qm"
    "/home/debian/lxqt/lxqt-about/lxqt-about-1.0.0/obj-x86_64-linux-gnu/lxqt-about_ia.qm"
    "/home/debian/lxqt/lxqt-about/lxqt-about-1.0.0/obj-x86_64-linux-gnu/lxqt-about_id.qm"
    "/home/debian/lxqt/lxqt-about/lxqt-about-1.0.0/obj-x86_64-linux-gnu/lxqt-about_it.qm"
    "/home/debian/lxqt/lxqt-about/lxqt-about-1.0.0/obj-x86_64-linux-gnu/lxqt-about_ja.qm"
    "/home/debian/lxqt/lxqt-about/lxqt-about-1.0.0/obj-x86_64-linux-gnu/lxqt-about_ko.qm"
    "/home/debian/lxqt/lxqt-about/lxqt-about-1.0.0/obj-x86_64-linux-gnu/lxqt-about_lt.qm"
    "/home/debian/lxqt/lxqt-about/lxqt-about-1.0.0/obj-x86_64-linux-gnu/lxqt-about_lv.qm"
    "/home/debian/lxqt/lxqt-about/lxqt-about-1.0.0/obj-x86_64-linux-gnu/lxqt-about_nb_NO.qm"
    "/home/debian/lxqt/lxqt-about/lxqt-about-1.0.0/obj-x86_64-linux-gnu/lxqt-about_nl.qm"
    "/home/debian/lxqt/lxqt-about/lxqt-about-1.0.0/obj-x86_64-linux-gnu/lxqt-about_oc.qm"
    "/home/debian/lxqt/lxqt-about/lxqt-about-1.0.0/obj-x86_64-linux-gnu/lxqt-about_pl.qm"
    "/home/debian/lxqt/lxqt-about/lxqt-about-1.0.0/obj-x86_64-linux-gnu/lxqt-about_pt.qm"
    "/home/debian/lxqt/lxqt-about/lxqt-about-1.0.0/obj-x86_64-linux-gnu/lxqt-about_pt_BR.qm"
    "/home/debian/lxqt/lxqt-about/lxqt-about-1.0.0/obj-x86_64-linux-gnu/lxqt-about_ro_RO.qm"
    "/home/debian/lxqt/lxqt-about/lxqt-about-1.0.0/obj-x86_64-linux-gnu/lxqt-about_ru.qm"
    "/home/debian/lxqt/lxqt-about/lxqt-about-1.0.0/obj-x86_64-linux-gnu/lxqt-about_si.qm"
    "/home/debian/lxqt/lxqt-about/lxqt-about-1.0.0/obj-x86_64-linux-gnu/lxqt-about_sk_SK.qm"
    "/home/debian/lxqt/lxqt-about/lxqt-about-1.0.0/obj-x86_64-linux-gnu/lxqt-about_sl.qm"
    "/home/debian/lxqt/lxqt-about/lxqt-about-1.0.0/obj-x86_64-linux-gnu/lxqt-about_sr@latin.qm"
    "/home/debian/lxqt/lxqt-about/lxqt-about-1.0.0/obj-x86_64-linux-gnu/lxqt-about_sr_RS.qm"
    "/home/debian/lxqt/lxqt-about/lxqt-about-1.0.0/obj-x86_64-linux-gnu/lxqt-about_sv.qm"
    "/home/debian/lxqt/lxqt-about/lxqt-about-1.0.0/obj-x86_64-linux-gnu/lxqt-about_th_TH.qm"
    "/home/debian/lxqt/lxqt-about/lxqt-about-1.0.0/obj-x86_64-linux-gnu/lxqt-about_tr.qm"
    "/home/debian/lxqt/lxqt-about/lxqt-about-1.0.0/obj-x86_64-linux-gnu/lxqt-about_uk.qm"
    "/home/debian/lxqt/lxqt-about/lxqt-about-1.0.0/obj-x86_64-linux-gnu/lxqt-about_vi.qm"
    "/home/debian/lxqt/lxqt-about/lxqt-about-1.0.0/obj-x86_64-linux-gnu/lxqt-about_zh_CN.qm"
    "/home/debian/lxqt/lxqt-about/lxqt-about-1.0.0/obj-x86_64-linux-gnu/lxqt-about_zh_TW.qm"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xRuntimex" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/lxqt-about" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/lxqt-about")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/lxqt-about"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "/home/debian/lxqt/lxqt-about/lxqt-about-1.0.0/obj-x86_64-linux-gnu/lxqt-about")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/lxqt-about" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/lxqt-about")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/lxqt-about")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xRuntimex" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/applications" TYPE FILE FILES "/home/debian/lxqt/lxqt-about/lxqt-about-1.0.0/obj-x86_64-linux-gnu/lxqt-about.desktop")
endif()

if(CMAKE_INSTALL_COMPONENT)
  set(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
else()
  set(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
endif()

string(REPLACE ";" "\n" CMAKE_INSTALL_MANIFEST_CONTENT
       "${CMAKE_INSTALL_MANIFEST_FILES}")
file(WRITE "/home/debian/lxqt/lxqt-about/lxqt-about-1.0.0/obj-x86_64-linux-gnu/${CMAKE_INSTALL_MANIFEST}"
     "${CMAKE_INSTALL_MANIFEST_CONTENT}")
