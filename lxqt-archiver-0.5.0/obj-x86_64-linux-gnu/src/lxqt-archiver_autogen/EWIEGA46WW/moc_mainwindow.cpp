/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.15.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../../../src/mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.15.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[52];
    char stringdata0[981];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 28), // "on_actionCreateNew_triggered"
QT_MOC_LITERAL(2, 40, 0), // ""
QT_MOC_LITERAL(3, 41, 7), // "checked"
QT_MOC_LITERAL(4, 49, 23), // "on_actionOpen_triggered"
QT_MOC_LITERAL(5, 73, 36), // "on_actionArchiveProperties_tr..."
QT_MOC_LITERAL(6, 110, 27), // "on_actionAddFiles_triggered"
QT_MOC_LITERAL(7, 138, 28), // "on_actionAddFolder_triggered"
QT_MOC_LITERAL(8, 167, 25), // "on_actionDelete_triggered"
QT_MOC_LITERAL(9, 193, 28), // "on_actionSelectAll_triggered"
QT_MOC_LITERAL(10, 222, 26), // "on_actionExtract_triggered"
QT_MOC_LITERAL(11, 249, 23), // "on_actionView_triggered"
QT_MOC_LITERAL(12, 273, 23), // "on_actionTest_triggered"
QT_MOC_LITERAL(13, 297, 27), // "on_actionPassword_triggered"
QT_MOC_LITERAL(14, 325, 24), // "on_actionDirTree_toggled"
QT_MOC_LITERAL(15, 350, 28), // "on_actionDirTreeMode_toggled"
QT_MOC_LITERAL(16, 379, 29), // "on_actionFlatListMode_toggled"
QT_MOC_LITERAL(17, 409, 25), // "on_actionExpand_triggered"
QT_MOC_LITERAL(18, 435, 27), // "on_actionCollapse_triggered"
QT_MOC_LITERAL(19, 463, 25), // "on_actionReload_triggered"
QT_MOC_LITERAL(20, 489, 23), // "on_actionStop_triggered"
QT_MOC_LITERAL(21, 513, 24), // "on_actionAbout_triggered"
QT_MOC_LITERAL(22, 538, 25), // "on_actionFilter_triggered"
QT_MOC_LITERAL(23, 564, 25), // "onDirTreeSelectionChanged"
QT_MOC_LITERAL(24, 590, 14), // "QItemSelection"
QT_MOC_LITERAL(25, 605, 8), // "selected"
QT_MOC_LITERAL(26, 614, 10), // "deselected"
QT_MOC_LITERAL(27, 625, 26), // "onFileListSelectionChanged"
QT_MOC_LITERAL(28, 652, 21), // "onFileListContextMenu"
QT_MOC_LITERAL(29, 674, 3), // "pos"
QT_MOC_LITERAL(30, 678, 23), // "onFileListDoubleClicked"
QT_MOC_LITERAL(31, 702, 11), // "QModelIndex"
QT_MOC_LITERAL(32, 714, 5), // "index"
QT_MOC_LITERAL(33, 720, 19), // "onFileListActivated"
QT_MOC_LITERAL(34, 740, 22), // "onFileListEnterPressed"
QT_MOC_LITERAL(35, 763, 6), // "filter"
QT_MOC_LITERAL(36, 770, 4), // "text"
QT_MOC_LITERAL(37, 775, 19), // "onInvalidateContent"
QT_MOC_LITERAL(38, 795, 15), // "onActionStarted"
QT_MOC_LITERAL(39, 811, 8), // "FrAction"
QT_MOC_LITERAL(40, 820, 6), // "action"
QT_MOC_LITERAL(41, 827, 16), // "onActionProgress"
QT_MOC_LITERAL(42, 844, 8), // "fraction"
QT_MOC_LITERAL(43, 853, 16), // "onActionFinished"
QT_MOC_LITERAL(44, 870, 13), // "ArchiverError"
QT_MOC_LITERAL(45, 884, 3), // "err"
QT_MOC_LITERAL(46, 888, 9), // "onMessage"
QT_MOC_LITERAL(47, 898, 7), // "message"
QT_MOC_LITERAL(48, 906, 18), // "onStoppableChanged"
QT_MOC_LITERAL(49, 925, 9), // "stoppable"
QT_MOC_LITERAL(50, 935, 31), // "onPropertiesFileInfoJobFinished"
QT_MOC_LITERAL(51, 967, 13) // "onDragStarted"

    },
    "MainWindow\0on_actionCreateNew_triggered\0"
    "\0checked\0on_actionOpen_triggered\0"
    "on_actionArchiveProperties_triggered\0"
    "on_actionAddFiles_triggered\0"
    "on_actionAddFolder_triggered\0"
    "on_actionDelete_triggered\0"
    "on_actionSelectAll_triggered\0"
    "on_actionExtract_triggered\0"
    "on_actionView_triggered\0on_actionTest_triggered\0"
    "on_actionPassword_triggered\0"
    "on_actionDirTree_toggled\0"
    "on_actionDirTreeMode_toggled\0"
    "on_actionFlatListMode_toggled\0"
    "on_actionExpand_triggered\0"
    "on_actionCollapse_triggered\0"
    "on_actionReload_triggered\0"
    "on_actionStop_triggered\0"
    "on_actionAbout_triggered\0"
    "on_actionFilter_triggered\0"
    "onDirTreeSelectionChanged\0QItemSelection\0"
    "selected\0deselected\0onFileListSelectionChanged\0"
    "onFileListContextMenu\0pos\0"
    "onFileListDoubleClicked\0QModelIndex\0"
    "index\0onFileListActivated\0"
    "onFileListEnterPressed\0filter\0text\0"
    "onInvalidateContent\0onActionStarted\0"
    "FrAction\0action\0onActionProgress\0"
    "fraction\0onActionFinished\0ArchiverError\0"
    "err\0onMessage\0message\0onStoppableChanged\0"
    "stoppable\0onPropertiesFileInfoJobFinished\0"
    "onDragStarted"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      35,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,  189,    2, 0x08 /* Private */,
       4,    1,  192,    2, 0x08 /* Private */,
       5,    1,  195,    2, 0x08 /* Private */,
       6,    1,  198,    2, 0x08 /* Private */,
       7,    1,  201,    2, 0x08 /* Private */,
       8,    1,  204,    2, 0x08 /* Private */,
       9,    1,  207,    2, 0x08 /* Private */,
      10,    1,  210,    2, 0x08 /* Private */,
      11,    1,  213,    2, 0x08 /* Private */,
      12,    1,  216,    2, 0x08 /* Private */,
      13,    1,  219,    2, 0x08 /* Private */,
      14,    1,  222,    2, 0x08 /* Private */,
      15,    1,  225,    2, 0x08 /* Private */,
      16,    1,  228,    2, 0x08 /* Private */,
      17,    1,  231,    2, 0x08 /* Private */,
      18,    1,  234,    2, 0x08 /* Private */,
      19,    1,  237,    2, 0x08 /* Private */,
      20,    1,  240,    2, 0x08 /* Private */,
      21,    1,  243,    2, 0x08 /* Private */,
      22,    1,  246,    2, 0x08 /* Private */,
      23,    2,  249,    2, 0x08 /* Private */,
      27,    2,  254,    2, 0x08 /* Private */,
      28,    1,  259,    2, 0x08 /* Private */,
      30,    1,  262,    2, 0x08 /* Private */,
      33,    1,  265,    2, 0x08 /* Private */,
      34,    0,  268,    2, 0x08 /* Private */,
      35,    1,  269,    2, 0x08 /* Private */,
      37,    0,  272,    2, 0x08 /* Private */,
      38,    1,  273,    2, 0x08 /* Private */,
      41,    1,  276,    2, 0x08 /* Private */,
      43,    2,  279,    2, 0x08 /* Private */,
      46,    1,  284,    2, 0x08 /* Private */,
      48,    1,  287,    2, 0x08 /* Private */,
      50,    0,  290,    2, 0x08 /* Private */,
      51,    0,  291,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, 0x80000000 | 24, 0x80000000 | 24,   25,   26,
    QMetaType::Void, 0x80000000 | 24, 0x80000000 | 24,   25,   26,
    QMetaType::Void, QMetaType::QPoint,   29,
    QMetaType::Void, 0x80000000 | 31,   32,
    QMetaType::Void, 0x80000000 | 31,   32,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   36,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 39,   40,
    QMetaType::Void, QMetaType::Double,   42,
    QMetaType::Void, 0x80000000 | 39, 0x80000000 | 44,   40,   45,
    QMetaType::Void, QMetaType::QString,   47,
    QMetaType::Void, QMetaType::Bool,   49,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_actionCreateNew_triggered((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 1: _t->on_actionOpen_triggered((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 2: _t->on_actionArchiveProperties_triggered((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 3: _t->on_actionAddFiles_triggered((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 4: _t->on_actionAddFolder_triggered((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 5: _t->on_actionDelete_triggered((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 6: _t->on_actionSelectAll_triggered((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 7: _t->on_actionExtract_triggered((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 8: _t->on_actionView_triggered((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 9: _t->on_actionTest_triggered((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 10: _t->on_actionPassword_triggered((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 11: _t->on_actionDirTree_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 12: _t->on_actionDirTreeMode_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 13: _t->on_actionFlatListMode_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 14: _t->on_actionExpand_triggered((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 15: _t->on_actionCollapse_triggered((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 16: _t->on_actionReload_triggered((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 17: _t->on_actionStop_triggered((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 18: _t->on_actionAbout_triggered((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 19: _t->on_actionFilter_triggered((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 20: _t->onDirTreeSelectionChanged((*reinterpret_cast< const QItemSelection(*)>(_a[1])),(*reinterpret_cast< const QItemSelection(*)>(_a[2]))); break;
        case 21: _t->onFileListSelectionChanged((*reinterpret_cast< const QItemSelection(*)>(_a[1])),(*reinterpret_cast< const QItemSelection(*)>(_a[2]))); break;
        case 22: _t->onFileListContextMenu((*reinterpret_cast< const QPoint(*)>(_a[1]))); break;
        case 23: _t->onFileListDoubleClicked((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 24: _t->onFileListActivated((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 25: _t->onFileListEnterPressed(); break;
        case 26: _t->filter((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 27: _t->onInvalidateContent(); break;
        case 28: _t->onActionStarted((*reinterpret_cast< FrAction(*)>(_a[1]))); break;
        case 29: _t->onActionProgress((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 30: _t->onActionFinished((*reinterpret_cast< FrAction(*)>(_a[1])),(*reinterpret_cast< ArchiverError(*)>(_a[2]))); break;
        case 31: _t->onMessage((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 32: _t->onStoppableChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 33: _t->onPropertiesFileInfoJobFinished(); break;
        case 34: _t->onDragStarted(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 28:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< FrAction >(); break;
            }
            break;
        case 30:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 1:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< ArchiverError >(); break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< FrAction >(); break;
            }
            break;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject MainWindow::staticMetaObject = { {
    QMetaObject::SuperData::link<QMainWindow::staticMetaObject>(),
    qt_meta_stringdata_MainWindow.data,
    qt_meta_data_MainWindow,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 35)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 35;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 35)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 35;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
