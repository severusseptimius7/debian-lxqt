/****************************************************************************
** Meta object code from reading C++ file 'archiver.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.15.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../../../src/archiver.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'archiver.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.15.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Archiver_t {
    QByteArrayData data[17];
    char stringdata0[154];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Archiver_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Archiver_t qt_meta_stringdata_Archiver = {
    {
QT_MOC_LITERAL(0, 0, 8), // "Archiver"
QT_MOC_LITERAL(1, 9, 17), // "invalidateContent"
QT_MOC_LITERAL(2, 27, 0), // ""
QT_MOC_LITERAL(3, 28, 5), // "start"
QT_MOC_LITERAL(4, 34, 8), // "FrAction"
QT_MOC_LITERAL(5, 43, 6), // "action"
QT_MOC_LITERAL(6, 50, 6), // "finish"
QT_MOC_LITERAL(7, 57, 13), // "ArchiverError"
QT_MOC_LITERAL(8, 71, 5), // "error"
QT_MOC_LITERAL(9, 77, 8), // "progress"
QT_MOC_LITERAL(10, 86, 8), // "fraction"
QT_MOC_LITERAL(11, 95, 7), // "message"
QT_MOC_LITERAL(12, 103, 3), // "msg"
QT_MOC_LITERAL(13, 107, 16), // "stoppableChanged"
QT_MOC_LITERAL(14, 124, 5), // "value"
QT_MOC_LITERAL(15, 130, 14), // "workingArchive"
QT_MOC_LITERAL(16, 145, 8) // "filename"

    },
    "Archiver\0invalidateContent\0\0start\0"
    "FrAction\0action\0finish\0ArchiverError\0"
    "error\0progress\0fraction\0message\0msg\0"
    "stoppableChanged\0value\0workingArchive\0"
    "filename"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Archiver[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       7,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   49,    2, 0x06 /* Public */,
       3,    1,   50,    2, 0x06 /* Public */,
       6,    2,   53,    2, 0x06 /* Public */,
       9,    1,   58,    2, 0x06 /* Public */,
      11,    1,   61,    2, 0x06 /* Public */,
      13,    1,   64,    2, 0x06 /* Public */,
      15,    1,   67,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 4,    5,
    QMetaType::Void, 0x80000000 | 4, 0x80000000 | 7,    5,    8,
    QMetaType::Void, QMetaType::Double,   10,
    QMetaType::Void, QMetaType::QString,   12,
    QMetaType::Void, QMetaType::Bool,   14,
    QMetaType::Void, QMetaType::QString,   16,

       0        // eod
};

void Archiver::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<Archiver *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->invalidateContent(); break;
        case 1: _t->start((*reinterpret_cast< FrAction(*)>(_a[1]))); break;
        case 2: _t->finish((*reinterpret_cast< FrAction(*)>(_a[1])),(*reinterpret_cast< ArchiverError(*)>(_a[2]))); break;
        case 3: _t->progress((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 4: _t->message((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 5: _t->stoppableChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 6: _t->workingArchive((*reinterpret_cast< QString(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 1:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< FrAction >(); break;
            }
            break;
        case 2:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 1:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< ArchiverError >(); break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< FrAction >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (Archiver::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Archiver::invalidateContent)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (Archiver::*)(FrAction );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Archiver::start)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (Archiver::*)(FrAction , ArchiverError );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Archiver::finish)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (Archiver::*)(double );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Archiver::progress)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (Archiver::*)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Archiver::message)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (Archiver::*)(bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Archiver::stoppableChanged)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (Archiver::*)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Archiver::workingArchive)) {
                *result = 6;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject Archiver::staticMetaObject = { {
    QMetaObject::SuperData::link<QObject::staticMetaObject>(),
    qt_meta_stringdata_Archiver.data,
    qt_meta_data_Archiver,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Archiver::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Archiver::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Archiver.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int Archiver::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    }
    return _id;
}

// SIGNAL 0
void Archiver::invalidateContent()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void Archiver::start(FrAction _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Archiver::finish(FrAction _t1, ArchiverError _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))), const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t2))) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void Archiver::progress(double _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void Archiver::message(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void Archiver::stoppableChanged(bool _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void Archiver::workingArchive(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
