/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QSplitter>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "dirtreeView.h"
#include "filetreeView.h"

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionAbout;
    QAction *actionCreateNew;
    QAction *actionOpen;
    QAction *actionSaveAs;
    QAction *actionExtract;
    QAction *actionTest;
    QAction *actionArchiveProperties;
    QAction *actionClose;
    QAction *actionCut;
    QAction *actionCopy;
    QAction *actionPaste;
    QAction *actionRename;
    QAction *actionDelete;
    QAction *actionSelectAll;
    QAction *actionFind;
    QAction *actionAddFiles;
    QAction *actionAddFolder;
    QAction *actionPassword;
    QAction *actionToolbar;
    QAction *actionStatusbar;
    QAction *actionDirTree;
    QAction *actionDirTreeMode;
    QAction *actionFlatListMode;
    QAction *actionStop;
    QAction *actionReload;
    QAction *actionFilenameEncoding;
    QAction *actionView;
    QAction *actionFilter;
    QAction *actionExpand;
    QAction *actionCollapse;
    QWidget *centralwidget;
    QVBoxLayout *verticalLayout;
    QSplitter *splitter;
    DirTreeView *dirTreeView;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout_2;
    FileTreeView *fileListView;
    QLineEdit *filterLineEdit;
    QMenuBar *menubar;
    QMenu *menu_File;
    QMenu *menu_Edit;
    QMenu *menu_View;
    QMenu *menu_Help;
    QToolBar *toolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(696, 505);
        QIcon icon;
        QString iconThemeName = QString::fromUtf8("lxqt-archiver");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon = QIcon::fromTheme(iconThemeName);
        } else {
            icon.addFile(QString::fromUtf8("."), QSize(), QIcon::Normal, QIcon::Off);
        }
        MainWindow->setWindowIcon(icon);
        actionAbout = new QAction(MainWindow);
        actionAbout->setObjectName(QString::fromUtf8("actionAbout"));
        QIcon icon1;
        iconThemeName = QString::fromUtf8("help-about");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon1 = QIcon::fromTheme(iconThemeName);
        } else {
            icon1.addFile(QString::fromUtf8("."), QSize(), QIcon::Normal, QIcon::Off);
        }
        actionAbout->setIcon(icon1);
        actionCreateNew = new QAction(MainWindow);
        actionCreateNew->setObjectName(QString::fromUtf8("actionCreateNew"));
        QIcon icon2;
        iconThemeName = QString::fromUtf8("document-new");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon2 = QIcon::fromTheme(iconThemeName);
        } else {
            icon2.addFile(QString::fromUtf8("."), QSize(), QIcon::Normal, QIcon::Off);
        }
        actionCreateNew->setIcon(icon2);
        actionOpen = new QAction(MainWindow);
        actionOpen->setObjectName(QString::fromUtf8("actionOpen"));
        QIcon icon3;
        iconThemeName = QString::fromUtf8("document-open");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon3 = QIcon::fromTheme(iconThemeName);
        } else {
            icon3.addFile(QString::fromUtf8("."), QSize(), QIcon::Normal, QIcon::Off);
        }
        actionOpen->setIcon(icon3);
        actionSaveAs = new QAction(MainWindow);
        actionSaveAs->setObjectName(QString::fromUtf8("actionSaveAs"));
        actionSaveAs->setEnabled(false);
        actionExtract = new QAction(MainWindow);
        actionExtract->setObjectName(QString::fromUtf8("actionExtract"));
        QIcon icon4;
        iconThemeName = QString::fromUtf8("archive-extract");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon4 = QIcon::fromTheme(iconThemeName);
        } else {
            icon4.addFile(QString::fromUtf8("."), QSize(), QIcon::Normal, QIcon::Off);
        }
        actionExtract->setIcon(icon4);
        actionTest = new QAction(MainWindow);
        actionTest->setObjectName(QString::fromUtf8("actionTest"));
        actionArchiveProperties = new QAction(MainWindow);
        actionArchiveProperties->setObjectName(QString::fromUtf8("actionArchiveProperties"));
        actionArchiveProperties->setEnabled(false);
        actionClose = new QAction(MainWindow);
        actionClose->setObjectName(QString::fromUtf8("actionClose"));
        QIcon icon5;
        iconThemeName = QString::fromUtf8("window-close");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon5 = QIcon::fromTheme(iconThemeName);
        } else {
            icon5.addFile(QString::fromUtf8("."), QSize(), QIcon::Normal, QIcon::Off);
        }
        actionClose->setIcon(icon5);
        actionCut = new QAction(MainWindow);
        actionCut->setObjectName(QString::fromUtf8("actionCut"));
        actionCut->setEnabled(false);
        actionCopy = new QAction(MainWindow);
        actionCopy->setObjectName(QString::fromUtf8("actionCopy"));
        actionCopy->setEnabled(false);
        actionPaste = new QAction(MainWindow);
        actionPaste->setObjectName(QString::fromUtf8("actionPaste"));
        actionPaste->setEnabled(false);
        actionRename = new QAction(MainWindow);
        actionRename->setObjectName(QString::fromUtf8("actionRename"));
        actionRename->setEnabled(false);
        actionDelete = new QAction(MainWindow);
        actionDelete->setObjectName(QString::fromUtf8("actionDelete"));
        actionSelectAll = new QAction(MainWindow);
        actionSelectAll->setObjectName(QString::fromUtf8("actionSelectAll"));
        actionFind = new QAction(MainWindow);
        actionFind->setObjectName(QString::fromUtf8("actionFind"));
        actionFind->setEnabled(false);
        actionAddFiles = new QAction(MainWindow);
        actionAddFiles->setObjectName(QString::fromUtf8("actionAddFiles"));
        QIcon icon6;
        iconThemeName = QString::fromUtf8("archive-insert");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon6 = QIcon::fromTheme(iconThemeName);
        } else {
            icon6.addFile(QString::fromUtf8("."), QSize(), QIcon::Normal, QIcon::Off);
        }
        actionAddFiles->setIcon(icon6);
        actionAddFolder = new QAction(MainWindow);
        actionAddFolder->setObjectName(QString::fromUtf8("actionAddFolder"));
        QIcon icon7;
        iconThemeName = QString::fromUtf8("archive-insert-directory");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon7 = QIcon::fromTheme(iconThemeName);
        } else {
            icon7.addFile(QString::fromUtf8("."), QSize(), QIcon::Normal, QIcon::Off);
        }
        actionAddFolder->setIcon(icon7);
        actionPassword = new QAction(MainWindow);
        actionPassword->setObjectName(QString::fromUtf8("actionPassword"));
        actionToolbar = new QAction(MainWindow);
        actionToolbar->setObjectName(QString::fromUtf8("actionToolbar"));
        actionToolbar->setCheckable(true);
        actionToolbar->setChecked(true);
        actionStatusbar = new QAction(MainWindow);
        actionStatusbar->setObjectName(QString::fromUtf8("actionStatusbar"));
        actionStatusbar->setCheckable(true);
        actionStatusbar->setChecked(true);
        actionDirTree = new QAction(MainWindow);
        actionDirTree->setObjectName(QString::fromUtf8("actionDirTree"));
        actionDirTree->setCheckable(true);
        actionDirTree->setChecked(true);
        actionDirTreeMode = new QAction(MainWindow);
        actionDirTreeMode->setObjectName(QString::fromUtf8("actionDirTreeMode"));
        actionDirTreeMode->setCheckable(true);
        actionDirTreeMode->setChecked(true);
        actionFlatListMode = new QAction(MainWindow);
        actionFlatListMode->setObjectName(QString::fromUtf8("actionFlatListMode"));
        actionFlatListMode->setCheckable(true);
        actionStop = new QAction(MainWindow);
        actionStop->setObjectName(QString::fromUtf8("actionStop"));
        QIcon icon8;
        iconThemeName = QString::fromUtf8("process-stop");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon8 = QIcon::fromTheme(iconThemeName);
        } else {
            icon8.addFile(QString::fromUtf8("."), QSize(), QIcon::Normal, QIcon::Off);
        }
        actionStop->setIcon(icon8);
        actionReload = new QAction(MainWindow);
        actionReload->setObjectName(QString::fromUtf8("actionReload"));
        actionFilenameEncoding = new QAction(MainWindow);
        actionFilenameEncoding->setObjectName(QString::fromUtf8("actionFilenameEncoding"));
        actionView = new QAction(MainWindow);
        actionView->setObjectName(QString::fromUtf8("actionView"));
        actionFilter = new QAction(MainWindow);
        actionFilter->setObjectName(QString::fromUtf8("actionFilter"));
        actionExpand = new QAction(MainWindow);
        actionExpand->setObjectName(QString::fromUtf8("actionExpand"));
        QIcon icon9(QIcon::fromTheme(QString::fromUtf8("expand-all")));
        actionExpand->setIcon(icon9);
        actionCollapse = new QAction(MainWindow);
        actionCollapse->setObjectName(QString::fromUtf8("actionCollapse"));
        QIcon icon10(QIcon::fromTheme(QString::fromUtf8("collapse-all")));
        actionCollapse->setIcon(icon10);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        verticalLayout = new QVBoxLayout(centralwidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        splitter = new QSplitter(centralwidget);
        splitter->setObjectName(QString::fromUtf8("splitter"));
        splitter->setOrientation(Qt::Horizontal);
        dirTreeView = new DirTreeView(splitter);
        dirTreeView->setObjectName(QString::fromUtf8("dirTreeView"));
        dirTreeView->setEnabled(false);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(dirTreeView->sizePolicy().hasHeightForWidth());
        dirTreeView->setSizePolicy(sizePolicy);
        splitter->addWidget(dirTreeView);
        verticalLayoutWidget = new QWidget(splitter);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayout_2 = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        fileListView = new FileTreeView(verticalLayoutWidget);
        fileListView->setObjectName(QString::fromUtf8("fileListView"));
        fileListView->setEnabled(false);

        verticalLayout_2->addWidget(fileListView);

        filterLineEdit = new QLineEdit(verticalLayoutWidget);
        filterLineEdit->setObjectName(QString::fromUtf8("filterLineEdit"));

        verticalLayout_2->addWidget(filterLineEdit);

        splitter->addWidget(verticalLayoutWidget);

        verticalLayout->addWidget(splitter);

        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 696, 31));
        menu_File = new QMenu(menubar);
        menu_File->setObjectName(QString::fromUtf8("menu_File"));
        menu_Edit = new QMenu(menubar);
        menu_Edit->setObjectName(QString::fromUtf8("menu_Edit"));
        menu_View = new QMenu(menubar);
        menu_View->setObjectName(QString::fromUtf8("menu_View"));
        menu_Help = new QMenu(menubar);
        menu_Help->setObjectName(QString::fromUtf8("menu_Help"));
        MainWindow->setMenuBar(menubar);
        toolBar = new QToolBar(MainWindow);
        toolBar->setObjectName(QString::fromUtf8("toolBar"));
        toolBar->setMovable(false);
        MainWindow->addToolBar(Qt::TopToolBarArea, toolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menubar->addAction(menu_File->menuAction());
        menubar->addAction(menu_Edit->menuAction());
        menubar->addAction(menu_View->menuAction());
        menubar->addAction(menu_Help->menuAction());
        menu_File->addAction(actionCreateNew);
        menu_File->addAction(actionOpen);
        menu_File->addAction(actionSaveAs);
        menu_File->addSeparator();
        menu_File->addAction(actionExtract);
        menu_File->addAction(actionTest);
        menu_File->addSeparator();
        menu_File->addAction(actionArchiveProperties);
        menu_File->addSeparator();
        menu_File->addAction(actionClose);
        menu_Edit->addAction(actionCut);
        menu_Edit->addAction(actionCopy);
        menu_Edit->addAction(actionPaste);
        menu_Edit->addAction(actionRename);
        menu_Edit->addAction(actionDelete);
        menu_Edit->addSeparator();
        menu_Edit->addAction(actionSelectAll);
        menu_Edit->addSeparator();
        menu_Edit->addAction(actionFind);
        menu_Edit->addSeparator();
        menu_Edit->addAction(actionAddFiles);
        menu_Edit->addAction(actionAddFolder);
        menu_Edit->addSeparator();
        menu_Edit->addAction(actionPassword);
        menu_View->addSeparator();
        menu_View->addAction(actionToolbar);
        menu_View->addAction(actionStatusbar);
        menu_View->addAction(actionDirTree);
        menu_View->addSeparator();
        menu_View->addAction(actionFlatListMode);
        menu_View->addAction(actionDirTreeMode);
        menu_View->addSeparator();
        menu_View->addAction(actionExpand);
        menu_View->addAction(actionCollapse);
        menu_View->addSeparator();
        menu_View->addAction(actionStop);
        menu_View->addAction(actionReload);
        menu_View->addSeparator();
        menu_View->addAction(actionFilter);
        menu_Help->addAction(actionAbout);
        toolBar->addAction(actionCreateNew);
        toolBar->addAction(actionOpen);
        toolBar->addSeparator();
        toolBar->addAction(actionExtract);
        toolBar->addSeparator();
        toolBar->addAction(actionAddFiles);
        toolBar->addAction(actionAddFolder);
        toolBar->addSeparator();
        toolBar->addAction(actionStop);

        retranslateUi(MainWindow);
        QObject::connect(actionClose, SIGNAL(triggered()), MainWindow, SLOT(close()));
        QObject::connect(actionStatusbar, SIGNAL(toggled(bool)), statusBar, SLOT(setVisible(bool)));
        QObject::connect(actionToolbar, SIGNAL(toggled(bool)), toolBar, SLOT(setVisible(bool)));

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "File Archiver", nullptr));
        actionAbout->setText(QCoreApplication::translate("MainWindow", "&About", nullptr));
        actionCreateNew->setText(QCoreApplication::translate("MainWindow", "&New", nullptr));
#if QT_CONFIG(shortcut)
        actionCreateNew->setShortcut(QCoreApplication::translate("MainWindow", "Ctrl+N", nullptr));
#endif // QT_CONFIG(shortcut)
        actionOpen->setText(QCoreApplication::translate("MainWindow", "&Open", nullptr));
#if QT_CONFIG(shortcut)
        actionOpen->setShortcut(QCoreApplication::translate("MainWindow", "Ctrl+O", nullptr));
#endif // QT_CONFIG(shortcut)
        actionSaveAs->setText(QCoreApplication::translate("MainWindow", "Save &As", nullptr));
        actionExtract->setText(QCoreApplication::translate("MainWindow", "&Extract", nullptr));
        actionTest->setText(QCoreApplication::translate("MainWindow", "&Test", nullptr));
        actionArchiveProperties->setText(QCoreApplication::translate("MainWindow", "&Properties", nullptr));
#if QT_CONFIG(shortcut)
        actionArchiveProperties->setShortcut(QCoreApplication::translate("MainWindow", "Alt+Return", nullptr));
#endif // QT_CONFIG(shortcut)
        actionClose->setText(QCoreApplication::translate("MainWindow", "&Close", nullptr));
#if QT_CONFIG(shortcut)
        actionClose->setShortcut(QCoreApplication::translate("MainWindow", "Ctrl+Q", nullptr));
#endif // QT_CONFIG(shortcut)
        actionCut->setText(QCoreApplication::translate("MainWindow", "Cu&t", nullptr));
#if QT_CONFIG(shortcut)
        actionCut->setShortcut(QCoreApplication::translate("MainWindow", "Ctrl+X", nullptr));
#endif // QT_CONFIG(shortcut)
        actionCopy->setText(QCoreApplication::translate("MainWindow", "&Copy", nullptr));
#if QT_CONFIG(shortcut)
        actionCopy->setShortcut(QCoreApplication::translate("MainWindow", "Ctrl+C", nullptr));
#endif // QT_CONFIG(shortcut)
        actionPaste->setText(QCoreApplication::translate("MainWindow", "&Paste", nullptr));
#if QT_CONFIG(shortcut)
        actionPaste->setShortcut(QCoreApplication::translate("MainWindow", "Ctrl+V", nullptr));
#endif // QT_CONFIG(shortcut)
        actionRename->setText(QCoreApplication::translate("MainWindow", "&Rename", nullptr));
#if QT_CONFIG(shortcut)
        actionRename->setShortcut(QCoreApplication::translate("MainWindow", "F2", nullptr));
#endif // QT_CONFIG(shortcut)
        actionDelete->setText(QCoreApplication::translate("MainWindow", "&Delete", nullptr));
#if QT_CONFIG(shortcut)
        actionDelete->setShortcut(QCoreApplication::translate("MainWindow", "Del", nullptr));
#endif // QT_CONFIG(shortcut)
        actionSelectAll->setText(QCoreApplication::translate("MainWindow", "&Select All", nullptr));
#if QT_CONFIG(shortcut)
        actionSelectAll->setShortcut(QCoreApplication::translate("MainWindow", "Ctrl+A", nullptr));
#endif // QT_CONFIG(shortcut)
        actionFind->setText(QCoreApplication::translate("MainWindow", "&Find", nullptr));
#if QT_CONFIG(shortcut)
        actionFind->setShortcut(QCoreApplication::translate("MainWindow", "Ctrl+F", nullptr));
#endif // QT_CONFIG(shortcut)
        actionAddFiles->setText(QCoreApplication::translate("MainWindow", "&Add Files", nullptr));
        actionAddFolder->setText(QCoreApplication::translate("MainWindow", "Add F&older", nullptr));
        actionPassword->setText(QCoreApplication::translate("MainWindow", "Pass&word", nullptr));
        actionToolbar->setText(QCoreApplication::translate("MainWindow", "&Toolbar", nullptr));
        actionStatusbar->setText(QCoreApplication::translate("MainWindow", "&Statusbar", nullptr));
        actionDirTree->setText(QCoreApplication::translate("MainWindow", "&Directory Tree", nullptr));
        actionDirTreeMode->setText(QCoreApplication::translate("MainWindow", "S&how as Folder", nullptr));
        actionFlatListMode->setText(QCoreApplication::translate("MainWindow", "Show &All Files", nullptr));
        actionStop->setText(QCoreApplication::translate("MainWindow", "St&op", nullptr));
#if QT_CONFIG(shortcut)
        actionStop->setShortcut(QCoreApplication::translate("MainWindow", "Esc", nullptr));
#endif // QT_CONFIG(shortcut)
        actionReload->setText(QCoreApplication::translate("MainWindow", "&Reload", nullptr));
#if QT_CONFIG(shortcut)
        actionReload->setShortcut(QCoreApplication::translate("MainWindow", "F5", nullptr));
#endif // QT_CONFIG(shortcut)
        actionFilenameEncoding->setText(QCoreApplication::translate("MainWindow", "Filename &Encoding", nullptr));
        actionView->setText(QCoreApplication::translate("MainWindow", "&View Selected Items", nullptr));
        actionFilter->setText(QCoreApplication::translate("MainWindow", "&Filter", nullptr));
#if QT_CONFIG(shortcut)
        actionFilter->setShortcut(QCoreApplication::translate("MainWindow", "Ctrl+F", nullptr));
#endif // QT_CONFIG(shortcut)
        actionExpand->setText(QCoreApplication::translate("MainWindow", "&Expand", nullptr));
#if QT_CONFIG(shortcut)
        actionExpand->setShortcut(QCoreApplication::translate("MainWindow", "Ctrl+Shift+Down", nullptr));
#endif // QT_CONFIG(shortcut)
        actionCollapse->setText(QCoreApplication::translate("MainWindow", "&Collapse", nullptr));
#if QT_CONFIG(shortcut)
        actionCollapse->setShortcut(QCoreApplication::translate("MainWindow", "Ctrl+Shift+Up", nullptr));
#endif // QT_CONFIG(shortcut)
        filterLineEdit->setPlaceholderText(QCoreApplication::translate("MainWindow", "Filter Files...", nullptr));
        menu_File->setTitle(QCoreApplication::translate("MainWindow", "&File", nullptr));
        menu_Edit->setTitle(QCoreApplication::translate("MainWindow", "&Edit", nullptr));
        menu_View->setTitle(QCoreApplication::translate("MainWindow", "&View", nullptr));
        menu_Help->setTitle(QCoreApplication::translate("MainWindow", "&Help", nullptr));
        toolBar->setWindowTitle(QCoreApplication::translate("MainWindow", "Main Toolbar", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
