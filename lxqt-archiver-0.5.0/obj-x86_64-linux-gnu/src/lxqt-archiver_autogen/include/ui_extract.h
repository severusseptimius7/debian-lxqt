/********************************************************************************
** Form generated from reading UI file 'extract.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_EXTRACT_H
#define UI_EXTRACT_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ExtractArchiveExtraWidget
{
public:
    QHBoxLayout *horizontalLayout;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout;
    QRadioButton *extractAll;
    QRadioButton *extractSelected;
    QSpacerItem *verticalSpacer;
    QGroupBox *groupBox_2;
    QVBoxLayout *verticalLayout_2;
    QCheckBox *reCreateFolders;
    QCheckBox *overwriteExisting;
    QCheckBox *skipOlder;
    QSpacerItem *verticalSpacer_2;

    void setupUi(QWidget *ExtractArchiveExtraWidget)
    {
        if (ExtractArchiveExtraWidget->objectName().isEmpty())
            ExtractArchiveExtraWidget->setObjectName(QString::fromUtf8("ExtractArchiveExtraWidget"));
        ExtractArchiveExtraWidget->resize(496, 142);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Minimum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(ExtractArchiveExtraWidget->sizePolicy().hasHeightForWidth());
        ExtractArchiveExtraWidget->setSizePolicy(sizePolicy);
        horizontalLayout = new QHBoxLayout(ExtractArchiveExtraWidget);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        groupBox = new QGroupBox(ExtractArchiveExtraWidget);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        verticalLayout = new QVBoxLayout(groupBox);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        extractAll = new QRadioButton(groupBox);
        extractAll->setObjectName(QString::fromUtf8("extractAll"));

        verticalLayout->addWidget(extractAll);

        extractSelected = new QRadioButton(groupBox);
        extractSelected->setObjectName(QString::fromUtf8("extractSelected"));

        verticalLayout->addWidget(extractSelected);

        verticalSpacer = new QSpacerItem(20, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        verticalLayout->setStretch(2, 1);

        horizontalLayout->addWidget(groupBox);

        groupBox_2 = new QGroupBox(ExtractArchiveExtraWidget);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        verticalLayout_2 = new QVBoxLayout(groupBox_2);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        reCreateFolders = new QCheckBox(groupBox_2);
        reCreateFolders->setObjectName(QString::fromUtf8("reCreateFolders"));

        verticalLayout_2->addWidget(reCreateFolders);

        overwriteExisting = new QCheckBox(groupBox_2);
        overwriteExisting->setObjectName(QString::fromUtf8("overwriteExisting"));

        verticalLayout_2->addWidget(overwriteExisting);

        skipOlder = new QCheckBox(groupBox_2);
        skipOlder->setObjectName(QString::fromUtf8("skipOlder"));

        verticalLayout_2->addWidget(skipOlder);

        verticalSpacer_2 = new QSpacerItem(20, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_2);

        verticalLayout_2->setStretch(3, 1);

        horizontalLayout->addWidget(groupBox_2);


        retranslateUi(ExtractArchiveExtraWidget);

        QMetaObject::connectSlotsByName(ExtractArchiveExtraWidget);
    } // setupUi

    void retranslateUi(QWidget *ExtractArchiveExtraWidget)
    {
        groupBox->setTitle(QCoreApplication::translate("ExtractArchiveExtraWidget", "Extract", nullptr));
        extractAll->setText(QCoreApplication::translate("ExtractArchiveExtraWidget", "E&xtract all files", nullptr));
        extractSelected->setText(QCoreApplication::translate("ExtractArchiveExtraWidget", "Ex&tract selected files", nullptr));
        groupBox_2->setTitle(QCoreApplication::translate("ExtractArchiveExtraWidget", "Actions", nullptr));
        reCreateFolders->setText(QCoreApplication::translate("ExtractArchiveExtraWidget", "Re-create folders", nullptr));
        overwriteExisting->setText(QCoreApplication::translate("ExtractArchiveExtraWidget", "Overwrite existing files", nullptr));
        skipOlder->setText(QCoreApplication::translate("ExtractArchiveExtraWidget", "Do not extract older files", nullptr));
        (void)ExtractArchiveExtraWidget;
    } // retranslateUi

};

namespace Ui {
    class ExtractArchiveExtraWidget: public Ui_ExtractArchiveExtraWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_EXTRACT_H
