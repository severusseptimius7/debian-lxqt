/********************************************************************************
** Form generated from reading UI file 'progressdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PROGRESSDIALOG_H
#define UI_PROGRESSDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_ProgressDialog
{
public:
    QVBoxLayout *verticalLayout;
    QLabel *operation;
    QLabel *currentFile;
    QProgressBar *progressBar;
    QLabel *message;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *ProgressDialog)
    {
        if (ProgressDialog->objectName().isEmpty())
            ProgressDialog->setObjectName(QString::fromUtf8("ProgressDialog"));
        ProgressDialog->resize(455, 155);
        verticalLayout = new QVBoxLayout(ProgressDialog);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        operation = new QLabel(ProgressDialog);
        operation->setObjectName(QString::fromUtf8("operation"));
        operation->setWordWrap(true);

        verticalLayout->addWidget(operation);

        currentFile = new QLabel(ProgressDialog);
        currentFile->setObjectName(QString::fromUtf8("currentFile"));

        verticalLayout->addWidget(currentFile);

        progressBar = new QProgressBar(ProgressDialog);
        progressBar->setObjectName(QString::fromUtf8("progressBar"));
        progressBar->setValue(24);

        verticalLayout->addWidget(progressBar);

        message = new QLabel(ProgressDialog);
        message->setObjectName(QString::fromUtf8("message"));

        verticalLayout->addWidget(message);

        buttonBox = new QDialogButtonBox(ProgressDialog);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel);

        verticalLayout->addWidget(buttonBox);


        retranslateUi(ProgressDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), ProgressDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), ProgressDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(ProgressDialog);
    } // setupUi

    void retranslateUi(QDialog *ProgressDialog)
    {
        ProgressDialog->setWindowTitle(QCoreApplication::translate("ProgressDialog", "Progress", nullptr));
        operation->setText(QString());
        currentFile->setText(QString());
        message->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class ProgressDialog: public Ui_ProgressDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PROGRESSDIALOG_H
