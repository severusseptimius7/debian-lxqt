/********************************************************************************
** Form generated from reading UI file 'create.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CREATE_H
#define UI_CREATE_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_CreateArchiveExtraWidget
{
public:
    QGridLayout *gridLayout;
    QSpinBox *volumeSize;
    QCheckBox *splitVolumes;
    QLabel *label;
    QCheckBox *encryptFileList;
    QLineEdit *password;
    QSpacerItem *horizontalSpacer;

    void setupUi(QWidget *CreateArchiveExtraWidget)
    {
        if (CreateArchiveExtraWidget->objectName().isEmpty())
            CreateArchiveExtraWidget->setObjectName(QString::fromUtf8("CreateArchiveExtraWidget"));
        CreateArchiveExtraWidget->resize(429, 113);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Minimum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(CreateArchiveExtraWidget->sizePolicy().hasHeightForWidth());
        CreateArchiveExtraWidget->setSizePolicy(sizePolicy);
        gridLayout = new QGridLayout(CreateArchiveExtraWidget);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        volumeSize = new QSpinBox(CreateArchiveExtraWidget);
        volumeSize->setObjectName(QString::fromUtf8("volumeSize"));
        volumeSize->setEnabled(false);
        volumeSize->setMaximum(1000);

        gridLayout->addWidget(volumeSize, 2, 1, 1, 1);

        splitVolumes = new QCheckBox(CreateArchiveExtraWidget);
        splitVolumes->setObjectName(QString::fromUtf8("splitVolumes"));

        gridLayout->addWidget(splitVolumes, 2, 0, 1, 1);

        label = new QLabel(CreateArchiveExtraWidget);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1);

        encryptFileList = new QCheckBox(CreateArchiveExtraWidget);
        encryptFileList->setObjectName(QString::fromUtf8("encryptFileList"));

        gridLayout->addWidget(encryptFileList, 1, 0, 1, 2);

        password = new QLineEdit(CreateArchiveExtraWidget);
        password->setObjectName(QString::fromUtf8("password"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(password->sizePolicy().hasHeightForWidth());
        password->setSizePolicy(sizePolicy1);
        password->setMinimumSize(QSize(250, 0));
        password->setEchoMode(QLineEdit::Password);

        gridLayout->addWidget(password, 0, 1, 1, 1);

        horizontalSpacer = new QSpacerItem(0, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer, 0, 2, 1, 1);

        gridLayout->setColumnStretch(2, 1);

        retranslateUi(CreateArchiveExtraWidget);
        QObject::connect(splitVolumes, SIGNAL(toggled(bool)), volumeSize, SLOT(setEnabled(bool)));

        QMetaObject::connectSlotsByName(CreateArchiveExtraWidget);
    } // setupUi

    void retranslateUi(QWidget *CreateArchiveExtraWidget)
    {
        volumeSize->setSuffix(QCoreApplication::translate("CreateArchiveExtraWidget", " MiB", nullptr));
        splitVolumes->setText(QCoreApplication::translate("CreateArchiveExtraWidget", "Split into volumes of", nullptr));
        label->setText(QCoreApplication::translate("CreateArchiveExtraWidget", "Password:", nullptr));
        encryptFileList->setText(QCoreApplication::translate("CreateArchiveExtraWidget", "Encrypt the file list too", nullptr));
        (void)CreateArchiveExtraWidget;
    } // retranslateUi

};

namespace Ui {
    class CreateArchiveExtraWidget: public Ui_CreateArchiveExtraWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CREATE_H
