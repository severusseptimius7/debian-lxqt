# Install script for directory: /home/debian/lxqt/lxqt-archiver/lxqt-archiver-0.5.0/src

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "RelWithDebInfo")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

# Set default install directory permissions.
if(NOT DEFINED CMAKE_OBJDUMP)
  set(CMAKE_OBJDUMP "/usr/bin/objdump")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xRuntimex" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/lxqt-archiver/translations" TYPE FILE FILES
    "/home/debian/lxqt/lxqt-archiver/lxqt-archiver-0.5.0/obj-x86_64-linux-gnu/src/lxqt-archiver_ar.qm"
    "/home/debian/lxqt/lxqt-archiver/lxqt-archiver-0.5.0/obj-x86_64-linux-gnu/src/lxqt-archiver_arn.qm"
    "/home/debian/lxqt/lxqt-archiver/lxqt-archiver-0.5.0/obj-x86_64-linux-gnu/src/lxqt-archiver_ast.qm"
    "/home/debian/lxqt/lxqt-archiver/lxqt-archiver-0.5.0/obj-x86_64-linux-gnu/src/lxqt-archiver_bg.qm"
    "/home/debian/lxqt/lxqt-archiver/lxqt-archiver-0.5.0/obj-x86_64-linux-gnu/src/lxqt-archiver_ca.qm"
    "/home/debian/lxqt/lxqt-archiver/lxqt-archiver-0.5.0/obj-x86_64-linux-gnu/src/lxqt-archiver_cs.qm"
    "/home/debian/lxqt/lxqt-archiver/lxqt-archiver-0.5.0/obj-x86_64-linux-gnu/src/lxqt-archiver_cy.qm"
    "/home/debian/lxqt/lxqt-archiver/lxqt-archiver-0.5.0/obj-x86_64-linux-gnu/src/lxqt-archiver_de.qm"
    "/home/debian/lxqt/lxqt-archiver/lxqt-archiver-0.5.0/obj-x86_64-linux-gnu/src/lxqt-archiver_el.qm"
    "/home/debian/lxqt/lxqt-archiver/lxqt-archiver-0.5.0/obj-x86_64-linux-gnu/src/lxqt-archiver_en_GB.qm"
    "/home/debian/lxqt/lxqt-archiver/lxqt-archiver-0.5.0/obj-x86_64-linux-gnu/src/lxqt-archiver_es.qm"
    "/home/debian/lxqt/lxqt-archiver/lxqt-archiver-0.5.0/obj-x86_64-linux-gnu/src/lxqt-archiver_et.qm"
    "/home/debian/lxqt/lxqt-archiver/lxqt-archiver-0.5.0/obj-x86_64-linux-gnu/src/lxqt-archiver_fr.qm"
    "/home/debian/lxqt/lxqt-archiver/lxqt-archiver-0.5.0/obj-x86_64-linux-gnu/src/lxqt-archiver_gl.qm"
    "/home/debian/lxqt/lxqt-archiver/lxqt-archiver-0.5.0/obj-x86_64-linux-gnu/src/lxqt-archiver_he.qm"
    "/home/debian/lxqt/lxqt-archiver/lxqt-archiver-0.5.0/obj-x86_64-linux-gnu/src/lxqt-archiver_hr.qm"
    "/home/debian/lxqt/lxqt-archiver/lxqt-archiver-0.5.0/obj-x86_64-linux-gnu/src/lxqt-archiver_hu.qm"
    "/home/debian/lxqt/lxqt-archiver/lxqt-archiver-0.5.0/obj-x86_64-linux-gnu/src/lxqt-archiver_id.qm"
    "/home/debian/lxqt/lxqt-archiver/lxqt-archiver-0.5.0/obj-x86_64-linux-gnu/src/lxqt-archiver_it.qm"
    "/home/debian/lxqt/lxqt-archiver/lxqt-archiver-0.5.0/obj-x86_64-linux-gnu/src/lxqt-archiver_ja.qm"
    "/home/debian/lxqt/lxqt-archiver/lxqt-archiver-0.5.0/obj-x86_64-linux-gnu/src/lxqt-archiver_lt.qm"
    "/home/debian/lxqt/lxqt-archiver/lxqt-archiver-0.5.0/obj-x86_64-linux-gnu/src/lxqt-archiver_nb_NO.qm"
    "/home/debian/lxqt/lxqt-archiver/lxqt-archiver-0.5.0/obj-x86_64-linux-gnu/src/lxqt-archiver_nl.qm"
    "/home/debian/lxqt/lxqt-archiver/lxqt-archiver-0.5.0/obj-x86_64-linux-gnu/src/lxqt-archiver_oc.qm"
    "/home/debian/lxqt/lxqt-archiver/lxqt-archiver-0.5.0/obj-x86_64-linux-gnu/src/lxqt-archiver_pl.qm"
    "/home/debian/lxqt/lxqt-archiver/lxqt-archiver-0.5.0/obj-x86_64-linux-gnu/src/lxqt-archiver_pt.qm"
    "/home/debian/lxqt/lxqt-archiver/lxqt-archiver-0.5.0/obj-x86_64-linux-gnu/src/lxqt-archiver_pt_BR.qm"
    "/home/debian/lxqt/lxqt-archiver/lxqt-archiver-0.5.0/obj-x86_64-linux-gnu/src/lxqt-archiver_ru.qm"
    "/home/debian/lxqt/lxqt-archiver/lxqt-archiver-0.5.0/obj-x86_64-linux-gnu/src/lxqt-archiver_si.qm"
    "/home/debian/lxqt/lxqt-archiver/lxqt-archiver-0.5.0/obj-x86_64-linux-gnu/src/lxqt-archiver_sk.qm"
    "/home/debian/lxqt/lxqt-archiver/lxqt-archiver-0.5.0/obj-x86_64-linux-gnu/src/lxqt-archiver_sl.qm"
    "/home/debian/lxqt/lxqt-archiver/lxqt-archiver-0.5.0/obj-x86_64-linux-gnu/src/lxqt-archiver_sv.qm"
    "/home/debian/lxqt/lxqt-archiver/lxqt-archiver-0.5.0/obj-x86_64-linux-gnu/src/lxqt-archiver_tr.qm"
    "/home/debian/lxqt/lxqt-archiver/lxqt-archiver-0.5.0/obj-x86_64-linux-gnu/src/lxqt-archiver_uk.qm"
    "/home/debian/lxqt/lxqt-archiver/lxqt-archiver-0.5.0/obj-x86_64-linux-gnu/src/lxqt-archiver_vi.qm"
    "/home/debian/lxqt/lxqt-archiver/lxqt-archiver-0.5.0/obj-x86_64-linux-gnu/src/lxqt-archiver_zh_TW.qm"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xRuntimex" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/lxqt-archiver" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/lxqt-archiver")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/lxqt-archiver"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "/home/debian/lxqt/lxqt-archiver/lxqt-archiver-0.5.0/obj-x86_64-linux-gnu/src/lxqt-archiver")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/lxqt-archiver" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/lxqt-archiver")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/lxqt-archiver")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xRuntimex" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/applications" TYPE FILE FILES "/home/debian/lxqt/lxqt-archiver/lxqt-archiver-0.5.0/obj-x86_64-linux-gnu/src/lxqt-archiver.desktop")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/icons/hicolor/scalable/apps" TYPE FILE FILES "/home/debian/lxqt/lxqt-archiver/lxqt-archiver-0.5.0/src/icons/lxqt-archiver.svg")
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("/home/debian/lxqt/lxqt-archiver/lxqt-archiver-0.5.0/obj-x86_64-linux-gnu/src/core/cmake_install.cmake")

endif()

