# Install script for directory: /home/debian/lxqt/qterminal/qterminal-1.0.0

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "RelWithDebInfo")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

# Set default install directory permissions.
if(NOT DEFINED CMAKE_OBJDUMP)
  set(CMAKE_OBJDUMP "/usr/bin/objdump")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/applications" TYPE FILE FILES
    "/home/debian/lxqt/qterminal/qterminal-1.0.0/obj-x86_64-linux-gnu/qterminal.desktop"
    "/home/debian/lxqt/qterminal/qterminal-1.0.0/obj-x86_64-linux-gnu/qterminal-drop.desktop"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/metainfo" TYPE FILE FILES "/home/debian/lxqt/qterminal/qterminal-1.0.0/qterminal.appdata.xml")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/qterminal" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/qterminal")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/qterminal"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "/home/debian/lxqt/qterminal/qterminal-1.0.0/obj-x86_64-linux-gnu/qterminal")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/qterminal" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/qterminal")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/qterminal")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/share/qterminal/translations/qterminal_ar.qm;/usr/share/qterminal/translations/qterminal_arn.qm;/usr/share/qterminal/translations/qterminal_ast.qm;/usr/share/qterminal/translations/qterminal_bg.qm;/usr/share/qterminal/translations/qterminal_ca.qm;/usr/share/qterminal/translations/qterminal_cs.qm;/usr/share/qterminal/translations/qterminal_cy.qm;/usr/share/qterminal/translations/qterminal_da.qm;/usr/share/qterminal/translations/qterminal_de.qm;/usr/share/qterminal/translations/qterminal_el.qm;/usr/share/qterminal/translations/qterminal_es.qm;/usr/share/qterminal/translations/qterminal_et.qm;/usr/share/qterminal/translations/qterminal_fa.qm;/usr/share/qterminal/translations/qterminal_fi.qm;/usr/share/qterminal/translations/qterminal_fr.qm;/usr/share/qterminal/translations/qterminal_gl.qm;/usr/share/qterminal/translations/qterminal_he.qm;/usr/share/qterminal/translations/qterminal_hr.qm;/usr/share/qterminal/translations/qterminal_hu.qm;/usr/share/qterminal/translations/qterminal_id.qm;/usr/share/qterminal/translations/qterminal_it.qm;/usr/share/qterminal/translations/qterminal_ja.qm;/usr/share/qterminal/translations/qterminal_kk.qm;/usr/share/qterminal/translations/qterminal_ko_KR.qm;/usr/share/qterminal/translations/qterminal_lt.qm;/usr/share/qterminal/translations/qterminal_nb_NO.qm;/usr/share/qterminal/translations/qterminal_nl.qm;/usr/share/qterminal/translations/qterminal_oc.qm;/usr/share/qterminal/translations/qterminal_pl.qm;/usr/share/qterminal/translations/qterminal_pt.qm;/usr/share/qterminal/translations/qterminal_pt_BR.qm;/usr/share/qterminal/translations/qterminal_ru.qm;/usr/share/qterminal/translations/qterminal_si.qm;/usr/share/qterminal/translations/qterminal_sk_SK.qm;/usr/share/qterminal/translations/qterminal_tr.qm;/usr/share/qterminal/translations/qterminal_uk.qm;/usr/share/qterminal/translations/qterminal_zh_CN.qm;/usr/share/qterminal/translations/qterminal_zh_TW.qm")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/usr/share/qterminal/translations" TYPE FILE FILES
    "/home/debian/lxqt/qterminal/qterminal-1.0.0/obj-x86_64-linux-gnu/qterminal_ar.qm"
    "/home/debian/lxqt/qterminal/qterminal-1.0.0/obj-x86_64-linux-gnu/qterminal_arn.qm"
    "/home/debian/lxqt/qterminal/qterminal-1.0.0/obj-x86_64-linux-gnu/qterminal_ast.qm"
    "/home/debian/lxqt/qterminal/qterminal-1.0.0/obj-x86_64-linux-gnu/qterminal_bg.qm"
    "/home/debian/lxqt/qterminal/qterminal-1.0.0/obj-x86_64-linux-gnu/qterminal_ca.qm"
    "/home/debian/lxqt/qterminal/qterminal-1.0.0/obj-x86_64-linux-gnu/qterminal_cs.qm"
    "/home/debian/lxqt/qterminal/qterminal-1.0.0/obj-x86_64-linux-gnu/qterminal_cy.qm"
    "/home/debian/lxqt/qterminal/qterminal-1.0.0/obj-x86_64-linux-gnu/qterminal_da.qm"
    "/home/debian/lxqt/qterminal/qterminal-1.0.0/obj-x86_64-linux-gnu/qterminal_de.qm"
    "/home/debian/lxqt/qterminal/qterminal-1.0.0/obj-x86_64-linux-gnu/qterminal_el.qm"
    "/home/debian/lxqt/qterminal/qterminal-1.0.0/obj-x86_64-linux-gnu/qterminal_es.qm"
    "/home/debian/lxqt/qterminal/qterminal-1.0.0/obj-x86_64-linux-gnu/qterminal_et.qm"
    "/home/debian/lxqt/qterminal/qterminal-1.0.0/obj-x86_64-linux-gnu/qterminal_fa.qm"
    "/home/debian/lxqt/qterminal/qterminal-1.0.0/obj-x86_64-linux-gnu/qterminal_fi.qm"
    "/home/debian/lxqt/qterminal/qterminal-1.0.0/obj-x86_64-linux-gnu/qterminal_fr.qm"
    "/home/debian/lxqt/qterminal/qterminal-1.0.0/obj-x86_64-linux-gnu/qterminal_gl.qm"
    "/home/debian/lxqt/qterminal/qterminal-1.0.0/obj-x86_64-linux-gnu/qterminal_he.qm"
    "/home/debian/lxqt/qterminal/qterminal-1.0.0/obj-x86_64-linux-gnu/qterminal_hr.qm"
    "/home/debian/lxqt/qterminal/qterminal-1.0.0/obj-x86_64-linux-gnu/qterminal_hu.qm"
    "/home/debian/lxqt/qterminal/qterminal-1.0.0/obj-x86_64-linux-gnu/qterminal_id.qm"
    "/home/debian/lxqt/qterminal/qterminal-1.0.0/obj-x86_64-linux-gnu/qterminal_it.qm"
    "/home/debian/lxqt/qterminal/qterminal-1.0.0/obj-x86_64-linux-gnu/qterminal_ja.qm"
    "/home/debian/lxqt/qterminal/qterminal-1.0.0/obj-x86_64-linux-gnu/qterminal_kk.qm"
    "/home/debian/lxqt/qterminal/qterminal-1.0.0/obj-x86_64-linux-gnu/qterminal_ko_KR.qm"
    "/home/debian/lxqt/qterminal/qterminal-1.0.0/obj-x86_64-linux-gnu/qterminal_lt.qm"
    "/home/debian/lxqt/qterminal/qterminal-1.0.0/obj-x86_64-linux-gnu/qterminal_nb_NO.qm"
    "/home/debian/lxqt/qterminal/qterminal-1.0.0/obj-x86_64-linux-gnu/qterminal_nl.qm"
    "/home/debian/lxqt/qterminal/qterminal-1.0.0/obj-x86_64-linux-gnu/qterminal_oc.qm"
    "/home/debian/lxqt/qterminal/qterminal-1.0.0/obj-x86_64-linux-gnu/qterminal_pl.qm"
    "/home/debian/lxqt/qterminal/qterminal-1.0.0/obj-x86_64-linux-gnu/qterminal_pt.qm"
    "/home/debian/lxqt/qterminal/qterminal-1.0.0/obj-x86_64-linux-gnu/qterminal_pt_BR.qm"
    "/home/debian/lxqt/qterminal/qterminal-1.0.0/obj-x86_64-linux-gnu/qterminal_ru.qm"
    "/home/debian/lxqt/qterminal/qterminal-1.0.0/obj-x86_64-linux-gnu/qterminal_si.qm"
    "/home/debian/lxqt/qterminal/qterminal-1.0.0/obj-x86_64-linux-gnu/qterminal_sk_SK.qm"
    "/home/debian/lxqt/qterminal/qterminal-1.0.0/obj-x86_64-linux-gnu/qterminal_tr.qm"
    "/home/debian/lxqt/qterminal/qterminal-1.0.0/obj-x86_64-linux-gnu/qterminal_uk.qm"
    "/home/debian/lxqt/qterminal/qterminal-1.0.0/obj-x86_64-linux-gnu/qterminal_zh_CN.qm"
    "/home/debian/lxqt/qterminal/qterminal-1.0.0/obj-x86_64-linux-gnu/qterminal_zh_TW.qm"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/icons/hicolor/64x64/apps" TYPE FILE FILES "/home/debian/lxqt/qterminal/qterminal-1.0.0/src/icons/qterminal.png")
endif()

if(CMAKE_INSTALL_COMPONENT)
  set(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
else()
  set(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
endif()

string(REPLACE ";" "\n" CMAKE_INSTALL_MANIFEST_CONTENT
       "${CMAKE_INSTALL_MANIFEST_FILES}")
file(WRITE "/home/debian/lxqt/qterminal/qterminal-1.0.0/obj-x86_64-linux-gnu/${CMAKE_INSTALL_MANIFEST}"
     "${CMAKE_INSTALL_MANIFEST_CONTENT}")
