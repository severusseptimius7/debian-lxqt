/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.15.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../src/mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.15.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[31];
    char stringdata0[454];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 8), // "showHide"
QT_MOC_LITERAL(2, 20, 0), // ""
QT_MOC_LITERAL(3, 21, 21), // "updateDisabledActions"
QT_MOC_LITERAL(4, 43, 34), // "on_consoleTabulator_currentCh..."
QT_MOC_LITERAL(5, 78, 17), // "propertiesChanged"
QT_MOC_LITERAL(6, 96, 18), // "actAbout_triggered"
QT_MOC_LITERAL(7, 115, 23), // "actProperties_triggered"
QT_MOC_LITERAL(8, 139, 17), // "updateActionGroup"
QT_MOC_LITERAL(9, 157, 8), // "QAction*"
QT_MOC_LITERAL(10, 166, 9), // "testClose"
QT_MOC_LITERAL(11, 176, 14), // "removeFinished"
QT_MOC_LITERAL(12, 191, 15), // "toggleBookmarks"
QT_MOC_LITERAL(13, 207, 16), // "toggleBorderless"
QT_MOC_LITERAL(14, 224, 12), // "toggleTabBar"
QT_MOC_LITERAL(15, 237, 10), // "toggleMenu"
QT_MOC_LITERAL(16, 248, 14), // "showFullscreen"
QT_MOC_LITERAL(17, 263, 10), // "fullscreen"
QT_MOC_LITERAL(18, 274, 11), // "setKeepOpen"
QT_MOC_LITERAL(19, 286, 5), // "value"
QT_MOC_LITERAL(20, 292, 4), // "find"
QT_MOC_LITERAL(21, 297, 17), // "newTerminalWindow"
QT_MOC_LITERAL(22, 315, 27), // "bookmarksWidget_callCommand"
QT_MOC_LITERAL(23, 343, 31), // "bookmarksDock_visibilityChanged"
QT_MOC_LITERAL(24, 375, 7), // "visible"
QT_MOC_LITERAL(25, 383, 9), // "addNewTab"
QT_MOC_LITERAL(26, 393, 14), // "TerminalConfig"
QT_MOC_LITERAL(27, 408, 3), // "cfg"
QT_MOC_LITERAL(28, 412, 21), // "onCurrentTitleChanged"
QT_MOC_LITERAL(29, 434, 5), // "index"
QT_MOC_LITERAL(30, 440, 13) // "handleHistory"

    },
    "MainWindow\0showHide\0\0updateDisabledActions\0"
    "on_consoleTabulator_currentChanged\0"
    "propertiesChanged\0actAbout_triggered\0"
    "actProperties_triggered\0updateActionGroup\0"
    "QAction*\0testClose\0removeFinished\0"
    "toggleBookmarks\0toggleBorderless\0"
    "toggleTabBar\0toggleMenu\0showFullscreen\0"
    "fullscreen\0setKeepOpen\0value\0find\0"
    "newTerminalWindow\0bookmarksWidget_callCommand\0"
    "bookmarksDock_visibilityChanged\0visible\0"
    "addNewTab\0TerminalConfig\0cfg\0"
    "onCurrentTitleChanged\0index\0handleHistory"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      22,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  124,    2, 0x0a /* Public */,
       3,    0,  125,    2, 0x0a /* Public */,
       4,    1,  126,    2, 0x08 /* Private */,
       5,    0,  129,    2, 0x08 /* Private */,
       6,    0,  130,    2, 0x08 /* Private */,
       7,    0,  131,    2, 0x08 /* Private */,
       8,    1,  132,    2, 0x08 /* Private */,
      10,    1,  135,    2, 0x08 /* Private */,
      12,    0,  138,    2, 0x08 /* Private */,
      13,    0,  139,    2, 0x08 /* Private */,
      14,    0,  140,    2, 0x08 /* Private */,
      15,    0,  141,    2, 0x08 /* Private */,
      16,    1,  142,    2, 0x08 /* Private */,
      18,    1,  145,    2, 0x08 /* Private */,
      20,    0,  148,    2, 0x08 /* Private */,
      21,    0,  149,    2, 0x08 /* Private */,
      22,    1,  150,    2, 0x08 /* Private */,
      23,    1,  153,    2, 0x08 /* Private */,
      25,    1,  156,    2, 0x08 /* Private */,
      25,    0,  159,    2, 0x28 /* Private | MethodCloned */,
      28,    1,  160,    2, 0x08 /* Private */,
      30,    0,  163,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 9,    2,
    QMetaType::Void, QMetaType::Bool,   11,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   17,
    QMetaType::Void, QMetaType::Bool,   19,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void, QMetaType::Bool,   24,
    QMetaType::Void, 0x80000000 | 26,   27,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   29,
    QMetaType::Void,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->showHide(); break;
        case 1: _t->updateDisabledActions(); break;
        case 2: _t->on_consoleTabulator_currentChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->propertiesChanged(); break;
        case 4: _t->actAbout_triggered(); break;
        case 5: _t->actProperties_triggered(); break;
        case 6: _t->updateActionGroup((*reinterpret_cast< QAction*(*)>(_a[1]))); break;
        case 7: _t->testClose((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 8: _t->toggleBookmarks(); break;
        case 9: _t->toggleBorderless(); break;
        case 10: _t->toggleTabBar(); break;
        case 11: _t->toggleMenu(); break;
        case 12: _t->showFullscreen((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 13: _t->setKeepOpen((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 14: _t->find(); break;
        case 15: _t->newTerminalWindow(); break;
        case 16: _t->bookmarksWidget_callCommand((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 17: _t->bookmarksDock_visibilityChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 18: _t->addNewTab((*reinterpret_cast< TerminalConfig(*)>(_a[1]))); break;
        case 19: _t->addNewTab(); break;
        case 20: _t->onCurrentTitleChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 21: _t->handleHistory(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject MainWindow::staticMetaObject = { {
    QMetaObject::SuperData::link<QMainWindow::staticMetaObject>(),
    qt_meta_stringdata_MainWindow.data,
    qt_meta_data_MainWindow,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "DBusAddressable"))
        return static_cast< DBusAddressable*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 22)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 22;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 22)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 22;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
