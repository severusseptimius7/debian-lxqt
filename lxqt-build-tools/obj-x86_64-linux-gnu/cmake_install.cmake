# Install script for directory: /home/debian/lxqt/build-tools/lxqt-build-tools-0.10.0

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "RelWithDebInfo")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

# Set default install directory permissions.
if(NOT DEFINED CMAKE_OBJDUMP)
  set(CMAKE_OBJDUMP "/usr/bin/objdump")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/cmake/lxqt-build-tools" TYPE FILE FILES
    "/home/debian/lxqt/build-tools/lxqt-build-tools-0.10.0/obj-x86_64-linux-gnu/install/lxqt-build-tools-config.cmake"
    "/home/debian/lxqt/build-tools/lxqt-build-tools-0.10.0/obj-x86_64-linux-gnu/lxqt-build-tools-config-version.cmake"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/cmake/lxqt-build-tools/find-modules" TYPE FILE FILES
    "/home/debian/lxqt/build-tools/lxqt-build-tools-0.10.0/cmake/find-modules/FindClazy.cmake"
    "/home/debian/lxqt/build-tools/lxqt-build-tools-0.10.0/cmake/find-modules/FindExif.cmake"
    "/home/debian/lxqt/build-tools/lxqt-build-tools-0.10.0/cmake/find-modules/FindFontconfig.cmake"
    "/home/debian/lxqt/build-tools/lxqt-build-tools-0.10.0/cmake/find-modules/FindGLIB.cmake"
    "/home/debian/lxqt/build-tools/lxqt-build-tools-0.10.0/cmake/find-modules/FindMenuCache.cmake"
    "/home/debian/lxqt/build-tools/lxqt-build-tools-0.10.0/cmake/find-modules/FindUDev.cmake"
    "/home/debian/lxqt/build-tools/lxqt-build-tools-0.10.0/cmake/find-modules/FindXCB.cmake"
    "/home/debian/lxqt/build-tools/lxqt-build-tools-0.10.0/cmake/find-modules/FindXKBCommon.cmake"
    "/home/debian/lxqt/build-tools/lxqt-build-tools-0.10.0/cmake/find-modules/FindXTerm.cmake"
    "/home/debian/lxqt/build-tools/lxqt-build-tools-0.10.0/cmake/find-modules/FindXdgEmail.cmake"
    "/home/debian/lxqt/build-tools/lxqt-build-tools-0.10.0/cmake/find-modules/FindXdgMime.cmake"
    "/home/debian/lxqt/build-tools/lxqt-build-tools-0.10.0/cmake/find-modules/FindXdgOpen.cmake"
    "/home/debian/lxqt/build-tools/lxqt-build-tools-0.10.0/cmake/find-modules/FindXdgScreensaver.cmake"
    "/home/debian/lxqt/build-tools/lxqt-build-tools-0.10.0/cmake/find-modules/FindXdgSettings.cmake"
    "/home/debian/lxqt/build-tools/lxqt-build-tools-0.10.0/cmake/find-modules/FindXdgUserDirs.cmake"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/cmake/lxqt-build-tools/modules" TYPE FILE FILES
    "/home/debian/lxqt/build-tools/lxqt-build-tools-0.10.0/cmake/modules/ECMFindModuleHelpers.cmake"
    "/home/debian/lxqt/build-tools/lxqt-build-tools-0.10.0/cmake/modules/LXQtAppTranslationLoader.cpp.in"
    "/home/debian/lxqt/build-tools/lxqt-build-tools-0.10.0/cmake/modules/LXQtCompilerSettings.cmake"
    "/home/debian/lxqt/build-tools/lxqt-build-tools-0.10.0/cmake/modules/LXQtCreatePkgConfigFile.cmake"
    "/home/debian/lxqt/build-tools/lxqt-build-tools-0.10.0/cmake/modules/LXQtCreatePortableHeaders.cmake"
    "/home/debian/lxqt/build-tools/lxqt-build-tools-0.10.0/cmake/modules/LXQtLibTranslationLoader.cpp.in"
    "/home/debian/lxqt/build-tools/lxqt-build-tools-0.10.0/cmake/modules/LXQtPluginTranslationLoader.cpp.in"
    "/home/debian/lxqt/build-tools/lxqt-build-tools-0.10.0/cmake/modules/LXQtPreventInSourceBuilds.cmake"
    "/home/debian/lxqt/build-tools/lxqt-build-tools-0.10.0/cmake/modules/LXQtTranslate.cmake"
    "/home/debian/lxqt/build-tools/lxqt-build-tools-0.10.0/cmake/modules/LXQtTranslateDesktop.cmake"
    "/home/debian/lxqt/build-tools/lxqt-build-tools-0.10.0/cmake/modules/LXQtTranslationLoader.cmake"
    "/home/debian/lxqt/build-tools/lxqt-build-tools-0.10.0/cmake/modules/Qt5TranslationLoader.cmake"
    "/home/debian/lxqt/build-tools/lxqt-build-tools-0.10.0/cmake/modules/Qt5TranslationLoader.cpp.in"
    "/home/debian/lxqt/build-tools/lxqt-build-tools-0.10.0/cmake/modules/LXQtTranslateDesktopYaml.pl"
    "/home/debian/lxqt/build-tools/lxqt-build-tools-0.10.0/obj-x86_64-linux-gnu/install/LXQtConfigVars.cmake"
    "/home/debian/lxqt/build-tools/lxqt-build-tools-0.10.0/obj-x86_64-linux-gnu/install/LXQtTranslateTs.cmake"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE PROGRAM FILES "/home/debian/lxqt/build-tools/lxqt-build-tools-0.10.0/lxqt-transupdate")
endif()

if(CMAKE_INSTALL_COMPONENT)
  set(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
else()
  set(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
endif()

string(REPLACE ";" "\n" CMAKE_INSTALL_MANIFEST_CONTENT
       "${CMAKE_INSTALL_MANIFEST_FILES}")
file(WRITE "/home/debian/lxqt/build-tools/lxqt-build-tools-0.10.0/obj-x86_64-linux-gnu/${CMAKE_INSTALL_MANIFEST}"
     "${CMAKE_INSTALL_MANIFEST_CONTENT}")
