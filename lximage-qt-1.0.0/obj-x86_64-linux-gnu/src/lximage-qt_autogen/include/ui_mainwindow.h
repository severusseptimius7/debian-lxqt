/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>
#include "imageview.h"
#include "mrumenu.h"
#include "statusbar.h"

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionAbout;
    QAction *actionHiddenShortcuts;
    QAction *actionOpenFile;
    QAction *actionReload;
    QAction *actionSave;
    QAction *actionSaveAs;
    QAction *actionClose;
    QAction *actionZoomIn;
    QAction *actionZoomOut;
    QAction *actionCopy;
    QAction *actionNext;
    QAction *actionPrevious;
    QAction *actionOriginalSize;
    QAction *actionZoomFit;
    QAction *actionRotateClockwise;
    QAction *actionRotateCounterclockwise;
    QAction *actionPreferences;
    QAction *actionPrint;
    QAction *actionFirst;
    QAction *actionLast;
    QAction *actionNewWindow;
    QAction *actionFlipHorizontal;
    QAction *actionScreenshot;
    QAction *actionFullScreen;
    QAction *actionFlipVertical;
    QAction *actionPaste;
    QAction *actionSlideShow;
    QAction *actionDelete;
    QAction *actionShowThumbnails;
    QAction *actionFileProperties;
    QAction *actionOpenDirectory;
    QAction *actionUpload;
    QAction *actionShowExifData;
    QAction *actionDrawNone;
    QAction *actionDrawArrow;
    QAction *actionDrawRectangle;
    QAction *actionDrawCircle;
    QAction *actionDrawNumber;
    QAction *actionMenubar;
    QAction *actionToolbar;
    QAction *actionAnnotations;
    QAction *actionShowOutline;
    QAction *actionCopyPath;
    QAction *actionRenameFile;
    QAction *actionResize;
    QWidget *centralWidget;
    QHBoxLayout *horizontalLayout;
    LxImage::ImageView *view;
    QMenuBar *menubar;
    QMenu *menu_File;
    QMenu *openWithMenu;
    LxImage::MruMenu *menuRecently_Opened_Files;
    QMenu *menu_Help;
    QMenu *menuGo;
    QMenu *menu_View;
    QMenu *menu_Edit;
    QToolBar *toolBar;
    LxImage::StatusBar *statusBar;
    QToolBar *annotationsToolBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(543, 425);
        QIcon icon;
        QString iconThemeName = QString::fromUtf8("lximage-qt");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon = QIcon::fromTheme(iconThemeName);
        } else {
            icon.addFile(QString::fromUtf8("."), QSize(), QIcon::Normal, QIcon::Off);
        }
        MainWindow->setWindowIcon(icon);
        actionAbout = new QAction(MainWindow);
        actionAbout->setObjectName(QString::fromUtf8("actionAbout"));
        QIcon icon1;
        iconThemeName = QString::fromUtf8("help-about");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon1 = QIcon::fromTheme(iconThemeName);
        } else {
            icon1.addFile(QString::fromUtf8("."), QSize(), QIcon::Normal, QIcon::Off);
        }
        actionAbout->setIcon(icon1);
        actionHiddenShortcuts = new QAction(MainWindow);
        actionHiddenShortcuts->setObjectName(QString::fromUtf8("actionHiddenShortcuts"));
        actionOpenFile = new QAction(MainWindow);
        actionOpenFile->setObjectName(QString::fromUtf8("actionOpenFile"));
        QIcon icon2;
        iconThemeName = QString::fromUtf8("document-open");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon2 = QIcon::fromTheme(iconThemeName);
        } else {
            icon2.addFile(QString::fromUtf8("."), QSize(), QIcon::Normal, QIcon::Off);
        }
        actionOpenFile->setIcon(icon2);
        actionReload = new QAction(MainWindow);
        actionReload->setObjectName(QString::fromUtf8("actionReload"));
        QIcon icon3;
        iconThemeName = QString::fromUtf8("view-refresh");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon3 = QIcon::fromTheme(iconThemeName);
        } else {
            icon3.addFile(QString::fromUtf8("."), QSize(), QIcon::Normal, QIcon::Off);
        }
        actionReload->setIcon(icon3);
        actionSave = new QAction(MainWindow);
        actionSave->setObjectName(QString::fromUtf8("actionSave"));
        QIcon icon4;
        iconThemeName = QString::fromUtf8("document-save");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon4 = QIcon::fromTheme(iconThemeName);
        } else {
            icon4.addFile(QString::fromUtf8("."), QSize(), QIcon::Normal, QIcon::Off);
        }
        actionSave->setIcon(icon4);
        actionSaveAs = new QAction(MainWindow);
        actionSaveAs->setObjectName(QString::fromUtf8("actionSaveAs"));
        QIcon icon5;
        iconThemeName = QString::fromUtf8("document-save-as");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon5 = QIcon::fromTheme(iconThemeName);
        } else {
            icon5.addFile(QString::fromUtf8("."), QSize(), QIcon::Normal, QIcon::Off);
        }
        actionSaveAs->setIcon(icon5);
        actionClose = new QAction(MainWindow);
        actionClose->setObjectName(QString::fromUtf8("actionClose"));
        QIcon icon6;
        iconThemeName = QString::fromUtf8("application-exit");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon6 = QIcon::fromTheme(iconThemeName);
        } else {
            icon6.addFile(QString::fromUtf8("."), QSize(), QIcon::Normal, QIcon::Off);
        }
        actionClose->setIcon(icon6);
        actionZoomIn = new QAction(MainWindow);
        actionZoomIn->setObjectName(QString::fromUtf8("actionZoomIn"));
        QIcon icon7;
        iconThemeName = QString::fromUtf8("zoom-in");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon7 = QIcon::fromTheme(iconThemeName);
        } else {
            icon7.addFile(QString::fromUtf8("."), QSize(), QIcon::Normal, QIcon::Off);
        }
        actionZoomIn->setIcon(icon7);
        actionZoomOut = new QAction(MainWindow);
        actionZoomOut->setObjectName(QString::fromUtf8("actionZoomOut"));
        QIcon icon8;
        iconThemeName = QString::fromUtf8("zoom-out");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon8 = QIcon::fromTheme(iconThemeName);
        } else {
            icon8.addFile(QString::fromUtf8("."), QSize(), QIcon::Normal, QIcon::Off);
        }
        actionZoomOut->setIcon(icon8);
        actionCopy = new QAction(MainWindow);
        actionCopy->setObjectName(QString::fromUtf8("actionCopy"));
        QIcon icon9;
        iconThemeName = QString::fromUtf8("edit-copy");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon9 = QIcon::fromTheme(iconThemeName);
        } else {
            icon9.addFile(QString::fromUtf8("."), QSize(), QIcon::Normal, QIcon::Off);
        }
        actionCopy->setIcon(icon9);
        actionNext = new QAction(MainWindow);
        actionNext->setObjectName(QString::fromUtf8("actionNext"));
        QIcon icon10;
        iconThemeName = QString::fromUtf8("go-next");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon10 = QIcon::fromTheme(iconThemeName);
        } else {
            icon10.addFile(QString::fromUtf8("."), QSize(), QIcon::Normal, QIcon::Off);
        }
        actionNext->setIcon(icon10);
        actionPrevious = new QAction(MainWindow);
        actionPrevious->setObjectName(QString::fromUtf8("actionPrevious"));
        QIcon icon11;
        iconThemeName = QString::fromUtf8("go-previous");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon11 = QIcon::fromTheme(iconThemeName);
        } else {
            icon11.addFile(QString::fromUtf8("."), QSize(), QIcon::Normal, QIcon::Off);
        }
        actionPrevious->setIcon(icon11);
        actionOriginalSize = new QAction(MainWindow);
        actionOriginalSize->setObjectName(QString::fromUtf8("actionOriginalSize"));
        actionOriginalSize->setCheckable(true);
        QIcon icon12;
        iconThemeName = QString::fromUtf8("zoom-original");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon12 = QIcon::fromTheme(iconThemeName);
        } else {
            icon12.addFile(QString::fromUtf8("."), QSize(), QIcon::Normal, QIcon::Off);
        }
        actionOriginalSize->setIcon(icon12);
        actionZoomFit = new QAction(MainWindow);
        actionZoomFit->setObjectName(QString::fromUtf8("actionZoomFit"));
        actionZoomFit->setCheckable(true);
        QIcon icon13;
        iconThemeName = QString::fromUtf8("zoom-fit-best");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon13 = QIcon::fromTheme(iconThemeName);
        } else {
            icon13.addFile(QString::fromUtf8("."), QSize(), QIcon::Normal, QIcon::Off);
        }
        actionZoomFit->setIcon(icon13);
        actionRotateClockwise = new QAction(MainWindow);
        actionRotateClockwise->setObjectName(QString::fromUtf8("actionRotateClockwise"));
        QIcon icon14;
        iconThemeName = QString::fromUtf8("object-rotate-right");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon14 = QIcon::fromTheme(iconThemeName);
        } else {
            icon14.addFile(QString::fromUtf8("."), QSize(), QIcon::Normal, QIcon::Off);
        }
        actionRotateClockwise->setIcon(icon14);
        actionRotateCounterclockwise = new QAction(MainWindow);
        actionRotateCounterclockwise->setObjectName(QString::fromUtf8("actionRotateCounterclockwise"));
        QIcon icon15;
        iconThemeName = QString::fromUtf8("object-rotate-left");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon15 = QIcon::fromTheme(iconThemeName);
        } else {
            icon15.addFile(QString::fromUtf8("."), QSize(), QIcon::Normal, QIcon::Off);
        }
        actionRotateCounterclockwise->setIcon(icon15);
        actionPreferences = new QAction(MainWindow);
        actionPreferences->setObjectName(QString::fromUtf8("actionPreferences"));
        actionPrint = new QAction(MainWindow);
        actionPrint->setObjectName(QString::fromUtf8("actionPrint"));
        actionFirst = new QAction(MainWindow);
        actionFirst->setObjectName(QString::fromUtf8("actionFirst"));
        QIcon icon16;
        iconThemeName = QString::fromUtf8("go-first");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon16 = QIcon::fromTheme(iconThemeName);
        } else {
            icon16.addFile(QString::fromUtf8("."), QSize(), QIcon::Normal, QIcon::Off);
        }
        actionFirst->setIcon(icon16);
        actionLast = new QAction(MainWindow);
        actionLast->setObjectName(QString::fromUtf8("actionLast"));
        QIcon icon17;
        iconThemeName = QString::fromUtf8("go-last");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon17 = QIcon::fromTheme(iconThemeName);
        } else {
            icon17.addFile(QString::fromUtf8("."), QSize(), QIcon::Normal, QIcon::Off);
        }
        actionLast->setIcon(icon17);
        actionNewWindow = new QAction(MainWindow);
        actionNewWindow->setObjectName(QString::fromUtf8("actionNewWindow"));
        QIcon icon18;
        iconThemeName = QString::fromUtf8("document-new");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon18 = QIcon::fromTheme(iconThemeName);
        } else {
            icon18.addFile(QString::fromUtf8("."), QSize(), QIcon::Normal, QIcon::Off);
        }
        actionNewWindow->setIcon(icon18);
        actionFlipHorizontal = new QAction(MainWindow);
        actionFlipHorizontal->setObjectName(QString::fromUtf8("actionFlipHorizontal"));
        actionScreenshot = new QAction(MainWindow);
        actionScreenshot->setObjectName(QString::fromUtf8("actionScreenshot"));
        QIcon icon19;
        iconThemeName = QString::fromUtf8("camera-photo");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon19 = QIcon::fromTheme(iconThemeName);
        } else {
            icon19.addFile(QString::fromUtf8("."), QSize(), QIcon::Normal, QIcon::Off);
        }
        actionScreenshot->setIcon(icon19);
        actionFullScreen = new QAction(MainWindow);
        actionFullScreen->setObjectName(QString::fromUtf8("actionFullScreen"));
        actionFullScreen->setCheckable(true);
        actionFlipVertical = new QAction(MainWindow);
        actionFlipVertical->setObjectName(QString::fromUtf8("actionFlipVertical"));
        actionPaste = new QAction(MainWindow);
        actionPaste->setObjectName(QString::fromUtf8("actionPaste"));
        QIcon icon20;
        iconThemeName = QString::fromUtf8("edit-paste");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon20 = QIcon::fromTheme(iconThemeName);
        } else {
            icon20.addFile(QString::fromUtf8("."), QSize(), QIcon::Normal, QIcon::Off);
        }
        actionPaste->setIcon(icon20);
        actionSlideShow = new QAction(MainWindow);
        actionSlideShow->setObjectName(QString::fromUtf8("actionSlideShow"));
        actionSlideShow->setCheckable(true);
        QIcon icon21;
        iconThemeName = QString::fromUtf8("media-playback-start");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon21 = QIcon::fromTheme(iconThemeName);
        } else {
            icon21.addFile(QString::fromUtf8("."), QSize(), QIcon::Normal, QIcon::Off);
        }
        actionSlideShow->setIcon(icon21);
        actionDelete = new QAction(MainWindow);
        actionDelete->setObjectName(QString::fromUtf8("actionDelete"));
        QIcon icon22;
        iconThemeName = QString::fromUtf8("edit-delete");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon22 = QIcon::fromTheme(iconThemeName);
        } else {
            icon22.addFile(QString::fromUtf8("."), QSize(), QIcon::Normal, QIcon::Off);
        }
        actionDelete->setIcon(icon22);
        actionShowThumbnails = new QAction(MainWindow);
        actionShowThumbnails->setObjectName(QString::fromUtf8("actionShowThumbnails"));
        actionShowThumbnails->setCheckable(true);
        actionFileProperties = new QAction(MainWindow);
        actionFileProperties->setObjectName(QString::fromUtf8("actionFileProperties"));
        actionOpenDirectory = new QAction(MainWindow);
        actionOpenDirectory->setObjectName(QString::fromUtf8("actionOpenDirectory"));
        actionOpenDirectory->setIcon(icon2);
        actionUpload = new QAction(MainWindow);
        actionUpload->setObjectName(QString::fromUtf8("actionUpload"));
        QIcon icon23;
        iconThemeName = QString::fromUtf8("upload-media");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon23 = QIcon::fromTheme(iconThemeName);
        } else {
            icon23.addFile(QString::fromUtf8("."), QSize(), QIcon::Normal, QIcon::Off);
        }
        actionUpload->setIcon(icon23);
        actionShowExifData = new QAction(MainWindow);
        actionShowExifData->setObjectName(QString::fromUtf8("actionShowExifData"));
        actionShowExifData->setCheckable(true);
        actionDrawNone = new QAction(MainWindow);
        actionDrawNone->setObjectName(QString::fromUtf8("actionDrawNone"));
        actionDrawNone->setCheckable(true);
        QIcon icon24;
        icon24.addFile(QString::fromUtf8(":/img/draw-none.svg"), QSize(), QIcon::Normal, QIcon::Off);
        actionDrawNone->setIcon(icon24);
        actionDrawArrow = new QAction(MainWindow);
        actionDrawArrow->setObjectName(QString::fromUtf8("actionDrawArrow"));
        actionDrawArrow->setCheckable(true);
        QIcon icon25;
        icon25.addFile(QString::fromUtf8(":/img/draw-arrow.svg"), QSize(), QIcon::Normal, QIcon::Off);
        actionDrawArrow->setIcon(icon25);
        actionDrawRectangle = new QAction(MainWindow);
        actionDrawRectangle->setObjectName(QString::fromUtf8("actionDrawRectangle"));
        actionDrawRectangle->setCheckable(true);
        QIcon icon26;
        icon26.addFile(QString::fromUtf8(":/img/draw-rectangle.svg"), QSize(), QIcon::Normal, QIcon::Off);
        actionDrawRectangle->setIcon(icon26);
        actionDrawCircle = new QAction(MainWindow);
        actionDrawCircle->setObjectName(QString::fromUtf8("actionDrawCircle"));
        actionDrawCircle->setCheckable(true);
        QIcon icon27;
        icon27.addFile(QString::fromUtf8(":/img/draw-circle.svg"), QSize(), QIcon::Normal, QIcon::Off);
        actionDrawCircle->setIcon(icon27);
        actionDrawNumber = new QAction(MainWindow);
        actionDrawNumber->setObjectName(QString::fromUtf8("actionDrawNumber"));
        actionDrawNumber->setCheckable(true);
        QIcon icon28;
        icon28.addFile(QString::fromUtf8(":/img/draw-number.svg"), QSize(), QIcon::Normal, QIcon::Off);
        actionDrawNumber->setIcon(icon28);
        actionMenubar = new QAction(MainWindow);
        actionMenubar->setObjectName(QString::fromUtf8("actionMenubar"));
        actionMenubar->setCheckable(true);
        actionToolbar = new QAction(MainWindow);
        actionToolbar->setObjectName(QString::fromUtf8("actionToolbar"));
        actionToolbar->setCheckable(true);
        actionAnnotations = new QAction(MainWindow);
        actionAnnotations->setObjectName(QString::fromUtf8("actionAnnotations"));
        actionAnnotations->setCheckable(true);
        actionShowOutline = new QAction(MainWindow);
        actionShowOutline->setObjectName(QString::fromUtf8("actionShowOutline"));
        actionShowOutline->setCheckable(true);
        actionCopyPath = new QAction(MainWindow);
        actionCopyPath->setObjectName(QString::fromUtf8("actionCopyPath"));
        QIcon icon29(QIcon::fromTheme(QString::fromUtf8("edit-copy")));
        actionCopyPath->setIcon(icon29);
        actionRenameFile = new QAction(MainWindow);
        actionRenameFile->setObjectName(QString::fromUtf8("actionRenameFile"));
        QIcon icon30(QIcon::fromTheme(QString::fromUtf8("edit-rename")));
        actionRenameFile->setIcon(icon30);
        actionResize = new QAction(MainWindow);
        actionResize->setObjectName(QString::fromUtf8("actionResize"));
        QIcon icon31;
        icon31.addFile(QString::fromUtf8("."), QSize(), QIcon::Normal, QIcon::Off);
        actionResize->setIcon(icon31);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        horizontalLayout = new QHBoxLayout(centralWidget);
        horizontalLayout->setSpacing(0);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        view = new LxImage::ImageView(centralWidget);
        view->setObjectName(QString::fromUtf8("view"));
        view->setRenderHints(QPainter::Antialiasing|QPainter::HighQualityAntialiasing|QPainter::TextAntialiasing);
        view->setDragMode(QGraphicsView::ScrollHandDrag);
        view->setTransformationAnchor(QGraphicsView::AnchorUnderMouse);

        horizontalLayout->addWidget(view);

        MainWindow->setCentralWidget(centralWidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 543, 22));
        menu_File = new QMenu(menubar);
        menu_File->setObjectName(QString::fromUtf8("menu_File"));
        openWithMenu = new QMenu(menu_File);
        openWithMenu->setObjectName(QString::fromUtf8("openWithMenu"));
        menuRecently_Opened_Files = new LxImage::MruMenu(menu_File);
        menuRecently_Opened_Files->setObjectName(QString::fromUtf8("menuRecently_Opened_Files"));
        menu_Help = new QMenu(menubar);
        menu_Help->setObjectName(QString::fromUtf8("menu_Help"));
        menuGo = new QMenu(menubar);
        menuGo->setObjectName(QString::fromUtf8("menuGo"));
        menu_View = new QMenu(menubar);
        menu_View->setObjectName(QString::fromUtf8("menu_View"));
        menu_Edit = new QMenu(menubar);
        menu_Edit->setObjectName(QString::fromUtf8("menu_Edit"));
        MainWindow->setMenuBar(menubar);
        toolBar = new QToolBar(MainWindow);
        toolBar->setObjectName(QString::fromUtf8("toolBar"));
        toolBar->setToolButtonStyle(Qt::ToolButtonIconOnly);
        toolBar->setFloatable(false);
        MainWindow->addToolBar(Qt::TopToolBarArea, toolBar);
        statusBar = new LxImage::StatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);
        annotationsToolBar = new QToolBar(MainWindow);
        annotationsToolBar->setObjectName(QString::fromUtf8("annotationsToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, annotationsToolBar);

        menubar->addAction(menu_File->menuAction());
        menubar->addAction(menu_Edit->menuAction());
        menubar->addAction(menu_View->menuAction());
        menubar->addAction(menuGo->menuAction());
        menubar->addAction(menu_Help->menuAction());
        menu_File->addAction(actionNewWindow);
        menu_File->addAction(actionOpenFile);
        menu_File->addAction(actionOpenDirectory);
        menu_File->addSeparator();
        menu_File->addAction(openWithMenu->menuAction());
        menu_File->addSeparator();
        menu_File->addAction(menuRecently_Opened_Files->menuAction());
        menu_File->addAction(actionReload);
        menu_File->addAction(actionScreenshot);
        menu_File->addAction(actionSave);
        menu_File->addAction(actionSaveAs);
        menu_File->addAction(actionDelete);
        menu_File->addSeparator();
        menu_File->addAction(actionFileProperties);
        menu_File->addAction(actionPrint);
        menu_File->addSeparator();
        menu_File->addAction(actionClose);
        menu_Help->addAction(actionAbout);
        menu_Help->addAction(actionHiddenShortcuts);
        menuGo->addAction(actionPrevious);
        menuGo->addAction(actionNext);
        menuGo->addAction(actionFirst);
        menuGo->addAction(actionLast);
        menu_View->addAction(actionZoomIn);
        menu_View->addAction(actionZoomOut);
        menu_View->addAction(actionOriginalSize);
        menu_View->addAction(actionZoomFit);
        menu_View->addSeparator();
        menu_View->addAction(actionShowThumbnails);
        menu_View->addAction(actionShowOutline);
        menu_View->addAction(actionShowExifData);
        menu_View->addSeparator();
        menu_View->addAction(actionFullScreen);
        menu_View->addAction(actionSlideShow);
        menu_View->addSeparator();
        menu_View->addAction(actionMenubar);
        menu_View->addAction(actionToolbar);
        menu_View->addAction(actionAnnotations);
        menu_Edit->addAction(actionRotateClockwise);
        menu_Edit->addAction(actionRotateCounterclockwise);
        menu_Edit->addAction(actionFlipHorizontal);
        menu_Edit->addAction(actionFlipVertical);
        menu_Edit->addAction(actionResize);
        menu_Edit->addSeparator();
        menu_Edit->addAction(actionCopy);
        menu_Edit->addAction(actionPaste);
        menu_Edit->addSeparator();
        menu_Edit->addAction(actionCopyPath);
        menu_Edit->addAction(actionRenameFile);
        menu_Edit->addSeparator();
        menu_Edit->addAction(actionUpload);
        menu_Edit->addSeparator();
        menu_Edit->addAction(actionPreferences);
        toolBar->addAction(actionPrevious);
        toolBar->addAction(actionNext);
        toolBar->addSeparator();
        toolBar->addAction(actionOpenFile);
        toolBar->addAction(actionReload);
        toolBar->addAction(actionSaveAs);
        toolBar->addAction(actionCopy);
        toolBar->addAction(actionPaste);
        toolBar->addSeparator();
        toolBar->addAction(actionRotateClockwise);
        toolBar->addAction(actionRotateCounterclockwise);
        toolBar->addAction(actionZoomIn);
        toolBar->addAction(actionZoomOut);
        toolBar->addAction(actionZoomFit);
        toolBar->addAction(actionOriginalSize);
        toolBar->addSeparator();
        toolBar->addAction(actionSlideShow);
        annotationsToolBar->addAction(actionDrawNone);
        annotationsToolBar->addAction(actionDrawArrow);
        annotationsToolBar->addAction(actionDrawRectangle);
        annotationsToolBar->addAction(actionDrawCircle);
        annotationsToolBar->addAction(actionDrawNumber);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "Image Viewer", nullptr));
        actionAbout->setText(QCoreApplication::translate("MainWindow", "&About", nullptr));
        actionHiddenShortcuts->setText(QCoreApplication::translate("MainWindow", "Hidden &Shortcuts", nullptr));
        actionOpenFile->setText(QCoreApplication::translate("MainWindow", "&Open\342\200\246", nullptr));
#if QT_CONFIG(shortcut)
        actionOpenFile->setShortcut(QCoreApplication::translate("MainWindow", "Ctrl+O", nullptr));
#endif // QT_CONFIG(shortcut)
        actionReload->setText(QCoreApplication::translate("MainWindow", "&Reload File", nullptr));
#if QT_CONFIG(shortcut)
        actionReload->setShortcut(QCoreApplication::translate("MainWindow", "Ctrl+R", nullptr));
#endif // QT_CONFIG(shortcut)
        actionSave->setText(QCoreApplication::translate("MainWindow", "&Save", nullptr));
#if QT_CONFIG(shortcut)
        actionSave->setShortcut(QCoreApplication::translate("MainWindow", "Ctrl+S", nullptr));
#endif // QT_CONFIG(shortcut)
        actionSaveAs->setText(QCoreApplication::translate("MainWindow", "Save &As", nullptr));
#if QT_CONFIG(shortcut)
        actionSaveAs->setShortcut(QCoreApplication::translate("MainWindow", "Ctrl+A", nullptr));
#endif // QT_CONFIG(shortcut)
        actionClose->setText(QCoreApplication::translate("MainWindow", "&Close", nullptr));
#if QT_CONFIG(shortcut)
        actionClose->setShortcut(QCoreApplication::translate("MainWindow", "Ctrl+W", nullptr));
#endif // QT_CONFIG(shortcut)
        actionZoomIn->setText(QCoreApplication::translate("MainWindow", "Zoom &In", nullptr));
#if QT_CONFIG(shortcut)
        actionZoomIn->setShortcut(QCoreApplication::translate("MainWindow", "Ctrl+=", nullptr));
#endif // QT_CONFIG(shortcut)
        actionZoomOut->setText(QCoreApplication::translate("MainWindow", "Zoom &Out", nullptr));
#if QT_CONFIG(shortcut)
        actionZoomOut->setShortcut(QCoreApplication::translate("MainWindow", "Ctrl+-", nullptr));
#endif // QT_CONFIG(shortcut)
        actionCopy->setText(QCoreApplication::translate("MainWindow", "&Copy to Clipboard", nullptr));
        actionNext->setText(QCoreApplication::translate("MainWindow", "Next File", nullptr));
#if QT_CONFIG(tooltip)
        actionNext->setToolTip(QCoreApplication::translate("MainWindow", "Next File", nullptr));
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(shortcut)
        actionNext->setShortcut(QCoreApplication::translate("MainWindow", "PgDown", nullptr));
#endif // QT_CONFIG(shortcut)
        actionPrevious->setText(QCoreApplication::translate("MainWindow", "Previous File", nullptr));
#if QT_CONFIG(tooltip)
        actionPrevious->setToolTip(QCoreApplication::translate("MainWindow", "Previous File", nullptr));
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(shortcut)
        actionPrevious->setShortcut(QCoreApplication::translate("MainWindow", "PgUp", nullptr));
#endif // QT_CONFIG(shortcut)
        actionOriginalSize->setText(QCoreApplication::translate("MainWindow", "Original Size", nullptr));
#if QT_CONFIG(shortcut)
        actionOriginalSize->setShortcut(QCoreApplication::translate("MainWindow", "Ctrl+0", nullptr));
#endif // QT_CONFIG(shortcut)
        actionZoomFit->setText(QCoreApplication::translate("MainWindow", "&Fit", nullptr));
        actionRotateClockwise->setText(QCoreApplication::translate("MainWindow", "&Rotate Clockwise", nullptr));
#if QT_CONFIG(shortcut)
        actionRotateClockwise->setShortcut(QCoreApplication::translate("MainWindow", "R", nullptr));
#endif // QT_CONFIG(shortcut)
        actionRotateCounterclockwise->setText(QCoreApplication::translate("MainWindow", "Rotate &Counterclockwise", nullptr));
#if QT_CONFIG(shortcut)
        actionRotateCounterclockwise->setShortcut(QCoreApplication::translate("MainWindow", "L", nullptr));
#endif // QT_CONFIG(shortcut)
        actionPreferences->setText(QCoreApplication::translate("MainWindow", "P&references", nullptr));
        actionPrint->setText(QCoreApplication::translate("MainWindow", "&Print", nullptr));
#if QT_CONFIG(shortcut)
        actionPrint->setShortcut(QCoreApplication::translate("MainWindow", "Ctrl+P", nullptr));
#endif // QT_CONFIG(shortcut)
        actionFirst->setText(QCoreApplication::translate("MainWindow", "First File", nullptr));
#if QT_CONFIG(shortcut)
        actionFirst->setShortcut(QCoreApplication::translate("MainWindow", "Home", nullptr));
#endif // QT_CONFIG(shortcut)
        actionLast->setText(QCoreApplication::translate("MainWindow", "Last File", nullptr));
#if QT_CONFIG(shortcut)
        actionLast->setShortcut(QCoreApplication::translate("MainWindow", "End", nullptr));
#endif // QT_CONFIG(shortcut)
        actionNewWindow->setText(QCoreApplication::translate("MainWindow", "&New Window", nullptr));
#if QT_CONFIG(shortcut)
        actionNewWindow->setShortcut(QCoreApplication::translate("MainWindow", "Ctrl+N", nullptr));
#endif // QT_CONFIG(shortcut)
        actionFlipHorizontal->setText(QCoreApplication::translate("MainWindow", "Flip &Horizontally", nullptr));
#if QT_CONFIG(shortcut)
        actionFlipHorizontal->setShortcut(QCoreApplication::translate("MainWindow", "H", nullptr));
#endif // QT_CONFIG(shortcut)
        actionScreenshot->setText(QCoreApplication::translate("MainWindow", "Capture Screenshot", nullptr));
        actionFullScreen->setText(QCoreApplication::translate("MainWindow", "F&ull Screen", nullptr));
#if QT_CONFIG(shortcut)
        actionFullScreen->setShortcut(QCoreApplication::translate("MainWindow", "F11", nullptr));
#endif // QT_CONFIG(shortcut)
        actionFlipVertical->setText(QCoreApplication::translate("MainWindow", "Flip &Vertically", nullptr));
#if QT_CONFIG(shortcut)
        actionFlipVertical->setShortcut(QCoreApplication::translate("MainWindow", "V", nullptr));
#endif // QT_CONFIG(shortcut)
        actionPaste->setText(QCoreApplication::translate("MainWindow", "&Paste from Clipboard", nullptr));
        actionSlideShow->setText(QCoreApplication::translate("MainWindow", "&Slide Show", nullptr));
        actionDelete->setText(QCoreApplication::translate("MainWindow", "&Delete", nullptr));
#if QT_CONFIG(shortcut)
        actionDelete->setShortcut(QCoreApplication::translate("MainWindow", "Del", nullptr));
#endif // QT_CONFIG(shortcut)
        actionShowThumbnails->setText(QCoreApplication::translate("MainWindow", "Show Thumbnails", nullptr));
#if QT_CONFIG(shortcut)
        actionShowThumbnails->setShortcut(QCoreApplication::translate("MainWindow", "T", nullptr));
#endif // QT_CONFIG(shortcut)
        actionFileProperties->setText(QCoreApplication::translate("MainWindow", "File Properties", nullptr));
        actionOpenDirectory->setText(QCoreApplication::translate("MainWindow", "Open &Directory", nullptr));
#if QT_CONFIG(shortcut)
        actionOpenDirectory->setShortcut(QCoreApplication::translate("MainWindow", "Ctrl+D", nullptr));
#endif // QT_CONFIG(shortcut)
        actionUpload->setText(QCoreApplication::translate("MainWindow", "Upload", nullptr));
#if QT_CONFIG(tooltip)
        actionUpload->setToolTip(QCoreApplication::translate("MainWindow", "Upload the image", nullptr));
#endif // QT_CONFIG(tooltip)
        actionShowExifData->setText(QCoreApplication::translate("MainWindow", "Show EXIF Data", nullptr));
        actionDrawNone->setText(QCoreApplication::translate("MainWindow", "No Tool", nullptr));
#if QT_CONFIG(tooltip)
        actionDrawNone->setToolTip(QCoreApplication::translate("MainWindow", "Deselect all drawing tools", nullptr));
#endif // QT_CONFIG(tooltip)
        actionDrawArrow->setText(QCoreApplication::translate("MainWindow", "Draw Arrow", nullptr));
#if QT_CONFIG(tooltip)
        actionDrawArrow->setToolTip(QCoreApplication::translate("MainWindow", "Draw an arrow", nullptr));
#endif // QT_CONFIG(tooltip)
        actionDrawRectangle->setText(QCoreApplication::translate("MainWindow", "Draw Rectangle", nullptr));
#if QT_CONFIG(tooltip)
        actionDrawRectangle->setToolTip(QCoreApplication::translate("MainWindow", "Draw a hollow rectangle", nullptr));
#endif // QT_CONFIG(tooltip)
        actionDrawCircle->setText(QCoreApplication::translate("MainWindow", "Draw Circle", nullptr));
#if QT_CONFIG(tooltip)
        actionDrawCircle->setToolTip(QCoreApplication::translate("MainWindow", "Draw a hollow circle", nullptr));
#endif // QT_CONFIG(tooltip)
        actionDrawNumber->setText(QCoreApplication::translate("MainWindow", "Draw Number", nullptr));
#if QT_CONFIG(tooltip)
        actionDrawNumber->setToolTip(QCoreApplication::translate("MainWindow", "Draw incrementing numbers", nullptr));
#endif // QT_CONFIG(tooltip)
        actionMenubar->setText(QCoreApplication::translate("MainWindow", "Menubar", nullptr));
#if QT_CONFIG(tooltip)
        actionMenubar->setToolTip(QCoreApplication::translate("MainWindow", "Menubar", nullptr));
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(shortcut)
        actionMenubar->setShortcut(QCoreApplication::translate("MainWindow", "Ctrl+M", nullptr));
#endif // QT_CONFIG(shortcut)
        actionToolbar->setText(QCoreApplication::translate("MainWindow", "&Main Toolbar", nullptr));
#if QT_CONFIG(tooltip)
        actionToolbar->setToolTip(QCoreApplication::translate("MainWindow", "Main Toolbar", nullptr));
#endif // QT_CONFIG(tooltip)
        actionAnnotations->setText(QCoreApplication::translate("MainWindow", "&Annotations Toolbar", nullptr));
#if QT_CONFIG(tooltip)
        actionAnnotations->setToolTip(QCoreApplication::translate("MainWindow", "Annotations Toolbar", nullptr));
#endif // QT_CONFIG(tooltip)
        actionShowOutline->setText(QCoreApplication::translate("MainWindow", "Show Outline", nullptr));
#if QT_CONFIG(tooltip)
        actionShowOutline->setToolTip(QCoreApplication::translate("MainWindow", "Show Outline", nullptr));
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(shortcut)
        actionShowOutline->setShortcut(QCoreApplication::translate("MainWindow", "O", nullptr));
#endif // QT_CONFIG(shortcut)
        actionCopyPath->setText(QCoreApplication::translate("MainWindow", "Copy Pa&th", nullptr));
#if QT_CONFIG(tooltip)
        actionCopyPath->setToolTip(QCoreApplication::translate("MainWindow", "Copy path", nullptr));
#endif // QT_CONFIG(tooltip)
        actionRenameFile->setText(QCoreApplication::translate("MainWindow", "Rename", nullptr));
#if QT_CONFIG(tooltip)
        actionRenameFile->setToolTip(QCoreApplication::translate("MainWindow", "Rename", nullptr));
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(shortcut)
        actionRenameFile->setShortcut(QCoreApplication::translate("MainWindow", "F2", nullptr));
#endif // QT_CONFIG(shortcut)
        actionResize->setText(QCoreApplication::translate("MainWindow", "Resize", nullptr));
        menu_File->setTitle(QCoreApplication::translate("MainWindow", "&File", nullptr));
        openWithMenu->setTitle(QCoreApplication::translate("MainWindow", "Open &With...", nullptr));
        menuRecently_Opened_Files->setTitle(QCoreApplication::translate("MainWindow", "&Recently Opened Files", nullptr));
        menu_Help->setTitle(QCoreApplication::translate("MainWindow", "&Help", nullptr));
        menuGo->setTitle(QCoreApplication::translate("MainWindow", "Go", nullptr));
        menu_View->setTitle(QCoreApplication::translate("MainWindow", "&View", nullptr));
        menu_Edit->setTitle(QCoreApplication::translate("MainWindow", "&Edit", nullptr));
        toolBar->setWindowTitle(QCoreApplication::translate("MainWindow", "Toolbar", nullptr));
        annotationsToolBar->setWindowTitle(QCoreApplication::translate("MainWindow", "Annotations Toolbar", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
