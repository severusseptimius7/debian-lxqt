/********************************************************************************
** Form generated from reading UI file 'preferencesdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PREFERENCESDIALOG_H
#define UI_PREFERENCESDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include <libfm-qt/colorbutton.h>

QT_BEGIN_NAMESPACE

class Ui_PreferencesDialog
{
public:
    QVBoxLayout *verticalLayout;
    QTabWidget *tabWidget;
    QWidget *tab_1;
    QFormLayout *formLayout_1;
    QLabel *iconThemeLabel;
    QComboBox *iconTheme;
    QLabel *label_1;
    QSpinBox *maxRecentFiles;
    QLabel *thumbnailLabel;
    QSpinBox *thumbnailSpin;
    QLabel *thumbnailSizeLabel;
    QComboBox *thumbnailSizeComboBox;
    QCheckBox *useTrashBox;
    QWidget *tab;
    QFormLayout *formLayout_11;
    QCheckBox *thumbnailBox;
    QComboBox *thumbnailsPositionComboBox;
    QCheckBox *exifDataBox;
    QCheckBox *menubarBox;
    QCheckBox *toolbarBox;
    QCheckBox *annotationBox;
    QWidget *tab_2;
    QFormLayout *formLayout_2;
    QLabel *label_2a;
    Fm::ColorButton *bgColor;
    QLabel *label_2b;
    Fm::ColorButton *fullScreenBgColor;
    QLabel *label_2c;
    QSpinBox *slideShowInterval;
    QCheckBox *oulineBox;
    QCheckBox *forceZoomFitBox;
    QCheckBox *smoothOnZoomBox;
    QWidget *tab_4;
    QVBoxLayout *verticalLayout_4;
    QTableWidget *tableWidget;
    QPushButton *defaultButton;
    QLabel *warningLabel;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *PreferencesDialog)
    {
        if (PreferencesDialog->objectName().isEmpty())
            PreferencesDialog->setObjectName(QString::fromUtf8("PreferencesDialog"));
        PreferencesDialog->resize(400, 400);
        verticalLayout = new QVBoxLayout(PreferencesDialog);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        tabWidget = new QTabWidget(PreferencesDialog);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tab_1 = new QWidget();
        tab_1->setObjectName(QString::fromUtf8("tab_1"));
        formLayout_1 = new QFormLayout(tab_1);
        formLayout_1->setObjectName(QString::fromUtf8("formLayout_1"));
        formLayout_1->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        iconThemeLabel = new QLabel(tab_1);
        iconThemeLabel->setObjectName(QString::fromUtf8("iconThemeLabel"));

        formLayout_1->setWidget(0, QFormLayout::LabelRole, iconThemeLabel);

        iconTheme = new QComboBox(tab_1);
        iconTheme->setObjectName(QString::fromUtf8("iconTheme"));

        formLayout_1->setWidget(0, QFormLayout::FieldRole, iconTheme);

        label_1 = new QLabel(tab_1);
        label_1->setObjectName(QString::fromUtf8("label_1"));

        formLayout_1->setWidget(1, QFormLayout::LabelRole, label_1);

        maxRecentFiles = new QSpinBox(tab_1);
        maxRecentFiles->setObjectName(QString::fromUtf8("maxRecentFiles"));

        formLayout_1->setWidget(1, QFormLayout::FieldRole, maxRecentFiles);

        thumbnailLabel = new QLabel(tab_1);
        thumbnailLabel->setObjectName(QString::fromUtf8("thumbnailLabel"));

        formLayout_1->setWidget(2, QFormLayout::LabelRole, thumbnailLabel);

        thumbnailSpin = new QSpinBox(tab_1);
        thumbnailSpin->setObjectName(QString::fromUtf8("thumbnailSpin"));
        thumbnailSpin->setMinimum(1);
        thumbnailSpin->setMaximum(1024);
        thumbnailSpin->setValue(4);

        formLayout_1->setWidget(2, QFormLayout::FieldRole, thumbnailSpin);

        thumbnailSizeLabel = new QLabel(tab_1);
        thumbnailSizeLabel->setObjectName(QString::fromUtf8("thumbnailSizeLabel"));

        formLayout_1->setWidget(3, QFormLayout::LabelRole, thumbnailSizeLabel);

        thumbnailSizeComboBox = new QComboBox(tab_1);
        thumbnailSizeComboBox->setObjectName(QString::fromUtf8("thumbnailSizeComboBox"));

        formLayout_1->setWidget(3, QFormLayout::FieldRole, thumbnailSizeComboBox);

        useTrashBox = new QCheckBox(tab_1);
        useTrashBox->setObjectName(QString::fromUtf8("useTrashBox"));

        formLayout_1->setWidget(4, QFormLayout::LabelRole, useTrashBox);

        tabWidget->addTab(tab_1, QString());
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        formLayout_11 = new QFormLayout(tab);
        formLayout_11->setObjectName(QString::fromUtf8("formLayout_11"));
        formLayout_11->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        thumbnailBox = new QCheckBox(tab);
        thumbnailBox->setObjectName(QString::fromUtf8("thumbnailBox"));

        formLayout_11->setWidget(0, QFormLayout::LabelRole, thumbnailBox);

        thumbnailsPositionComboBox = new QComboBox(tab);
        thumbnailsPositionComboBox->setObjectName(QString::fromUtf8("thumbnailsPositionComboBox"));

        formLayout_11->setWidget(0, QFormLayout::FieldRole, thumbnailsPositionComboBox);

        exifDataBox = new QCheckBox(tab);
        exifDataBox->setObjectName(QString::fromUtf8("exifDataBox"));

        formLayout_11->setWidget(1, QFormLayout::LabelRole, exifDataBox);

        menubarBox = new QCheckBox(tab);
        menubarBox->setObjectName(QString::fromUtf8("menubarBox"));

        formLayout_11->setWidget(2, QFormLayout::LabelRole, menubarBox);

        toolbarBox = new QCheckBox(tab);
        toolbarBox->setObjectName(QString::fromUtf8("toolbarBox"));

        formLayout_11->setWidget(3, QFormLayout::LabelRole, toolbarBox);

        annotationBox = new QCheckBox(tab);
        annotationBox->setObjectName(QString::fromUtf8("annotationBox"));

        formLayout_11->setWidget(4, QFormLayout::LabelRole, annotationBox);

        tabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QString::fromUtf8("tab_2"));
        formLayout_2 = new QFormLayout(tab_2);
        formLayout_2->setObjectName(QString::fromUtf8("formLayout_2"));
        formLayout_2->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        label_2a = new QLabel(tab_2);
        label_2a->setObjectName(QString::fromUtf8("label_2a"));

        formLayout_2->setWidget(0, QFormLayout::LabelRole, label_2a);

        bgColor = new Fm::ColorButton(tab_2);
        bgColor->setObjectName(QString::fromUtf8("bgColor"));

        formLayout_2->setWidget(0, QFormLayout::FieldRole, bgColor);

        label_2b = new QLabel(tab_2);
        label_2b->setObjectName(QString::fromUtf8("label_2b"));

        formLayout_2->setWidget(1, QFormLayout::LabelRole, label_2b);

        fullScreenBgColor = new Fm::ColorButton(tab_2);
        fullScreenBgColor->setObjectName(QString::fromUtf8("fullScreenBgColor"));

        formLayout_2->setWidget(1, QFormLayout::FieldRole, fullScreenBgColor);

        label_2c = new QLabel(tab_2);
        label_2c->setObjectName(QString::fromUtf8("label_2c"));

        formLayout_2->setWidget(4, QFormLayout::LabelRole, label_2c);

        slideShowInterval = new QSpinBox(tab_2);
        slideShowInterval->setObjectName(QString::fromUtf8("slideShowInterval"));
        slideShowInterval->setMinimum(1);

        formLayout_2->setWidget(4, QFormLayout::FieldRole, slideShowInterval);

        oulineBox = new QCheckBox(tab_2);
        oulineBox->setObjectName(QString::fromUtf8("oulineBox"));

        formLayout_2->setWidget(5, QFormLayout::LabelRole, oulineBox);

        forceZoomFitBox = new QCheckBox(tab_2);
        forceZoomFitBox->setObjectName(QString::fromUtf8("forceZoomFitBox"));

        formLayout_2->setWidget(6, QFormLayout::LabelRole, forceZoomFitBox);

        smoothOnZoomBox = new QCheckBox(tab_2);
        smoothOnZoomBox->setObjectName(QString::fromUtf8("smoothOnZoomBox"));

        formLayout_2->setWidget(7, QFormLayout::LabelRole, smoothOnZoomBox);

        tabWidget->addTab(tab_2, QString());
        tab_4 = new QWidget();
        tab_4->setObjectName(QString::fromUtf8("tab_4"));
        verticalLayout_4 = new QVBoxLayout(tab_4);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        tableWidget = new QTableWidget(tab_4);
        if (tableWidget->columnCount() < 2)
            tableWidget->setColumnCount(2);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        tableWidget->setObjectName(QString::fromUtf8("tableWidget"));
        tableWidget->setEditTriggers(QAbstractItemView::DoubleClicked|QAbstractItemView::EditKeyPressed|QAbstractItemView::SelectedClicked);
        tableWidget->setAlternatingRowColors(true);
        tableWidget->setSelectionMode(QAbstractItemView::SingleSelection);
        tableWidget->verticalHeader()->setVisible(false);

        verticalLayout_4->addWidget(tableWidget);

        defaultButton = new QPushButton(tab_4);
        defaultButton->setObjectName(QString::fromUtf8("defaultButton"));

        verticalLayout_4->addWidget(defaultButton, 0, Qt::AlignRight);

        tabWidget->addTab(tab_4, QString());

        verticalLayout->addWidget(tabWidget);

        warningLabel = new QLabel(PreferencesDialog);
        warningLabel->setObjectName(QString::fromUtf8("warningLabel"));
        warningLabel->setAlignment(Qt::AlignCenter);
        warningLabel->setTextInteractionFlags(Qt::NoTextInteraction);

        verticalLayout->addWidget(warningLabel);

        buttonBox = new QDialogButtonBox(PreferencesDialog);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        verticalLayout->addWidget(buttonBox);


        retranslateUi(PreferencesDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), PreferencesDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), PreferencesDialog, SLOT(reject()));

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(PreferencesDialog);
    } // setupUi

    void retranslateUi(QDialog *PreferencesDialog)
    {
        PreferencesDialog->setWindowTitle(QCoreApplication::translate("PreferencesDialog", "Preferences", nullptr));
        iconThemeLabel->setText(QCoreApplication::translate("PreferencesDialog", "Icon theme:", nullptr));
        label_1->setText(QCoreApplication::translate("PreferencesDialog", "Maximum number of recent files:", nullptr));
        thumbnailLabel->setText(QCoreApplication::translate("PreferencesDialog", "Thumbnailer file size limit:", nullptr));
        thumbnailSpin->setSuffix(QCoreApplication::translate("PreferencesDialog", " MiB", nullptr));
        thumbnailSizeLabel->setText(QCoreApplication::translate("PreferencesDialog", "Thumbnail image dimensions:", nullptr));
        useTrashBox->setText(QCoreApplication::translate("PreferencesDialog", "Use system Trash (and do not prompt)", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_1), QCoreApplication::translate("PreferencesDialog", "General", nullptr));
        thumbnailBox->setText(QCoreApplication::translate("PreferencesDialog", "Show thumbnails dock by default", nullptr));
        exifDataBox->setText(QCoreApplication::translate("PreferencesDialog", "Show Exif data dock by default", nullptr));
        menubarBox->setText(QCoreApplication::translate("PreferencesDialog", "Show menubar by default", nullptr));
        toolbarBox->setText(QCoreApplication::translate("PreferencesDialog", "Show main toolbar by default", nullptr));
        annotationBox->setText(QCoreApplication::translate("PreferencesDialog", "Show annotations toolbar by default", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab), QCoreApplication::translate("PreferencesDialog", "Window", nullptr));
        label_2a->setText(QCoreApplication::translate("PreferencesDialog", "Normal background color:", nullptr));
        bgColor->setText(QString());
        label_2b->setText(QCoreApplication::translate("PreferencesDialog", "Fullscreen background color:", nullptr));
        fullScreenBgColor->setText(QString());
        label_2c->setText(QCoreApplication::translate("PreferencesDialog", "Slide show interval (seconds):", nullptr));
        oulineBox->setText(QCoreApplication::translate("PreferencesDialog", "Show image outline by default", nullptr));
        forceZoomFitBox->setText(QCoreApplication::translate("PreferencesDialog", "Fit images when navigating", nullptr));
        smoothOnZoomBox->setText(QCoreApplication::translate("PreferencesDialog", "Smooth images on zooming", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QCoreApplication::translate("PreferencesDialog", "Image", nullptr));
        QTableWidgetItem *___qtablewidgetitem = tableWidget->horizontalHeaderItem(0);
        ___qtablewidgetitem->setText(QCoreApplication::translate("PreferencesDialog", "Action", nullptr));
        QTableWidgetItem *___qtablewidgetitem1 = tableWidget->horizontalHeaderItem(1);
        ___qtablewidgetitem1->setText(QCoreApplication::translate("PreferencesDialog", "Shortcut", nullptr));
        defaultButton->setText(QCoreApplication::translate("PreferencesDialog", "Default", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_4), QCoreApplication::translate("PreferencesDialog", "Shortcuts", nullptr));
    } // retranslateUi

};

namespace Ui {
    class PreferencesDialog: public Ui_PreferencesDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PREFERENCESDIALOG_H
