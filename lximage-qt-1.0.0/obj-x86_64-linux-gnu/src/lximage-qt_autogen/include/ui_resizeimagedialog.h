/********************************************************************************
** Form generated from reading UI file 'resizeimagedialog.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_RESIZEIMAGEDIALOG_H
#define UI_RESIZEIMAGEDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSpinBox>

QT_BEGIN_NAMESPACE

class Ui_ResizeImageDialog
{
public:
    QGridLayout *gridLayout;
    QLabel *label_3;
    QLabel *label_5;
    QLabel *label;
    QLabel *originalWidthLabel;
    QLabel *label_6;
    QLabel *originalHeightLabel;
    QLabel *label_2;
    QSpinBox *widthSpinBox;
    QLabel *label_4;
    QSpinBox *heightSpinBox;
    QDoubleSpinBox *widthPercentSpinBox;
    QLabel *label_7;
    QDoubleSpinBox *heightPercentSpinBox;
    QCheckBox *keepAspectCheckBox;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *ResizeImageDialog)
    {
        if (ResizeImageDialog->objectName().isEmpty())
            ResizeImageDialog->setObjectName(QString::fromUtf8("ResizeImageDialog"));
        ResizeImageDialog->resize(302, 158);
        gridLayout = new QGridLayout(ResizeImageDialog);
        gridLayout->setSpacing(4);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setSizeConstraint(QLayout::SetFixedSize);
        label_3 = new QLabel(ResizeImageDialog);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        gridLayout->addWidget(label_3, 0, 1, 1, 1, Qt::AlignRight);

        label_5 = new QLabel(ResizeImageDialog);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        gridLayout->addWidget(label_5, 0, 3, 1, 1, Qt::AlignLeft);

        label = new QLabel(ResizeImageDialog);
        label->setObjectName(QString::fromUtf8("label"));
        label->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label, 1, 0, 1, 1);

        originalWidthLabel = new QLabel(ResizeImageDialog);
        originalWidthLabel->setObjectName(QString::fromUtf8("originalWidthLabel"));
        originalWidthLabel->setTextInteractionFlags(Qt::LinksAccessibleByMouse|Qt::TextSelectableByMouse);

        gridLayout->addWidget(originalWidthLabel, 1, 1, 1, 1, Qt::AlignRight);

        label_6 = new QLabel(ResizeImageDialog);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setText(QString::fromUtf8("\303\227"));
        label_6->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_6, 1, 2, 1, 1);

        originalHeightLabel = new QLabel(ResizeImageDialog);
        originalHeightLabel->setObjectName(QString::fromUtf8("originalHeightLabel"));
        originalHeightLabel->setTextInteractionFlags(Qt::LinksAccessibleByMouse|Qt::TextSelectableByMouse);

        gridLayout->addWidget(originalHeightLabel, 1, 3, 1, 1, Qt::AlignLeft);

        label_2 = new QLabel(ResizeImageDialog);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_2, 2, 0, 1, 1);

        widthSpinBox = new QSpinBox(ResizeImageDialog);
        widthSpinBox->setObjectName(QString::fromUtf8("widthSpinBox"));
        widthSpinBox->setMinimum(1);
        widthSpinBox->setMaximum(99999);

        gridLayout->addWidget(widthSpinBox, 2, 1, 1, 1);

        label_4 = new QLabel(ResizeImageDialog);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setText(QString::fromUtf8("\303\227"));
        label_4->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_4, 2, 2, 1, 1);

        heightSpinBox = new QSpinBox(ResizeImageDialog);
        heightSpinBox->setObjectName(QString::fromUtf8("heightSpinBox"));
        heightSpinBox->setMinimum(1);
        heightSpinBox->setMaximum(99999);

        gridLayout->addWidget(heightSpinBox, 2, 3, 1, 1);

        widthPercentSpinBox = new QDoubleSpinBox(ResizeImageDialog);
        widthPercentSpinBox->setObjectName(QString::fromUtf8("widthPercentSpinBox"));
        widthPercentSpinBox->setDecimals(0);
        widthPercentSpinBox->setMinimum(1.000000000000000);
        widthPercentSpinBox->setMaximum(1000.000000000000000);

        gridLayout->addWidget(widthPercentSpinBox, 3, 1, 1, 1);

        label_7 = new QLabel(ResizeImageDialog);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setText(QString::fromUtf8("\303\227"));
        label_7->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_7, 3, 2, 1, 1);

        heightPercentSpinBox = new QDoubleSpinBox(ResizeImageDialog);
        heightPercentSpinBox->setObjectName(QString::fromUtf8("heightPercentSpinBox"));
        heightPercentSpinBox->setEnabled(false);
        heightPercentSpinBox->setDecimals(0);
        heightPercentSpinBox->setMinimum(1.000000000000000);
        heightPercentSpinBox->setMaximum(1000.000000000000000);

        gridLayout->addWidget(heightPercentSpinBox, 3, 3, 1, 1);

        keepAspectCheckBox = new QCheckBox(ResizeImageDialog);
        keepAspectCheckBox->setObjectName(QString::fromUtf8("keepAspectCheckBox"));
        keepAspectCheckBox->setChecked(true);

        gridLayout->addWidget(keepAspectCheckBox, 4, 1, 1, 3);

        buttonBox = new QDialogButtonBox(ResizeImageDialog);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        gridLayout->addWidget(buttonBox, 6, 0, 1, 4);

#if QT_CONFIG(shortcut)
        label_2->setBuddy(widthSpinBox);
#endif // QT_CONFIG(shortcut)
        QWidget::setTabOrder(widthSpinBox, heightSpinBox);
        QWidget::setTabOrder(heightSpinBox, widthPercentSpinBox);
        QWidget::setTabOrder(widthPercentSpinBox, heightPercentSpinBox);
        QWidget::setTabOrder(heightPercentSpinBox, keepAspectCheckBox);

        retranslateUi(ResizeImageDialog);
        QObject::connect(keepAspectCheckBox, SIGNAL(toggled(bool)), heightPercentSpinBox, SLOT(setDisabled(bool)));
        QObject::connect(buttonBox, SIGNAL(accepted()), ResizeImageDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), ResizeImageDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(ResizeImageDialog);
    } // setupUi

    void retranslateUi(QDialog *ResizeImageDialog)
    {
        ResizeImageDialog->setWindowTitle(QCoreApplication::translate("ResizeImageDialog", "Resize Image", nullptr));
        label_3->setText(QCoreApplication::translate("ResizeImageDialog", "Width", nullptr));
        label_5->setText(QCoreApplication::translate("ResizeImageDialog", "Height", nullptr));
        label->setText(QCoreApplication::translate("ResizeImageDialog", "Current size:", nullptr));
        originalWidthLabel->setText(QString());
        originalHeightLabel->setText(QString());
        label_2->setText(QCoreApplication::translate("ResizeImageDialog", "New size:", nullptr));
        widthSpinBox->setSuffix(QCoreApplication::translate("ResizeImageDialog", " px", nullptr));
        heightSpinBox->setSuffix(QCoreApplication::translate("ResizeImageDialog", " px", nullptr));
        widthPercentSpinBox->setSuffix(QCoreApplication::translate("ResizeImageDialog", " %", nullptr));
        heightPercentSpinBox->setSuffix(QCoreApplication::translate("ResizeImageDialog", " %", nullptr));
        keepAspectCheckBox->setText(QCoreApplication::translate("ResizeImageDialog", "Keep aspect ratio", nullptr));
    } // retranslateUi

};

namespace Ui {
    class ResizeImageDialog: public Ui_ResizeImageDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_RESIZEIMAGEDIALOG_H
