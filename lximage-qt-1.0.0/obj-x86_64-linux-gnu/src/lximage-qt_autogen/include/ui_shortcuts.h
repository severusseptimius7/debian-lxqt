/********************************************************************************
** Form generated from reading UI file 'shortcuts.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SHORTCUTS_H
#define UI_SHORTCUTS_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QTreeWidget>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_HiddenShortcutsDialog
{
public:
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QTreeWidget *treeWidget;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *HiddenShortcutsDialog)
    {
        if (HiddenShortcutsDialog->objectName().isEmpty())
            HiddenShortcutsDialog->setObjectName(QString::fromUtf8("HiddenShortcutsDialog"));
        HiddenShortcutsDialog->resize(500, 400);
        verticalLayout = new QVBoxLayout(HiddenShortcutsDialog);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        label = new QLabel(HiddenShortcutsDialog);
        label->setObjectName(QString::fromUtf8("label"));
        label->setWordWrap(true);
        label->setMargin(5);

        verticalLayout->addWidget(label);

        treeWidget = new QTreeWidget(HiddenShortcutsDialog);
        new QTreeWidgetItem(treeWidget);
        new QTreeWidgetItem(treeWidget);
        new QTreeWidgetItem(treeWidget);
        new QTreeWidgetItem(treeWidget);
        new QTreeWidgetItem(treeWidget);
        new QTreeWidgetItem(treeWidget);
        new QTreeWidgetItem(treeWidget);
        treeWidget->setObjectName(QString::fromUtf8("treeWidget"));

        verticalLayout->addWidget(treeWidget);

        buttonBox = new QDialogButtonBox(HiddenShortcutsDialog);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Close);

        verticalLayout->addWidget(buttonBox);


        retranslateUi(HiddenShortcutsDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), HiddenShortcutsDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), HiddenShortcutsDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(HiddenShortcutsDialog);
    } // setupUi

    void retranslateUi(QDialog *HiddenShortcutsDialog)
    {
        HiddenShortcutsDialog->setWindowTitle(QCoreApplication::translate("HiddenShortcutsDialog", "Hidden Shortcuts", nullptr));
        label->setText(QCoreApplication::translate("HiddenShortcutsDialog", "These hard coded shortcuts will be overridden if they are also assigned to other actions in Preferences \342\206\222 Shortcuts.", nullptr));
        QTreeWidgetItem *___qtreewidgetitem = treeWidget->headerItem();
        ___qtreewidgetitem->setText(1, QCoreApplication::translate("HiddenShortcutsDialog", "Action", nullptr));
        ___qtreewidgetitem->setText(0, QCoreApplication::translate("HiddenShortcutsDialog", "Shortcut", nullptr));

        const bool __sortingEnabled = treeWidget->isSortingEnabled();
        treeWidget->setSortingEnabled(false);
        QTreeWidgetItem *___qtreewidgetitem1 = treeWidget->topLevelItem(0);
        ___qtreewidgetitem1->setText(1, QCoreApplication::translate("HiddenShortcutsDialog", "Close window or exit fullscreen mode", nullptr));
        ___qtreewidgetitem1->setText(0, QCoreApplication::translate("HiddenShortcutsDialog", "Esc", nullptr));
        QTreeWidgetItem *___qtreewidgetitem2 = treeWidget->topLevelItem(1);
        ___qtreewidgetitem2->setText(1, QCoreApplication::translate("HiddenShortcutsDialog", "Next image", nullptr));
        ___qtreewidgetitem2->setText(0, QCoreApplication::translate("HiddenShortcutsDialog", "Right", nullptr));
        QTreeWidgetItem *___qtreewidgetitem3 = treeWidget->topLevelItem(2);
        ___qtreewidgetitem3->setText(1, QCoreApplication::translate("HiddenShortcutsDialog", "Next image", nullptr));
        ___qtreewidgetitem3->setText(0, QCoreApplication::translate("HiddenShortcutsDialog", "Space", nullptr));
        QTreeWidgetItem *___qtreewidgetitem4 = treeWidget->topLevelItem(3);
        ___qtreewidgetitem4->setText(1, QCoreApplication::translate("HiddenShortcutsDialog", "Last image", nullptr));
        ___qtreewidgetitem4->setText(0, QCoreApplication::translate("HiddenShortcutsDialog", "End", nullptr));
        QTreeWidgetItem *___qtreewidgetitem5 = treeWidget->topLevelItem(4);
        ___qtreewidgetitem5->setText(1, QCoreApplication::translate("HiddenShortcutsDialog", "Previous image", nullptr));
        ___qtreewidgetitem5->setText(0, QCoreApplication::translate("HiddenShortcutsDialog", "Left", nullptr));
        QTreeWidgetItem *___qtreewidgetitem6 = treeWidget->topLevelItem(5);
        ___qtreewidgetitem6->setText(1, QCoreApplication::translate("HiddenShortcutsDialog", "Previous image", nullptr));
        ___qtreewidgetitem6->setText(0, QCoreApplication::translate("HiddenShortcutsDialog", "Backspace", nullptr));
        QTreeWidgetItem *___qtreewidgetitem7 = treeWidget->topLevelItem(6);
        ___qtreewidgetitem7->setText(1, QCoreApplication::translate("HiddenShortcutsDialog", "First image", nullptr));
        ___qtreewidgetitem7->setText(0, QCoreApplication::translate("HiddenShortcutsDialog", "Home", nullptr));
        treeWidget->setSortingEnabled(__sortingEnabled);

    } // retranslateUi

};

namespace Ui {
    class HiddenShortcutsDialog: public Ui_HiddenShortcutsDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SHORTCUTS_H
