/********************************************************************************
** Form generated from reading UI file 'uploaddialog.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_UPLOADDIALOG_H
#define UI_UPLOADDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_UploadDialog
{
public:
    QVBoxLayout *verticalLayout;
    QComboBox *providerComboBox;
    QProgressBar *progressBar;
    QHBoxLayout *horizontalLayout_2;
    QLineEdit *linkLineEdit;
    QPushButton *copyButton;
    QSpacerItem *verticalSpacer;
    QFrame *line;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QPushButton *actionButton;

    void setupUi(QDialog *UploadDialog)
    {
        if (UploadDialog->objectName().isEmpty())
            UploadDialog->setObjectName(QString::fromUtf8("UploadDialog"));
        UploadDialog->resize(300, 100);
        verticalLayout = new QVBoxLayout(UploadDialog);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        providerComboBox = new QComboBox(UploadDialog);
        providerComboBox->setObjectName(QString::fromUtf8("providerComboBox"));

        verticalLayout->addWidget(providerComboBox);

        progressBar = new QProgressBar(UploadDialog);
        progressBar->setObjectName(QString::fromUtf8("progressBar"));
        progressBar->setEnabled(true);

        verticalLayout->addWidget(progressBar);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        linkLineEdit = new QLineEdit(UploadDialog);
        linkLineEdit->setObjectName(QString::fromUtf8("linkLineEdit"));
        linkLineEdit->setReadOnly(true);

        horizontalLayout_2->addWidget(linkLineEdit);

        copyButton = new QPushButton(UploadDialog);
        copyButton->setObjectName(QString::fromUtf8("copyButton"));

        horizontalLayout_2->addWidget(copyButton);


        verticalLayout->addLayout(horizontalLayout_2);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        line = new QFrame(UploadDialog);
        line->setObjectName(QString::fromUtf8("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        verticalLayout->addWidget(line);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        actionButton = new QPushButton(UploadDialog);
        actionButton->setObjectName(QString::fromUtf8("actionButton"));

        horizontalLayout->addWidget(actionButton);


        verticalLayout->addLayout(horizontalLayout);


        retranslateUi(UploadDialog);

        QMetaObject::connectSlotsByName(UploadDialog);
    } // setupUi

    void retranslateUi(QDialog *UploadDialog)
    {
        UploadDialog->setWindowTitle(QCoreApplication::translate("UploadDialog", "Upload", nullptr));
        copyButton->setText(QCoreApplication::translate("UploadDialog", "Copy", nullptr));
        actionButton->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class UploadDialog: public Ui_UploadDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_UPLOADDIALOG_H
