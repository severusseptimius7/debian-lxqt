/********************************************************************************
** Form generated from reading UI file 'screenshotdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SCREENSHOTDIALOG_H
#define UI_SCREENSHOTDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_ScreenshotDialog
{
public:
    QFormLayout *formLayout;
    QLabel *label;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout;
    QRadioButton *wholeScreen;
    QRadioButton *currentWindow;
    QRadioButton *screenArea;
    QCheckBox *includeCursor;
    QSpinBox *delay;
    QDialogButtonBox *buttonBox;
    QLabel *label_2;
    QCheckBox *includeFrame;

    void setupUi(QDialog *ScreenshotDialog)
    {
        if (ScreenshotDialog->objectName().isEmpty())
            ScreenshotDialog->setObjectName(QString::fromUtf8("ScreenshotDialog"));
        ScreenshotDialog->resize(348, 261);
        QIcon icon;
        QString iconThemeName = QString::fromUtf8("camera-photo");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon = QIcon::fromTheme(iconThemeName);
        } else {
            icon.addFile(QString::fromUtf8("."), QSize(), QIcon::Normal, QIcon::Off);
        }
        ScreenshotDialog->setWindowIcon(icon);
        formLayout = new QFormLayout(ScreenshotDialog);
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        formLayout->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        label = new QLabel(ScreenshotDialog);
        label->setObjectName(QString::fromUtf8("label"));

        formLayout->setWidget(0, QFormLayout::SpanningRole, label);

        groupBox = new QGroupBox(ScreenshotDialog);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        verticalLayout = new QVBoxLayout(groupBox);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        wholeScreen = new QRadioButton(groupBox);
        wholeScreen->setObjectName(QString::fromUtf8("wholeScreen"));
        wholeScreen->setChecked(true);

        verticalLayout->addWidget(wholeScreen);

        currentWindow = new QRadioButton(groupBox);
        currentWindow->setObjectName(QString::fromUtf8("currentWindow"));

        verticalLayout->addWidget(currentWindow);

        screenArea = new QRadioButton(groupBox);
        screenArea->setObjectName(QString::fromUtf8("screenArea"));

        verticalLayout->addWidget(screenArea);


        formLayout->setWidget(1, QFormLayout::SpanningRole, groupBox);

        includeCursor = new QCheckBox(ScreenshotDialog);
        includeCursor->setObjectName(QString::fromUtf8("includeCursor"));

        formLayout->setWidget(3, QFormLayout::SpanningRole, includeCursor);

        delay = new QSpinBox(ScreenshotDialog);
        delay->setObjectName(QString::fromUtf8("delay"));

        formLayout->setWidget(5, QFormLayout::FieldRole, delay);

        buttonBox = new QDialogButtonBox(ScreenshotDialog);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        formLayout->setWidget(6, QFormLayout::SpanningRole, buttonBox);

        label_2 = new QLabel(ScreenshotDialog);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        formLayout->setWidget(5, QFormLayout::LabelRole, label_2);

        includeFrame = new QCheckBox(ScreenshotDialog);
        includeFrame->setObjectName(QString::fromUtf8("includeFrame"));
        includeFrame->setEnabled(false);
        includeFrame->setChecked(true);

        formLayout->setWidget(2, QFormLayout::LabelRole, includeFrame);


        retranslateUi(ScreenshotDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), ScreenshotDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), ScreenshotDialog, SLOT(reject()));
        QObject::connect(currentWindow, SIGNAL(toggled(bool)), includeFrame, SLOT(setEnabled(bool)));

        QMetaObject::connectSlotsByName(ScreenshotDialog);
    } // setupUi

    void retranslateUi(QDialog *ScreenshotDialog)
    {
        ScreenshotDialog->setWindowTitle(QCoreApplication::translate("ScreenshotDialog", "Screenshot", nullptr));
        label->setText(QCoreApplication::translate("ScreenshotDialog", "Take a screenshot", nullptr));
        groupBox->setTitle(QCoreApplication::translate("ScreenshotDialog", "Region", nullptr));
        wholeScreen->setText(QCoreApplication::translate("ScreenshotDialog", "Whole screen", nullptr));
        currentWindow->setText(QCoreApplication::translate("ScreenshotDialog", "Current window only", nullptr));
        screenArea->setText(QCoreApplication::translate("ScreenshotDialog", "Capture an area of the screen", nullptr));
        includeCursor->setText(QCoreApplication::translate("ScreenshotDialog", "Include mouse cursor", nullptr));
        delay->setSuffix(QCoreApplication::translate("ScreenshotDialog", " sec", nullptr));
        label_2->setText(QCoreApplication::translate("ScreenshotDialog", "Delay:", nullptr));
        includeFrame->setText(QCoreApplication::translate("ScreenshotDialog", "Include window title and frame", nullptr));
    } // retranslateUi

};

namespace Ui {
    class ScreenshotDialog: public Ui_ScreenshotDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SCREENSHOTDIALOG_H
