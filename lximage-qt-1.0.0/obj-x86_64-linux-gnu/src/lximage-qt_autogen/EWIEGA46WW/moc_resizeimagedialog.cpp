/****************************************************************************
** Meta object code from reading C++ file 'resizeimagedialog.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.15.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../../../src/resizeimagedialog.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'resizeimagedialog.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.15.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_LxImage__ResizeImageDialog_t {
    QByteArrayData data[7];
    char stringdata0[124];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_LxImage__ResizeImageDialog_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_LxImage__ResizeImageDialog_t qt_meta_stringdata_LxImage__ResizeImageDialog = {
    {
QT_MOC_LITERAL(0, 0, 26), // "LxImage::ResizeImageDialog"
QT_MOC_LITERAL(1, 27, 14), // "onWidthChanged"
QT_MOC_LITERAL(2, 42, 0), // ""
QT_MOC_LITERAL(3, 43, 15), // "onHeightChanged"
QT_MOC_LITERAL(4, 59, 21), // "onWidthPercentChanged"
QT_MOC_LITERAL(5, 81, 22), // "onHeightPercentChanged"
QT_MOC_LITERAL(6, 104, 19) // "onKeepAspectChanged"

    },
    "LxImage::ResizeImageDialog\0onWidthChanged\0"
    "\0onHeightChanged\0onWidthPercentChanged\0"
    "onHeightPercentChanged\0onKeepAspectChanged"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_LxImage__ResizeImageDialog[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   39,    2, 0x08 /* Private */,
       3,    1,   42,    2, 0x08 /* Private */,
       4,    1,   45,    2, 0x08 /* Private */,
       5,    1,   48,    2, 0x08 /* Private */,
       6,    1,   51,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Bool,    2,

       0        // eod
};

void LxImage::ResizeImageDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<ResizeImageDialog *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->onWidthChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->onHeightChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->onWidthPercentChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 3: _t->onHeightPercentChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 4: _t->onKeepAspectChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject LxImage::ResizeImageDialog::staticMetaObject = { {
    QMetaObject::SuperData::link<QDialog::staticMetaObject>(),
    qt_meta_stringdata_LxImage__ResizeImageDialog.data,
    qt_meta_data_LxImage__ResizeImageDialog,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *LxImage::ResizeImageDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *LxImage::ResizeImageDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_LxImage__ResizeImageDialog.stringdata0))
        return static_cast<void*>(this);
    return QDialog::qt_metacast(_clname);
}

int LxImage::ResizeImageDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 5)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 5;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
