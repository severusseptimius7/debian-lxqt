/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.15.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../../../src/mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.15.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_LxImage__MainWindow_t {
    QByteArrayData data[60];
    char stringdata0[1430];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_LxImage__MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_LxImage__MainWindow_t qt_meta_stringdata_LxImage__MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 19), // "LxImage::MainWindow"
QT_MOC_LITERAL(1, 20, 26), // "on_actionMenubar_triggered"
QT_MOC_LITERAL(2, 47, 0), // ""
QT_MOC_LITERAL(3, 48, 7), // "checked"
QT_MOC_LITERAL(4, 56, 24), // "on_actionAbout_triggered"
QT_MOC_LITERAL(5, 81, 34), // "on_actionHiddenShortcuts_trig..."
QT_MOC_LITERAL(6, 116, 27), // "on_actionOpenFile_triggered"
QT_MOC_LITERAL(7, 144, 32), // "on_actionOpenDirectory_triggered"
QT_MOC_LITERAL(8, 177, 25), // "on_actionReload_triggered"
QT_MOC_LITERAL(9, 203, 28), // "on_actionNewWindow_triggered"
QT_MOC_LITERAL(10, 232, 23), // "on_actionSave_triggered"
QT_MOC_LITERAL(11, 256, 25), // "on_actionSaveAs_triggered"
QT_MOC_LITERAL(12, 282, 24), // "on_actionPrint_triggered"
QT_MOC_LITERAL(13, 307, 25), // "on_actionDelete_triggered"
QT_MOC_LITERAL(14, 333, 33), // "on_actionFileProperties_trigg..."
QT_MOC_LITERAL(15, 367, 24), // "on_actionClose_triggered"
QT_MOC_LITERAL(16, 392, 34), // "on_actionRotateClockwise_trig..."
QT_MOC_LITERAL(17, 427, 41), // "on_actionRotateCounterclockwi..."
QT_MOC_LITERAL(18, 469, 31), // "on_actionFlipVertical_triggered"
QT_MOC_LITERAL(19, 501, 33), // "on_actionFlipHorizontal_trigg..."
QT_MOC_LITERAL(20, 535, 25), // "on_actionResize_triggered"
QT_MOC_LITERAL(21, 561, 23), // "on_actionCopy_triggered"
QT_MOC_LITERAL(22, 585, 27), // "on_actionCopyPath_triggered"
QT_MOC_LITERAL(23, 613, 29), // "on_actionRenameFile_triggered"
QT_MOC_LITERAL(24, 643, 24), // "on_actionPaste_triggered"
QT_MOC_LITERAL(25, 668, 25), // "on_actionUpload_triggered"
QT_MOC_LITERAL(26, 694, 33), // "on_actionShowThumbnails_trigg..."
QT_MOC_LITERAL(27, 728, 30), // "on_actionShowOutline_triggered"
QT_MOC_LITERAL(28, 759, 31), // "on_actionShowExifData_triggered"
QT_MOC_LITERAL(29, 791, 29), // "on_actionFullScreen_triggered"
QT_MOC_LITERAL(30, 821, 28), // "on_actionSlideShow_triggered"
QT_MOC_LITERAL(31, 850, 27), // "on_actionPrevious_triggered"
QT_MOC_LITERAL(32, 878, 23), // "on_actionNext_triggered"
QT_MOC_LITERAL(33, 902, 24), // "on_actionFirst_triggered"
QT_MOC_LITERAL(34, 927, 23), // "on_actionLast_triggered"
QT_MOC_LITERAL(35, 951, 25), // "on_actionZoomIn_triggered"
QT_MOC_LITERAL(36, 977, 26), // "on_actionZoomOut_triggered"
QT_MOC_LITERAL(37, 1004, 31), // "on_actionOriginalSize_triggered"
QT_MOC_LITERAL(38, 1036, 26), // "on_actionZoomFit_triggered"
QT_MOC_LITERAL(39, 1063, 9), // "onZooming"
QT_MOC_LITERAL(40, 1073, 27), // "on_actionDrawNone_triggered"
QT_MOC_LITERAL(41, 1101, 28), // "on_actionDrawArrow_triggered"
QT_MOC_LITERAL(42, 1130, 32), // "on_actionDrawRectangle_triggered"
QT_MOC_LITERAL(43, 1163, 29), // "on_actionDrawCircle_triggered"
QT_MOC_LITERAL(44, 1193, 29), // "on_actionDrawNumber_triggered"
QT_MOC_LITERAL(45, 1223, 13), // "onContextMenu"
QT_MOC_LITERAL(46, 1237, 3), // "pos"
QT_MOC_LITERAL(47, 1241, 16), // "onKeyboardEscape"
QT_MOC_LITERAL(48, 1258, 21), // "onThumbnailSelChanged"
QT_MOC_LITERAL(49, 1280, 14), // "QItemSelection"
QT_MOC_LITERAL(50, 1295, 8), // "selected"
QT_MOC_LITERAL(51, 1304, 10), // "deselected"
QT_MOC_LITERAL(52, 1315, 14), // "onFilesRemoved"
QT_MOC_LITERAL(53, 1330, 16), // "Fm::FileInfoList"
QT_MOC_LITERAL(54, 1347, 5), // "files"
QT_MOC_LITERAL(55, 1353, 13), // "onFileDropped"
QT_MOC_LITERAL(56, 1367, 4), // "path"
QT_MOC_LITERAL(57, 1372, 19), // "fileMenuAboutToShow"
QT_MOC_LITERAL(58, 1392, 18), // "createOpenWithMenu"
QT_MOC_LITERAL(59, 1411, 18) // "deleteOpenWithMenu"

    },
    "LxImage::MainWindow\0on_actionMenubar_triggered\0"
    "\0checked\0on_actionAbout_triggered\0"
    "on_actionHiddenShortcuts_triggered\0"
    "on_actionOpenFile_triggered\0"
    "on_actionOpenDirectory_triggered\0"
    "on_actionReload_triggered\0"
    "on_actionNewWindow_triggered\0"
    "on_actionSave_triggered\0"
    "on_actionSaveAs_triggered\0"
    "on_actionPrint_triggered\0"
    "on_actionDelete_triggered\0"
    "on_actionFileProperties_triggered\0"
    "on_actionClose_triggered\0"
    "on_actionRotateClockwise_triggered\0"
    "on_actionRotateCounterclockwise_triggered\0"
    "on_actionFlipVertical_triggered\0"
    "on_actionFlipHorizontal_triggered\0"
    "on_actionResize_triggered\0"
    "on_actionCopy_triggered\0"
    "on_actionCopyPath_triggered\0"
    "on_actionRenameFile_triggered\0"
    "on_actionPaste_triggered\0"
    "on_actionUpload_triggered\0"
    "on_actionShowThumbnails_triggered\0"
    "on_actionShowOutline_triggered\0"
    "on_actionShowExifData_triggered\0"
    "on_actionFullScreen_triggered\0"
    "on_actionSlideShow_triggered\0"
    "on_actionPrevious_triggered\0"
    "on_actionNext_triggered\0"
    "on_actionFirst_triggered\0"
    "on_actionLast_triggered\0"
    "on_actionZoomIn_triggered\0"
    "on_actionZoomOut_triggered\0"
    "on_actionOriginalSize_triggered\0"
    "on_actionZoomFit_triggered\0onZooming\0"
    "on_actionDrawNone_triggered\0"
    "on_actionDrawArrow_triggered\0"
    "on_actionDrawRectangle_triggered\0"
    "on_actionDrawCircle_triggered\0"
    "on_actionDrawNumber_triggered\0"
    "onContextMenu\0pos\0onKeyboardEscape\0"
    "onThumbnailSelChanged\0QItemSelection\0"
    "selected\0deselected\0onFilesRemoved\0"
    "Fm::FileInfoList\0files\0onFileDropped\0"
    "path\0fileMenuAboutToShow\0createOpenWithMenu\0"
    "deleteOpenWithMenu"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_LxImage__MainWindow[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      50,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,  264,    2, 0x08 /* Private */,
       4,    0,  267,    2, 0x08 /* Private */,
       5,    0,  268,    2, 0x08 /* Private */,
       6,    0,  269,    2, 0x08 /* Private */,
       7,    0,  270,    2, 0x08 /* Private */,
       8,    0,  271,    2, 0x08 /* Private */,
       9,    0,  272,    2, 0x08 /* Private */,
      10,    0,  273,    2, 0x08 /* Private */,
      11,    0,  274,    2, 0x08 /* Private */,
      12,    0,  275,    2, 0x08 /* Private */,
      13,    0,  276,    2, 0x08 /* Private */,
      14,    0,  277,    2, 0x08 /* Private */,
      15,    0,  278,    2, 0x08 /* Private */,
      16,    0,  279,    2, 0x08 /* Private */,
      17,    0,  280,    2, 0x08 /* Private */,
      18,    0,  281,    2, 0x08 /* Private */,
      19,    0,  282,    2, 0x08 /* Private */,
      20,    0,  283,    2, 0x08 /* Private */,
      21,    0,  284,    2, 0x08 /* Private */,
      22,    0,  285,    2, 0x08 /* Private */,
      23,    0,  286,    2, 0x08 /* Private */,
      24,    0,  287,    2, 0x08 /* Private */,
      25,    0,  288,    2, 0x08 /* Private */,
      26,    1,  289,    2, 0x08 /* Private */,
      27,    1,  292,    2, 0x08 /* Private */,
      28,    1,  295,    2, 0x08 /* Private */,
      29,    1,  298,    2, 0x08 /* Private */,
      30,    1,  301,    2, 0x08 /* Private */,
      31,    0,  304,    2, 0x08 /* Private */,
      32,    0,  305,    2, 0x08 /* Private */,
      33,    0,  306,    2, 0x08 /* Private */,
      34,    0,  307,    2, 0x08 /* Private */,
      35,    0,  308,    2, 0x08 /* Private */,
      36,    0,  309,    2, 0x08 /* Private */,
      37,    0,  310,    2, 0x08 /* Private */,
      38,    0,  311,    2, 0x08 /* Private */,
      39,    0,  312,    2, 0x08 /* Private */,
      40,    0,  313,    2, 0x08 /* Private */,
      41,    0,  314,    2, 0x08 /* Private */,
      42,    0,  315,    2, 0x08 /* Private */,
      43,    0,  316,    2, 0x08 /* Private */,
      44,    0,  317,    2, 0x08 /* Private */,
      45,    1,  318,    2, 0x08 /* Private */,
      47,    0,  321,    2, 0x08 /* Private */,
      48,    2,  322,    2, 0x08 /* Private */,
      52,    1,  327,    2, 0x08 /* Private */,
      55,    1,  330,    2, 0x08 /* Private */,
      57,    0,  333,    2, 0x08 /* Private */,
      58,    0,  334,    2, 0x08 /* Private */,
      59,    0,  335,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QPoint,   46,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 49, 0x80000000 | 49,   50,   51,
    QMetaType::Void, 0x80000000 | 53,   54,
    QMetaType::Void, QMetaType::QString,   56,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void LxImage::MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_actionMenubar_triggered((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 1: _t->on_actionAbout_triggered(); break;
        case 2: _t->on_actionHiddenShortcuts_triggered(); break;
        case 3: _t->on_actionOpenFile_triggered(); break;
        case 4: _t->on_actionOpenDirectory_triggered(); break;
        case 5: _t->on_actionReload_triggered(); break;
        case 6: _t->on_actionNewWindow_triggered(); break;
        case 7: _t->on_actionSave_triggered(); break;
        case 8: _t->on_actionSaveAs_triggered(); break;
        case 9: _t->on_actionPrint_triggered(); break;
        case 10: _t->on_actionDelete_triggered(); break;
        case 11: _t->on_actionFileProperties_triggered(); break;
        case 12: _t->on_actionClose_triggered(); break;
        case 13: _t->on_actionRotateClockwise_triggered(); break;
        case 14: _t->on_actionRotateCounterclockwise_triggered(); break;
        case 15: _t->on_actionFlipVertical_triggered(); break;
        case 16: _t->on_actionFlipHorizontal_triggered(); break;
        case 17: _t->on_actionResize_triggered(); break;
        case 18: _t->on_actionCopy_triggered(); break;
        case 19: _t->on_actionCopyPath_triggered(); break;
        case 20: _t->on_actionRenameFile_triggered(); break;
        case 21: _t->on_actionPaste_triggered(); break;
        case 22: _t->on_actionUpload_triggered(); break;
        case 23: _t->on_actionShowThumbnails_triggered((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 24: _t->on_actionShowOutline_triggered((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 25: _t->on_actionShowExifData_triggered((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 26: _t->on_actionFullScreen_triggered((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 27: _t->on_actionSlideShow_triggered((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 28: _t->on_actionPrevious_triggered(); break;
        case 29: _t->on_actionNext_triggered(); break;
        case 30: _t->on_actionFirst_triggered(); break;
        case 31: _t->on_actionLast_triggered(); break;
        case 32: _t->on_actionZoomIn_triggered(); break;
        case 33: _t->on_actionZoomOut_triggered(); break;
        case 34: _t->on_actionOriginalSize_triggered(); break;
        case 35: _t->on_actionZoomFit_triggered(); break;
        case 36: _t->onZooming(); break;
        case 37: _t->on_actionDrawNone_triggered(); break;
        case 38: _t->on_actionDrawArrow_triggered(); break;
        case 39: _t->on_actionDrawRectangle_triggered(); break;
        case 40: _t->on_actionDrawCircle_triggered(); break;
        case 41: _t->on_actionDrawNumber_triggered(); break;
        case 42: _t->onContextMenu((*reinterpret_cast< QPoint(*)>(_a[1]))); break;
        case 43: _t->onKeyboardEscape(); break;
        case 44: _t->onThumbnailSelChanged((*reinterpret_cast< const QItemSelection(*)>(_a[1])),(*reinterpret_cast< const QItemSelection(*)>(_a[2]))); break;
        case 45: _t->onFilesRemoved((*reinterpret_cast< const Fm::FileInfoList(*)>(_a[1]))); break;
        case 46: _t->onFileDropped((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 47: _t->fileMenuAboutToShow(); break;
        case 48: _t->createOpenWithMenu(); break;
        case 49: _t->deleteOpenWithMenu(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 44:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 1:
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QItemSelection >(); break;
            }
            break;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject LxImage::MainWindow::staticMetaObject = { {
    QMetaObject::SuperData::link<QMainWindow::staticMetaObject>(),
    qt_meta_stringdata_LxImage__MainWindow.data,
    qt_meta_data_LxImage__MainWindow,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *LxImage::MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *LxImage::MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_LxImage__MainWindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int LxImage::MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 50)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 50;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 50)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 50;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
