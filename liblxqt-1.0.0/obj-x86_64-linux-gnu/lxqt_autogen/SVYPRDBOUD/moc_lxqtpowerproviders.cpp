/****************************************************************************
** Meta object code from reading C++ file 'lxqtpowerproviders.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.15.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../../lxqtpower/lxqtpowerproviders.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'lxqtpowerproviders.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.15.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_LXQt__PowerProvider_t {
    QByteArrayData data[5];
    char stringdata0[51];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_LXQt__PowerProvider_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_LXQt__PowerProvider_t qt_meta_stringdata_LXQt__PowerProvider = {
    {
QT_MOC_LITERAL(0, 0, 19), // "LXQt::PowerProvider"
QT_MOC_LITERAL(1, 20, 8), // "doAction"
QT_MOC_LITERAL(2, 29, 0), // ""
QT_MOC_LITERAL(3, 30, 13), // "Power::Action"
QT_MOC_LITERAL(4, 44, 6) // "action"

    },
    "LXQt::PowerProvider\0doAction\0\0"
    "Power::Action\0action"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_LXQt__PowerProvider[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   19,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Bool, 0x80000000 | 3,    4,

       0        // eod
};

void LXQt::PowerProvider::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<PowerProvider *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: { bool _r = _t->doAction((*reinterpret_cast< Power::Action(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject LXQt::PowerProvider::staticMetaObject = { {
    QMetaObject::SuperData::link<QObject::staticMetaObject>(),
    qt_meta_stringdata_LXQt__PowerProvider.data,
    qt_meta_data_LXQt__PowerProvider,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *LXQt::PowerProvider::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *LXQt::PowerProvider::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_LXQt__PowerProvider.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int LXQt::PowerProvider::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 1)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 1;
    }
    return _id;
}
struct qt_meta_stringdata_LXQt__UPowerProvider_t {
    QByteArrayData data[5];
    char stringdata0[52];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_LXQt__UPowerProvider_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_LXQt__UPowerProvider_t qt_meta_stringdata_LXQt__UPowerProvider = {
    {
QT_MOC_LITERAL(0, 0, 20), // "LXQt::UPowerProvider"
QT_MOC_LITERAL(1, 21, 8), // "doAction"
QT_MOC_LITERAL(2, 30, 0), // ""
QT_MOC_LITERAL(3, 31, 13), // "Power::Action"
QT_MOC_LITERAL(4, 45, 6) // "action"

    },
    "LXQt::UPowerProvider\0doAction\0\0"
    "Power::Action\0action"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_LXQt__UPowerProvider[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   19,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Bool, 0x80000000 | 3,    4,

       0        // eod
};

void LXQt::UPowerProvider::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<UPowerProvider *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: { bool _r = _t->doAction((*reinterpret_cast< Power::Action(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject LXQt::UPowerProvider::staticMetaObject = { {
    QMetaObject::SuperData::link<PowerProvider::staticMetaObject>(),
    qt_meta_stringdata_LXQt__UPowerProvider.data,
    qt_meta_data_LXQt__UPowerProvider,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *LXQt::UPowerProvider::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *LXQt::UPowerProvider::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_LXQt__UPowerProvider.stringdata0))
        return static_cast<void*>(this);
    return PowerProvider::qt_metacast(_clname);
}

int LXQt::UPowerProvider::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = PowerProvider::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 1)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 1;
    }
    return _id;
}
struct qt_meta_stringdata_LXQt__ConsoleKitProvider_t {
    QByteArrayData data[5];
    char stringdata0[56];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_LXQt__ConsoleKitProvider_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_LXQt__ConsoleKitProvider_t qt_meta_stringdata_LXQt__ConsoleKitProvider = {
    {
QT_MOC_LITERAL(0, 0, 24), // "LXQt::ConsoleKitProvider"
QT_MOC_LITERAL(1, 25, 8), // "doAction"
QT_MOC_LITERAL(2, 34, 0), // ""
QT_MOC_LITERAL(3, 35, 13), // "Power::Action"
QT_MOC_LITERAL(4, 49, 6) // "action"

    },
    "LXQt::ConsoleKitProvider\0doAction\0\0"
    "Power::Action\0action"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_LXQt__ConsoleKitProvider[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   19,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Bool, 0x80000000 | 3,    4,

       0        // eod
};

void LXQt::ConsoleKitProvider::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<ConsoleKitProvider *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: { bool _r = _t->doAction((*reinterpret_cast< Power::Action(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject LXQt::ConsoleKitProvider::staticMetaObject = { {
    QMetaObject::SuperData::link<PowerProvider::staticMetaObject>(),
    qt_meta_stringdata_LXQt__ConsoleKitProvider.data,
    qt_meta_data_LXQt__ConsoleKitProvider,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *LXQt::ConsoleKitProvider::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *LXQt::ConsoleKitProvider::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_LXQt__ConsoleKitProvider.stringdata0))
        return static_cast<void*>(this);
    return PowerProvider::qt_metacast(_clname);
}

int LXQt::ConsoleKitProvider::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = PowerProvider::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 1)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 1;
    }
    return _id;
}
struct qt_meta_stringdata_LXQt__SystemdProvider_t {
    QByteArrayData data[5];
    char stringdata0[53];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_LXQt__SystemdProvider_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_LXQt__SystemdProvider_t qt_meta_stringdata_LXQt__SystemdProvider = {
    {
QT_MOC_LITERAL(0, 0, 21), // "LXQt::SystemdProvider"
QT_MOC_LITERAL(1, 22, 8), // "doAction"
QT_MOC_LITERAL(2, 31, 0), // ""
QT_MOC_LITERAL(3, 32, 13), // "Power::Action"
QT_MOC_LITERAL(4, 46, 6) // "action"

    },
    "LXQt::SystemdProvider\0doAction\0\0"
    "Power::Action\0action"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_LXQt__SystemdProvider[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   19,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Bool, 0x80000000 | 3,    4,

       0        // eod
};

void LXQt::SystemdProvider::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<SystemdProvider *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: { bool _r = _t->doAction((*reinterpret_cast< Power::Action(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject LXQt::SystemdProvider::staticMetaObject = { {
    QMetaObject::SuperData::link<PowerProvider::staticMetaObject>(),
    qt_meta_stringdata_LXQt__SystemdProvider.data,
    qt_meta_data_LXQt__SystemdProvider,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *LXQt::SystemdProvider::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *LXQt::SystemdProvider::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_LXQt__SystemdProvider.stringdata0))
        return static_cast<void*>(this);
    return PowerProvider::qt_metacast(_clname);
}

int LXQt::SystemdProvider::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = PowerProvider::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 1)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 1;
    }
    return _id;
}
struct qt_meta_stringdata_LXQt__LXQtProvider_t {
    QByteArrayData data[5];
    char stringdata0[50];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_LXQt__LXQtProvider_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_LXQt__LXQtProvider_t qt_meta_stringdata_LXQt__LXQtProvider = {
    {
QT_MOC_LITERAL(0, 0, 18), // "LXQt::LXQtProvider"
QT_MOC_LITERAL(1, 19, 8), // "doAction"
QT_MOC_LITERAL(2, 28, 0), // ""
QT_MOC_LITERAL(3, 29, 13), // "Power::Action"
QT_MOC_LITERAL(4, 43, 6) // "action"

    },
    "LXQt::LXQtProvider\0doAction\0\0Power::Action\0"
    "action"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_LXQt__LXQtProvider[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   19,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Bool, 0x80000000 | 3,    4,

       0        // eod
};

void LXQt::LXQtProvider::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<LXQtProvider *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: { bool _r = _t->doAction((*reinterpret_cast< Power::Action(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject LXQt::LXQtProvider::staticMetaObject = { {
    QMetaObject::SuperData::link<PowerProvider::staticMetaObject>(),
    qt_meta_stringdata_LXQt__LXQtProvider.data,
    qt_meta_data_LXQt__LXQtProvider,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *LXQt::LXQtProvider::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *LXQt::LXQtProvider::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_LXQt__LXQtProvider.stringdata0))
        return static_cast<void*>(this);
    return PowerProvider::qt_metacast(_clname);
}

int LXQt::LXQtProvider::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = PowerProvider::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 1)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 1;
    }
    return _id;
}
struct qt_meta_stringdata_LXQt__LxSessionProvider_t {
    QByteArrayData data[5];
    char stringdata0[55];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_LXQt__LxSessionProvider_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_LXQt__LxSessionProvider_t qt_meta_stringdata_LXQt__LxSessionProvider = {
    {
QT_MOC_LITERAL(0, 0, 23), // "LXQt::LxSessionProvider"
QT_MOC_LITERAL(1, 24, 8), // "doAction"
QT_MOC_LITERAL(2, 33, 0), // ""
QT_MOC_LITERAL(3, 34, 13), // "Power::Action"
QT_MOC_LITERAL(4, 48, 6) // "action"

    },
    "LXQt::LxSessionProvider\0doAction\0\0"
    "Power::Action\0action"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_LXQt__LxSessionProvider[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   19,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Bool, 0x80000000 | 3,    4,

       0        // eod
};

void LXQt::LxSessionProvider::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<LxSessionProvider *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: { bool _r = _t->doAction((*reinterpret_cast< Power::Action(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject LXQt::LxSessionProvider::staticMetaObject = { {
    QMetaObject::SuperData::link<PowerProvider::staticMetaObject>(),
    qt_meta_stringdata_LXQt__LxSessionProvider.data,
    qt_meta_data_LXQt__LxSessionProvider,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *LXQt::LxSessionProvider::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *LXQt::LxSessionProvider::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_LXQt__LxSessionProvider.stringdata0))
        return static_cast<void*>(this);
    return PowerProvider::qt_metacast(_clname);
}

int LXQt::LxSessionProvider::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = PowerProvider::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 1)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 1;
    }
    return _id;
}
struct qt_meta_stringdata_LXQt__HalProvider_t {
    QByteArrayData data[5];
    char stringdata0[49];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_LXQt__HalProvider_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_LXQt__HalProvider_t qt_meta_stringdata_LXQt__HalProvider = {
    {
QT_MOC_LITERAL(0, 0, 17), // "LXQt::HalProvider"
QT_MOC_LITERAL(1, 18, 8), // "doAction"
QT_MOC_LITERAL(2, 27, 0), // ""
QT_MOC_LITERAL(3, 28, 13), // "Power::Action"
QT_MOC_LITERAL(4, 42, 6) // "action"

    },
    "LXQt::HalProvider\0doAction\0\0Power::Action\0"
    "action"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_LXQt__HalProvider[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   19,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Bool, 0x80000000 | 3,    4,

       0        // eod
};

void LXQt::HalProvider::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<HalProvider *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: { bool _r = _t->doAction((*reinterpret_cast< Power::Action(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject LXQt::HalProvider::staticMetaObject = { {
    QMetaObject::SuperData::link<PowerProvider::staticMetaObject>(),
    qt_meta_stringdata_LXQt__HalProvider.data,
    qt_meta_data_LXQt__HalProvider,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *LXQt::HalProvider::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *LXQt::HalProvider::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_LXQt__HalProvider.stringdata0))
        return static_cast<void*>(this);
    return PowerProvider::qt_metacast(_clname);
}

int LXQt::HalProvider::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = PowerProvider::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 1)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 1;
    }
    return _id;
}
struct qt_meta_stringdata_LXQt__CustomProvider_t {
    QByteArrayData data[5];
    char stringdata0[52];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_LXQt__CustomProvider_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_LXQt__CustomProvider_t qt_meta_stringdata_LXQt__CustomProvider = {
    {
QT_MOC_LITERAL(0, 0, 20), // "LXQt::CustomProvider"
QT_MOC_LITERAL(1, 21, 8), // "doAction"
QT_MOC_LITERAL(2, 30, 0), // ""
QT_MOC_LITERAL(3, 31, 13), // "Power::Action"
QT_MOC_LITERAL(4, 45, 6) // "action"

    },
    "LXQt::CustomProvider\0doAction\0\0"
    "Power::Action\0action"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_LXQt__CustomProvider[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   19,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Bool, 0x80000000 | 3,    4,

       0        // eod
};

void LXQt::CustomProvider::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<CustomProvider *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: { bool _r = _t->doAction((*reinterpret_cast< Power::Action(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject LXQt::CustomProvider::staticMetaObject = { {
    QMetaObject::SuperData::link<PowerProvider::staticMetaObject>(),
    qt_meta_stringdata_LXQt__CustomProvider.data,
    qt_meta_data_LXQt__CustomProvider,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *LXQt::CustomProvider::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *LXQt::CustomProvider::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_LXQt__CustomProvider.stringdata0))
        return static_cast<void*>(this);
    return PowerProvider::qt_metacast(_clname);
}

int LXQt::CustomProvider::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = PowerProvider::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 1)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 1;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
