# - Finds the lxqt package


####### Expanded from @PACKAGE_INIT@ by configure_package_config_file() #######
####### Any changes to this file will be overwritten by the next CMake run ####
####### The input file was lxqt-config.cmake.in                            ########

get_filename_component(PACKAGE_PREFIX_DIR "${CMAKE_CURRENT_LIST_DIR}/../" ABSOLUTE)

macro(set_and_check _var _file)
  set(${_var} "${_file}")
  if(NOT EXISTS "${_file}")
    message(FATAL_ERROR "File or directory ${_file} referenced by variable ${_var} does not exist !")
  endif()
endmacro()

macro(check_required_components _NAME)
  foreach(comp ${${_NAME}_FIND_COMPONENTS})
    if(NOT ${_NAME}_${comp}_FOUND)
      if(${_NAME}_FIND_REQUIRED_${comp})
        set(${_NAME}_FOUND FALSE)
      endif()
    endif()
  endforeach()
endmacro()

####################################################################################

if (CMAKE_VERSION VERSION_LESS 3.0.2)
    message(FATAL_ERROR \"liblxqt requires at least CMake version 3.0.2\")
endif()

include(CMakeFindDependencyMacro)

find_dependency(Qt5Widgets 5.15.0)
find_dependency(Qt5DBus 5.15.0)
find_dependency(Qt5LinguistTools 5.15.0)
find_dependency(Qt5Xdg 3.8.0)
find_dependency(KF5WindowSystem)
find_dependency(lxqt-build-tools 0.10.0)
if (NOT APPLE)
    find_dependency(Qt5X11Extras 5.15.0)
endif (NOT APPLE)
include(LXQtConfigVars)


#  - Set version informations
set(LXQT_MAJOR_VERSION      "1")
set(LXQT_MINOR_VERSION      "0")
set(LXQT_PATCH_VERSION      "0")
set(LXQT_VERSION            "1.0.0")

add_definitions("-DLXQT_MAJOR_VERSION=\"${LXQT_MAJOR_VERSION}\"")
add_definitions("-DLXQT_MINOR_VERSION=\"${LXQT_MINOR_VERSION}\"")
add_definitions("-DLXQT_PATCH_VERSION=\"${LXQT_PATCH_VERSION}\"")
add_definitions("-DLXQT_VERSION=\"${LXQT_VERSION}\"")

if (NOT TARGET lxqt)
    if (POLICY CMP0024)
        cmake_policy(SET CMP0024 NEW)
    endif()
    include("${CMAKE_CURRENT_LIST_DIR}/lxqt-targets.cmake")
endif()
