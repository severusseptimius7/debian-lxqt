# Install script for directory: /home/debian/lxqt/liblxqt/liblxqt-1.0.0

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "RelWithDebInfo")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

# Set default install directory permissions.
if(NOT DEFINED CMAKE_OBJDUMP)
  set(CMAKE_OBJDUMP "/usr/bin/objdump")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xRuntimex" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/share/lxqt/translations/liblxqt/liblxqt_ar.qm;/usr/share/lxqt/translations/liblxqt/liblxqt_arn.qm;/usr/share/lxqt/translations/liblxqt/liblxqt_ast.qm;/usr/share/lxqt/translations/liblxqt/liblxqt_bg.qm;/usr/share/lxqt/translations/liblxqt/liblxqt_ca.qm;/usr/share/lxqt/translations/liblxqt/liblxqt_cs.qm;/usr/share/lxqt/translations/liblxqt/liblxqt_cy.qm;/usr/share/lxqt/translations/liblxqt/liblxqt_da.qm;/usr/share/lxqt/translations/liblxqt/liblxqt_de.qm;/usr/share/lxqt/translations/liblxqt/liblxqt_el.qm;/usr/share/lxqt/translations/liblxqt/liblxqt_en_GB.qm;/usr/share/lxqt/translations/liblxqt/liblxqt_eo.qm;/usr/share/lxqt/translations/liblxqt/liblxqt_es.qm;/usr/share/lxqt/translations/liblxqt/liblxqt_es_VE.qm;/usr/share/lxqt/translations/liblxqt/liblxqt_et.qm;/usr/share/lxqt/translations/liblxqt/liblxqt_eu.qm;/usr/share/lxqt/translations/liblxqt/liblxqt_fi.qm;/usr/share/lxqt/translations/liblxqt/liblxqt_fr.qm;/usr/share/lxqt/translations/liblxqt/liblxqt_gl.qm;/usr/share/lxqt/translations/liblxqt/liblxqt_he.qm;/usr/share/lxqt/translations/liblxqt/liblxqt_hr.qm;/usr/share/lxqt/translations/liblxqt/liblxqt_hu.qm;/usr/share/lxqt/translations/liblxqt/liblxqt_ia.qm;/usr/share/lxqt/translations/liblxqt/liblxqt_id.qm;/usr/share/lxqt/translations/liblxqt/liblxqt_it.qm;/usr/share/lxqt/translations/liblxqt/liblxqt_ja.qm;/usr/share/lxqt/translations/liblxqt/liblxqt_ko.qm;/usr/share/lxqt/translations/liblxqt/liblxqt_lt.qm;/usr/share/lxqt/translations/liblxqt/liblxqt_lv.qm;/usr/share/lxqt/translations/liblxqt/liblxqt_nb_NO.qm;/usr/share/lxqt/translations/liblxqt/liblxqt_nl.qm;/usr/share/lxqt/translations/liblxqt/liblxqt_pl.qm;/usr/share/lxqt/translations/liblxqt/liblxqt_pt.qm;/usr/share/lxqt/translations/liblxqt/liblxqt_pt_BR.qm;/usr/share/lxqt/translations/liblxqt/liblxqt_ro_RO.qm;/usr/share/lxqt/translations/liblxqt/liblxqt_ru.qm;/usr/share/lxqt/translations/liblxqt/liblxqt_si.qm;/usr/share/lxqt/translations/liblxqt/liblxqt_sk_SK.qm;/usr/share/lxqt/translations/liblxqt/liblxqt_sl.qm;/usr/share/lxqt/translations/liblxqt/liblxqt_sr@latin.qm;/usr/share/lxqt/translations/liblxqt/liblxqt_sr_RS.qm;/usr/share/lxqt/translations/liblxqt/liblxqt_th_TH.qm;/usr/share/lxqt/translations/liblxqt/liblxqt_tr.qm;/usr/share/lxqt/translations/liblxqt/liblxqt_uk.qm;/usr/share/lxqt/translations/liblxqt/liblxqt_zh_CN.qm;/usr/share/lxqt/translations/liblxqt/liblxqt_zh_TW.qm")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/usr/share/lxqt/translations/liblxqt" TYPE FILE FILES
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/liblxqt_ar.qm"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/liblxqt_arn.qm"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/liblxqt_ast.qm"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/liblxqt_bg.qm"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/liblxqt_ca.qm"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/liblxqt_cs.qm"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/liblxqt_cy.qm"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/liblxqt_da.qm"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/liblxqt_de.qm"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/liblxqt_el.qm"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/liblxqt_en_GB.qm"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/liblxqt_eo.qm"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/liblxqt_es.qm"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/liblxqt_es_VE.qm"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/liblxqt_et.qm"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/liblxqt_eu.qm"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/liblxqt_fi.qm"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/liblxqt_fr.qm"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/liblxqt_gl.qm"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/liblxqt_he.qm"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/liblxqt_hr.qm"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/liblxqt_hu.qm"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/liblxqt_ia.qm"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/liblxqt_id.qm"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/liblxqt_it.qm"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/liblxqt_ja.qm"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/liblxqt_ko.qm"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/liblxqt_lt.qm"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/liblxqt_lv.qm"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/liblxqt_nb_NO.qm"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/liblxqt_nl.qm"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/liblxqt_pl.qm"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/liblxqt_pt.qm"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/liblxqt_pt_BR.qm"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/liblxqt_ro_RO.qm"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/liblxqt_ru.qm"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/liblxqt_si.qm"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/liblxqt_sk_SK.qm"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/liblxqt_sl.qm"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/liblxqt_sr@latin.qm"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/liblxqt_sr_RS.qm"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/liblxqt_th_TH.qm"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/liblxqt_tr.qm"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/liblxqt_uk.qm"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/liblxqt_zh_CN.qm"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/liblxqt_zh_TW.qm"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xDevelx" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/share/cmake/lxqt/lxqt-config.cmake;/usr/share/cmake/lxqt/lxqt-config-version.cmake")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/usr/share/cmake/lxqt" TYPE FILE FILES
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/install/lxqt-config.cmake"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/lxqt-config-version.cmake"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xDevelx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/share/cmake/lxqt/lxqt-targets.cmake")
    file(DIFFERENT EXPORT_FILE_CHANGED FILES
         "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/share/cmake/lxqt/lxqt-targets.cmake"
         "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/CMakeFiles/Export/share/cmake/lxqt/lxqt-targets.cmake")
    if(EXPORT_FILE_CHANGED)
      file(GLOB OLD_CONFIG_FILES "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/share/cmake/lxqt/lxqt-targets-*.cmake")
      if(OLD_CONFIG_FILES)
        message(STATUS "Old export file \"$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/share/cmake/lxqt/lxqt-targets.cmake\" will be replaced.  Removing files [${OLD_CONFIG_FILES}].")
        file(REMOVE ${OLD_CONFIG_FILES})
      endif()
    endif()
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/cmake/lxqt" TYPE FILE FILES "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/CMakeFiles/Export/share/cmake/lxqt/lxqt-targets.cmake")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/cmake/lxqt" TYPE FILE FILES "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/CMakeFiles/Export/share/cmake/lxqt/lxqt-targets-relwithdebinfo.cmake")
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xRuntimex" OR NOT CMAKE_INSTALL_COMPONENT)
  foreach(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/x86_64-linux-gnu/liblxqt.so.1.0.0"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/x86_64-linux-gnu/liblxqt.so.1"
      )
    if(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      file(RPATH_CHECK
           FILE "${file}"
           RPATH "")
    endif()
  endforeach()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/x86_64-linux-gnu" TYPE SHARED_LIBRARY FILES
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/liblxqt.so.1.0.0"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/liblxqt.so.1"
    )
  foreach(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/x86_64-linux-gnu/liblxqt.so.1.0.0"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/x86_64-linux-gnu/liblxqt.so.1"
      )
    if(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      if(CMAKE_INSTALL_DO_STRIP)
        execute_process(COMMAND "/usr/bin/strip" "${file}")
      endif()
    endif()
  endforeach()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xRuntimex" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/x86_64-linux-gnu/liblxqt.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/x86_64-linux-gnu/liblxqt.so")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/x86_64-linux-gnu/liblxqt.so"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/x86_64-linux-gnu" TYPE SHARED_LIBRARY FILES "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/liblxqt.so")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/x86_64-linux-gnu/liblxqt.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/x86_64-linux-gnu/liblxqt.so")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/x86_64-linux-gnu/liblxqt.so")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xDevelx" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/include/lxqt/LXQt/lxqthtmldelegate.h;/usr/include/lxqt/LXQt/lxqtsettings.h;/usr/include/lxqt/LXQt/lxqtplugininfo.h;/usr/include/lxqt/LXQt/lxqtpowermanager.h;/usr/include/lxqt/LXQt/lxqtapplication.h;/usr/include/lxqt/LXQt/lxqtsingleapplication.h;/usr/include/lxqt/LXQt/lxqttranslator.h;/usr/include/lxqt/LXQt/lxqtprogramfinder.h;/usr/include/lxqt/LXQt/lxqtconfigdialog.h;/usr/include/lxqt/LXQt/lxqtconfigdialogcmdlineoptions.h;/usr/include/lxqt/LXQt/lxqtpageselectwidget.h;/usr/include/lxqt/LXQt/lxqtpower.h;/usr/include/lxqt/LXQt/lxqtnotification.h;/usr/include/lxqt/LXQt/lxqtautostartentry.h;/usr/include/lxqt/LXQt/lxqtgridlayout.h;/usr/include/lxqt/LXQt/lxqtrotatedwidget.h;/usr/include/lxqt/LXQt/lxqtglobals.h;/usr/include/lxqt/LXQt/lxqtbacklight.h;/usr/include/lxqt/LXQt/lxqtscreensaver.h")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/usr/include/lxqt/LXQt" TYPE FILE FILES
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/lxqthtmldelegate.h"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/lxqtsettings.h"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/lxqtplugininfo.h"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/lxqtpowermanager.h"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/lxqtapplication.h"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/lxqtsingleapplication.h"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/lxqttranslator.h"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/lxqtprogramfinder.h"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/configdialog/lxqtconfigdialog.h"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/configdialog/lxqtconfigdialogcmdlineoptions.h"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/configdialog/lxqtpageselectwidget.h"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/lxqtpower/lxqtpower.h"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/lxqtnotification.h"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/lxqtautostartentry.h"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/lxqtgridlayout.h"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/lxqtrotatedwidget.h"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/lxqtglobals.h"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/lxqtbacklight.h"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/lxqtscreensaver.h"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xDevelx" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/include/lxqt/LXQt/HtmlDelegate;/usr/include/lxqt/LXQt/Settings;/usr/include/lxqt/LXQt/PluginInfo;/usr/include/lxqt/LXQt/PowerManager;/usr/include/lxqt/LXQt/Application;/usr/include/lxqt/LXQt/SingleApplication;/usr/include/lxqt/LXQt/Translator;/usr/include/lxqt/LXQt/ProgramFinder;/usr/include/lxqt/LXQt/ConfigDialog;/usr/include/lxqt/LXQt/ConfigDialogCmdLineOptions;/usr/include/lxqt/LXQt/PageSelectWidget;/usr/include/lxqt/LXQt/Power;/usr/include/lxqt/LXQt/Notification;/usr/include/lxqt/LXQt/AutostartEntry;/usr/include/lxqt/LXQt/GridLayout;/usr/include/lxqt/LXQt/RotatedWidget;/usr/include/lxqt/LXQt/Globals;/usr/include/lxqt/LXQt/Backlight;/usr/include/lxqt/LXQt/ScreenSaver")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/usr/include/lxqt/LXQt" TYPE FILE FILES
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/CMakeFiles/include/LXQt/HtmlDelegate"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/CMakeFiles/include/LXQt/Settings"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/CMakeFiles/include/LXQt/PluginInfo"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/CMakeFiles/include/LXQt/PowerManager"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/CMakeFiles/include/LXQt/Application"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/CMakeFiles/include/LXQt/SingleApplication"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/CMakeFiles/include/LXQt/Translator"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/CMakeFiles/include/LXQt/ProgramFinder"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/CMakeFiles/include/LXQt/ConfigDialog"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/CMakeFiles/include/LXQt/ConfigDialogCmdLineOptions"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/CMakeFiles/include/LXQt/PageSelectWidget"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/CMakeFiles/include/LXQt/Power"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/CMakeFiles/include/LXQt/Notification"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/CMakeFiles/include/LXQt/AutostartEntry"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/CMakeFiles/include/LXQt/GridLayout"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/CMakeFiles/include/LXQt/RotatedWidget"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/CMakeFiles/include/LXQt/Globals"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/CMakeFiles/include/LXQt/Backlight"
    "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/CMakeFiles/include/LXQt/ScreenSaver"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xRuntimex" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/share/lxqt/power.conf")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/usr/share/lxqt" TYPE FILE FILES "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/resources/power.conf")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/share/polkit-1/actions/org.lxqt.backlight.pkexec.policy")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/usr/share/polkit-1/actions" TYPE FILE FILES "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/org.lxqt.backlight.pkexec.policy")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xDevelx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/x86_64-linux-gnu/pkgconfig" TYPE FILE FILES "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/lxqt.pc")
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/lxqtbacklight/linux_backend/driver/cmake_install.cmake")

endif()

if(CMAKE_INSTALL_COMPONENT)
  set(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
else()
  set(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
endif()

string(REPLACE ";" "\n" CMAKE_INSTALL_MANIFEST_CONTENT
       "${CMAKE_INSTALL_MANIFEST_FILES}")
file(WRITE "/home/debian/lxqt/liblxqt/liblxqt-1.0.0/obj-x86_64-linux-gnu/${CMAKE_INSTALL_MANIFEST}"
     "${CMAKE_INSTALL_MANIFEST_CONTENT}")
