/********************************************************************************
** Form generated from reading UI file 'keyboardlayoutconfig.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_KEYBOARDLAYOUTCONFIG_H
#define UI_KEYBOARDLAYOUTCONFIG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTreeWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_KeyboardLayoutConfig
{
public:
    QFormLayout *formLayout;
    QLabel *label;
    QHBoxLayout *horizontalLayout;
    QTreeWidget *layouts;
    QVBoxLayout *verticalLayout;
    QPushButton *addLayout;
    QPushButton *removeLayout;
    QPushButton *moveUp;
    QPushButton *moveDown;
    QSpacerItem *verticalSpacer;
    QLabel *label_2;
    QComboBox *keyboardModel;
    QLabel *label_3;
    QComboBox *switchKey;
    QLabel *label_4;

    void setupUi(QWidget *KeyboardLayoutConfig)
    {
        if (KeyboardLayoutConfig->objectName().isEmpty())
            KeyboardLayoutConfig->setObjectName(QString::fromUtf8("KeyboardLayoutConfig"));
        KeyboardLayoutConfig->resize(428, 322);
        formLayout = new QFormLayout(KeyboardLayoutConfig);
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        formLayout->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        label = new QLabel(KeyboardLayoutConfig);
        label->setObjectName(QString::fromUtf8("label"));

        formLayout->setWidget(0, QFormLayout::SpanningRole, label);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        layouts = new QTreeWidget(KeyboardLayoutConfig);
        layouts->setObjectName(QString::fromUtf8("layouts"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(1);
        sizePolicy.setVerticalStretch(1);
        sizePolicy.setHeightForWidth(layouts->sizePolicy().hasHeightForWidth());
        layouts->setSizePolicy(sizePolicy);
        layouts->setDragEnabled(true);
        layouts->setDragDropMode(QAbstractItemView::InternalMove);
        layouts->setRootIsDecorated(false);
        layouts->setItemsExpandable(false);

        horizontalLayout->addWidget(layouts);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        addLayout = new QPushButton(KeyboardLayoutConfig);
        addLayout->setObjectName(QString::fromUtf8("addLayout"));

        verticalLayout->addWidget(addLayout);

        removeLayout = new QPushButton(KeyboardLayoutConfig);
        removeLayout->setObjectName(QString::fromUtf8("removeLayout"));

        verticalLayout->addWidget(removeLayout);

        moveUp = new QPushButton(KeyboardLayoutConfig);
        moveUp->setObjectName(QString::fromUtf8("moveUp"));

        verticalLayout->addWidget(moveUp);

        moveDown = new QPushButton(KeyboardLayoutConfig);
        moveDown->setObjectName(QString::fromUtf8("moveDown"));

        verticalLayout->addWidget(moveDown);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        verticalLayout->setStretch(4, 1);

        horizontalLayout->addLayout(verticalLayout);

        horizontalLayout->setStretch(0, 1);

        formLayout->setLayout(1, QFormLayout::SpanningRole, horizontalLayout);

        label_2 = new QLabel(KeyboardLayoutConfig);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        formLayout->setWidget(2, QFormLayout::LabelRole, label_2);

        keyboardModel = new QComboBox(KeyboardLayoutConfig);
        keyboardModel->setObjectName(QString::fromUtf8("keyboardModel"));
        keyboardModel->setSizeAdjustPolicy(QComboBox::AdjustToMinimumContentsLengthWithIcon);

        formLayout->setWidget(2, QFormLayout::FieldRole, keyboardModel);

        label_3 = new QLabel(KeyboardLayoutConfig);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        formLayout->setWidget(3, QFormLayout::LabelRole, label_3);

        switchKey = new QComboBox(KeyboardLayoutConfig);
        switchKey->addItem(QString());
        switchKey->setObjectName(QString::fromUtf8("switchKey"));
        switchKey->setSizeAdjustPolicy(QComboBox::AdjustToMinimumContentsLengthWithIcon);

        formLayout->setWidget(3, QFormLayout::FieldRole, switchKey);

        label_4 = new QLabel(KeyboardLayoutConfig);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setWordWrap(true);

        formLayout->setWidget(4, QFormLayout::SpanningRole, label_4);


        retranslateUi(KeyboardLayoutConfig);

        QMetaObject::connectSlotsByName(KeyboardLayoutConfig);
    } // setupUi

    void retranslateUi(QWidget *KeyboardLayoutConfig)
    {
        label->setText(QCoreApplication::translate("KeyboardLayoutConfig", "<b>Keyboard Layout</b>", nullptr));
        QTreeWidgetItem *___qtreewidgetitem = layouts->headerItem();
        ___qtreewidgetitem->setText(1, QCoreApplication::translate("KeyboardLayoutConfig", "Variant", nullptr));
        ___qtreewidgetitem->setText(0, QCoreApplication::translate("KeyboardLayoutConfig", "Layout", nullptr));
        addLayout->setText(QCoreApplication::translate("KeyboardLayoutConfig", "&Add", nullptr));
        removeLayout->setText(QCoreApplication::translate("KeyboardLayoutConfig", "&Remove", nullptr));
        moveUp->setText(QCoreApplication::translate("KeyboardLayoutConfig", "Up", nullptr));
        moveDown->setText(QCoreApplication::translate("KeyboardLayoutConfig", "Down", nullptr));
        label_2->setText(QCoreApplication::translate("KeyboardLayoutConfig", "Keyboard model:", nullptr));
        label_3->setText(QCoreApplication::translate("KeyboardLayoutConfig", "Keys to change layout:", nullptr));
        switchKey->setItemText(0, QCoreApplication::translate("KeyboardLayoutConfig", "None", nullptr));

        label_4->setText(QCoreApplication::translate("KeyboardLayoutConfig", "<html><head/><body><p><span style=\" font-weight:600;\">Note</span>: If you are using an <span style=\" font-weight:600;\">input method</span>, such as IBus, uim, fcitx, or gcin, the settings here <span style=\" font-weight:600;\">might not work</span> because they are overridden by the input methods.</p></body></html>", nullptr));
        (void)KeyboardLayoutConfig;
    } // retranslateUi

};

namespace Ui {
    class KeyboardLayoutConfig: public Ui_KeyboardLayoutConfig {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_KEYBOARDLAYOUTCONFIG_H
