/********************************************************************************
** Form generated from reading UI file 'touchpadconfig.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TOUCHPADCONFIG_H
#define UI_TOUCHPADCONFIG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_TouchpadConfig
{
public:
    QFormLayout *formLayout;
    QLabel *label;
    QComboBox *devicesComboBox;
    QLabel *label_3;
    QCheckBox *tappingEnabledCheckBox;
    QCheckBox *naturalScrollingEnabledCheckBox;
    QCheckBox *tapToDragEnabledCheckBox;
    QDoubleSpinBox *accelSpeedDoubleSpinBox;
    QHBoxLayout *horizontalLayout;
    QLabel *label_2;
    QSpacerItem *horizontalSpacer_2;
    QRadioButton *noScrollingRadioButton;
    QRadioButton *twoFingerScrollingRadioButton;
    QRadioButton *edgeScrollingRadioButton;
    QRadioButton *buttonScrollingRadioButton;
    QSpacerItem *horizontalSpacer;
    QLabel *deviceInfoLabel;

    void setupUi(QWidget *TouchpadConfig)
    {
        if (TouchpadConfig->objectName().isEmpty())
            TouchpadConfig->setObjectName(QString::fromUtf8("TouchpadConfig"));
        TouchpadConfig->resize(450, 300);
        formLayout = new QFormLayout(TouchpadConfig);
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        label = new QLabel(TouchpadConfig);
        label->setObjectName(QString::fromUtf8("label"));

        formLayout->setWidget(0, QFormLayout::LabelRole, label);

        devicesComboBox = new QComboBox(TouchpadConfig);
        devicesComboBox->setObjectName(QString::fromUtf8("devicesComboBox"));

        formLayout->setWidget(0, QFormLayout::FieldRole, devicesComboBox);

        label_3 = new QLabel(TouchpadConfig);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        formLayout->setWidget(2, QFormLayout::LabelRole, label_3);

        tappingEnabledCheckBox = new QCheckBox(TouchpadConfig);
        tappingEnabledCheckBox->setObjectName(QString::fromUtf8("tappingEnabledCheckBox"));

        formLayout->setWidget(4, QFormLayout::SpanningRole, tappingEnabledCheckBox);

        naturalScrollingEnabledCheckBox = new QCheckBox(TouchpadConfig);
        naturalScrollingEnabledCheckBox->setObjectName(QString::fromUtf8("naturalScrollingEnabledCheckBox"));

        formLayout->setWidget(6, QFormLayout::SpanningRole, naturalScrollingEnabledCheckBox);

        tapToDragEnabledCheckBox = new QCheckBox(TouchpadConfig);
        tapToDragEnabledCheckBox->setObjectName(QString::fromUtf8("tapToDragEnabledCheckBox"));

        formLayout->setWidget(8, QFormLayout::SpanningRole, tapToDragEnabledCheckBox);

        accelSpeedDoubleSpinBox = new QDoubleSpinBox(TouchpadConfig);
        accelSpeedDoubleSpinBox->setObjectName(QString::fromUtf8("accelSpeedDoubleSpinBox"));
        accelSpeedDoubleSpinBox->setDecimals(2);
        accelSpeedDoubleSpinBox->setMinimum(-1.000000000000000);
        accelSpeedDoubleSpinBox->setMaximum(1.000000000000000);
        accelSpeedDoubleSpinBox->setSingleStep(0.100000000000000);

        formLayout->setWidget(2, QFormLayout::FieldRole, accelSpeedDoubleSpinBox);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(10);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label_2 = new QLabel(TouchpadConfig);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        horizontalLayout->addWidget(label_2);

        horizontalSpacer_2 = new QSpacerItem(10, 5, QSizePolicy::MinimumExpanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);

        noScrollingRadioButton = new QRadioButton(TouchpadConfig);
        noScrollingRadioButton->setObjectName(QString::fromUtf8("noScrollingRadioButton"));

        horizontalLayout->addWidget(noScrollingRadioButton);

        twoFingerScrollingRadioButton = new QRadioButton(TouchpadConfig);
        twoFingerScrollingRadioButton->setObjectName(QString::fromUtf8("twoFingerScrollingRadioButton"));

        horizontalLayout->addWidget(twoFingerScrollingRadioButton);

        edgeScrollingRadioButton = new QRadioButton(TouchpadConfig);
        edgeScrollingRadioButton->setObjectName(QString::fromUtf8("edgeScrollingRadioButton"));

        horizontalLayout->addWidget(edgeScrollingRadioButton);

        buttonScrollingRadioButton = new QRadioButton(TouchpadConfig);
        buttonScrollingRadioButton->setObjectName(QString::fromUtf8("buttonScrollingRadioButton"));

        horizontalLayout->addWidget(buttonScrollingRadioButton);

        horizontalSpacer = new QSpacerItem(10, 5, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);


        formLayout->setLayout(9, QFormLayout::SpanningRole, horizontalLayout);

        deviceInfoLabel = new QLabel(TouchpadConfig);
        deviceInfoLabel->setObjectName(QString::fromUtf8("deviceInfoLabel"));

        formLayout->setWidget(1, QFormLayout::FieldRole, deviceInfoLabel);


        retranslateUi(TouchpadConfig);

        QMetaObject::connectSlotsByName(TouchpadConfig);
    } // setupUi

    void retranslateUi(QWidget *TouchpadConfig)
    {
        TouchpadConfig->setWindowTitle(QCoreApplication::translate("TouchpadConfig", "TouchpadConfig ", nullptr));
        label->setText(QCoreApplication::translate("TouchpadConfig", "Device:", nullptr));
        label_3->setText(QCoreApplication::translate("TouchpadConfig", "Acceleration speed:", nullptr));
        tappingEnabledCheckBox->setText(QCoreApplication::translate("TouchpadConfig", "Tap to click", nullptr));
        naturalScrollingEnabledCheckBox->setText(QCoreApplication::translate("TouchpadConfig", "Natural Scrolling", nullptr));
        tapToDragEnabledCheckBox->setText(QCoreApplication::translate("TouchpadConfig", "Tap and drag", nullptr));
        label_2->setText(QCoreApplication::translate("TouchpadConfig", "Scrolling:", nullptr));
        noScrollingRadioButton->setText(QCoreApplication::translate("TouchpadConfig", "&Disabled", nullptr));
        twoFingerScrollingRadioButton->setText(QCoreApplication::translate("TouchpadConfig", "&Two-Finger", nullptr));
        edgeScrollingRadioButton->setText(QCoreApplication::translate("TouchpadConfig", "Ed&ge", nullptr));
        buttonScrollingRadioButton->setText(QCoreApplication::translate("TouchpadConfig", "B&utton", nullptr));
        deviceInfoLabel->setText(QCoreApplication::translate("TouchpadConfig", "DeviceInfoLabel", nullptr));
    } // retranslateUi

};

namespace Ui {
    class TouchpadConfig: public Ui_TouchpadConfig {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TOUCHPADCONFIG_H
