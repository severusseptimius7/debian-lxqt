/********************************************************************************
** Form generated from reading UI file 'selectkeyboardlayoutdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SELECTKEYBOARDLAYOUTDIALOG_H
#define UI_SELECTKEYBOARDLAYOUTDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>

QT_BEGIN_NAMESPACE

class Ui_SelectKeyboardLayoutDialog
{
public:
    QGridLayout *gridLayout;
    QLabel *label;
    QLabel *label_2;
    QListWidget *layouts;
    QListWidget *variants;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *SelectKeyboardLayoutDialog)
    {
        if (SelectKeyboardLayoutDialog->objectName().isEmpty())
            SelectKeyboardLayoutDialog->setObjectName(QString::fromUtf8("SelectKeyboardLayoutDialog"));
        SelectKeyboardLayoutDialog->resize(480, 384);
        gridLayout = new QGridLayout(SelectKeyboardLayoutDialog);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        label = new QLabel(SelectKeyboardLayoutDialog);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1);

        label_2 = new QLabel(SelectKeyboardLayoutDialog);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout->addWidget(label_2, 0, 2, 1, 1);

        layouts = new QListWidget(SelectKeyboardLayoutDialog);
        layouts->setObjectName(QString::fromUtf8("layouts"));
        layouts->setSortingEnabled(true);

        gridLayout->addWidget(layouts, 1, 0, 1, 2);

        variants = new QListWidget(SelectKeyboardLayoutDialog);
        variants->setObjectName(QString::fromUtf8("variants"));

        gridLayout->addWidget(variants, 1, 2, 1, 1);

        buttonBox = new QDialogButtonBox(SelectKeyboardLayoutDialog);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        gridLayout->addWidget(buttonBox, 2, 0, 1, 3);


        retranslateUi(SelectKeyboardLayoutDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), SelectKeyboardLayoutDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), SelectKeyboardLayoutDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(SelectKeyboardLayoutDialog);
    } // setupUi

    void retranslateUi(QDialog *SelectKeyboardLayoutDialog)
    {
        SelectKeyboardLayoutDialog->setWindowTitle(QCoreApplication::translate("SelectKeyboardLayoutDialog", "Select a keyboard layout", nullptr));
        label->setText(QCoreApplication::translate("SelectKeyboardLayoutDialog", "Keyboard layout", nullptr));
        label_2->setText(QCoreApplication::translate("SelectKeyboardLayoutDialog", "Variant", nullptr));
    } // retranslateUi

};

namespace Ui {
    class SelectKeyboardLayoutDialog: public Ui_SelectKeyboardLayoutDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SELECTKEYBOARDLAYOUTDIALOG_H
