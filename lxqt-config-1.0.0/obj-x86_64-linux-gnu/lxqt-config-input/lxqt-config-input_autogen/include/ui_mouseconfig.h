/********************************************************************************
** Form generated from reading UI file 'mouseconfig.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MOUSECONFIG_H
#define UI_MOUSECONFIG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MouseConfig
{
public:
    QFormLayout *formLayout;
    QLabel *label_43;
    QLabel *label;
    QSpinBox *doubleClickInterval;
    QLabel *label_2;
    QSpinBox *wheelScrollLines;
    QCheckBox *singleClick;
    QCheckBox *mouseLeftHanded;

    void setupUi(QWidget *MouseConfig)
    {
        if (MouseConfig->objectName().isEmpty())
            MouseConfig->setObjectName(QString::fromUtf8("MouseConfig"));
        MouseConfig->resize(324, 123);
        formLayout = new QFormLayout(MouseConfig);
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        formLayout->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        label_43 = new QLabel(MouseConfig);
        label_43->setObjectName(QString::fromUtf8("label_43"));

        formLayout->setWidget(0, QFormLayout::SpanningRole, label_43);

        label = new QLabel(MouseConfig);
        label->setObjectName(QString::fromUtf8("label"));

        formLayout->setWidget(3, QFormLayout::LabelRole, label);

        doubleClickInterval = new QSpinBox(MouseConfig);
        doubleClickInterval->setObjectName(QString::fromUtf8("doubleClickInterval"));
        doubleClickInterval->setMaximum(10000);

        formLayout->setWidget(3, QFormLayout::FieldRole, doubleClickInterval);

        label_2 = new QLabel(MouseConfig);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        formLayout->setWidget(4, QFormLayout::LabelRole, label_2);

        wheelScrollLines = new QSpinBox(MouseConfig);
        wheelScrollLines->setObjectName(QString::fromUtf8("wheelScrollLines"));

        formLayout->setWidget(4, QFormLayout::FieldRole, wheelScrollLines);

        singleClick = new QCheckBox(MouseConfig);
        singleClick->setObjectName(QString::fromUtf8("singleClick"));

        formLayout->setWidget(5, QFormLayout::SpanningRole, singleClick);

        mouseLeftHanded = new QCheckBox(MouseConfig);
        mouseLeftHanded->setObjectName(QString::fromUtf8("mouseLeftHanded"));

        formLayout->setWidget(6, QFormLayout::SpanningRole, mouseLeftHanded);


        retranslateUi(MouseConfig);

        QMetaObject::connectSlotsByName(MouseConfig);
    } // setupUi

    void retranslateUi(QWidget *MouseConfig)
    {
        MouseConfig->setWindowTitle(QCoreApplication::translate("MouseConfig", "Form", nullptr));
        label_43->setText(QCoreApplication::translate("MouseConfig", "<b>Mouse</b>", nullptr));
        label->setText(QCoreApplication::translate("MouseConfig", "Double click interval:", nullptr));
        doubleClickInterval->setSuffix(QCoreApplication::translate("MouseConfig", " ms", nullptr));
        label_2->setText(QCoreApplication::translate("MouseConfig", "Wheel scroll lines:", nullptr));
        singleClick->setText(QCoreApplication::translate("MouseConfig", "Single click to activate items", nullptr));
        mouseLeftHanded->setText(QCoreApplication::translate("MouseConfig", "Left handed (Swap left and right mouse buttons)", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MouseConfig: public Ui_MouseConfig {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MOUSECONFIG_H
