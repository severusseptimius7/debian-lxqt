/********************************************************************************
** Form generated from reading UI file 'keyboardconfig.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_KEYBOARDCONFIG_H
#define UI_KEYBOARDCONFIG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_KeyboardConfig
{
public:
    QFormLayout *formLayout;
    QLabel *label_2;
    QLabel *label;
    QCheckBox *keyboardBeep;
    QSpinBox *cursorFlashTime;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout_2;
    QLineEdit *lineEdit;
    QSlider *keyboardInterval;
    QLabel *label_12;
    QSlider *keyboardDelay;
    QLabel *label_7;
    QLabel *label_10;
    QLabel *label_8;
    QLabel *label_9;
    QLabel *label_11;
    QLabel *label_13;
    QLabel *label_16;
    QLabel *label_17;
    QCheckBox *keyboardNumLock;

    void setupUi(QWidget *KeyboardConfig)
    {
        if (KeyboardConfig->objectName().isEmpty())
            KeyboardConfig->setObjectName(QString::fromUtf8("KeyboardConfig"));
        KeyboardConfig->resize(375, 276);
        formLayout = new QFormLayout(KeyboardConfig);
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        label_2 = new QLabel(KeyboardConfig);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        formLayout->setWidget(2, QFormLayout::LabelRole, label_2);

        label = new QLabel(KeyboardConfig);
        label->setObjectName(QString::fromUtf8("label"));

        formLayout->setWidget(0, QFormLayout::SpanningRole, label);

        keyboardBeep = new QCheckBox(KeyboardConfig);
        keyboardBeep->setObjectName(QString::fromUtf8("keyboardBeep"));

        formLayout->setWidget(3, QFormLayout::SpanningRole, keyboardBeep);

        cursorFlashTime = new QSpinBox(KeyboardConfig);
        cursorFlashTime->setObjectName(QString::fromUtf8("cursorFlashTime"));
        cursorFlashTime->setMaximum(10000);

        formLayout->setWidget(2, QFormLayout::FieldRole, cursorFlashTime);

        groupBox_2 = new QGroupBox(KeyboardConfig);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        gridLayout_2 = new QGridLayout(groupBox_2);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        lineEdit = new QLineEdit(groupBox_2);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));

        gridLayout_2->addWidget(lineEdit, 6, 0, 1, 4);

        keyboardInterval = new QSlider(groupBox_2);
        keyboardInterval->setObjectName(QString::fromUtf8("keyboardInterval"));
        keyboardInterval->setMinimum(10);
        keyboardInterval->setMaximum(210);
        keyboardInterval->setSingleStep(10);
        keyboardInterval->setOrientation(Qt::Horizontal);
        keyboardInterval->setTickPosition(QSlider::TicksAbove);

        gridLayout_2->addWidget(keyboardInterval, 2, 2, 1, 1);

        label_12 = new QLabel(groupBox_2);
        label_12->setObjectName(QString::fromUtf8("label_12"));

        gridLayout_2->addWidget(label_12, 0, 3, 1, 1);

        keyboardDelay = new QSlider(groupBox_2);
        keyboardDelay->setObjectName(QString::fromUtf8("keyboardDelay"));
        keyboardDelay->setMinimum(100);
        keyboardDelay->setMaximum(1100);
        keyboardDelay->setSingleStep(100);
        keyboardDelay->setPageStep(100);
        keyboardDelay->setOrientation(Qt::Horizontal);
        keyboardDelay->setTickPosition(QSlider::TicksAbove);

        gridLayout_2->addWidget(keyboardDelay, 0, 2, 1, 1);

        label_7 = new QLabel(groupBox_2);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        gridLayout_2->addWidget(label_7, 0, 0, 1, 1);

        label_10 = new QLabel(groupBox_2);
        label_10->setObjectName(QString::fromUtf8("label_10"));

        gridLayout_2->addWidget(label_10, 0, 1, 1, 1);

        label_8 = new QLabel(groupBox_2);
        label_8->setObjectName(QString::fromUtf8("label_8"));

        gridLayout_2->addWidget(label_8, 2, 0, 1, 1);

        label_9 = new QLabel(groupBox_2);
        label_9->setObjectName(QString::fromUtf8("label_9"));

        gridLayout_2->addWidget(label_9, 5, 0, 1, 4);

        label_11 = new QLabel(groupBox_2);
        label_11->setObjectName(QString::fromUtf8("label_11"));

        gridLayout_2->addWidget(label_11, 2, 1, 1, 1);

        label_13 = new QLabel(groupBox_2);
        label_13->setObjectName(QString::fromUtf8("label_13"));

        gridLayout_2->addWidget(label_13, 2, 3, 1, 1);

        label_16 = new QLabel(groupBox_2);
        label_16->setObjectName(QString::fromUtf8("label_16"));

        gridLayout_2->addWidget(label_16, 0, 4, 1, 1);

        label_17 = new QLabel(groupBox_2);
        label_17->setObjectName(QString::fromUtf8("label_17"));

        gridLayout_2->addWidget(label_17, 2, 4, 1, 1);


        formLayout->setWidget(1, QFormLayout::SpanningRole, groupBox_2);

        keyboardNumLock = new QCheckBox(KeyboardConfig);
        keyboardNumLock->setObjectName(QString::fromUtf8("keyboardNumLock"));

        formLayout->setWidget(4, QFormLayout::SpanningRole, keyboardNumLock);


        retranslateUi(KeyboardConfig);
        QObject::connect(keyboardDelay, SIGNAL(valueChanged(int)), label_16, SLOT(setNum(int)));
        QObject::connect(keyboardInterval, SIGNAL(valueChanged(int)), label_17, SLOT(setNum(int)));

        QMetaObject::connectSlotsByName(KeyboardConfig);
    } // setupUi

    void retranslateUi(QWidget *KeyboardConfig)
    {
        KeyboardConfig->setWindowTitle(QCoreApplication::translate("KeyboardConfig", "Form", nullptr));
        label_2->setText(QCoreApplication::translate("KeyboardConfig", "Cursor flash time:", nullptr));
        label->setText(QCoreApplication::translate("KeyboardConfig", "<b>Keyboard</b>", nullptr));
        keyboardBeep->setText(QCoreApplication::translate("KeyboardConfig", "Beep when there is an error of keyboard input", nullptr));
        cursorFlashTime->setSuffix(QCoreApplication::translate("KeyboardConfig", " ms", nullptr));
        groupBox_2->setTitle(QCoreApplication::translate("KeyboardConfig", "Character Repeat", nullptr));
        label_12->setText(QCoreApplication::translate("KeyboardConfig", "Long", nullptr));
        label_7->setText(QCoreApplication::translate("KeyboardConfig", "Repeat delay:", nullptr));
        label_10->setText(QCoreApplication::translate("KeyboardConfig", "Short", nullptr));
        label_8->setText(QCoreApplication::translate("KeyboardConfig", "Repeat interval:", nullptr));
        label_9->setText(QCoreApplication::translate("KeyboardConfig", "Type in the following box to test your keyboard settings", nullptr));
        label_11->setText(QCoreApplication::translate("KeyboardConfig", "Short", nullptr));
        label_13->setText(QCoreApplication::translate("KeyboardConfig", "Long", nullptr));
        label_16->setText(QCoreApplication::translate("KeyboardConfig", "0", nullptr));
        label_17->setText(QCoreApplication::translate("KeyboardConfig", "0", nullptr));
        keyboardNumLock->setText(QCoreApplication::translate("KeyboardConfig", "Turn on NumLock after login", nullptr));
    } // retranslateUi

};

namespace Ui {
    class KeyboardConfig: public Ui_KeyboardConfig {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_KEYBOARDCONFIG_H
