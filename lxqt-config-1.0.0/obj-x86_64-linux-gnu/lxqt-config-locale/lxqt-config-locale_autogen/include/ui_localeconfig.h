/********************************************************************************
** Form generated from reading UI file 'localeconfig.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LOCALECONFIG_H
#define UI_LOCALECONFIG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "combobox.h"

QT_BEGIN_NAMESPACE

class Ui_LocaleConfig
{
public:
    QVBoxLayout *verticalLayout;
    QSpacerItem *verticalSpacer_2;
    QGridLayout *gridLayout;
    QFormLayout *formLayout;
    QLabel *label_10;
    LXQtLocale::ComboBox *comboGlobal;
    QCheckBox *checkDetailed;
    QLabel *labelNumbers;
    LXQtLocale::ComboBox *comboNumbers;
    QLabel *labelTime;
    LXQtLocale::ComboBox *comboTime;
    QLabel *labelCurrency;
    LXQtLocale::ComboBox *comboCurrency;
    QLabel *labelMeasurement;
    LXQtLocale::ComboBox *comboMeasurement;
    QLabel *labelCollate;
    LXQtLocale::ComboBox *comboCollate;
    QLabel *label_6;
    QLabel *labelMeasurement_2;
    QLabel *lexNumbers;
    QLabel *exampleNumbers;
    QLabel *lexTime;
    QLabel *exampleTime;
    QLabel *lexCurrency;
    QLabel *exampleCurrency;
    QLabel *lexMeasurement_2;
    QLabel *exampleMeasurement;
    QSpacerItem *horizontalSpacer_2;
    QSpacerItem *horizontalSpacer;
    QSpacerItem *verticalSpacer_3;
    QLabel *label_5;

    void setupUi(QWidget *LocaleConfig)
    {
        if (LocaleConfig->objectName().isEmpty())
            LocaleConfig->setObjectName(QString::fromUtf8("LocaleConfig"));
        LocaleConfig->resize(600, 450);
        verticalLayout = new QVBoxLayout(LocaleConfig);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalSpacer_2 = new QSpacerItem(20, 16, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout->addItem(verticalSpacer_2);

        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        formLayout = new QFormLayout();
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        formLayout->setFieldGrowthPolicy(QFormLayout::ExpandingFieldsGrow);
        formLayout->setHorizontalSpacing(6);
        formLayout->setVerticalSpacing(6);
        label_10 = new QLabel(LocaleConfig);
        label_10->setObjectName(QString::fromUtf8("label_10"));
        label_10->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        formLayout->setWidget(0, QFormLayout::LabelRole, label_10);

        comboGlobal = new LXQtLocale::ComboBox(LocaleConfig);
        comboGlobal->setObjectName(QString::fromUtf8("comboGlobal"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(comboGlobal->sizePolicy().hasHeightForWidth());
        comboGlobal->setSizePolicy(sizePolicy);
        comboGlobal->setMinimumSize(QSize(300, 0));

        formLayout->setWidget(0, QFormLayout::FieldRole, comboGlobal);

        checkDetailed = new QCheckBox(LocaleConfig);
        checkDetailed->setObjectName(QString::fromUtf8("checkDetailed"));

        formLayout->setWidget(1, QFormLayout::LabelRole, checkDetailed);

        labelNumbers = new QLabel(LocaleConfig);
        labelNumbers->setObjectName(QString::fromUtf8("labelNumbers"));
        labelNumbers->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        formLayout->setWidget(2, QFormLayout::LabelRole, labelNumbers);

        comboNumbers = new LXQtLocale::ComboBox(LocaleConfig);
        comboNumbers->setObjectName(QString::fromUtf8("comboNumbers"));
        sizePolicy.setHeightForWidth(comboNumbers->sizePolicy().hasHeightForWidth());
        comboNumbers->setSizePolicy(sizePolicy);
        comboNumbers->setMinimumSize(QSize(300, 0));

        formLayout->setWidget(2, QFormLayout::FieldRole, comboNumbers);

        labelTime = new QLabel(LocaleConfig);
        labelTime->setObjectName(QString::fromUtf8("labelTime"));
        labelTime->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        formLayout->setWidget(3, QFormLayout::LabelRole, labelTime);

        comboTime = new LXQtLocale::ComboBox(LocaleConfig);
        comboTime->setObjectName(QString::fromUtf8("comboTime"));
        sizePolicy.setHeightForWidth(comboTime->sizePolicy().hasHeightForWidth());
        comboTime->setSizePolicy(sizePolicy);
        comboTime->setMinimumSize(QSize(300, 0));

        formLayout->setWidget(3, QFormLayout::FieldRole, comboTime);

        labelCurrency = new QLabel(LocaleConfig);
        labelCurrency->setObjectName(QString::fromUtf8("labelCurrency"));
        labelCurrency->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        formLayout->setWidget(4, QFormLayout::LabelRole, labelCurrency);

        comboCurrency = new LXQtLocale::ComboBox(LocaleConfig);
        comboCurrency->setObjectName(QString::fromUtf8("comboCurrency"));
        sizePolicy.setHeightForWidth(comboCurrency->sizePolicy().hasHeightForWidth());
        comboCurrency->setSizePolicy(sizePolicy);
        comboCurrency->setMinimumSize(QSize(300, 0));

        formLayout->setWidget(4, QFormLayout::FieldRole, comboCurrency);

        labelMeasurement = new QLabel(LocaleConfig);
        labelMeasurement->setObjectName(QString::fromUtf8("labelMeasurement"));
        labelMeasurement->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        formLayout->setWidget(5, QFormLayout::LabelRole, labelMeasurement);

        comboMeasurement = new LXQtLocale::ComboBox(LocaleConfig);
        comboMeasurement->setObjectName(QString::fromUtf8("comboMeasurement"));
        sizePolicy.setHeightForWidth(comboMeasurement->sizePolicy().hasHeightForWidth());
        comboMeasurement->setSizePolicy(sizePolicy);
        comboMeasurement->setMinimumSize(QSize(300, 0));

        formLayout->setWidget(5, QFormLayout::FieldRole, comboMeasurement);

        labelCollate = new QLabel(LocaleConfig);
        labelCollate->setObjectName(QString::fromUtf8("labelCollate"));
        labelCollate->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        formLayout->setWidget(6, QFormLayout::LabelRole, labelCollate);

        comboCollate = new LXQtLocale::ComboBox(LocaleConfig);
        comboCollate->setObjectName(QString::fromUtf8("comboCollate"));
        sizePolicy.setHeightForWidth(comboCollate->sizePolicy().hasHeightForWidth());
        comboCollate->setSizePolicy(sizePolicy);
        comboCollate->setMinimumSize(QSize(300, 0));

        formLayout->setWidget(6, QFormLayout::FieldRole, comboCollate);

        label_6 = new QLabel(LocaleConfig);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);

        formLayout->setWidget(7, QFormLayout::LabelRole, label_6);

        labelMeasurement_2 = new QLabel(LocaleConfig);
        labelMeasurement_2->setObjectName(QString::fromUtf8("labelMeasurement_2"));
        labelMeasurement_2->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        formLayout->setWidget(8, QFormLayout::LabelRole, labelMeasurement_2);

        lexNumbers = new QLabel(LocaleConfig);
        lexNumbers->setObjectName(QString::fromUtf8("lexNumbers"));
        lexNumbers->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);

        formLayout->setWidget(9, QFormLayout::LabelRole, lexNumbers);

        exampleNumbers = new QLabel(LocaleConfig);
        exampleNumbers->setObjectName(QString::fromUtf8("exampleNumbers"));
        exampleNumbers->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);

        formLayout->setWidget(9, QFormLayout::FieldRole, exampleNumbers);

        lexTime = new QLabel(LocaleConfig);
        lexTime->setObjectName(QString::fromUtf8("lexTime"));
        lexTime->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);

        formLayout->setWidget(10, QFormLayout::LabelRole, lexTime);

        exampleTime = new QLabel(LocaleConfig);
        exampleTime->setObjectName(QString::fromUtf8("exampleTime"));
        exampleTime->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);

        formLayout->setWidget(10, QFormLayout::FieldRole, exampleTime);

        lexCurrency = new QLabel(LocaleConfig);
        lexCurrency->setObjectName(QString::fromUtf8("lexCurrency"));
        lexCurrency->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);

        formLayout->setWidget(11, QFormLayout::LabelRole, lexCurrency);

        exampleCurrency = new QLabel(LocaleConfig);
        exampleCurrency->setObjectName(QString::fromUtf8("exampleCurrency"));
        exampleCurrency->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);

        formLayout->setWidget(11, QFormLayout::FieldRole, exampleCurrency);

        lexMeasurement_2 = new QLabel(LocaleConfig);
        lexMeasurement_2->setObjectName(QString::fromUtf8("lexMeasurement_2"));
        lexMeasurement_2->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);

        formLayout->setWidget(12, QFormLayout::LabelRole, lexMeasurement_2);

        exampleMeasurement = new QLabel(LocaleConfig);
        exampleMeasurement->setObjectName(QString::fromUtf8("exampleMeasurement"));
        exampleMeasurement->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);

        formLayout->setWidget(12, QFormLayout::FieldRole, exampleMeasurement);


        gridLayout->addLayout(formLayout, 0, 1, 1, 1);

        horizontalSpacer_2 = new QSpacerItem(16, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_2, 0, 2, 1, 1);

        horizontalSpacer = new QSpacerItem(16, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer, 0, 0, 1, 1);


        verticalLayout->addLayout(gridLayout);

        verticalSpacer_3 = new QSpacerItem(20, 32, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_3);

        label_5 = new QLabel(LocaleConfig);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        verticalLayout->addWidget(label_5);

#if QT_CONFIG(shortcut)
        label_10->setBuddy(comboGlobal);
        labelNumbers->setBuddy(comboNumbers);
        labelTime->setBuddy(comboTime);
        labelCurrency->setBuddy(comboCurrency);
        labelMeasurement->setBuddy(comboMeasurement);
        labelCollate->setBuddy(comboCollate);
#endif // QT_CONFIG(shortcut)

        retranslateUi(LocaleConfig);

        QMetaObject::connectSlotsByName(LocaleConfig);
    } // setupUi

    void retranslateUi(QWidget *LocaleConfig)
    {
        label_10->setText(QCoreApplication::translate("LocaleConfig", "Re&gion:", nullptr));
        checkDetailed->setText(QCoreApplication::translate("LocaleConfig", "De&tailed Settings", nullptr));
        labelNumbers->setText(QCoreApplication::translate("LocaleConfig", "&Numbers:", nullptr));
        labelTime->setText(QCoreApplication::translate("LocaleConfig", "&Time:", nullptr));
        labelCurrency->setText(QCoreApplication::translate("LocaleConfig", "Currenc&y:", nullptr));
        labelMeasurement->setText(QCoreApplication::translate("LocaleConfig", "Measurement &Units:", nullptr));
        labelCollate->setText(QCoreApplication::translate("LocaleConfig", "Co&llation and Sorting:", nullptr));
        label_6->setText(QString());
        labelMeasurement_2->setText(QCoreApplication::translate("LocaleConfig", "<b>Examples</b>", nullptr));
        lexNumbers->setText(QCoreApplication::translate("LocaleConfig", "Numbers:", nullptr));
        exampleNumbers->setText(QString());
        lexTime->setText(QCoreApplication::translate("LocaleConfig", "Time:", nullptr));
        exampleTime->setText(QString());
        lexCurrency->setText(QCoreApplication::translate("LocaleConfig", "Currency:", nullptr));
        exampleCurrency->setText(QString());
        lexMeasurement_2->setText(QCoreApplication::translate("LocaleConfig", "Measurement Units:", nullptr));
        exampleMeasurement->setText(QString());
        label_5->setText(QString());
        (void)LocaleConfig;
    } // retranslateUi

};

namespace Ui {
    class LocaleConfig: public Ui_LocaleConfig {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LOCALECONFIG_H
