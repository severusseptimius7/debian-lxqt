# Install script for directory: /home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/lxqt-config-locale

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "RelWithDebInfo")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

# Set default install directory permissions.
if(NOT DEFINED CMAKE_OBJDUMP)
  set(CMAKE_OBJDUMP "/usr/bin/objdump")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xRuntimex" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/share/lxqt/translations/lxqt-config-locale/lxqt-config-locale_ar.qm;/usr/share/lxqt/translations/lxqt-config-locale/lxqt-config-locale_arn.qm;/usr/share/lxqt/translations/lxqt-config-locale/lxqt-config-locale_ast.qm;/usr/share/lxqt/translations/lxqt-config-locale/lxqt-config-locale_bg.qm;/usr/share/lxqt/translations/lxqt-config-locale/lxqt-config-locale_ca.qm;/usr/share/lxqt/translations/lxqt-config-locale/lxqt-config-locale_cs.qm;/usr/share/lxqt/translations/lxqt-config-locale/lxqt-config-locale_cy.qm;/usr/share/lxqt/translations/lxqt-config-locale/lxqt-config-locale_da.qm;/usr/share/lxqt/translations/lxqt-config-locale/lxqt-config-locale_de.qm;/usr/share/lxqt/translations/lxqt-config-locale/lxqt-config-locale_el.qm;/usr/share/lxqt/translations/lxqt-config-locale/lxqt-config-locale_en_GB.qm;/usr/share/lxqt/translations/lxqt-config-locale/lxqt-config-locale_es.qm;/usr/share/lxqt/translations/lxqt-config-locale/lxqt-config-locale_et.qm;/usr/share/lxqt/translations/lxqt-config-locale/lxqt-config-locale_fr.qm;/usr/share/lxqt/translations/lxqt-config-locale/lxqt-config-locale_gl.qm;/usr/share/lxqt/translations/lxqt-config-locale/lxqt-config-locale_he.qm;/usr/share/lxqt/translations/lxqt-config-locale/lxqt-config-locale_hr.qm;/usr/share/lxqt/translations/lxqt-config-locale/lxqt-config-locale_hu.qm;/usr/share/lxqt/translations/lxqt-config-locale/lxqt-config-locale_id.qm;/usr/share/lxqt/translations/lxqt-config-locale/lxqt-config-locale_it.qm;/usr/share/lxqt/translations/lxqt-config-locale/lxqt-config-locale_ja.qm;/usr/share/lxqt/translations/lxqt-config-locale/lxqt-config-locale_ko.qm;/usr/share/lxqt/translations/lxqt-config-locale/lxqt-config-locale_lt.qm;/usr/share/lxqt/translations/lxqt-config-locale/lxqt-config-locale_lv.qm;/usr/share/lxqt/translations/lxqt-config-locale/lxqt-config-locale_nb_NO.qm;/usr/share/lxqt/translations/lxqt-config-locale/lxqt-config-locale_nl.qm;/usr/share/lxqt/translations/lxqt-config-locale/lxqt-config-locale_oc.qm;/usr/share/lxqt/translations/lxqt-config-locale/lxqt-config-locale_pl.qm;/usr/share/lxqt/translations/lxqt-config-locale/lxqt-config-locale_pt.qm;/usr/share/lxqt/translations/lxqt-config-locale/lxqt-config-locale_pt_BR.qm;/usr/share/lxqt/translations/lxqt-config-locale/lxqt-config-locale_ru.qm;/usr/share/lxqt/translations/lxqt-config-locale/lxqt-config-locale_si.qm;/usr/share/lxqt/translations/lxqt-config-locale/lxqt-config-locale_sk_SK.qm;/usr/share/lxqt/translations/lxqt-config-locale/lxqt-config-locale_sv.qm;/usr/share/lxqt/translations/lxqt-config-locale/lxqt-config-locale_tr.qm;/usr/share/lxqt/translations/lxqt-config-locale/lxqt-config-locale_uk.qm;/usr/share/lxqt/translations/lxqt-config-locale/lxqt-config-locale_zh_CN.qm")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/usr/share/lxqt/translations/lxqt-config-locale" TYPE FILE FILES
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-locale/lxqt-config-locale_ar.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-locale/lxqt-config-locale_arn.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-locale/lxqt-config-locale_ast.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-locale/lxqt-config-locale_bg.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-locale/lxqt-config-locale_ca.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-locale/lxqt-config-locale_cs.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-locale/lxqt-config-locale_cy.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-locale/lxqt-config-locale_da.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-locale/lxqt-config-locale_de.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-locale/lxqt-config-locale_el.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-locale/lxqt-config-locale_en_GB.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-locale/lxqt-config-locale_es.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-locale/lxqt-config-locale_et.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-locale/lxqt-config-locale_fr.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-locale/lxqt-config-locale_gl.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-locale/lxqt-config-locale_he.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-locale/lxqt-config-locale_hr.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-locale/lxqt-config-locale_hu.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-locale/lxqt-config-locale_id.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-locale/lxqt-config-locale_it.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-locale/lxqt-config-locale_ja.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-locale/lxqt-config-locale_ko.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-locale/lxqt-config-locale_lt.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-locale/lxqt-config-locale_lv.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-locale/lxqt-config-locale_nb_NO.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-locale/lxqt-config-locale_nl.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-locale/lxqt-config-locale_oc.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-locale/lxqt-config-locale_pl.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-locale/lxqt-config-locale_pt.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-locale/lxqt-config-locale_pt_BR.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-locale/lxqt-config-locale_ru.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-locale/lxqt-config-locale_si.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-locale/lxqt-config-locale_sk_SK.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-locale/lxqt-config-locale_sv.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-locale/lxqt-config-locale_tr.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-locale/lxqt-config-locale_uk.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-locale/lxqt-config-locale_zh_CN.qm"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xRuntimex" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/lxqt-config-locale" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/lxqt-config-locale")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/lxqt-config-locale"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-locale/lxqt-config-locale")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/lxqt-config-locale" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/lxqt-config-locale")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/lxqt-config-locale")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xRuntimex" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/applications" TYPE FILE FILES "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-locale/lxqt-config-locale.desktop")
endif()

