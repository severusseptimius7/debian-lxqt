# Install script for directory: /home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/lxqt-config-monitor

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "RelWithDebInfo")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

# Set default install directory permissions.
if(NOT DEFINED CMAKE_OBJDUMP)
  set(CMAKE_OBJDUMP "/usr/bin/objdump")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xRuntimex" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/share/lxqt/translations/lxqt-config-monitor/lxqt-config-monitor_ar.qm;/usr/share/lxqt/translations/lxqt-config-monitor/lxqt-config-monitor_arn.qm;/usr/share/lxqt/translations/lxqt-config-monitor/lxqt-config-monitor_ast.qm;/usr/share/lxqt/translations/lxqt-config-monitor/lxqt-config-monitor_bg.qm;/usr/share/lxqt/translations/lxqt-config-monitor/lxqt-config-monitor_ca.qm;/usr/share/lxqt/translations/lxqt-config-monitor/lxqt-config-monitor_cs.qm;/usr/share/lxqt/translations/lxqt-config-monitor/lxqt-config-monitor_cy.qm;/usr/share/lxqt/translations/lxqt-config-monitor/lxqt-config-monitor_da.qm;/usr/share/lxqt/translations/lxqt-config-monitor/lxqt-config-monitor_de.qm;/usr/share/lxqt/translations/lxqt-config-monitor/lxqt-config-monitor_el.qm;/usr/share/lxqt/translations/lxqt-config-monitor/lxqt-config-monitor_en_GB.qm;/usr/share/lxqt/translations/lxqt-config-monitor/lxqt-config-monitor_es.qm;/usr/share/lxqt/translations/lxqt-config-monitor/lxqt-config-monitor_et.qm;/usr/share/lxqt/translations/lxqt-config-monitor/lxqt-config-monitor_fr.qm;/usr/share/lxqt/translations/lxqt-config-monitor/lxqt-config-monitor_gl.qm;/usr/share/lxqt/translations/lxqt-config-monitor/lxqt-config-monitor_he.qm;/usr/share/lxqt/translations/lxqt-config-monitor/lxqt-config-monitor_hr.qm;/usr/share/lxqt/translations/lxqt-config-monitor/lxqt-config-monitor_hu.qm;/usr/share/lxqt/translations/lxqt-config-monitor/lxqt-config-monitor_id.qm;/usr/share/lxqt/translations/lxqt-config-monitor/lxqt-config-monitor_it.qm;/usr/share/lxqt/translations/lxqt-config-monitor/lxqt-config-monitor_ja.qm;/usr/share/lxqt/translations/lxqt-config-monitor/lxqt-config-monitor_ko.qm;/usr/share/lxqt/translations/lxqt-config-monitor/lxqt-config-monitor_lt.qm;/usr/share/lxqt/translations/lxqt-config-monitor/lxqt-config-monitor_nb_NO.qm;/usr/share/lxqt/translations/lxqt-config-monitor/lxqt-config-monitor_nl.qm;/usr/share/lxqt/translations/lxqt-config-monitor/lxqt-config-monitor_pl.qm;/usr/share/lxqt/translations/lxqt-config-monitor/lxqt-config-monitor_pt.qm;/usr/share/lxqt/translations/lxqt-config-monitor/lxqt-config-monitor_pt_BR.qm;/usr/share/lxqt/translations/lxqt-config-monitor/lxqt-config-monitor_ru.qm;/usr/share/lxqt/translations/lxqt-config-monitor/lxqt-config-monitor_si.qm;/usr/share/lxqt/translations/lxqt-config-monitor/lxqt-config-monitor_sk_SK.qm;/usr/share/lxqt/translations/lxqt-config-monitor/lxqt-config-monitor_sv.qm;/usr/share/lxqt/translations/lxqt-config-monitor/lxqt-config-monitor_tr.qm;/usr/share/lxqt/translations/lxqt-config-monitor/lxqt-config-monitor_uk.qm;/usr/share/lxqt/translations/lxqt-config-monitor/lxqt-config-monitor_zh_CN.qm;/usr/share/lxqt/translations/lxqt-config-monitor/lxqt-config-monitor_zh_TW.qm")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/usr/share/lxqt/translations/lxqt-config-monitor" TYPE FILE FILES
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-monitor/lxqt-config-monitor_ar.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-monitor/lxqt-config-monitor_arn.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-monitor/lxqt-config-monitor_ast.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-monitor/lxqt-config-monitor_bg.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-monitor/lxqt-config-monitor_ca.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-monitor/lxqt-config-monitor_cs.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-monitor/lxqt-config-monitor_cy.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-monitor/lxqt-config-monitor_da.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-monitor/lxqt-config-monitor_de.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-monitor/lxqt-config-monitor_el.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-monitor/lxqt-config-monitor_en_GB.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-monitor/lxqt-config-monitor_es.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-monitor/lxqt-config-monitor_et.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-monitor/lxqt-config-monitor_fr.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-monitor/lxqt-config-monitor_gl.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-monitor/lxqt-config-monitor_he.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-monitor/lxqt-config-monitor_hr.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-monitor/lxqt-config-monitor_hu.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-monitor/lxqt-config-monitor_id.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-monitor/lxqt-config-monitor_it.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-monitor/lxqt-config-monitor_ja.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-monitor/lxqt-config-monitor_ko.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-monitor/lxqt-config-monitor_lt.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-monitor/lxqt-config-monitor_nb_NO.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-monitor/lxqt-config-monitor_nl.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-monitor/lxqt-config-monitor_pl.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-monitor/lxqt-config-monitor_pt.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-monitor/lxqt-config-monitor_pt_BR.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-monitor/lxqt-config-monitor_ru.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-monitor/lxqt-config-monitor_si.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-monitor/lxqt-config-monitor_sk_SK.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-monitor/lxqt-config-monitor_sv.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-monitor/lxqt-config-monitor_tr.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-monitor/lxqt-config-monitor_uk.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-monitor/lxqt-config-monitor_zh_CN.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-monitor/lxqt-config-monitor_zh_TW.qm"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/bin/lxqt-config-monitor" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/bin/lxqt-config-monitor")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/bin/lxqt-config-monitor"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/bin/lxqt-config-monitor")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/usr/bin" TYPE EXECUTABLE FILES "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-monitor/lxqt-config-monitor")
  if(EXISTS "$ENV{DESTDIR}/usr/bin/lxqt-config-monitor" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/bin/lxqt-config-monitor")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/bin/lxqt-config-monitor")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/share/applications/lxqt-config-monitor.desktop")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/usr/share/applications" TYPE FILE FILES "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-monitor/lxqt-config-monitor.desktop")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/share/lxqt/icons/monitor.svg")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/usr/share/lxqt/icons" TYPE FILE FILES "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/lxqt-config-monitor/icons/monitor.svg")
endif()

