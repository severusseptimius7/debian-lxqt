/****************************************************************************
** Meta object code from reading C++ file 'monitorwidget.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.15.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../../../lxqt-config-monitor/monitorwidget.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'monitorwidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.15.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MonitorWidget_t {
    QByteArrayData data[14];
    char stringdata0[218];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MonitorWidget_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MonitorWidget_t qt_meta_stringdata_MonitorWidget = {
    {
QT_MOC_LITERAL(0, 0, 13), // "MonitorWidget"
QT_MOC_LITERAL(1, 14, 20), // "primaryOutputChanged"
QT_MOC_LITERAL(2, 35, 0), // ""
QT_MOC_LITERAL(3, 36, 14), // "MonitorWidget*"
QT_MOC_LITERAL(4, 51, 6), // "widget"
QT_MOC_LITERAL(5, 58, 14), // "setOnlyMonitor"
QT_MOC_LITERAL(6, 73, 13), // "isOnlyMonitor"
QT_MOC_LITERAL(7, 87, 22), // "onPrimaryOutputChanged"
QT_MOC_LITERAL(8, 110, 16), // "onEnabledChanged"
QT_MOC_LITERAL(9, 127, 17), // "onBehaviorChanged"
QT_MOC_LITERAL(10, 145, 17), // "onPositionChanged"
QT_MOC_LITERAL(11, 163, 19), // "onResolutionChanged"
QT_MOC_LITERAL(12, 183, 13), // "onRateChanged"
QT_MOC_LITERAL(13, 197, 20) // "onOrientationChanged"

    },
    "MonitorWidget\0primaryOutputChanged\0\0"
    "MonitorWidget*\0widget\0setOnlyMonitor\0"
    "isOnlyMonitor\0onPrimaryOutputChanged\0"
    "onEnabledChanged\0onBehaviorChanged\0"
    "onPositionChanged\0onResolutionChanged\0"
    "onRateChanged\0onOrientationChanged"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MonitorWidget[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   59,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       5,    1,   62,    2, 0x0a /* Public */,
       7,    1,   65,    2, 0x0a /* Public */,
       8,    1,   68,    2, 0x08 /* Private */,
       9,    1,   71,    2, 0x08 /* Private */,
      10,    1,   74,    2, 0x08 /* Private */,
      11,    1,   77,    2, 0x08 /* Private */,
      12,    1,   80,    2, 0x08 /* Private */,
      13,    1,   83,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,

 // slots: parameters
    QMetaType::Void, QMetaType::Bool,    6,
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Int,    2,

       0        // eod
};

void MonitorWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<MonitorWidget *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->primaryOutputChanged((*reinterpret_cast< MonitorWidget*(*)>(_a[1]))); break;
        case 1: _t->setOnlyMonitor((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 2: _t->onPrimaryOutputChanged((*reinterpret_cast< MonitorWidget*(*)>(_a[1]))); break;
        case 3: _t->onEnabledChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 4: _t->onBehaviorChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->onPositionChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: _t->onResolutionChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: _t->onRateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 8: _t->onOrientationChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 0:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< MonitorWidget* >(); break;
            }
            break;
        case 2:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< MonitorWidget* >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (MonitorWidget::*)(MonitorWidget * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MonitorWidget::primaryOutputChanged)) {
                *result = 0;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject MonitorWidget::staticMetaObject = { {
    QMetaObject::SuperData::link<QGroupBox::staticMetaObject>(),
    qt_meta_stringdata_MonitorWidget.data,
    qt_meta_data_MonitorWidget,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *MonitorWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MonitorWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MonitorWidget.stringdata0))
        return static_cast<void*>(this);
    return QGroupBox::qt_metacast(_clname);
}

int MonitorWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QGroupBox::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    }
    return _id;
}

// SIGNAL 0
void MonitorWidget::primaryOutputChanged(MonitorWidget * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
