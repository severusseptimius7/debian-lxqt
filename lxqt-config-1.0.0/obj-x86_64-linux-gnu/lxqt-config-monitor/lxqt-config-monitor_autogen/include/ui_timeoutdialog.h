/********************************************************************************
** Form generated from reading UI file 'timeoutdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TIMEOUTDIALOG_H
#define UI_TIMEOUTDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QProgressBar>

QT_BEGIN_NAMESPACE

class Ui_TimeoutDialog
{
public:
    QGridLayout *gridLayout;
    QLabel *icon;
    QDialogButtonBox *buttonBox;
    QLabel *label;
    QProgressBar *progressBar;
    QLabel *remainingTime;

    void setupUi(QDialog *TimeoutDialog)
    {
        if (TimeoutDialog->objectName().isEmpty())
            TimeoutDialog->setObjectName(QString::fromUtf8("TimeoutDialog"));
        TimeoutDialog->resize(255, 152);
        TimeoutDialog->setModal(true);
        gridLayout = new QGridLayout(TimeoutDialog);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        icon = new QLabel(TimeoutDialog);
        icon->setObjectName(QString::fromUtf8("icon"));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(icon->sizePolicy().hasHeightForWidth());
        icon->setSizePolicy(sizePolicy);
        icon->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(icon, 0, 0, 1, 1);

        buttonBox = new QDialogButtonBox(TimeoutDialog);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::No|QDialogButtonBox::Yes);

        gridLayout->addWidget(buttonBox, 3, 0, 1, 2);

        label = new QLabel(TimeoutDialog);
        label->setObjectName(QString::fromUtf8("label"));
        label->setMargin(10);

        gridLayout->addWidget(label, 0, 1, 1, 1);

        progressBar = new QProgressBar(TimeoutDialog);
        progressBar->setObjectName(QString::fromUtf8("progressBar"));
        progressBar->setTextVisible(false);

        gridLayout->addWidget(progressBar, 1, 0, 1, 2);

        remainingTime = new QLabel(TimeoutDialog);
        remainingTime->setObjectName(QString::fromUtf8("remainingTime"));
        remainingTime->setAlignment(Qt::AlignCenter);
        remainingTime->setMargin(5);

        gridLayout->addWidget(remainingTime, 2, 0, 1, 2);


        retranslateUi(TimeoutDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), TimeoutDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), TimeoutDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(TimeoutDialog);
    } // setupUi

    void retranslateUi(QDialog *TimeoutDialog)
    {
        TimeoutDialog->setWindowTitle(QCoreApplication::translate("TimeoutDialog", "Settings are changed", nullptr));
        label->setText(QCoreApplication::translate("TimeoutDialog", "Are the current settings OK for you?", nullptr));
    } // retranslateUi

};

namespace Ui {
    class TimeoutDialog: public Ui_TimeoutDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TIMEOUTDIALOG_H
