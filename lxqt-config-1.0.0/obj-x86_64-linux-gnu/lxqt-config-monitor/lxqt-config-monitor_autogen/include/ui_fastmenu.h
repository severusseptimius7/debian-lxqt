/********************************************************************************
** Form generated from reading UI file 'fastmenu.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FASTMENU_H
#define UI_FASTMENU_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_FastMenu
{
public:
    QVBoxLayout *verticalLayout;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout_2;
    QComboBox *comboBox;
    QSpacerItem *verticalSpacer;

    void setupUi(QWidget *FastMenu)
    {
        if (FastMenu->objectName().isEmpty())
            FastMenu->setObjectName(QString::fromUtf8("FastMenu"));
        FastMenu->resize(298, 164);
        FastMenu->setWindowTitle(QString::fromUtf8("Form"));
        verticalLayout = new QVBoxLayout(FastMenu);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        groupBox = new QGroupBox(FastMenu);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setFlat(false);
        verticalLayout_2 = new QVBoxLayout(groupBox);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        comboBox = new QComboBox(groupBox);
        comboBox->addItem(QString());
        comboBox->addItem(QString());
        comboBox->addItem(QString());
        comboBox->addItem(QString());
        comboBox->addItem(QString());
        comboBox->setObjectName(QString::fromUtf8("comboBox"));
        QFont font;
        font.setPointSize(20);
        comboBox->setFont(font);

        verticalLayout_2->addWidget(comboBox);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer);


        verticalLayout->addWidget(groupBox);


        retranslateUi(FastMenu);

        QMetaObject::connectSlotsByName(FastMenu);
    } // setupUi

    void retranslateUi(QWidget *FastMenu)
    {
        groupBox->setTitle(QCoreApplication::translate("FastMenu", "Fast options", nullptr));
        comboBox->setItemText(0, QString());
        comboBox->setItemText(1, QCoreApplication::translate("FastMenu", " \342\235\266 \342\235\267 Extended view", nullptr));
        comboBox->setItemText(2, QCoreApplication::translate("FastMenu", " \342\235\266 \342\235\266 Unified view", nullptr));
        comboBox->setItemText(3, QCoreApplication::translate("FastMenu", " \342\235\266     Only first", nullptr));
        comboBox->setItemText(4, QCoreApplication::translate("FastMenu", "     \342\235\267 Only second", nullptr));

        (void)FastMenu;
    } // retranslateUi

};

namespace Ui {
    class FastMenu: public Ui_FastMenu {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FASTMENU_H
