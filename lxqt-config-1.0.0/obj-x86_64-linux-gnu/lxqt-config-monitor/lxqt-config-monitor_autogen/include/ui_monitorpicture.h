/********************************************************************************
** Form generated from reading UI file 'monitorpicture.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MONITORPICTURE_H
#define UI_MONITORPICTURE_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_MonitorPictureDialog
{
public:
    QVBoxLayout *verticalLayout_2;
    QVBoxLayout *verticalLayout;
    QGraphicsView *graphicsView;
    QCheckBox *magneticCheckBox;

    void setupUi(QDialog *MonitorPictureDialog)
    {
        if (MonitorPictureDialog->objectName().isEmpty())
            MonitorPictureDialog->setObjectName(QString::fromUtf8("MonitorPictureDialog"));
        MonitorPictureDialog->resize(462, 362);
        MonitorPictureDialog->setWindowTitle(QString::fromUtf8("Dialog"));
        verticalLayout_2 = new QVBoxLayout(MonitorPictureDialog);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        graphicsView = new QGraphicsView(MonitorPictureDialog);
        graphicsView->setObjectName(QString::fromUtf8("graphicsView"));

        verticalLayout->addWidget(graphicsView);


        verticalLayout_2->addLayout(verticalLayout);

        magneticCheckBox = new QCheckBox(MonitorPictureDialog);
        magneticCheckBox->setObjectName(QString::fromUtf8("magneticCheckBox"));
        magneticCheckBox->setChecked(true);

        verticalLayout_2->addWidget(magneticCheckBox);


        retranslateUi(MonitorPictureDialog);

        QMetaObject::connectSlotsByName(MonitorPictureDialog);
    } // setupUi

    void retranslateUi(QDialog *MonitorPictureDialog)
    {
        magneticCheckBox->setText(QCoreApplication::translate("MonitorPictureDialog", "Keep monitors attached", nullptr));
        (void)MonitorPictureDialog;
    } // retranslateUi

};

namespace Ui {
    class MonitorPictureDialog: public Ui_MonitorPictureDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MONITORPICTURE_H
