/********************************************************************************
** Form generated from reading UI file 'monitorsettingsdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MONITORSETTINGSDIALOG_H
#define UI_MONITORSETTINGSDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QSplitter>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_MonitorSettingsDialog
{
public:
    QVBoxLayout *verticalLayout;
    QSplitter *splitter;
    QListWidget *monitorList;
    QStackedWidget *stackedWidget;
    QHBoxLayout *horizontalLayout;
    QToolButton *settingsButton;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *MonitorSettingsDialog)
    {
        if (MonitorSettingsDialog->objectName().isEmpty())
            MonitorSettingsDialog->setObjectName(QString::fromUtf8("MonitorSettingsDialog"));
        MonitorSettingsDialog->resize(522, 131);
        QIcon icon;
        QString iconThemeName = QString::fromUtf8("preferences-desktop-display");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon = QIcon::fromTheme(iconThemeName);
        } else {
            icon.addFile(QString::fromUtf8("."), QSize(), QIcon::Normal, QIcon::Off);
        }
        MonitorSettingsDialog->setWindowIcon(icon);
        verticalLayout = new QVBoxLayout(MonitorSettingsDialog);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        splitter = new QSplitter(MonitorSettingsDialog);
        splitter->setObjectName(QString::fromUtf8("splitter"));
        splitter->setOrientation(Qt::Horizontal);
        splitter->setChildrenCollapsible(false);
        monitorList = new QListWidget(splitter);
        monitorList->setObjectName(QString::fromUtf8("monitorList"));
        QSizePolicy sizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(monitorList->sizePolicy().hasHeightForWidth());
        monitorList->setSizePolicy(sizePolicy);
        monitorList->setMinimumSize(QSize(150, 0));
        monitorList->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        monitorList->setResizeMode(QListView::Adjust);
        splitter->addWidget(monitorList);
        stackedWidget = new QStackedWidget(splitter);
        stackedWidget->setObjectName(QString::fromUtf8("stackedWidget"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(1);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(stackedWidget->sizePolicy().hasHeightForWidth());
        stackedWidget->setSizePolicy(sizePolicy1);
        stackedWidget->setMinimumSize(QSize(350, 0));
        splitter->addWidget(stackedWidget);

        verticalLayout->addWidget(splitter);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        settingsButton = new QToolButton(MonitorSettingsDialog);
        settingsButton->setObjectName(QString::fromUtf8("settingsButton"));
        QIcon icon1;
        iconThemeName = QString::fromUtf8("preferences-system");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon1 = QIcon::fromTheme(iconThemeName);
        } else {
            icon1.addFile(QString::fromUtf8("."), QSize(), QIcon::Normal, QIcon::Off);
        }
        settingsButton->setIcon(icon1);
        settingsButton->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
        settingsButton->setAutoRaise(false);

        horizontalLayout->addWidget(settingsButton);

        buttonBox = new QDialogButtonBox(MonitorSettingsDialog);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Apply|QDialogButtonBox::Close|QDialogButtonBox::Save);
        buttonBox->setCenterButtons(false);

        horizontalLayout->addWidget(buttonBox);


        verticalLayout->addLayout(horizontalLayout);


        retranslateUi(MonitorSettingsDialog);
        QObject::connect(monitorList, SIGNAL(currentRowChanged(int)), stackedWidget, SLOT(setCurrentIndex(int)));
        QObject::connect(buttonBox, SIGNAL(rejected()), MonitorSettingsDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(MonitorSettingsDialog);
    } // setupUi

    void retranslateUi(QDialog *MonitorSettingsDialog)
    {
        MonitorSettingsDialog->setWindowTitle(QCoreApplication::translate("MonitorSettingsDialog", "Monitor Settings", nullptr));
        settingsButton->setText(QCoreApplication::translate("MonitorSettingsDialog", "Settings", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MonitorSettingsDialog: public Ui_MonitorSettingsDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MONITORSETTINGSDIALOG_H
