/********************************************************************************
** Form generated from reading UI file 'managesavedsettings.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MANAGESAVEDSETTINGS_H
#define UI_MANAGESAVEDSETTINGS_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_ManageSavedSettings
{
public:
    QVBoxLayout *verticalLayout;
    QVBoxLayout *verticalLayout_3;
    QGroupBox *groupBox;
    QGridLayout *gridLayout;
    QListWidget *allConfigs;
    QHBoxLayout *horizontalLayout;
    QPushButton *renamePushButton;
    QPushButton *deletePushButton;
    QPushButton *applyPushButton;
    QTextEdit *selectedSettingsTextEdit;

    void setupUi(QDialog *ManageSavedSettings)
    {
        if (ManageSavedSettings->objectName().isEmpty())
            ManageSavedSettings->setObjectName(QString::fromUtf8("ManageSavedSettings"));
        ManageSavedSettings->resize(521, 312);
        QIcon icon;
        QString iconThemeName = QString::fromUtf8("preferences-desktop-display");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon = QIcon::fromTheme(iconThemeName);
        } else {
            icon.addFile(QString::fromUtf8(""), QSize(), QIcon::Normal, QIcon::Off);
        }
        ManageSavedSettings->setWindowIcon(icon);
        verticalLayout = new QVBoxLayout(ManageSavedSettings);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        groupBox = new QGroupBox(ManageSavedSettings);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        gridLayout = new QGridLayout(groupBox);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        allConfigs = new QListWidget(groupBox);
        allConfigs->setObjectName(QString::fromUtf8("allConfigs"));

        gridLayout->addWidget(allConfigs, 0, 0, 1, 1);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        renamePushButton = new QPushButton(groupBox);
        renamePushButton->setObjectName(QString::fromUtf8("renamePushButton"));

        horizontalLayout->addWidget(renamePushButton);

        deletePushButton = new QPushButton(groupBox);
        deletePushButton->setObjectName(QString::fromUtf8("deletePushButton"));

        horizontalLayout->addWidget(deletePushButton);

        applyPushButton = new QPushButton(groupBox);
        applyPushButton->setObjectName(QString::fromUtf8("applyPushButton"));

        horizontalLayout->addWidget(applyPushButton);


        gridLayout->addLayout(horizontalLayout, 3, 0, 1, 1);

        selectedSettingsTextEdit = new QTextEdit(groupBox);
        selectedSettingsTextEdit->setObjectName(QString::fromUtf8("selectedSettingsTextEdit"));
        selectedSettingsTextEdit->setReadOnly(true);

        gridLayout->addWidget(selectedSettingsTextEdit, 0, 1, 1, 1);


        verticalLayout_3->addWidget(groupBox);


        verticalLayout->addLayout(verticalLayout_3);


        retranslateUi(ManageSavedSettings);

        QMetaObject::connectSlotsByName(ManageSavedSettings);
    } // setupUi

    void retranslateUi(QDialog *ManageSavedSettings)
    {
        ManageSavedSettings->setWindowTitle(QCoreApplication::translate("ManageSavedSettings", "Monitor Settings", nullptr));
        groupBox->setTitle(QCoreApplication::translate("ManageSavedSettings", "Saved settings", nullptr));
        renamePushButton->setText(QCoreApplication::translate("ManageSavedSettings", "Rename", nullptr));
        deletePushButton->setText(QCoreApplication::translate("ManageSavedSettings", "Delete", nullptr));
        applyPushButton->setText(QCoreApplication::translate("ManageSavedSettings", "Apply", nullptr));
    } // retranslateUi

};

namespace Ui {
    class ManageSavedSettings: public Ui_ManageSavedSettings {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MANAGESAVEDSETTINGS_H
