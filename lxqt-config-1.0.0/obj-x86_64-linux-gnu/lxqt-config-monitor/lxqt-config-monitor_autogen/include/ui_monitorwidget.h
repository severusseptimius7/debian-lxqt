/********************************************************************************
** Form generated from reading UI file 'monitorwidget.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MONITORWIDGET_H
#define UI_MONITORWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MonitorWidget
{
public:
    QHBoxLayout *horizontalLayout;
    QTabWidget *tabWidget;
    QWidget *tabSettings;
    QGridLayout *gridLayout;
    QHBoxLayout *xyPosLayout;
    QSpinBox *xPosSpinBox;
    QSpinBox *yPosSpinBox;
    QSpacerItem *horizontalSpacer;
    QHBoxLayout *horizontalLayout_4;
    QLabel *resolutionLabel;
    QComboBox *resolutionCombo;
    QCheckBox *enabledCheckbox;
    QComboBox *behaviorCombo;
    QSpacerItem *verticalSpacer;
    QWidget *tabAdvanced;
    QGridLayout *gridLayout_3;
    QGridLayout *gridLayout_2;
    QSpacerItem *verticalSpacer_2;
    QLabel *rateLabel;
    QLabel *orientationLabel;
    QComboBox *orientationCombo;
    QComboBox *rateCombo;
    QWidget *tab;
    QVBoxLayout *verticalLayout;
    QLabel *outputInfoLabel;
    QSpacerItem *verticalSpacer_3;

    void setupUi(QWidget *MonitorWidget)
    {
        if (MonitorWidget->objectName().isEmpty())
            MonitorWidget->setObjectName(QString::fromUtf8("MonitorWidget"));
        MonitorWidget->resize(313, 274);
        horizontalLayout = new QHBoxLayout(MonitorWidget);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        tabWidget = new QTabWidget(MonitorWidget);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tabSettings = new QWidget();
        tabSettings->setObjectName(QString::fromUtf8("tabSettings"));
        gridLayout = new QGridLayout(tabSettings);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        xyPosLayout = new QHBoxLayout();
        xyPosLayout->setObjectName(QString::fromUtf8("xyPosLayout"));
        xPosSpinBox = new QSpinBox(tabSettings);
        xPosSpinBox->setObjectName(QString::fromUtf8("xPosSpinBox"));
        xPosSpinBox->setMinimum(-10000000);
        xPosSpinBox->setMaximum(10000000);

        xyPosLayout->addWidget(xPosSpinBox);

        yPosSpinBox = new QSpinBox(tabSettings);
        yPosSpinBox->setObjectName(QString::fromUtf8("yPosSpinBox"));
        yPosSpinBox->setMinimum(-10000000);
        yPosSpinBox->setMaximum(10000000);

        xyPosLayout->addWidget(yPosSpinBox);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        xyPosLayout->addItem(horizontalSpacer);


        gridLayout->addLayout(xyPosLayout, 7, 0, 1, 2);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        resolutionLabel = new QLabel(tabSettings);
        resolutionLabel->setObjectName(QString::fromUtf8("resolutionLabel"));

        horizontalLayout_4->addWidget(resolutionLabel);

        resolutionCombo = new QComboBox(tabSettings);
        resolutionCombo->setObjectName(QString::fromUtf8("resolutionCombo"));

        horizontalLayout_4->addWidget(resolutionCombo);


        gridLayout->addLayout(horizontalLayout_4, 1, 0, 1, 2);

        enabledCheckbox = new QCheckBox(tabSettings);
        enabledCheckbox->setObjectName(QString::fromUtf8("enabledCheckbox"));

        gridLayout->addWidget(enabledCheckbox, 0, 0, 1, 2);

        behaviorCombo = new QComboBox(tabSettings);
        behaviorCombo->addItem(QString());
        behaviorCombo->addItem(QString());
        behaviorCombo->setObjectName(QString::fromUtf8("behaviorCombo"));

        gridLayout->addWidget(behaviorCombo, 2, 0, 1, 2);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer, 8, 0, 1, 2);

        tabWidget->addTab(tabSettings, QString());
        tabAdvanced = new QWidget();
        tabAdvanced->setObjectName(QString::fromUtf8("tabAdvanced"));
        gridLayout_3 = new QGridLayout(tabAdvanced);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        gridLayout_2 = new QGridLayout();
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_2->addItem(verticalSpacer_2, 3, 0, 1, 2);

        rateLabel = new QLabel(tabAdvanced);
        rateLabel->setObjectName(QString::fromUtf8("rateLabel"));

        gridLayout_2->addWidget(rateLabel, 1, 0, 1, 1);

        orientationLabel = new QLabel(tabAdvanced);
        orientationLabel->setObjectName(QString::fromUtf8("orientationLabel"));

        gridLayout_2->addWidget(orientationLabel, 0, 0, 1, 1);

        orientationCombo = new QComboBox(tabAdvanced);
        orientationCombo->setObjectName(QString::fromUtf8("orientationCombo"));

        gridLayout_2->addWidget(orientationCombo, 0, 1, 1, 1);

        rateCombo = new QComboBox(tabAdvanced);
        rateCombo->setObjectName(QString::fromUtf8("rateCombo"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(rateCombo->sizePolicy().hasHeightForWidth());
        rateCombo->setSizePolicy(sizePolicy);

        gridLayout_2->addWidget(rateCombo, 1, 1, 1, 1);


        gridLayout_3->addLayout(gridLayout_2, 0, 0, 1, 1);

        tabWidget->addTab(tabAdvanced, QString());
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        verticalLayout = new QVBoxLayout(tab);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        outputInfoLabel = new QLabel(tab);
        outputInfoLabel->setObjectName(QString::fromUtf8("outputInfoLabel"));
        outputInfoLabel->setWordWrap(true);

        verticalLayout->addWidget(outputInfoLabel);

        verticalSpacer_3 = new QSpacerItem(20, 1, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_3);

        tabWidget->addTab(tab, QString());

        horizontalLayout->addWidget(tabWidget);


        retranslateUi(MonitorWidget);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MonitorWidget);
    } // setupUi

    void retranslateUi(QWidget *MonitorWidget)
    {
        xPosSpinBox->setSuffix(QCoreApplication::translate("MonitorWidget", " (x)", nullptr));
        xPosSpinBox->setPrefix(QString());
        yPosSpinBox->setSuffix(QCoreApplication::translate("MonitorWidget", " (y)", nullptr));
        yPosSpinBox->setPrefix(QString());
        resolutionLabel->setText(QCoreApplication::translate("MonitorWidget", "Resolution:", nullptr));
        enabledCheckbox->setText(QCoreApplication::translate("MonitorWidget", "Enable this display", nullptr));
        behaviorCombo->setItemText(0, QCoreApplication::translate("MonitorWidget", "This is my primary display", nullptr));
        behaviorCombo->setItemText(1, QCoreApplication::translate("MonitorWidget", "This screen extends another display", nullptr));

        tabWidget->setTabText(tabWidget->indexOf(tabSettings), QCoreApplication::translate("MonitorWidget", "Setup", nullptr));
        rateLabel->setText(QCoreApplication::translate("MonitorWidget", "Refresh rate:", nullptr));
        orientationLabel->setText(QCoreApplication::translate("MonitorWidget", "Rotation:", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tabAdvanced), QCoreApplication::translate("MonitorWidget", "Advanced", nullptr));
        outputInfoLabel->setText(QCoreApplication::translate("MonitorWidget", "Display information", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab), QCoreApplication::translate("MonitorWidget", "Info", nullptr));
        (void)MonitorWidget;
    } // retranslateUi

};

namespace Ui {
    class MonitorWidget: public Ui_MonitorWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MONITORWIDGET_H
