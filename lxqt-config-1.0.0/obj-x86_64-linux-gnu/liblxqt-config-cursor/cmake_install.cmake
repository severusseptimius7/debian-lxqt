# Install script for directory: /home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/liblxqt-config-cursor

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "RelWithDebInfo")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

# Set default install directory permissions.
if(NOT DEFINED CMAKE_OBJDUMP)
  set(CMAKE_OBJDUMP "/usr/bin/objdump")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xRuntimex" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/share/lxqt/translations/lxqt-config-cursor/lxqt-config-cursor_ar.qm;/usr/share/lxqt/translations/lxqt-config-cursor/lxqt-config-cursor_arn.qm;/usr/share/lxqt/translations/lxqt-config-cursor/lxqt-config-cursor_ast.qm;/usr/share/lxqt/translations/lxqt-config-cursor/lxqt-config-cursor_bg.qm;/usr/share/lxqt/translations/lxqt-config-cursor/lxqt-config-cursor_ca.qm;/usr/share/lxqt/translations/lxqt-config-cursor/lxqt-config-cursor_cs.qm;/usr/share/lxqt/translations/lxqt-config-cursor/lxqt-config-cursor_cy.qm;/usr/share/lxqt/translations/lxqt-config-cursor/lxqt-config-cursor_da.qm;/usr/share/lxqt/translations/lxqt-config-cursor/lxqt-config-cursor_de.qm;/usr/share/lxqt/translations/lxqt-config-cursor/lxqt-config-cursor_el.qm;/usr/share/lxqt/translations/lxqt-config-cursor/lxqt-config-cursor_en_GB.qm;/usr/share/lxqt/translations/lxqt-config-cursor/lxqt-config-cursor_eo.qm;/usr/share/lxqt/translations/lxqt-config-cursor/lxqt-config-cursor_es.qm;/usr/share/lxqt/translations/lxqt-config-cursor/lxqt-config-cursor_es_VE.qm;/usr/share/lxqt/translations/lxqt-config-cursor/lxqt-config-cursor_et.qm;/usr/share/lxqt/translations/lxqt-config-cursor/lxqt-config-cursor_eu.qm;/usr/share/lxqt/translations/lxqt-config-cursor/lxqt-config-cursor_fi.qm;/usr/share/lxqt/translations/lxqt-config-cursor/lxqt-config-cursor_fr.qm;/usr/share/lxqt/translations/lxqt-config-cursor/lxqt-config-cursor_gl.qm;/usr/share/lxqt/translations/lxqt-config-cursor/lxqt-config-cursor_he.qm;/usr/share/lxqt/translations/lxqt-config-cursor/lxqt-config-cursor_hr.qm;/usr/share/lxqt/translations/lxqt-config-cursor/lxqt-config-cursor_hu.qm;/usr/share/lxqt/translations/lxqt-config-cursor/lxqt-config-cursor_id.qm;/usr/share/lxqt/translations/lxqt-config-cursor/lxqt-config-cursor_it.qm;/usr/share/lxqt/translations/lxqt-config-cursor/lxqt-config-cursor_ja.qm;/usr/share/lxqt/translations/lxqt-config-cursor/lxqt-config-cursor_ko.qm;/usr/share/lxqt/translations/lxqt-config-cursor/lxqt-config-cursor_lt.qm;/usr/share/lxqt/translations/lxqt-config-cursor/lxqt-config-cursor_lv.qm;/usr/share/lxqt/translations/lxqt-config-cursor/lxqt-config-cursor_nb_NO.qm;/usr/share/lxqt/translations/lxqt-config-cursor/lxqt-config-cursor_nl.qm;/usr/share/lxqt/translations/lxqt-config-cursor/lxqt-config-cursor_pl.qm;/usr/share/lxqt/translations/lxqt-config-cursor/lxqt-config-cursor_pt.qm;/usr/share/lxqt/translations/lxqt-config-cursor/lxqt-config-cursor_pt_BR.qm;/usr/share/lxqt/translations/lxqt-config-cursor/lxqt-config-cursor_ro_RO.qm;/usr/share/lxqt/translations/lxqt-config-cursor/lxqt-config-cursor_ru.qm;/usr/share/lxqt/translations/lxqt-config-cursor/lxqt-config-cursor_si.qm;/usr/share/lxqt/translations/lxqt-config-cursor/lxqt-config-cursor_sk_SK.qm;/usr/share/lxqt/translations/lxqt-config-cursor/lxqt-config-cursor_sl.qm;/usr/share/lxqt/translations/lxqt-config-cursor/lxqt-config-cursor_sv.qm;/usr/share/lxqt/translations/lxqt-config-cursor/lxqt-config-cursor_th_TH.qm;/usr/share/lxqt/translations/lxqt-config-cursor/lxqt-config-cursor_tr.qm;/usr/share/lxqt/translations/lxqt-config-cursor/lxqt-config-cursor_uk.qm;/usr/share/lxqt/translations/lxqt-config-cursor/lxqt-config-cursor_zh_CN.qm;/usr/share/lxqt/translations/lxqt-config-cursor/lxqt-config-cursor_zh_TW.qm")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/usr/share/lxqt/translations/lxqt-config-cursor" TYPE FILE FILES
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/liblxqt-config-cursor/lxqt-config-cursor_ar.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/liblxqt-config-cursor/lxqt-config-cursor_arn.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/liblxqt-config-cursor/lxqt-config-cursor_ast.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/liblxqt-config-cursor/lxqt-config-cursor_bg.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/liblxqt-config-cursor/lxqt-config-cursor_ca.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/liblxqt-config-cursor/lxqt-config-cursor_cs.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/liblxqt-config-cursor/lxqt-config-cursor_cy.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/liblxqt-config-cursor/lxqt-config-cursor_da.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/liblxqt-config-cursor/lxqt-config-cursor_de.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/liblxqt-config-cursor/lxqt-config-cursor_el.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/liblxqt-config-cursor/lxqt-config-cursor_en_GB.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/liblxqt-config-cursor/lxqt-config-cursor_eo.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/liblxqt-config-cursor/lxqt-config-cursor_es.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/liblxqt-config-cursor/lxqt-config-cursor_es_VE.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/liblxqt-config-cursor/lxqt-config-cursor_et.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/liblxqt-config-cursor/lxqt-config-cursor_eu.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/liblxqt-config-cursor/lxqt-config-cursor_fi.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/liblxqt-config-cursor/lxqt-config-cursor_fr.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/liblxqt-config-cursor/lxqt-config-cursor_gl.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/liblxqt-config-cursor/lxqt-config-cursor_he.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/liblxqt-config-cursor/lxqt-config-cursor_hr.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/liblxqt-config-cursor/lxqt-config-cursor_hu.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/liblxqt-config-cursor/lxqt-config-cursor_id.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/liblxqt-config-cursor/lxqt-config-cursor_it.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/liblxqt-config-cursor/lxqt-config-cursor_ja.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/liblxqt-config-cursor/lxqt-config-cursor_ko.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/liblxqt-config-cursor/lxqt-config-cursor_lt.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/liblxqt-config-cursor/lxqt-config-cursor_lv.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/liblxqt-config-cursor/lxqt-config-cursor_nb_NO.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/liblxqt-config-cursor/lxqt-config-cursor_nl.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/liblxqt-config-cursor/lxqt-config-cursor_pl.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/liblxqt-config-cursor/lxqt-config-cursor_pt.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/liblxqt-config-cursor/lxqt-config-cursor_pt_BR.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/liblxqt-config-cursor/lxqt-config-cursor_ro_RO.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/liblxqt-config-cursor/lxqt-config-cursor_ru.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/liblxqt-config-cursor/lxqt-config-cursor_si.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/liblxqt-config-cursor/lxqt-config-cursor_sk_SK.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/liblxqt-config-cursor/lxqt-config-cursor_sl.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/liblxqt-config-cursor/lxqt-config-cursor_sv.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/liblxqt-config-cursor/lxqt-config-cursor_th_TH.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/liblxqt-config-cursor/lxqt-config-cursor_tr.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/liblxqt-config-cursor/lxqt-config-cursor_uk.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/liblxqt-config-cursor/lxqt-config-cursor_zh_CN.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/liblxqt-config-cursor/lxqt-config-cursor_zh_TW.qm"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xRuntimex" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/x86_64-linux-gnu/lxqt-config/liblxqt-config-cursor.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/x86_64-linux-gnu/lxqt-config/liblxqt-config-cursor.so")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/x86_64-linux-gnu/lxqt-config/liblxqt-config-cursor.so"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/x86_64-linux-gnu/lxqt-config" TYPE SHARED_LIBRARY FILES "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/liblxqt-config-cursor/liblxqt-config-cursor.so")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/x86_64-linux-gnu/lxqt-config/liblxqt-config-cursor.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/x86_64-linux-gnu/lxqt-config/liblxqt-config-cursor.so")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/x86_64-linux-gnu/lxqt-config/liblxqt-config-cursor.so")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xRuntimex" OR NOT CMAKE_INSTALL_COMPONENT)
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xRuntimex" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/man/man1" TYPE FILE FILES "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/liblxqt-config-cursor/man/lxqt-config-mouse.1")
endif()

