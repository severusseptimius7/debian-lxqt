/********************************************************************************
** Form generated from reading UI file 'selectwnd.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SELECTWND_H
#define UI_SELECTWND_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListView>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QWidget>
#include "previewwidget.h"
#include "warninglabel.h"

QT_BEGIN_NAMESPACE

class Ui_SelectWnd
{
public:
    QGridLayout *gridLayout;
    QPushButton *btInstall;
    PreviewWidget *preview;
    QLabel *infoLabel;
    QListView *lbThemes;
    QSpinBox *cursorSizeSpinBox;
    WarningLabel *warningLabel;
    QPushButton *btRemove;
    QSpacerItem *spacerItem;
    QLabel *sizeLabel;

    void setupUi(QWidget *SelectWnd)
    {
        if (SelectWnd->objectName().isEmpty())
            SelectWnd->setObjectName(QString::fromUtf8("SelectWnd"));
        SelectWnd->resize(438, 364);
        gridLayout = new QGridLayout(SelectWnd);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        btInstall = new QPushButton(SelectWnd);
        btInstall->setObjectName(QString::fromUtf8("btInstall"));
        btInstall->setEnabled(false);

        gridLayout->addWidget(btInstall, 5, 3, 1, 1);

        preview = new PreviewWidget(SelectWnd);
        preview->setObjectName(QString::fromUtf8("preview"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(preview->sizePolicy().hasHeightForWidth());
        preview->setSizePolicy(sizePolicy);
        preview->setMinimumSize(QSize(0, 48));

        gridLayout->addWidget(preview, 2, 0, 1, 6);

        infoLabel = new QLabel(SelectWnd);
        infoLabel->setObjectName(QString::fromUtf8("infoLabel"));
        infoLabel->setWordWrap(true);

        gridLayout->addWidget(infoLabel, 0, 0, 1, 6);

        lbThemes = new QListView(SelectWnd);
        lbThemes->setObjectName(QString::fromUtf8("lbThemes"));
        lbThemes->setEditTriggers(QAbstractItemView::NoEditTriggers);
        lbThemes->setProperty("showDropIndicator", QVariant(false));
        lbThemes->setAlternatingRowColors(true);
        lbThemes->setVerticalScrollMode(QAbstractItemView::ScrollPerPixel);

        gridLayout->addWidget(lbThemes, 3, 0, 1, 6);

        cursorSizeSpinBox = new QSpinBox(SelectWnd);
        cursorSizeSpinBox->setObjectName(QString::fromUtf8("cursorSizeSpinBox"));
        cursorSizeSpinBox->setMinimum(12);
        cursorSizeSpinBox->setMaximum(128);
        cursorSizeSpinBox->setValue(16);

        gridLayout->addWidget(cursorSizeSpinBox, 5, 1, 1, 1);

        warningLabel = new WarningLabel(SelectWnd);
        warningLabel->setObjectName(QString::fromUtf8("warningLabel"));

        gridLayout->addWidget(warningLabel, 1, 0, 1, 6);

        btRemove = new QPushButton(SelectWnd);
        btRemove->setObjectName(QString::fromUtf8("btRemove"));

        gridLayout->addWidget(btRemove, 5, 4, 1, 1);

        spacerItem = new QSpacerItem(174, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(spacerItem, 5, 2, 1, 1);

        sizeLabel = new QLabel(SelectWnd);
        sizeLabel->setObjectName(QString::fromUtf8("sizeLabel"));

        gridLayout->addWidget(sizeLabel, 5, 0, 1, 1);

        QWidget::setTabOrder(lbThemes, btInstall);
        QWidget::setTabOrder(btInstall, btRemove);

        retranslateUi(SelectWnd);

        QMetaObject::connectSlotsByName(SelectWnd);
    } // setupUi

    void retranslateUi(QWidget *SelectWnd)
    {
        SelectWnd->setWindowTitle(QCoreApplication::translate("SelectWnd", "LXQt Mouse Theme Configuration", nullptr));
        btInstall->setText(QCoreApplication::translate("SelectWnd", "&Install New Theme...", nullptr));
        infoLabel->setText(QCoreApplication::translate("SelectWnd", "Select the cursor theme you want to use (hover preview to test cursor). <b>LXQt session needs restart after this change</b>:", nullptr));
#if QT_CONFIG(tooltip)
        cursorSizeSpinBox->setToolTip(QCoreApplication::translate("SelectWnd", "LXQt session needs restart to view this change.", nullptr));
#endif // QT_CONFIG(tooltip)
        btRemove->setText(QCoreApplication::translate("SelectWnd", "&Remove Theme", nullptr));
        sizeLabel->setText(QCoreApplication::translate("SelectWnd", "Size", nullptr));
    } // retranslateUi

};

namespace Ui {
    class SelectWnd: public Ui_SelectWnd {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SELECTWND_H
