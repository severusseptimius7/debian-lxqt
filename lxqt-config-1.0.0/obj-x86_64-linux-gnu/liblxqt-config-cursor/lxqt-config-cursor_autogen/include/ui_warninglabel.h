/********************************************************************************
** Form generated from reading UI file 'warninglabel.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WARNINGLABEL_H
#define UI_WARNINGLABEL_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_WarningLabel
{
public:
    QGridLayout *gridLayout;
    QHBoxLayout *horizontalLayout;
    QLabel *iconLabel;
    QLabel *warningLabel;
    QPushButton *showDirButton;

    void setupUi(QWidget *WarningLabel)
    {
        if (WarningLabel->objectName().isEmpty())
            WarningLabel->setObjectName(QString::fromUtf8("WarningLabel"));
        WarningLabel->resize(334, 72);
        gridLayout = new QGridLayout(WarningLabel);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        iconLabel = new QLabel(WarningLabel);
        iconLabel->setObjectName(QString::fromUtf8("iconLabel"));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(iconLabel->sizePolicy().hasHeightForWidth());
        iconLabel->setSizePolicy(sizePolicy);
        iconLabel->setMinimumSize(QSize(64, 64));

        horizontalLayout->addWidget(iconLabel);

        warningLabel = new QLabel(WarningLabel);
        warningLabel->setObjectName(QString::fromUtf8("warningLabel"));
        QSizePolicy sizePolicy1(QSizePolicy::MinimumExpanding, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(warningLabel->sizePolicy().hasHeightForWidth());
        warningLabel->setSizePolicy(sizePolicy1);
        warningLabel->setWordWrap(true);

        horizontalLayout->addWidget(warningLabel);

        showDirButton = new QPushButton(WarningLabel);
        showDirButton->setObjectName(QString::fromUtf8("showDirButton"));

        horizontalLayout->addWidget(showDirButton);


        gridLayout->addLayout(horizontalLayout, 0, 0, 1, 1);


        retranslateUi(WarningLabel);

        QMetaObject::connectSlotsByName(WarningLabel);
    } // setupUi

    void retranslateUi(QWidget *WarningLabel)
    {
        WarningLabel->setWindowTitle(QCoreApplication::translate("WarningLabel", "Form", nullptr));
        warningLabel->setText(QCoreApplication::translate("WarningLabel", "LXQt could not find any cursor theme. The default X11 cursor theme will be used instead. LXQt looked in the following directories:", nullptr));
        showDirButton->setText(QCoreApplication::translate("WarningLabel", "Show...", nullptr));
    } // retranslateUi

};

namespace Ui {
    class WarningLabel: public Ui_WarningLabel {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WARNINGLABEL_H
