# Install script for directory: /home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/lxqt-config-file-associations

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "RelWithDebInfo")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

# Set default install directory permissions.
if(NOT DEFINED CMAKE_OBJDUMP)
  set(CMAKE_OBJDUMP "/usr/bin/objdump")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xRuntimex" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/share/lxqt/translations/lxqt-config-file-associations/lxqt-config-file-associations_ar.qm;/usr/share/lxqt/translations/lxqt-config-file-associations/lxqt-config-file-associations_arn.qm;/usr/share/lxqt/translations/lxqt-config-file-associations/lxqt-config-file-associations_ast.qm;/usr/share/lxqt/translations/lxqt-config-file-associations/lxqt-config-file-associations_bg.qm;/usr/share/lxqt/translations/lxqt-config-file-associations/lxqt-config-file-associations_ca.qm;/usr/share/lxqt/translations/lxqt-config-file-associations/lxqt-config-file-associations_cs.qm;/usr/share/lxqt/translations/lxqt-config-file-associations/lxqt-config-file-associations_cy.qm;/usr/share/lxqt/translations/lxqt-config-file-associations/lxqt-config-file-associations_da.qm;/usr/share/lxqt/translations/lxqt-config-file-associations/lxqt-config-file-associations_de.qm;/usr/share/lxqt/translations/lxqt-config-file-associations/lxqt-config-file-associations_el.qm;/usr/share/lxqt/translations/lxqt-config-file-associations/lxqt-config-file-associations_en_GB.qm;/usr/share/lxqt/translations/lxqt-config-file-associations/lxqt-config-file-associations_es.qm;/usr/share/lxqt/translations/lxqt-config-file-associations/lxqt-config-file-associations_et.qm;/usr/share/lxqt/translations/lxqt-config-file-associations/lxqt-config-file-associations_fr.qm;/usr/share/lxqt/translations/lxqt-config-file-associations/lxqt-config-file-associations_gl.qm;/usr/share/lxqt/translations/lxqt-config-file-associations/lxqt-config-file-associations_he.qm;/usr/share/lxqt/translations/lxqt-config-file-associations/lxqt-config-file-associations_hr.qm;/usr/share/lxqt/translations/lxqt-config-file-associations/lxqt-config-file-associations_hu.qm;/usr/share/lxqt/translations/lxqt-config-file-associations/lxqt-config-file-associations_id.qm;/usr/share/lxqt/translations/lxqt-config-file-associations/lxqt-config-file-associations_it.qm;/usr/share/lxqt/translations/lxqt-config-file-associations/lxqt-config-file-associations_ja.qm;/usr/share/lxqt/translations/lxqt-config-file-associations/lxqt-config-file-associations_ko.qm;/usr/share/lxqt/translations/lxqt-config-file-associations/lxqt-config-file-associations_lt.qm;/usr/share/lxqt/translations/lxqt-config-file-associations/lxqt-config-file-associations_lv.qm;/usr/share/lxqt/translations/lxqt-config-file-associations/lxqt-config-file-associations_nb_NO.qm;/usr/share/lxqt/translations/lxqt-config-file-associations/lxqt-config-file-associations_nl.qm;/usr/share/lxqt/translations/lxqt-config-file-associations/lxqt-config-file-associations_pl.qm;/usr/share/lxqt/translations/lxqt-config-file-associations/lxqt-config-file-associations_pt.qm;/usr/share/lxqt/translations/lxqt-config-file-associations/lxqt-config-file-associations_ru.qm;/usr/share/lxqt/translations/lxqt-config-file-associations/lxqt-config-file-associations_si.qm;/usr/share/lxqt/translations/lxqt-config-file-associations/lxqt-config-file-associations_sk_SK.qm;/usr/share/lxqt/translations/lxqt-config-file-associations/lxqt-config-file-associations_tr.qm;/usr/share/lxqt/translations/lxqt-config-file-associations/lxqt-config-file-associations_uk.qm;/usr/share/lxqt/translations/lxqt-config-file-associations/lxqt-config-file-associations_zh_CN.qm")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/usr/share/lxqt/translations/lxqt-config-file-associations" TYPE FILE FILES
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-file-associations/lxqt-config-file-associations_ar.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-file-associations/lxqt-config-file-associations_arn.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-file-associations/lxqt-config-file-associations_ast.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-file-associations/lxqt-config-file-associations_bg.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-file-associations/lxqt-config-file-associations_ca.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-file-associations/lxqt-config-file-associations_cs.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-file-associations/lxqt-config-file-associations_cy.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-file-associations/lxqt-config-file-associations_da.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-file-associations/lxqt-config-file-associations_de.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-file-associations/lxqt-config-file-associations_el.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-file-associations/lxqt-config-file-associations_en_GB.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-file-associations/lxqt-config-file-associations_es.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-file-associations/lxqt-config-file-associations_et.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-file-associations/lxqt-config-file-associations_fr.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-file-associations/lxqt-config-file-associations_gl.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-file-associations/lxqt-config-file-associations_he.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-file-associations/lxqt-config-file-associations_hr.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-file-associations/lxqt-config-file-associations_hu.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-file-associations/lxqt-config-file-associations_id.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-file-associations/lxqt-config-file-associations_it.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-file-associations/lxqt-config-file-associations_ja.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-file-associations/lxqt-config-file-associations_ko.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-file-associations/lxqt-config-file-associations_lt.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-file-associations/lxqt-config-file-associations_lv.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-file-associations/lxqt-config-file-associations_nb_NO.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-file-associations/lxqt-config-file-associations_nl.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-file-associations/lxqt-config-file-associations_pl.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-file-associations/lxqt-config-file-associations_pt.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-file-associations/lxqt-config-file-associations_ru.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-file-associations/lxqt-config-file-associations_si.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-file-associations/lxqt-config-file-associations_sk_SK.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-file-associations/lxqt-config-file-associations_tr.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-file-associations/lxqt-config-file-associations_uk.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-file-associations/lxqt-config-file-associations_zh_CN.qm"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xRuntimex" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/lxqt-config-file-associations" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/lxqt-config-file-associations")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/lxqt-config-file-associations"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-file-associations/lxqt-config-file-associations")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/lxqt-config-file-associations" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/lxqt-config-file-associations")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/lxqt-config-file-associations")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xRuntimex" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/applications" TYPE FILE FILES "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-file-associations/lxqt-config-file-associations.desktop")
endif()

