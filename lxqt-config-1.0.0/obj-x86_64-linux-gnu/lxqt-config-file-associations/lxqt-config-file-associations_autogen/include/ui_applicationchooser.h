/********************************************************************************
** Form generated from reading UI file 'applicationchooser.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_APPLICATIONCHOOSER_H
#define UI_APPLICATIONCHOOSER_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QTreeWidget>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_ApplicationChooser
{
public:
    QVBoxLayout *verticalLayout;
    QFrame *frame_2;
    QGridLayout *gridLayout;
    QLabel *headingLabel;
    QLabel *mimetypeIconLabel;
    QLabel *mimetypeLabel;
    QTreeWidget *applicationTreeWidget;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *ApplicationChooser)
    {
        if (ApplicationChooser->objectName().isEmpty())
            ApplicationChooser->setObjectName(QString::fromUtf8("ApplicationChooser"));
        ApplicationChooser->resize(385, 417);
        verticalLayout = new QVBoxLayout(ApplicationChooser);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        frame_2 = new QFrame(ApplicationChooser);
        frame_2->setObjectName(QString::fromUtf8("frame_2"));
        frame_2->setFrameShape(QFrame::NoFrame);
        frame_2->setFrameShadow(QFrame::Raised);
        gridLayout = new QGridLayout(frame_2);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        headingLabel = new QLabel(frame_2);
        headingLabel->setObjectName(QString::fromUtf8("headingLabel"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(headingLabel->sizePolicy().hasHeightForWidth());
        headingLabel->setSizePolicy(sizePolicy);
        headingLabel->setMinimumSize(QSize(0, 30));
        QFont font;
        font.setPointSize(12);
        headingLabel->setFont(font);

        gridLayout->addWidget(headingLabel, 0, 0, 1, 2);

        mimetypeIconLabel = new QLabel(frame_2);
        mimetypeIconLabel->setObjectName(QString::fromUtf8("mimetypeIconLabel"));
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(mimetypeIconLabel->sizePolicy().hasHeightForWidth());
        mimetypeIconLabel->setSizePolicy(sizePolicy1);
        mimetypeIconLabel->setMinimumSize(QSize(30, 30));
        mimetypeIconLabel->setText(QString::fromUtf8("Icon"));

        gridLayout->addWidget(mimetypeIconLabel, 1, 0, 1, 1);

        mimetypeLabel = new QLabel(frame_2);
        mimetypeLabel->setObjectName(QString::fromUtf8("mimetypeLabel"));
        mimetypeLabel->setText(QString::fromUtf8("Mimetype"));

        gridLayout->addWidget(mimetypeLabel, 1, 1, 1, 1);


        verticalLayout->addWidget(frame_2);

        applicationTreeWidget = new QTreeWidget(ApplicationChooser);
        QTreeWidgetItem *__qtreewidgetitem = new QTreeWidgetItem(applicationTreeWidget);
        new QTreeWidgetItem(__qtreewidgetitem);
        new QTreeWidgetItem(__qtreewidgetitem);
        new QTreeWidgetItem(__qtreewidgetitem);
        QTreeWidgetItem *__qtreewidgetitem1 = new QTreeWidgetItem(applicationTreeWidget);
        new QTreeWidgetItem(__qtreewidgetitem1);
        new QTreeWidgetItem(__qtreewidgetitem1);
        applicationTreeWidget->setObjectName(QString::fromUtf8("applicationTreeWidget"));
        applicationTreeWidget->setMinimumSize(QSize(240, 200));
        applicationTreeWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
        applicationTreeWidget->setProperty("showDropIndicator", QVariant(false));
        applicationTreeWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
        applicationTreeWidget->setRootIsDecorated(false);
        applicationTreeWidget->setItemsExpandable(false);
        applicationTreeWidget->setHeaderHidden(true);
        applicationTreeWidget->header()->setVisible(false);

        verticalLayout->addWidget(applicationTreeWidget);

        buttonBox = new QDialogButtonBox(ApplicationChooser);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setLayoutDirection(Qt::LeftToRight);
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        verticalLayout->addWidget(buttonBox);


        retranslateUi(ApplicationChooser);
        QObject::connect(buttonBox, SIGNAL(accepted()), ApplicationChooser, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), ApplicationChooser, SLOT(reject()));

        QMetaObject::connectSlotsByName(ApplicationChooser);
    } // setupUi

    void retranslateUi(QDialog *ApplicationChooser)
    {
        ApplicationChooser->setWindowTitle(QCoreApplication::translate("ApplicationChooser", "ApplicationChooser", nullptr));
        headingLabel->setText(QCoreApplication::translate("ApplicationChooser", "Pick an application for:", nullptr));
        QTreeWidgetItem *___qtreewidgetitem = applicationTreeWidget->headerItem();
        ___qtreewidgetitem->setText(0, QCoreApplication::translate("ApplicationChooser", "1", nullptr));

        const bool __sortingEnabled = applicationTreeWidget->isSortingEnabled();
        applicationTreeWidget->setSortingEnabled(false);
        QTreeWidgetItem *___qtreewidgetitem1 = applicationTreeWidget->topLevelItem(0);
        ___qtreewidgetitem1->setText(0, QCoreApplication::translate("ApplicationChooser", "Applications that handle JPEG", nullptr));
        QTreeWidgetItem *___qtreewidgetitem2 = ___qtreewidgetitem1->child(1);
        ___qtreewidgetitem2->setText(0, QCoreApplication::translate("ApplicationChooser", "New Subitem", nullptr));
        QTreeWidgetItem *___qtreewidgetitem3 = ___qtreewidgetitem1->child(2);
        ___qtreewidgetitem3->setText(0, QCoreApplication::translate("ApplicationChooser", "New Subitem", nullptr));
        QTreeWidgetItem *___qtreewidgetitem4 = applicationTreeWidget->topLevelItem(1);
        ___qtreewidgetitem4->setText(0, QCoreApplication::translate("ApplicationChooser", "Other applications", nullptr));
        QTreeWidgetItem *___qtreewidgetitem5 = ___qtreewidgetitem4->child(0);
        ___qtreewidgetitem5->setText(0, QCoreApplication::translate("ApplicationChooser", "New Subitem", nullptr));
        QTreeWidgetItem *___qtreewidgetitem6 = ___qtreewidgetitem4->child(1);
        ___qtreewidgetitem6->setText(0, QCoreApplication::translate("ApplicationChooser", "New Subitem", nullptr));
        applicationTreeWidget->setSortingEnabled(__sortingEnabled);

    } // retranslateUi

};

namespace Ui {
    class ApplicationChooser: public Ui_ApplicationChooser {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_APPLICATIONCHOOSER_H
