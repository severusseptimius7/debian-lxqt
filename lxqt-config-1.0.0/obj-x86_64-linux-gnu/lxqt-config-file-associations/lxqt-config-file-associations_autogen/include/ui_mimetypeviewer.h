/********************************************************************************
** Form generated from reading UI file 'mimetypeviewer.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MIMETYPEVIEWER_H
#define UI_MIMETYPEVIEWER_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTreeWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_mimetypeviewer
{
public:
    QVBoxLayout *verticalLayout;
    QTabWidget *tabWidget;
    QWidget *tab_1;
    QGridLayout *gridLayout;
    QLineEdit *searchTermLineEdit;
    QTreeWidget *mimetypeTreeWidget;
    QFrame *mimeFrame;
    QVBoxLayout *verticalLayout_1;
    QScrollArea *scrollArea_2;
    QWidget *scrollAreaWidgetContents_3;
    QVBoxLayout *verticalLayout_2;
    QGroupBox *mimetypeGroupBox;
    QHBoxLayout *horizontalLayout;
    QLabel *iconLabel;
    QLabel *descriptionLabel;
    QGroupBox *patternsGroupBox;
    QHBoxLayout *horizontalLayout_1;
    QLabel *patternsLabel;
    QGroupBox *applicationsGroupBox;
    QGridLayout *gridLayout_1;
    QFrame *applicationLabelFrame;
    QHBoxLayout *horizontalLayout_2;
    QLabel *appIcon;
    QLabel *applicationLabel;
    QPushButton *chooseApplicationsButton;
    QSpacerItem *verticalSpacer;
    QWidget *tab_2;
    QVBoxLayout *verticalLayout_3;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents;
    QVBoxLayout *verticalLayout_4;
    QGroupBox *browserGroupBox;
    QGridLayout *gridLayout_2;
    QFrame *browserLabelFrame;
    QHBoxLayout *horizontalLayout_3;
    QLabel *browserIcon;
    QLabel *browserLabel;
    QPushButton *chooseBrowserButton;
    QGroupBox *emailClientGroupBox;
    QGridLayout *gridLayout_3;
    QFrame *emailClientLabelFrame;
    QHBoxLayout *horizontalLayout_4;
    QLabel *emailClientIcon;
    QLabel *emailClientLabel;
    QPushButton *chooseEmailClientButton;
    QGroupBox *fileManagerGroupBox;
    QGridLayout *gridLayout_4;
    QFrame *fileManagerLabelFrame;
    QHBoxLayout *horizontalLayout_5;
    QLabel *fileManagerIcon;
    QLabel *fileManagerLabel;
    QPushButton *chooseFileManagerButton;
    QSpacerItem *verticalSpacer_2;
    QDialogButtonBox *dialogButtonBox;

    void setupUi(QWidget *mimetypeviewer)
    {
        if (mimetypeviewer->objectName().isEmpty())
            mimetypeviewer->setObjectName(QString::fromUtf8("mimetypeviewer"));
        mimetypeviewer->resize(565, 400);
        mimetypeviewer->setMinimumSize(QSize(530, 400));
        verticalLayout = new QVBoxLayout(mimetypeviewer);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        tabWidget = new QTabWidget(mimetypeviewer);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tab_1 = new QWidget();
        tab_1->setObjectName(QString::fromUtf8("tab_1"));
        gridLayout = new QGridLayout(tab_1);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        searchTermLineEdit = new QLineEdit(tab_1);
        searchTermLineEdit->setObjectName(QString::fromUtf8("searchTermLineEdit"));
        searchTermLineEdit->setMinimumSize(QSize(0, 30));

        gridLayout->addWidget(searchTermLineEdit, 0, 0, 1, 1);

        mimetypeTreeWidget = new QTreeWidget(tab_1);
        mimetypeTreeWidget->setObjectName(QString::fromUtf8("mimetypeTreeWidget"));
        QSizePolicy sizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(mimetypeTreeWidget->sizePolicy().hasHeightForWidth());
        mimetypeTreeWidget->setSizePolicy(sizePolicy);
        mimetypeTreeWidget->setMinimumSize(QSize(180, 0));
        mimetypeTreeWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
        mimetypeTreeWidget->setProperty("showDropIndicator", QVariant(false));
        mimetypeTreeWidget->setHeaderHidden(true);
        mimetypeTreeWidget->setExpandsOnDoubleClick(false);
        mimetypeTreeWidget->setColumnCount(0);
        mimetypeTreeWidget->header()->setVisible(false);
        mimetypeTreeWidget->header()->setDefaultSectionSize(500);

        gridLayout->addWidget(mimetypeTreeWidget, 1, 0, 1, 1);

        mimeFrame = new QFrame(tab_1);
        mimeFrame->setObjectName(QString::fromUtf8("mimeFrame"));
        mimeFrame->setEnabled(true);
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::MinimumExpanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(mimeFrame->sizePolicy().hasHeightForWidth());
        mimeFrame->setSizePolicy(sizePolicy1);
        mimeFrame->setMinimumSize(QSize(280, 260));
        mimeFrame->setFrameShape(QFrame::NoFrame);
        mimeFrame->setFrameShadow(QFrame::Plain);
        verticalLayout_1 = new QVBoxLayout(mimeFrame);
        verticalLayout_1->setObjectName(QString::fromUtf8("verticalLayout_1"));
        verticalLayout_1->setContentsMargins(0, 0, 0, 0);
        scrollArea_2 = new QScrollArea(mimeFrame);
        scrollArea_2->setObjectName(QString::fromUtf8("scrollArea_2"));
        scrollArea_2->setFrameShape(QFrame::NoFrame);
        scrollArea_2->setFrameShadow(QFrame::Plain);
        scrollArea_2->setWidgetResizable(true);
        scrollAreaWidgetContents_3 = new QWidget();
        scrollAreaWidgetContents_3->setObjectName(QString::fromUtf8("scrollAreaWidgetContents_3"));
        scrollAreaWidgetContents_3->setGeometry(QRect(0, 0, 280, 314));
        verticalLayout_2 = new QVBoxLayout(scrollAreaWidgetContents_3);
        verticalLayout_2->setSpacing(10);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        mimetypeGroupBox = new QGroupBox(scrollAreaWidgetContents_3);
        mimetypeGroupBox->setObjectName(QString::fromUtf8("mimetypeGroupBox"));
        horizontalLayout = new QHBoxLayout(mimetypeGroupBox);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        iconLabel = new QLabel(mimetypeGroupBox);
        iconLabel->setObjectName(QString::fromUtf8("iconLabel"));
        QSizePolicy sizePolicy2(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(iconLabel->sizePolicy().hasHeightForWidth());
        iconLabel->setSizePolicy(sizePolicy2);
        iconLabel->setMinimumSize(QSize(48, 48));

        horizontalLayout->addWidget(iconLabel);

        descriptionLabel = new QLabel(mimetypeGroupBox);
        descriptionLabel->setObjectName(QString::fromUtf8("descriptionLabel"));
        descriptionLabel->setWordWrap(true);
        descriptionLabel->setIndent(10);

        horizontalLayout->addWidget(descriptionLabel);


        verticalLayout_2->addWidget(mimetypeGroupBox);

        patternsGroupBox = new QGroupBox(scrollAreaWidgetContents_3);
        patternsGroupBox->setObjectName(QString::fromUtf8("patternsGroupBox"));
        horizontalLayout_1 = new QHBoxLayout(patternsGroupBox);
        horizontalLayout_1->setObjectName(QString::fromUtf8("horizontalLayout_1"));
        patternsLabel = new QLabel(patternsGroupBox);
        patternsLabel->setObjectName(QString::fromUtf8("patternsLabel"));
        patternsLabel->setWordWrap(true);

        horizontalLayout_1->addWidget(patternsLabel);


        verticalLayout_2->addWidget(patternsGroupBox);

        applicationsGroupBox = new QGroupBox(scrollAreaWidgetContents_3);
        applicationsGroupBox->setObjectName(QString::fromUtf8("applicationsGroupBox"));
        gridLayout_1 = new QGridLayout(applicationsGroupBox);
        gridLayout_1->setObjectName(QString::fromUtf8("gridLayout_1"));
        applicationLabelFrame = new QFrame(applicationsGroupBox);
        applicationLabelFrame->setObjectName(QString::fromUtf8("applicationLabelFrame"));
        applicationLabelFrame->setFrameShape(QFrame::NoFrame);
        applicationLabelFrame->setFrameShadow(QFrame::Raised);
        horizontalLayout_2 = new QHBoxLayout(applicationLabelFrame);
        horizontalLayout_2->setSpacing(0);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(0, 0, 0, 0);
        appIcon = new QLabel(applicationLabelFrame);
        appIcon->setObjectName(QString::fromUtf8("appIcon"));
        sizePolicy2.setHeightForWidth(appIcon->sizePolicy().hasHeightForWidth());
        appIcon->setSizePolicy(sizePolicy2);
        appIcon->setMinimumSize(QSize(48, 48));

        horizontalLayout_2->addWidget(appIcon);

        applicationLabel = new QLabel(applicationLabelFrame);
        applicationLabel->setObjectName(QString::fromUtf8("applicationLabel"));
        applicationLabel->setWordWrap(true);
        applicationLabel->setIndent(10);

        horizontalLayout_2->addWidget(applicationLabel);


        gridLayout_1->addWidget(applicationLabelFrame, 0, 0, 1, 1);

        chooseApplicationsButton = new QPushButton(applicationsGroupBox);
        chooseApplicationsButton->setObjectName(QString::fromUtf8("chooseApplicationsButton"));

        gridLayout_1->addWidget(chooseApplicationsButton, 1, 0, 1, 1, Qt::AlignRight);


        verticalLayout_2->addWidget(applicationsGroupBox);

        verticalSpacer = new QSpacerItem(20, 5, QSizePolicy::Minimum, QSizePolicy::MinimumExpanding);

        verticalLayout_2->addItem(verticalSpacer);

        scrollArea_2->setWidget(scrollAreaWidgetContents_3);

        verticalLayout_1->addWidget(scrollArea_2);


        gridLayout->addWidget(mimeFrame, 0, 1, 2, 1);

        tabWidget->addTab(tab_1, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QString::fromUtf8("tab_2"));
        verticalLayout_3 = new QVBoxLayout(tab_2);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        scrollArea = new QScrollArea(tab_2);
        scrollArea->setObjectName(QString::fromUtf8("scrollArea"));
        scrollArea->setFrameShape(QFrame::NoFrame);
        scrollArea->setFrameShadow(QFrame::Plain);
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QString::fromUtf8("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 106, 231));
        verticalLayout_4 = new QVBoxLayout(scrollAreaWidgetContents);
        verticalLayout_4->setSpacing(5);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        verticalLayout_4->setContentsMargins(0, 0, 0, 0);
        browserGroupBox = new QGroupBox(scrollAreaWidgetContents);
        browserGroupBox->setObjectName(QString::fromUtf8("browserGroupBox"));
        gridLayout_2 = new QGridLayout(browserGroupBox);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        browserLabelFrame = new QFrame(browserGroupBox);
        browserLabelFrame->setObjectName(QString::fromUtf8("browserLabelFrame"));
        browserLabelFrame->setFrameShape(QFrame::NoFrame);
        browserLabelFrame->setFrameShadow(QFrame::Raised);
        horizontalLayout_3 = new QHBoxLayout(browserLabelFrame);
        horizontalLayout_3->setSpacing(0);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalLayout_3->setContentsMargins(0, 0, 0, 0);
        browserIcon = new QLabel(browserLabelFrame);
        browserIcon->setObjectName(QString::fromUtf8("browserIcon"));
        sizePolicy2.setHeightForWidth(browserIcon->sizePolicy().hasHeightForWidth());
        browserIcon->setSizePolicy(sizePolicy2);
        browserIcon->setMinimumSize(QSize(48, 48));

        horizontalLayout_3->addWidget(browserIcon);

        browserLabel = new QLabel(browserLabelFrame);
        browserLabel->setObjectName(QString::fromUtf8("browserLabel"));
        browserLabel->setWordWrap(true);
        browserLabel->setIndent(10);

        horizontalLayout_3->addWidget(browserLabel);


        gridLayout_2->addWidget(browserLabelFrame, 0, 0, 1, 1);

        chooseBrowserButton = new QPushButton(browserGroupBox);
        chooseBrowserButton->setObjectName(QString::fromUtf8("chooseBrowserButton"));

        gridLayout_2->addWidget(chooseBrowserButton, 1, 0, 1, 1, Qt::AlignRight);


        verticalLayout_4->addWidget(browserGroupBox);

        emailClientGroupBox = new QGroupBox(scrollAreaWidgetContents);
        emailClientGroupBox->setObjectName(QString::fromUtf8("emailClientGroupBox"));
        gridLayout_3 = new QGridLayout(emailClientGroupBox);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        emailClientLabelFrame = new QFrame(emailClientGroupBox);
        emailClientLabelFrame->setObjectName(QString::fromUtf8("emailClientLabelFrame"));
        emailClientLabelFrame->setFrameShape(QFrame::NoFrame);
        emailClientLabelFrame->setFrameShadow(QFrame::Raised);
        horizontalLayout_4 = new QHBoxLayout(emailClientLabelFrame);
        horizontalLayout_4->setSpacing(0);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        horizontalLayout_4->setContentsMargins(0, 0, 0, 0);
        emailClientIcon = new QLabel(emailClientLabelFrame);
        emailClientIcon->setObjectName(QString::fromUtf8("emailClientIcon"));
        sizePolicy2.setHeightForWidth(emailClientIcon->sizePolicy().hasHeightForWidth());
        emailClientIcon->setSizePolicy(sizePolicy2);
        emailClientIcon->setMinimumSize(QSize(48, 48));

        horizontalLayout_4->addWidget(emailClientIcon);

        emailClientLabel = new QLabel(emailClientLabelFrame);
        emailClientLabel->setObjectName(QString::fromUtf8("emailClientLabel"));
        emailClientLabel->setWordWrap(true);
        emailClientLabel->setIndent(10);

        horizontalLayout_4->addWidget(emailClientLabel);


        gridLayout_3->addWidget(emailClientLabelFrame, 0, 0, 1, 1);

        chooseEmailClientButton = new QPushButton(emailClientGroupBox);
        chooseEmailClientButton->setObjectName(QString::fromUtf8("chooseEmailClientButton"));

        gridLayout_3->addWidget(chooseEmailClientButton, 1, 0, 1, 1, Qt::AlignRight);


        verticalLayout_4->addWidget(emailClientGroupBox);

        fileManagerGroupBox = new QGroupBox(scrollAreaWidgetContents);
        fileManagerGroupBox->setObjectName(QString::fromUtf8("fileManagerGroupBox"));
        gridLayout_4 = new QGridLayout(fileManagerGroupBox);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        fileManagerLabelFrame = new QFrame(fileManagerGroupBox);
        fileManagerLabelFrame->setObjectName(QString::fromUtf8("fileManagerLabelFrame"));
        fileManagerLabelFrame->setFrameShape(QFrame::NoFrame);
        fileManagerLabelFrame->setFrameShadow(QFrame::Raised);
        horizontalLayout_5 = new QHBoxLayout(fileManagerLabelFrame);
        horizontalLayout_5->setSpacing(0);
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        horizontalLayout_5->setContentsMargins(0, 0, 0, 0);
        fileManagerIcon = new QLabel(fileManagerLabelFrame);
        fileManagerIcon->setObjectName(QString::fromUtf8("fileManagerIcon"));
        sizePolicy2.setHeightForWidth(fileManagerIcon->sizePolicy().hasHeightForWidth());
        fileManagerIcon->setSizePolicy(sizePolicy2);
        fileManagerIcon->setMinimumSize(QSize(48, 48));

        horizontalLayout_5->addWidget(fileManagerIcon);

        fileManagerLabel = new QLabel(fileManagerLabelFrame);
        fileManagerLabel->setObjectName(QString::fromUtf8("fileManagerLabel"));
        fileManagerLabel->setWordWrap(true);
        fileManagerLabel->setIndent(10);

        horizontalLayout_5->addWidget(fileManagerLabel);


        gridLayout_4->addWidget(fileManagerLabelFrame, 0, 0, 1, 1);

        chooseFileManagerButton = new QPushButton(fileManagerGroupBox);
        chooseFileManagerButton->setObjectName(QString::fromUtf8("chooseFileManagerButton"));

        gridLayout_4->addWidget(chooseFileManagerButton, 1, 0, 1, 1, Qt::AlignRight);


        verticalLayout_4->addWidget(fileManagerGroupBox);

        verticalSpacer_2 = new QSpacerItem(20, 5, QSizePolicy::Minimum, QSizePolicy::MinimumExpanding);

        verticalLayout_4->addItem(verticalSpacer_2);

        scrollArea->setWidget(scrollAreaWidgetContents);

        verticalLayout_3->addWidget(scrollArea);

        tabWidget->addTab(tab_2, QString());

        verticalLayout->addWidget(tabWidget);

        dialogButtonBox = new QDialogButtonBox(mimetypeviewer);
        dialogButtonBox->setObjectName(QString::fromUtf8("dialogButtonBox"));
        dialogButtonBox->setStandardButtons(QDialogButtonBox::Close|QDialogButtonBox::Reset);

        verticalLayout->addWidget(dialogButtonBox);


        retranslateUi(mimetypeviewer);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(mimetypeviewer);
    } // setupUi

    void retranslateUi(QWidget *mimetypeviewer)
    {
        mimetypeviewer->setWindowTitle(QCoreApplication::translate("mimetypeviewer", "File Associations", nullptr));
        searchTermLineEdit->setText(QString());
        mimetypeGroupBox->setTitle(QCoreApplication::translate("mimetypeviewer", "MIME type", nullptr));
        iconLabel->setText(QCoreApplication::translate("mimetypeviewer", "Icon", nullptr));
        descriptionLabel->setText(QCoreApplication::translate("mimetypeviewer", "Description", nullptr));
        patternsGroupBox->setTitle(QCoreApplication::translate("mimetypeviewer", "Patterns", nullptr));
        patternsLabel->setText(QCoreApplication::translate("mimetypeviewer", "*.txt *.xml", nullptr));
        applicationsGroupBox->setTitle(QCoreApplication::translate("mimetypeviewer", "Default application", nullptr));
        appIcon->setText(QCoreApplication::translate("mimetypeviewer", "Icon", nullptr));
        applicationLabel->setText(QCoreApplication::translate("mimetypeviewer", "None", nullptr));
        chooseApplicationsButton->setText(QCoreApplication::translate("mimetypeviewer", "&Choose...", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_1), QCoreApplication::translate("mimetypeviewer", "Associations", nullptr));
        browserGroupBox->setTitle(QCoreApplication::translate("mimetypeviewer", "Web Browser", nullptr));
        browserIcon->setText(QCoreApplication::translate("mimetypeviewer", "Icon", nullptr));
        browserLabel->setText(QCoreApplication::translate("mimetypeviewer", "None", nullptr));
        chooseBrowserButton->setText(QCoreApplication::translate("mimetypeviewer", "Choose...", nullptr));
        emailClientGroupBox->setTitle(QCoreApplication::translate("mimetypeviewer", "Email Client", nullptr));
        emailClientIcon->setText(QCoreApplication::translate("mimetypeviewer", "Icon", nullptr));
        emailClientLabel->setText(QCoreApplication::translate("mimetypeviewer", "None", nullptr));
        chooseEmailClientButton->setText(QCoreApplication::translate("mimetypeviewer", "Choose...", nullptr));
        fileManagerGroupBox->setTitle(QCoreApplication::translate("mimetypeviewer", "File Manager", nullptr));
        fileManagerIcon->setText(QCoreApplication::translate("mimetypeviewer", "Icon", nullptr));
        fileManagerLabel->setText(QCoreApplication::translate("mimetypeviewer", "None", nullptr));
        chooseFileManagerButton->setText(QCoreApplication::translate("mimetypeviewer", "Choose...", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QCoreApplication::translate("mimetypeviewer", "Default Applications", nullptr));
    } // retranslateUi

};

namespace Ui {
    class mimetypeviewer: public Ui_mimetypeviewer {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MIMETYPEVIEWER_H
