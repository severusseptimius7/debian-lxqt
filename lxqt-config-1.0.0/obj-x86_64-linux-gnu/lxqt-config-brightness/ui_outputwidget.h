/********************************************************************************
** Form generated from reading UI file 'outputwidget.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_OUTPUTWIDGET_H
#define UI_OUTPUTWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSlider>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_OutputWidget
{
public:
    QVBoxLayout *verticalLayout;
    QFrame *frame;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QToolButton *brightnessDownButton;
    QSlider *brightnessSlider;
    QToolButton *brightnessUpButton;

    void setupUi(QWidget *OutputWidget)
    {
        if (OutputWidget->objectName().isEmpty())
            OutputWidget->setObjectName(QString::fromUtf8("OutputWidget"));
        OutputWidget->resize(478, 86);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Minimum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(OutputWidget->sizePolicy().hasHeightForWidth());
        OutputWidget->setSizePolicy(sizePolicy);
        OutputWidget->setWindowTitle(QString::fromUtf8("Form"));
        verticalLayout = new QVBoxLayout(OutputWidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        frame = new QFrame(OutputWidget);
        frame->setObjectName(QString::fromUtf8("frame"));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        frame->setLineWidth(5);
        horizontalLayout = new QHBoxLayout(frame);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label = new QLabel(frame);
        label->setObjectName(QString::fromUtf8("label"));
        label->setTextFormat(Qt::RichText);

        horizontalLayout->addWidget(label);

        brightnessDownButton = new QToolButton(frame);
        brightnessDownButton->setObjectName(QString::fromUtf8("brightnessDownButton"));
        brightnessDownButton->setAutoRaise(true);

        horizontalLayout->addWidget(brightnessDownButton);

        brightnessSlider = new QSlider(frame);
        brightnessSlider->setObjectName(QString::fromUtf8("brightnessSlider"));
        brightnessSlider->setMaximum(200);
        brightnessSlider->setSingleStep(5);
        brightnessSlider->setValue(100);
        brightnessSlider->setOrientation(Qt::Horizontal);
        brightnessSlider->setTickPosition(QSlider::TicksBelow);
        brightnessSlider->setTickInterval(100);

        horizontalLayout->addWidget(brightnessSlider);

        brightnessUpButton = new QToolButton(frame);
        brightnessUpButton->setObjectName(QString::fromUtf8("brightnessUpButton"));
        brightnessUpButton->setAutoRaise(true);

        horizontalLayout->addWidget(brightnessUpButton);


        verticalLayout->addWidget(frame);


        retranslateUi(OutputWidget);

        QMetaObject::connectSlotsByName(OutputWidget);
    } // setupUi

    void retranslateUi(QWidget *OutputWidget)
    {
        label->setText(QCoreApplication::translate("OutputWidget", "<b>Output:</b>", nullptr));
        brightnessDownButton->setText(QCoreApplication::translate("OutputWidget", "\342\230\274", nullptr));
        brightnessUpButton->setText(QCoreApplication::translate("OutputWidget", "\342\230\200", nullptr));
        (void)OutputWidget;
    } // retranslateUi

};

namespace Ui {
    class OutputWidget: public Ui_OutputWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_OUTPUTWIDGET_H
