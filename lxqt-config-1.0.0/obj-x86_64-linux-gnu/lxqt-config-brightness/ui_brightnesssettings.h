/********************************************************************************
** Form generated from reading UI file 'brightnesssettings.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_BRIGHTNESSSETTINGS_H
#define UI_BRIGHTNESSSETTINGS_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSlider>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_BrightnessSettings
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_2;
    QLabel *headIconLabel;
    QLabel *headLabel;
    QGroupBox *backlightGroupBox;
    QHBoxLayout *horizontalLayout;
    QToolButton *backlightDownButton;
    QSlider *backlightSlider;
    QToolButton *backlightUpButton;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout_3;
    QVBoxLayout *layout;
    QCheckBox *confirmCB;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *BrightnessSettings)
    {
        if (BrightnessSettings->objectName().isEmpty())
            BrightnessSettings->setObjectName(QString::fromUtf8("BrightnessSettings"));
        BrightnessSettings->resize(466, 252);
        QIcon icon;
        QString iconThemeName = QString::fromUtf8("system");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon = QIcon::fromTheme(iconThemeName);
        } else {
            icon.addFile(QString::fromUtf8("."), QSize(), QIcon::Normal, QIcon::Off);
        }
        BrightnessSettings->setWindowIcon(icon);
        verticalLayout = new QVBoxLayout(BrightnessSettings);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(10);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        headIconLabel = new QLabel(BrightnessSettings);
        headIconLabel->setObjectName(QString::fromUtf8("headIconLabel"));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(headIconLabel->sizePolicy().hasHeightForWidth());
        headIconLabel->setSizePolicy(sizePolicy);

        horizontalLayout_2->addWidget(headIconLabel);

        headLabel = new QLabel(BrightnessSettings);
        headLabel->setObjectName(QString::fromUtf8("headLabel"));

        horizontalLayout_2->addWidget(headLabel);


        verticalLayout->addLayout(horizontalLayout_2);

        backlightGroupBox = new QGroupBox(BrightnessSettings);
        backlightGroupBox->setObjectName(QString::fromUtf8("backlightGroupBox"));
        horizontalLayout = new QHBoxLayout(backlightGroupBox);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        backlightDownButton = new QToolButton(backlightGroupBox);
        backlightDownButton->setObjectName(QString::fromUtf8("backlightDownButton"));
        backlightDownButton->setAutoRaise(true);

        horizontalLayout->addWidget(backlightDownButton);

        backlightSlider = new QSlider(backlightGroupBox);
        backlightSlider->setObjectName(QString::fromUtf8("backlightSlider"));
        backlightSlider->setOrientation(Qt::Horizontal);

        horizontalLayout->addWidget(backlightSlider);

        backlightUpButton = new QToolButton(backlightGroupBox);
        backlightUpButton->setObjectName(QString::fromUtf8("backlightUpButton"));
        backlightUpButton->setAutoRaise(true);

        horizontalLayout->addWidget(backlightUpButton);


        verticalLayout->addWidget(backlightGroupBox);

        groupBox = new QGroupBox(BrightnessSettings);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        verticalLayout_3 = new QVBoxLayout(groupBox);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(2, -1, 2, -1);
        layout = new QVBoxLayout();
        layout->setObjectName(QString::fromUtf8("layout"));

        verticalLayout_3->addLayout(layout);


        verticalLayout->addWidget(groupBox);

        confirmCB = new QCheckBox(BrightnessSettings);
        confirmCB->setObjectName(QString::fromUtf8("confirmCB"));
        confirmCB->setChecked(true);

        verticalLayout->addWidget(confirmCB);

        buttonBox = new QDialogButtonBox(BrightnessSettings);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Close|QDialogButtonBox::Reset);

        verticalLayout->addWidget(buttonBox);


        retranslateUi(BrightnessSettings);
        QObject::connect(buttonBox, SIGNAL(accepted()), BrightnessSettings, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), BrightnessSettings, SLOT(reject()));

        QMetaObject::connectSlotsByName(BrightnessSettings);
    } // setupUi

    void retranslateUi(QDialog *BrightnessSettings)
    {
        BrightnessSettings->setWindowTitle(QCoreApplication::translate("BrightnessSettings", "Brightness Settings", nullptr));
        headIconLabel->setText(QCoreApplication::translate("BrightnessSettings", "\342\230\200", nullptr));
        headLabel->setText(QCoreApplication::translate("BrightnessSettings", "<html><head/><body><p><span style=\" font-weight:600;\">Backlight and brightness settings:</span></p></body></html>", nullptr));
        backlightGroupBox->setTitle(QCoreApplication::translate("BrightnessSettings", "Backlight", nullptr));
        backlightDownButton->setText(QCoreApplication::translate("BrightnessSettings", "\342\230\274", nullptr));
        backlightUpButton->setText(QCoreApplication::translate("BrightnessSettings", "\342\230\200", nullptr));
        groupBox->setTitle(QCoreApplication::translate("BrightnessSettings", "Brightness", nullptr));
        confirmCB->setText(QCoreApplication::translate("BrightnessSettings", "Require confirmation after settings change", nullptr));
    } // retranslateUi

};

namespace Ui {
    class BrightnessSettings: public Ui_BrightnessSettings {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_BRIGHTNESSSETTINGS_H
