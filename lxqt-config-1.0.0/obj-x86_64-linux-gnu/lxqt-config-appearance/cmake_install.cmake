# Install script for directory: /home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/lxqt-config-appearance

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "RelWithDebInfo")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

# Set default install directory permissions.
if(NOT DEFINED CMAKE_OBJDUMP)
  set(CMAKE_OBJDUMP "/usr/bin/objdump")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xRuntimex" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/share/lxqt/translations/lxqt-config-appearance/lxqt-config-appearance_ar.qm;/usr/share/lxqt/translations/lxqt-config-appearance/lxqt-config-appearance_arn.qm;/usr/share/lxqt/translations/lxqt-config-appearance/lxqt-config-appearance_ast.qm;/usr/share/lxqt/translations/lxqt-config-appearance/lxqt-config-appearance_bg.qm;/usr/share/lxqt/translations/lxqt-config-appearance/lxqt-config-appearance_ca.qm;/usr/share/lxqt/translations/lxqt-config-appearance/lxqt-config-appearance_cs.qm;/usr/share/lxqt/translations/lxqt-config-appearance/lxqt-config-appearance_cy.qm;/usr/share/lxqt/translations/lxqt-config-appearance/lxqt-config-appearance_da.qm;/usr/share/lxqt/translations/lxqt-config-appearance/lxqt-config-appearance_de.qm;/usr/share/lxqt/translations/lxqt-config-appearance/lxqt-config-appearance_el.qm;/usr/share/lxqt/translations/lxqt-config-appearance/lxqt-config-appearance_en_GB.qm;/usr/share/lxqt/translations/lxqt-config-appearance/lxqt-config-appearance_eo.qm;/usr/share/lxqt/translations/lxqt-config-appearance/lxqt-config-appearance_es.qm;/usr/share/lxqt/translations/lxqt-config-appearance/lxqt-config-appearance_es_VE.qm;/usr/share/lxqt/translations/lxqt-config-appearance/lxqt-config-appearance_et.qm;/usr/share/lxqt/translations/lxqt-config-appearance/lxqt-config-appearance_eu.qm;/usr/share/lxqt/translations/lxqt-config-appearance/lxqt-config-appearance_fa.qm;/usr/share/lxqt/translations/lxqt-config-appearance/lxqt-config-appearance_fi.qm;/usr/share/lxqt/translations/lxqt-config-appearance/lxqt-config-appearance_fr.qm;/usr/share/lxqt/translations/lxqt-config-appearance/lxqt-config-appearance_gl.qm;/usr/share/lxqt/translations/lxqt-config-appearance/lxqt-config-appearance_he.qm;/usr/share/lxqt/translations/lxqt-config-appearance/lxqt-config-appearance_hr.qm;/usr/share/lxqt/translations/lxqt-config-appearance/lxqt-config-appearance_hu.qm;/usr/share/lxqt/translations/lxqt-config-appearance/lxqt-config-appearance_ia.qm;/usr/share/lxqt/translations/lxqt-config-appearance/lxqt-config-appearance_id.qm;/usr/share/lxqt/translations/lxqt-config-appearance/lxqt-config-appearance_it.qm;/usr/share/lxqt/translations/lxqt-config-appearance/lxqt-config-appearance_ja.qm;/usr/share/lxqt/translations/lxqt-config-appearance/lxqt-config-appearance_ko.qm;/usr/share/lxqt/translations/lxqt-config-appearance/lxqt-config-appearance_lt.qm;/usr/share/lxqt/translations/lxqt-config-appearance/lxqt-config-appearance_nb_NO.qm;/usr/share/lxqt/translations/lxqt-config-appearance/lxqt-config-appearance_nl.qm;/usr/share/lxqt/translations/lxqt-config-appearance/lxqt-config-appearance_pl.qm;/usr/share/lxqt/translations/lxqt-config-appearance/lxqt-config-appearance_pt.qm;/usr/share/lxqt/translations/lxqt-config-appearance/lxqt-config-appearance_pt_BR.qm;/usr/share/lxqt/translations/lxqt-config-appearance/lxqt-config-appearance_ro_RO.qm;/usr/share/lxqt/translations/lxqt-config-appearance/lxqt-config-appearance_ru.qm;/usr/share/lxqt/translations/lxqt-config-appearance/lxqt-config-appearance_si.qm;/usr/share/lxqt/translations/lxqt-config-appearance/lxqt-config-appearance_sk.qm;/usr/share/lxqt/translations/lxqt-config-appearance/lxqt-config-appearance_sl.qm;/usr/share/lxqt/translations/lxqt-config-appearance/lxqt-config-appearance_sr@latin.qm;/usr/share/lxqt/translations/lxqt-config-appearance/lxqt-config-appearance_sr_BA.qm;/usr/share/lxqt/translations/lxqt-config-appearance/lxqt-config-appearance_sr_RS.qm;/usr/share/lxqt/translations/lxqt-config-appearance/lxqt-config-appearance_th_TH.qm;/usr/share/lxqt/translations/lxqt-config-appearance/lxqt-config-appearance_tr.qm;/usr/share/lxqt/translations/lxqt-config-appearance/lxqt-config-appearance_uk.qm;/usr/share/lxqt/translations/lxqt-config-appearance/lxqt-config-appearance_vi.qm;/usr/share/lxqt/translations/lxqt-config-appearance/lxqt-config-appearance_zh_CN.qm;/usr/share/lxqt/translations/lxqt-config-appearance/lxqt-config-appearance_zh_TW.qm")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/usr/share/lxqt/translations/lxqt-config-appearance" TYPE FILE FILES
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-appearance/lxqt-config-appearance_ar.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-appearance/lxqt-config-appearance_arn.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-appearance/lxqt-config-appearance_ast.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-appearance/lxqt-config-appearance_bg.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-appearance/lxqt-config-appearance_ca.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-appearance/lxqt-config-appearance_cs.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-appearance/lxqt-config-appearance_cy.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-appearance/lxqt-config-appearance_da.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-appearance/lxqt-config-appearance_de.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-appearance/lxqt-config-appearance_el.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-appearance/lxqt-config-appearance_en_GB.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-appearance/lxqt-config-appearance_eo.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-appearance/lxqt-config-appearance_es.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-appearance/lxqt-config-appearance_es_VE.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-appearance/lxqt-config-appearance_et.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-appearance/lxqt-config-appearance_eu.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-appearance/lxqt-config-appearance_fa.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-appearance/lxqt-config-appearance_fi.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-appearance/lxqt-config-appearance_fr.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-appearance/lxqt-config-appearance_gl.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-appearance/lxqt-config-appearance_he.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-appearance/lxqt-config-appearance_hr.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-appearance/lxqt-config-appearance_hu.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-appearance/lxqt-config-appearance_ia.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-appearance/lxqt-config-appearance_id.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-appearance/lxqt-config-appearance_it.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-appearance/lxqt-config-appearance_ja.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-appearance/lxqt-config-appearance_ko.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-appearance/lxqt-config-appearance_lt.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-appearance/lxqt-config-appearance_nb_NO.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-appearance/lxqt-config-appearance_nl.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-appearance/lxqt-config-appearance_pl.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-appearance/lxqt-config-appearance_pt.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-appearance/lxqt-config-appearance_pt_BR.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-appearance/lxqt-config-appearance_ro_RO.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-appearance/lxqt-config-appearance_ru.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-appearance/lxqt-config-appearance_si.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-appearance/lxqt-config-appearance_sk.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-appearance/lxqt-config-appearance_sl.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-appearance/lxqt-config-appearance_sr@latin.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-appearance/lxqt-config-appearance_sr_BA.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-appearance/lxqt-config-appearance_sr_RS.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-appearance/lxqt-config-appearance_th_TH.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-appearance/lxqt-config-appearance_tr.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-appearance/lxqt-config-appearance_uk.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-appearance/lxqt-config-appearance_vi.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-appearance/lxqt-config-appearance_zh_CN.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-appearance/lxqt-config-appearance_zh_TW.qm"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xRuntimex" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/lxqt-config-appearance" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/lxqt-config-appearance")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/lxqt-config-appearance"
         RPATH "/usr/lib/x86_64-linux-gnu/lxqt-config")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-appearance/lxqt-config-appearance")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/lxqt-config-appearance" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/lxqt-config-appearance")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/lxqt-config-appearance"
         OLD_RPATH "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/liblxqt-config-cursor:"
         NEW_RPATH "/usr/lib/x86_64-linux-gnu/lxqt-config")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/lxqt-config-appearance")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xRuntimex" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/applications" TYPE FILE FILES "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/lxqt-config-appearance/lxqt-config-appearance.desktop")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xRuntimex" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/man/man1" TYPE FILE FILES "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/lxqt-config-appearance/man/lxqt-config-appearance.1")
endif()

