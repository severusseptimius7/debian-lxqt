/********************************************************************************
** Form generated from reading UI file 'iconthemeconfig.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ICONTHEMECONFIG_H
#define UI_ICONTHEMECONFIG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QTreeWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_IconThemeConfig
{
public:
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QTreeWidget *iconThemeList;
    QCheckBox *iconFollowColorSchemeCB;

    void setupUi(QWidget *IconThemeConfig)
    {
        if (IconThemeConfig->objectName().isEmpty())
            IconThemeConfig->setObjectName(QString::fromUtf8("IconThemeConfig"));
        IconThemeConfig->resize(450, 327);
        verticalLayout = new QVBoxLayout(IconThemeConfig);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        label = new QLabel(IconThemeConfig);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font;
        font.setBold(true);
        font.setWeight(75);
        label->setFont(font);

        verticalLayout->addWidget(label);

        iconThemeList = new QTreeWidget(IconThemeConfig);
        QTreeWidgetItem *__qtreewidgetitem = new QTreeWidgetItem();
        __qtreewidgetitem->setText(3, QString::fromUtf8("4"));
        __qtreewidgetitem->setText(2, QString::fromUtf8("3"));
        __qtreewidgetitem->setText(1, QString::fromUtf8("2"));
        __qtreewidgetitem->setText(0, QString::fromUtf8("1"));
        iconThemeList->setHeaderItem(__qtreewidgetitem);
        iconThemeList->setObjectName(QString::fromUtf8("iconThemeList"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(iconThemeList->sizePolicy().hasHeightForWidth());
        iconThemeList->setSizePolicy(sizePolicy);
        iconThemeList->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        iconThemeList->setAlternatingRowColors(true);
        iconThemeList->setIconSize(QSize(22, 22));
        iconThemeList->setRootIsDecorated(false);
        iconThemeList->setUniformRowHeights(true);
        iconThemeList->setColumnCount(4);
        iconThemeList->header()->setVisible(false);

        verticalLayout->addWidget(iconThemeList);

        iconFollowColorSchemeCB = new QCheckBox(IconThemeConfig);
        iconFollowColorSchemeCB->setObjectName(QString::fromUtf8("iconFollowColorSchemeCB"));

        verticalLayout->addWidget(iconFollowColorSchemeCB);


        retranslateUi(IconThemeConfig);

        QMetaObject::connectSlotsByName(IconThemeConfig);
    } // setupUi

    void retranslateUi(QWidget *IconThemeConfig)
    {
        IconThemeConfig->setWindowTitle(QCoreApplication::translate("IconThemeConfig", "LXQt Appearance Configuration", nullptr));
        label->setText(QCoreApplication::translate("IconThemeConfig", "Icons Theme", nullptr));
#if QT_CONFIG(tooltip)
        iconFollowColorSchemeCB->setToolTip(QCoreApplication::translate("IconThemeConfig", "The KDE extension of XDG icon themes -> FollowsColorScheme", nullptr));
#endif // QT_CONFIG(tooltip)
        iconFollowColorSchemeCB->setText(QCoreApplication::translate("IconThemeConfig", "Colorize icons based on widget style (palette)", nullptr));
    } // retranslateUi

};

namespace Ui {
    class IconThemeConfig: public Ui_IconThemeConfig {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ICONTHEMECONFIG_H
