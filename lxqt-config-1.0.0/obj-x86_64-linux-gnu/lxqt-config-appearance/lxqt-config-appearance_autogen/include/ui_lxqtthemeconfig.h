/********************************************************************************
** Form generated from reading UI file 'lxqtthemeconfig.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LXQTTHEMECONFIG_H
#define UI_LXQTTHEMECONFIG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QTreeWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_LXQtThemeConfig
{
public:
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QTreeWidget *lxqtThemeList;
    QCheckBox *wallpaperOverride;

    void setupUi(QWidget *LXQtThemeConfig)
    {
        if (LXQtThemeConfig->objectName().isEmpty())
            LXQtThemeConfig->setObjectName(QString::fromUtf8("LXQtThemeConfig"));
        LXQtThemeConfig->resize(400, 300);
        verticalLayout = new QVBoxLayout(LXQtThemeConfig);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        label = new QLabel(LXQtThemeConfig);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font;
        font.setBold(true);
        font.setWeight(75);
        label->setFont(font);

        verticalLayout->addWidget(label);

        lxqtThemeList = new QTreeWidget(LXQtThemeConfig);
        QTreeWidgetItem *__qtreewidgetitem = new QTreeWidgetItem();
        __qtreewidgetitem->setText(0, QString::fromUtf8("1"));
        lxqtThemeList->setHeaderItem(__qtreewidgetitem);
        lxqtThemeList->setObjectName(QString::fromUtf8("lxqtThemeList"));
        lxqtThemeList->setAlternatingRowColors(true);
        lxqtThemeList->setIconSize(QSize(100, 32));
        lxqtThemeList->setRootIsDecorated(false);
        lxqtThemeList->setUniformRowHeights(true);
        lxqtThemeList->header()->setVisible(false);

        verticalLayout->addWidget(lxqtThemeList);

        wallpaperOverride = new QCheckBox(LXQtThemeConfig);
        wallpaperOverride->setObjectName(QString::fromUtf8("wallpaperOverride"));

        verticalLayout->addWidget(wallpaperOverride);


        retranslateUi(LXQtThemeConfig);

        QMetaObject::connectSlotsByName(LXQtThemeConfig);
    } // setupUi

    void retranslateUi(QWidget *LXQtThemeConfig)
    {
        label->setText(QCoreApplication::translate("LXQtThemeConfig", "LXQt Theme", nullptr));
        wallpaperOverride->setText(QCoreApplication::translate("LXQtThemeConfig", "Override user-defined wallpaper", nullptr));
        (void)LXQtThemeConfig;
    } // retranslateUi

};

namespace Ui {
    class LXQtThemeConfig: public Ui_LXQtThemeConfig {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LXQTTHEMECONFIG_H
