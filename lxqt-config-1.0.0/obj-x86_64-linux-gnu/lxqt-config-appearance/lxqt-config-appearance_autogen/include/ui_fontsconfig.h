/********************************************************************************
** Form generated from reading UI file 'fontsconfig.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FONTSCONFIG_H
#define UI_FONTSCONFIG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFontComboBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_FontsConfig
{
public:
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QGroupBox *groupBox;
    QFormLayout *formLayout_3;
    QLabel *label_2;
    QFontComboBox *fontName;
    QLabel *label_6;
    QLabel *label_7;
    QSpinBox *fontSize;
    QComboBox *fontStyle;
    QGroupBox *xftSettings;
    QFormLayout *formLayout_2;
    QCheckBox *antialias;
    QLabel *label_3;
    QComboBox *hintStyle;
    QCheckBox *hinting;
    QLabel *label_5;
    QCheckBox *autohint;
    QSpinBox *dpi;
    QLabel *label_4;
    QComboBox *subpixel;
    QSpacerItem *verticalSpacer;

    void setupUi(QWidget *FontsConfig)
    {
        if (FontsConfig->objectName().isEmpty())
            FontsConfig->setObjectName(QString::fromUtf8("FontsConfig"));
        FontsConfig->resize(421, 379);
        verticalLayout = new QVBoxLayout(FontsConfig);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        label = new QLabel(FontsConfig);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font;
        font.setBold(true);
        font.setWeight(75);
        label->setFont(font);

        verticalLayout->addWidget(label);

        groupBox = new QGroupBox(FontsConfig);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        formLayout_3 = new QFormLayout(groupBox);
        formLayout_3->setObjectName(QString::fromUtf8("formLayout_3"));
        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        formLayout_3->setWidget(0, QFormLayout::LabelRole, label_2);

        fontName = new QFontComboBox(groupBox);
        fontName->setObjectName(QString::fromUtf8("fontName"));

        formLayout_3->setWidget(0, QFormLayout::FieldRole, fontName);

        label_6 = new QLabel(groupBox);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        formLayout_3->setWidget(1, QFormLayout::LabelRole, label_6);

        label_7 = new QLabel(groupBox);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        formLayout_3->setWidget(2, QFormLayout::LabelRole, label_7);

        fontSize = new QSpinBox(groupBox);
        fontSize->setObjectName(QString::fromUtf8("fontSize"));
        fontSize->setMinimum(4);

        formLayout_3->setWidget(2, QFormLayout::FieldRole, fontSize);

        fontStyle = new QComboBox(groupBox);
        fontStyle->addItem(QString());
        fontStyle->addItem(QString());
        fontStyle->addItem(QString());
        fontStyle->addItem(QString());
        fontStyle->setObjectName(QString::fromUtf8("fontStyle"));

        formLayout_3->setWidget(1, QFormLayout::FieldRole, fontStyle);


        verticalLayout->addWidget(groupBox);

        xftSettings = new QGroupBox(FontsConfig);
        xftSettings->setObjectName(QString::fromUtf8("xftSettings"));
        formLayout_2 = new QFormLayout(xftSettings);
        formLayout_2->setObjectName(QString::fromUtf8("formLayout_2"));
        antialias = new QCheckBox(xftSettings);
        antialias->setObjectName(QString::fromUtf8("antialias"));

        formLayout_2->setWidget(0, QFormLayout::SpanningRole, antialias);

        label_3 = new QLabel(xftSettings);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        formLayout_2->setWidget(3, QFormLayout::LabelRole, label_3);

        hintStyle = new QComboBox(xftSettings);
        hintStyle->addItem(QString());
        hintStyle->addItem(QString());
        hintStyle->addItem(QString());
        hintStyle->addItem(QString());
        hintStyle->setObjectName(QString::fromUtf8("hintStyle"));
        hintStyle->setEnabled(false);

        formLayout_2->setWidget(3, QFormLayout::FieldRole, hintStyle);

        hinting = new QCheckBox(xftSettings);
        hinting->setObjectName(QString::fromUtf8("hinting"));

        formLayout_2->setWidget(2, QFormLayout::SpanningRole, hinting);

        label_5 = new QLabel(xftSettings);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        formLayout_2->setWidget(5, QFormLayout::LabelRole, label_5);

        autohint = new QCheckBox(xftSettings);
        autohint->setObjectName(QString::fromUtf8("autohint"));

        formLayout_2->setWidget(6, QFormLayout::SpanningRole, autohint);

        dpi = new QSpinBox(xftSettings);
        dpi->setObjectName(QString::fromUtf8("dpi"));
        dpi->setMinimum(-1);
        dpi->setMaximum(1048576);

        formLayout_2->setWidget(5, QFormLayout::FieldRole, dpi);

        label_4 = new QLabel(xftSettings);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        formLayout_2->setWidget(1, QFormLayout::LabelRole, label_4);

        subpixel = new QComboBox(xftSettings);
        subpixel->addItem(QString());
        subpixel->addItem(QString());
        subpixel->addItem(QString());
        subpixel->addItem(QString());
        subpixel->addItem(QString());
        subpixel->setObjectName(QString::fromUtf8("subpixel"));
        subpixel->setEnabled(false);

        formLayout_2->setWidget(1, QFormLayout::FieldRole, subpixel);


        verticalLayout->addWidget(xftSettings);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);


        retranslateUi(FontsConfig);
        QObject::connect(antialias, SIGNAL(toggled(bool)), subpixel, SLOT(setEnabled(bool)));
        QObject::connect(hinting, SIGNAL(toggled(bool)), hintStyle, SLOT(setEnabled(bool)));

        QMetaObject::connectSlotsByName(FontsConfig);
    } // setupUi

    void retranslateUi(QWidget *FontsConfig)
    {
        label->setText(QCoreApplication::translate("FontsConfig", "Font", nullptr));
        groupBox->setTitle(QCoreApplication::translate("FontsConfig", "Default font for user interface", nullptr));
        label_2->setText(QCoreApplication::translate("FontsConfig", "Font name:", nullptr));
        label_6->setText(QCoreApplication::translate("FontsConfig", "Style:", nullptr));
        label_7->setText(QCoreApplication::translate("FontsConfig", "Point size:", nullptr));
        fontStyle->setItemText(0, QCoreApplication::translate("FontsConfig", "Normal", nullptr));
        fontStyle->setItemText(1, QCoreApplication::translate("FontsConfig", "Bold", nullptr));
        fontStyle->setItemText(2, QCoreApplication::translate("FontsConfig", "Italic", nullptr));
        fontStyle->setItemText(3, QCoreApplication::translate("FontsConfig", "Bold Italic", nullptr));

        xftSettings->setTitle(QCoreApplication::translate("FontsConfig", "The following settings only affect newly started applications", nullptr));
        antialias->setText(QCoreApplication::translate("FontsConfig", "Use antialias fonts", nullptr));
        label_3->setText(QCoreApplication::translate("FontsConfig", "Font hinting style:", nullptr));
        hintStyle->setItemText(0, QCoreApplication::translate("FontsConfig", "None", nullptr));
        hintStyle->setItemText(1, QCoreApplication::translate("FontsConfig", "Slight", nullptr));
        hintStyle->setItemText(2, QCoreApplication::translate("FontsConfig", "Medium", nullptr));
        hintStyle->setItemText(3, QCoreApplication::translate("FontsConfig", "Full", nullptr));

        hinting->setText(QCoreApplication::translate("FontsConfig", "Font hinting", nullptr));
        label_5->setText(QCoreApplication::translate("FontsConfig", "Resolution (DPI):", nullptr));
        autohint->setText(QCoreApplication::translate("FontsConfig", "Autohint", nullptr));
        label_4->setText(QCoreApplication::translate("FontsConfig", "Subpixel antialiasing:", nullptr));
        subpixel->setItemText(0, QCoreApplication::translate("FontsConfig", "None", nullptr));
        subpixel->setItemText(1, QCoreApplication::translate("FontsConfig", "RGB", nullptr));
        subpixel->setItemText(2, QCoreApplication::translate("FontsConfig", "BGR", nullptr));
        subpixel->setItemText(3, QCoreApplication::translate("FontsConfig", "VRGB", nullptr));
        subpixel->setItemText(4, QCoreApplication::translate("FontsConfig", "VBGR", nullptr));

        (void)FontsConfig;
    } // retranslateUi

};

namespace Ui {
    class FontsConfig: public Ui_FontsConfig {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FONTSCONFIG_H
