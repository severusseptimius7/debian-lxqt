/********************************************************************************
** Form generated from reading UI file 'styleconfig.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_STYLECONFIG_H
#define UI_STYLECONFIG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "colorLabel.h"

QT_BEGIN_NAMESPACE

class Ui_StyleConfig
{
public:
    QVBoxLayout *verticalLayout;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents;
    QFormLayout *formLayout;
    QLabel *label;
    QFormLayout *formLayout_3;
    QLabel *label_5;
    QComboBox *qtComboBox;
    QGroupBox *paletteGroupBox;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout;
    QLabel *label_9;
    QHBoxLayout *horizontalLayout_3;
    QFormLayout *formLayout_4;
    QLabel *label_6;
    ColorLabel *winColorLabel;
    QLabel *label_7;
    ColorLabel *baseColorLabel;
    QLabel *label_8;
    ColorLabel *highlightColorLabel;
    QLabel *label_13;
    ColorLabel *linkColorLabel;
    QFormLayout *formLayout_5;
    QLabel *label_10;
    ColorLabel *windowTextColorLabel;
    QLabel *label_11;
    ColorLabel *viewTextColorLabel;
    QLabel *label_12;
    ColorLabel *highlightedTextColorLabel;
    QLabel *label_14;
    ColorLabel *linkVisitedColorLabel;
    QSpacerItem *horizontalSpacer;
    QHBoxLayout *horizontalLayout_2;
    QToolButton *savePaletteBtn;
    QToolButton *loadPaletteBtn;
    QSpacerItem *horizontalSpacer_2;
    QToolButton *defaultPaletteBtn;
    QGroupBox *advancedOptionsGroupBox;
    QVBoxLayout *verticalLayout_3;
    QFormLayout *formLayout_2;
    QLabel *label_4;
    QComboBox *gtk3ComboBox;
    QLabel *label_3;
    QComboBox *gtk2ComboBox;
    QLabel *uniformThemeLabel;
    QLabel *label_2;
    QComboBox *toolButtonStyle;
    QCheckBox *singleClickActivate;

    void setupUi(QWidget *StyleConfig)
    {
        if (StyleConfig->objectName().isEmpty())
            StyleConfig->setObjectName(QString::fromUtf8("StyleConfig"));
        StyleConfig->resize(609, 344);
        verticalLayout = new QVBoxLayout(StyleConfig);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        scrollArea = new QScrollArea(StyleConfig);
        scrollArea->setObjectName(QString::fromUtf8("scrollArea"));
        scrollArea->setFrameShape(QFrame::NoFrame);
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QString::fromUtf8("scrollAreaWidgetContents"));
        formLayout = new QFormLayout(scrollAreaWidgetContents);
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        label = new QLabel(scrollAreaWidgetContents);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font;
        font.setBold(true);
        font.setWeight(75);
        label->setFont(font);

        formLayout->setWidget(0, QFormLayout::LabelRole, label);

        formLayout_3 = new QFormLayout();
        formLayout_3->setObjectName(QString::fromUtf8("formLayout_3"));
        formLayout_3->setHorizontalSpacing(5);
        formLayout_3->setContentsMargins(-1, 10, -1, 10);
        label_5 = new QLabel(scrollAreaWidgetContents);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        formLayout_3->setWidget(0, QFormLayout::LabelRole, label_5);

        qtComboBox = new QComboBox(scrollAreaWidgetContents);
        qtComboBox->setObjectName(QString::fromUtf8("qtComboBox"));

        formLayout_3->setWidget(0, QFormLayout::FieldRole, qtComboBox);


        formLayout->setLayout(1, QFormLayout::SpanningRole, formLayout_3);

        paletteGroupBox = new QGroupBox(scrollAreaWidgetContents);
        paletteGroupBox->setObjectName(QString::fromUtf8("paletteGroupBox"));
        verticalLayout_2 = new QVBoxLayout(paletteGroupBox);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(-1, 5, -1, 10);
        label_9 = new QLabel(paletteGroupBox);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setWordWrap(true);

        horizontalLayout->addWidget(label_9);


        verticalLayout_2->addLayout(horizontalLayout);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(10);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        formLayout_4 = new QFormLayout();
        formLayout_4->setObjectName(QString::fromUtf8("formLayout_4"));
        formLayout_4->setFormAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        formLayout_4->setVerticalSpacing(5);
        label_6 = new QLabel(paletteGroupBox);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        formLayout_4->setWidget(0, QFormLayout::LabelRole, label_6);

        winColorLabel = new ColorLabel(paletteGroupBox);
        winColorLabel->setObjectName(QString::fromUtf8("winColorLabel"));

        formLayout_4->setWidget(0, QFormLayout::FieldRole, winColorLabel);

        label_7 = new QLabel(paletteGroupBox);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        formLayout_4->setWidget(1, QFormLayout::LabelRole, label_7);

        baseColorLabel = new ColorLabel(paletteGroupBox);
        baseColorLabel->setObjectName(QString::fromUtf8("baseColorLabel"));

        formLayout_4->setWidget(1, QFormLayout::FieldRole, baseColorLabel);

        label_8 = new QLabel(paletteGroupBox);
        label_8->setObjectName(QString::fromUtf8("label_8"));

        formLayout_4->setWidget(2, QFormLayout::LabelRole, label_8);

        highlightColorLabel = new ColorLabel(paletteGroupBox);
        highlightColorLabel->setObjectName(QString::fromUtf8("highlightColorLabel"));

        formLayout_4->setWidget(2, QFormLayout::FieldRole, highlightColorLabel);

        label_13 = new QLabel(paletteGroupBox);
        label_13->setObjectName(QString::fromUtf8("label_13"));

        formLayout_4->setWidget(3, QFormLayout::LabelRole, label_13);

        linkColorLabel = new ColorLabel(paletteGroupBox);
        linkColorLabel->setObjectName(QString::fromUtf8("linkColorLabel"));

        formLayout_4->setWidget(3, QFormLayout::FieldRole, linkColorLabel);


        horizontalLayout_3->addLayout(formLayout_4);

        formLayout_5 = new QFormLayout();
        formLayout_5->setObjectName(QString::fromUtf8("formLayout_5"));
        formLayout_5->setFormAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        formLayout_5->setVerticalSpacing(5);
        label_10 = new QLabel(paletteGroupBox);
        label_10->setObjectName(QString::fromUtf8("label_10"));

        formLayout_5->setWidget(0, QFormLayout::LabelRole, label_10);

        windowTextColorLabel = new ColorLabel(paletteGroupBox);
        windowTextColorLabel->setObjectName(QString::fromUtf8("windowTextColorLabel"));

        formLayout_5->setWidget(0, QFormLayout::FieldRole, windowTextColorLabel);

        label_11 = new QLabel(paletteGroupBox);
        label_11->setObjectName(QString::fromUtf8("label_11"));

        formLayout_5->setWidget(1, QFormLayout::LabelRole, label_11);

        viewTextColorLabel = new ColorLabel(paletteGroupBox);
        viewTextColorLabel->setObjectName(QString::fromUtf8("viewTextColorLabel"));

        formLayout_5->setWidget(1, QFormLayout::FieldRole, viewTextColorLabel);

        label_12 = new QLabel(paletteGroupBox);
        label_12->setObjectName(QString::fromUtf8("label_12"));

        formLayout_5->setWidget(2, QFormLayout::LabelRole, label_12);

        highlightedTextColorLabel = new ColorLabel(paletteGroupBox);
        highlightedTextColorLabel->setObjectName(QString::fromUtf8("highlightedTextColorLabel"));

        formLayout_5->setWidget(2, QFormLayout::FieldRole, highlightedTextColorLabel);

        label_14 = new QLabel(paletteGroupBox);
        label_14->setObjectName(QString::fromUtf8("label_14"));

        formLayout_5->setWidget(3, QFormLayout::LabelRole, label_14);

        linkVisitedColorLabel = new ColorLabel(paletteGroupBox);
        linkVisitedColorLabel->setObjectName(QString::fromUtf8("linkVisitedColorLabel"));

        formLayout_5->setWidget(3, QFormLayout::FieldRole, linkVisitedColorLabel);


        horizontalLayout_3->addLayout(formLayout_5);

        horizontalSpacer = new QSpacerItem(5, 5, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer);


        verticalLayout_2->addLayout(horizontalLayout_3);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(-1, 10, -1, -1);
        savePaletteBtn = new QToolButton(paletteGroupBox);
        savePaletteBtn->setObjectName(QString::fromUtf8("savePaletteBtn"));

        horizontalLayout_2->addWidget(savePaletteBtn);

        loadPaletteBtn = new QToolButton(paletteGroupBox);
        loadPaletteBtn->setObjectName(QString::fromUtf8("loadPaletteBtn"));

        horizontalLayout_2->addWidget(loadPaletteBtn);

        horizontalSpacer_2 = new QSpacerItem(5, 5, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);

        defaultPaletteBtn = new QToolButton(paletteGroupBox);
        defaultPaletteBtn->setObjectName(QString::fromUtf8("defaultPaletteBtn"));
        defaultPaletteBtn->setToolButtonStyle(Qt::ToolButtonTextOnly);

        horizontalLayout_2->addWidget(defaultPaletteBtn);


        verticalLayout_2->addLayout(horizontalLayout_2);


        formLayout->setWidget(2, QFormLayout::SpanningRole, paletteGroupBox);

        advancedOptionsGroupBox = new QGroupBox(scrollAreaWidgetContents);
        advancedOptionsGroupBox->setObjectName(QString::fromUtf8("advancedOptionsGroupBox"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(advancedOptionsGroupBox->sizePolicy().hasHeightForWidth());
        advancedOptionsGroupBox->setSizePolicy(sizePolicy);
        advancedOptionsGroupBox->setMaximumSize(QSize(16777215, 16777215));
        advancedOptionsGroupBox->setCheckable(true);
        advancedOptionsGroupBox->setChecked(false);
        verticalLayout_3 = new QVBoxLayout(advancedOptionsGroupBox);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        formLayout_2 = new QFormLayout();
        formLayout_2->setObjectName(QString::fromUtf8("formLayout_2"));
        label_4 = new QLabel(advancedOptionsGroupBox);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        sizePolicy.setHeightForWidth(label_4->sizePolicy().hasHeightForWidth());
        label_4->setSizePolicy(sizePolicy);

        formLayout_2->setWidget(2, QFormLayout::LabelRole, label_4);

        gtk3ComboBox = new QComboBox(advancedOptionsGroupBox);
        gtk3ComboBox->setObjectName(QString::fromUtf8("gtk3ComboBox"));

        formLayout_2->setWidget(2, QFormLayout::FieldRole, gtk3ComboBox);

        label_3 = new QLabel(advancedOptionsGroupBox);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        formLayout_2->setWidget(1, QFormLayout::LabelRole, label_3);

        gtk2ComboBox = new QComboBox(advancedOptionsGroupBox);
        gtk2ComboBox->setObjectName(QString::fromUtf8("gtk2ComboBox"));

        formLayout_2->setWidget(1, QFormLayout::FieldRole, gtk2ComboBox);

        uniformThemeLabel = new QLabel(advancedOptionsGroupBox);
        uniformThemeLabel->setObjectName(QString::fromUtf8("uniformThemeLabel"));
        uniformThemeLabel->setWordWrap(true);

        formLayout_2->setWidget(3, QFormLayout::FieldRole, uniformThemeLabel);


        verticalLayout_3->addLayout(formLayout_2);


        formLayout->setWidget(3, QFormLayout::SpanningRole, advancedOptionsGroupBox);

        label_2 = new QLabel(scrollAreaWidgetContents);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        formLayout->setWidget(4, QFormLayout::LabelRole, label_2);

        toolButtonStyle = new QComboBox(scrollAreaWidgetContents);
        toolButtonStyle->addItem(QString());
        toolButtonStyle->addItem(QString());
        toolButtonStyle->addItem(QString());
        toolButtonStyle->addItem(QString());
        toolButtonStyle->addItem(QString());
        toolButtonStyle->setObjectName(QString::fromUtf8("toolButtonStyle"));

        formLayout->setWidget(4, QFormLayout::FieldRole, toolButtonStyle);

        singleClickActivate = new QCheckBox(scrollAreaWidgetContents);
        singleClickActivate->setObjectName(QString::fromUtf8("singleClickActivate"));

        formLayout->setWidget(5, QFormLayout::SpanningRole, singleClickActivate);

        scrollArea->setWidget(scrollAreaWidgetContents);

        verticalLayout->addWidget(scrollArea);


        retranslateUi(StyleConfig);

        QMetaObject::connectSlotsByName(StyleConfig);
    } // setupUi

    void retranslateUi(QWidget *StyleConfig)
    {
        label->setText(QCoreApplication::translate("StyleConfig", "Widget Style", nullptr));
        label_5->setText(QCoreApplication::translate("StyleConfig", "Qt Style", nullptr));
        paletteGroupBox->setTitle(QCoreApplication::translate("StyleConfig", "Qt Palette", nullptr));
        label_9->setText(QCoreApplication::translate("StyleConfig", "Some Qt styles may ignore these colors.", nullptr));
        label_6->setText(QCoreApplication::translate("StyleConfig", "Window:", nullptr));
        label_7->setText(QCoreApplication::translate("StyleConfig", "View:", nullptr));
        label_8->setText(QCoreApplication::translate("StyleConfig", "Selection:", nullptr));
        label_13->setText(QCoreApplication::translate("StyleConfig", "Link:", nullptr));
        label_10->setText(QCoreApplication::translate("StyleConfig", "Window Text:", nullptr));
        label_11->setText(QCoreApplication::translate("StyleConfig", "View Text:", nullptr));
        label_12->setText(QCoreApplication::translate("StyleConfig", "Selected Text:", nullptr));
        label_14->setText(QCoreApplication::translate("StyleConfig", "Visited Link:", nullptr));
        savePaletteBtn->setText(QCoreApplication::translate("StyleConfig", "&Save Palette", nullptr));
        loadPaletteBtn->setText(QCoreApplication::translate("StyleConfig", "&Load Palette", nullptr));
        defaultPaletteBtn->setText(QCoreApplication::translate("StyleConfig", "&Default Palette", nullptr));
        advancedOptionsGroupBox->setTitle(QCoreApplication::translate("StyleConfig", "Set GTK themes (GTK configuration files will be overwritten!)", nullptr));
        label_4->setText(QCoreApplication::translate("StyleConfig", "GTK 3 Theme", nullptr));
        label_3->setText(QCoreApplication::translate("StyleConfig", "GTK 2 Theme", nullptr));
        uniformThemeLabel->setText(QCoreApplication::translate("StyleConfig", "To attempt uniform theming, either select similar style/theme (if available) across all lists, or select 'gtk2' Qt style (if available) to mimic GTK themes.\n"
"\n"
"Make sure 'xsettingsd' is installed to help GTK applications apply themes on the fly.", nullptr));
        label_2->setText(QCoreApplication::translate("StyleConfig", "Toolbar button style:", nullptr));
        toolButtonStyle->setItemText(0, QCoreApplication::translate("StyleConfig", "Only display the icon", nullptr));
        toolButtonStyle->setItemText(1, QCoreApplication::translate("StyleConfig", "Only display the text", nullptr));
        toolButtonStyle->setItemText(2, QCoreApplication::translate("StyleConfig", "The text appears beside the icon", nullptr));
        toolButtonStyle->setItemText(3, QCoreApplication::translate("StyleConfig", "The text appears under the icon", nullptr));
        toolButtonStyle->setItemText(4, QCoreApplication::translate("StyleConfig", "Default", nullptr));

        singleClickActivate->setText(QCoreApplication::translate("StyleConfig", "Activate item on single click", nullptr));
        (void)StyleConfig;
    } // retranslateUi

};

namespace Ui {
    class StyleConfig: public Ui_StyleConfig {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_STYLECONFIG_H
