# Install script for directory: /home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/src

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "RelWithDebInfo")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

# Set default install directory permissions.
if(NOT DEFINED CMAKE_OBJDUMP)
  set(CMAKE_OBJDUMP "/usr/bin/objdump")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xRuntimex" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/share/lxqt/translations/lxqt-config/lxqt-config_ar.qm;/usr/share/lxqt/translations/lxqt-config/lxqt-config_arn.qm;/usr/share/lxqt/translations/lxqt-config/lxqt-config_ast.qm;/usr/share/lxqt/translations/lxqt-config/lxqt-config_bg.qm;/usr/share/lxqt/translations/lxqt-config/lxqt-config_ca.qm;/usr/share/lxqt/translations/lxqt-config/lxqt-config_cs.qm;/usr/share/lxqt/translations/lxqt-config/lxqt-config_cy.qm;/usr/share/lxqt/translations/lxqt-config/lxqt-config_da.qm;/usr/share/lxqt/translations/lxqt-config/lxqt-config_de.qm;/usr/share/lxqt/translations/lxqt-config/lxqt-config_el.qm;/usr/share/lxqt/translations/lxqt-config/lxqt-config_en_GB.qm;/usr/share/lxqt/translations/lxqt-config/lxqt-config_es.qm;/usr/share/lxqt/translations/lxqt-config/lxqt-config_es_VE.qm;/usr/share/lxqt/translations/lxqt-config/lxqt-config_et.qm;/usr/share/lxqt/translations/lxqt-config/lxqt-config_eu.qm;/usr/share/lxqt/translations/lxqt-config/lxqt-config_fa.qm;/usr/share/lxqt/translations/lxqt-config/lxqt-config_fi.qm;/usr/share/lxqt/translations/lxqt-config/lxqt-config_fr.qm;/usr/share/lxqt/translations/lxqt-config/lxqt-config_gl.qm;/usr/share/lxqt/translations/lxqt-config/lxqt-config_he.qm;/usr/share/lxqt/translations/lxqt-config/lxqt-config_hr.qm;/usr/share/lxqt/translations/lxqt-config/lxqt-config_hu.qm;/usr/share/lxqt/translations/lxqt-config/lxqt-config_id.qm;/usr/share/lxqt/translations/lxqt-config/lxqt-config_it.qm;/usr/share/lxqt/translations/lxqt-config/lxqt-config_ja.qm;/usr/share/lxqt/translations/lxqt-config/lxqt-config_ko.qm;/usr/share/lxqt/translations/lxqt-config/lxqt-config_lt.qm;/usr/share/lxqt/translations/lxqt-config/lxqt-config_lv.qm;/usr/share/lxqt/translations/lxqt-config/lxqt-config_nb_NO.qm;/usr/share/lxqt/translations/lxqt-config/lxqt-config_nl.qm;/usr/share/lxqt/translations/lxqt-config/lxqt-config_oc.qm;/usr/share/lxqt/translations/lxqt-config/lxqt-config_pl.qm;/usr/share/lxqt/translations/lxqt-config/lxqt-config_pt.qm;/usr/share/lxqt/translations/lxqt-config/lxqt-config_pt_BR.qm;/usr/share/lxqt/translations/lxqt-config/lxqt-config_ro_RO.qm;/usr/share/lxqt/translations/lxqt-config/lxqt-config_ru.qm;/usr/share/lxqt/translations/lxqt-config/lxqt-config_si.qm;/usr/share/lxqt/translations/lxqt-config/lxqt-config_sk_SK.qm;/usr/share/lxqt/translations/lxqt-config/lxqt-config_th_TH.qm;/usr/share/lxqt/translations/lxqt-config/lxqt-config_tr.qm;/usr/share/lxqt/translations/lxqt-config/lxqt-config_uk.qm;/usr/share/lxqt/translations/lxqt-config/lxqt-config_vi.qm;/usr/share/lxqt/translations/lxqt-config/lxqt-config_zh_CN.qm;/usr/share/lxqt/translations/lxqt-config/lxqt-config_zh_TW.qm")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/usr/share/lxqt/translations/lxqt-config" TYPE FILE FILES
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/src/lxqt-config_ar.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/src/lxqt-config_arn.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/src/lxqt-config_ast.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/src/lxqt-config_bg.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/src/lxqt-config_ca.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/src/lxqt-config_cs.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/src/lxqt-config_cy.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/src/lxqt-config_da.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/src/lxqt-config_de.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/src/lxqt-config_el.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/src/lxqt-config_en_GB.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/src/lxqt-config_es.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/src/lxqt-config_es_VE.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/src/lxqt-config_et.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/src/lxqt-config_eu.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/src/lxqt-config_fa.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/src/lxqt-config_fi.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/src/lxqt-config_fr.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/src/lxqt-config_gl.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/src/lxqt-config_he.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/src/lxqt-config_hr.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/src/lxqt-config_hu.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/src/lxqt-config_id.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/src/lxqt-config_it.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/src/lxqt-config_ja.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/src/lxqt-config_ko.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/src/lxqt-config_lt.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/src/lxqt-config_lv.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/src/lxqt-config_nb_NO.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/src/lxqt-config_nl.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/src/lxqt-config_oc.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/src/lxqt-config_pl.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/src/lxqt-config_pt.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/src/lxqt-config_pt_BR.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/src/lxqt-config_ro_RO.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/src/lxqt-config_ru.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/src/lxqt-config_si.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/src/lxqt-config_sk_SK.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/src/lxqt-config_th_TH.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/src/lxqt-config_tr.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/src/lxqt-config_uk.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/src/lxqt-config_vi.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/src/lxqt-config_zh_CN.qm"
    "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/src/lxqt-config_zh_TW.qm"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xRuntimex" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/lxqt-config" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/lxqt-config")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/lxqt-config"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/src/lxqt-config")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/lxqt-config" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/lxqt-config")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/lxqt-config")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xRuntimex" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/applications" TYPE FILE FILES "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/src/lxqt-config.desktop")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xRuntimex" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/etc/xdg/menus/lxqt-config.menu")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/etc/xdg/menus" TYPE FILE FILES "/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/src/lxqt-config.menu")
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/src/qcategorizedview/cmake_install.cmake")
  include("/home/debian/lxqt/lxqt-config/lxqt-config-1.0.0/obj-x86_64-linux-gnu/src/menuname/cmake_install.cmake")

endif()

