<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../about.ui" line="14"/>
        <source>About</source>
        <translation>À propos</translation>
    </message>
    <message>
        <location filename="../about.ui" line="48"/>
        <source>Lightweight file manager</source>
        <translation>Gestionnaire de fichiers léger</translation>
    </message>
    <message>
        <location filename="../about.ui" line="91"/>
        <source>Programming:
* Hong Jen Yee (PCMan) &lt;pcman.tw@gmail.com&gt;
</source>
        <translation>Programmation :
* Hong Jen Yee (PCMan) &lt;pcman.tw@gmail.com&gt;
</translation>
    </message>
    <message>
        <location filename="../about.ui" line="82"/>
        <source>Authors</source>
        <translation>Auteurs</translation>
    </message>
    <message>
        <location filename="../about.ui" line="25"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:16pt; font-weight:600;&quot;&gt;PCManFM-Qt&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot;font-size:16pt; font-weight:600;&quot;&gt;PCManFM-Qt&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../about.ui" line="58"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;https://github.com/lxqt/pcmanfm-qt&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;https://github.com/lxqt/pcmanfm-qt&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;
</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;https://github.com/lxqt/pcmanfm-qt&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;https://github.com/lxqt/pcmanfm-qt&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;
</translation>
    </message>
    <message>
        <location filename="../about.ui" line="104"/>
        <source>License</source>
        <translation>Licence</translation>
    </message>
    <message>
        <location filename="../about.ui" line="113"/>
        <source>PCManFM-Qt File Manager

Copyright (C) 2009 - 2014 洪任諭 (Hong Jen Yee)

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.</source>
        <translation>PCManFM-Qt Gestionnaire de fichiers

Copyright (C) 2009 - 2014 洪任諭 (Hong Jen Yee)

Ce programme est un logiciel libre ; vous pouvez le redistribuer et/ou
le modifier selon les termes de la licence publique générale GNU
tel que publié par la Free Software Foundation ; soit la version 2
de la licence, ou (à votre choix) toute version ultérieure.

Ce programme est distribué dans l’espoir qu’il sera utile,
mais SANS AUCUNE GARANTIE ; sans même la garantie implicite de
QUALITÉ MARCHANDE ou ADAPTATION À UN USAGE PARTICULIER. Voir le
Licence publique générale GNU pour plus de détails.

Vous devriez avoir reçu une copie de la licence publique générale GNU
avec ce programme ; sinon écriver à la Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.</translation>
    </message>
</context>
<context>
    <name>AutoRunDialog</name>
    <message>
        <location filename="../autorun.ui" line="14"/>
        <source>Removable medium is inserted</source>
        <translation>Un média amovible a été inséré</translation>
    </message>
    <message>
        <location filename="../autorun.ui" line="33"/>
        <source>&lt;b&gt;Removable medium is inserted&lt;/b&gt;</source>
        <translation>&lt;b&gt;Un média amovible a été inséré&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../autorun.ui" line="40"/>
        <source>Type of medium:</source>
        <translation>Type de média :</translation>
    </message>
    <message>
        <location filename="../autorun.ui" line="47"/>
        <source>Detecting...</source>
        <translation>Détection en cours…</translation>
    </message>
    <message>
        <location filename="../autorun.ui" line="56"/>
        <source>Please select the action you want to perform:</source>
        <translation>Veuillez sélectionner l&apos;action que vous souhaitez effectuer :</translation>
    </message>
</context>
<context>
    <name>BulkRenameDialog</name>
    <message>
        <location filename="../bulk-rename.ui" line="6"/>
        <source>Bulk Rename</source>
        <translation>Renommer en masse</translation>
    </message>
    <message>
        <location filename="../bulk-rename.ui" line="48"/>
        <source># will be replaced by numbers starting with:</source>
        <translation># sera remplacé par un numéro commençant par :</translation>
    </message>
    <message>
        <location filename="../bulk-rename.ui" line="71"/>
        <source>Rename selected files to:</source>
        <translation>Renommer les fichiers sélectionnés en :</translation>
    </message>
    <message>
        <location filename="../bulk-rename.ui" line="84"/>
        <source>Name#</source>
        <translation>Nom#</translation>
    </message>
</context>
<context>
    <name>ConnectServerDialog</name>
    <message>
        <location filename="../connect.ui" line="14"/>
        <source>Connect to remote server</source>
        <translation>Se connecter au serveur distant</translation>
    </message>
    <message>
        <location filename="../connect.ui" line="23"/>
        <source>Anonymous &amp;login</source>
        <translation>Connexion &amp;anonyme</translation>
    </message>
    <message>
        <location filename="../connect.ui" line="36"/>
        <source>Login as &amp;user:</source>
        <translation>Connectez-vous en tant qu&apos;utilisateur :</translation>
    </message>
    <message>
        <location filename="../connect.ui" line="65"/>
        <source>Specify remote folder to connect</source>
        <translation>Spécifier le répertoire distant à connecter</translation>
    </message>
    <message>
        <location filename="../connect.ui" line="72"/>
        <source>Type:</source>
        <translation>Type :</translation>
    </message>
    <message>
        <location filename="../connect.ui" line="79"/>
        <source>Port:</source>
        <translation>Port :</translation>
    </message>
    <message>
        <location filename="../connect.ui" line="86"/>
        <source>Path:</source>
        <translation>Chemin :</translation>
    </message>
    <message>
        <location filename="../connect.ui" line="96"/>
        <source>Host:</source>
        <translation>Hôte :</translation>
    </message>
</context>
<context>
    <name>DesktopEntryDialog</name>
    <message>
        <location filename="../desktopentrydialog.ui" line="14"/>
        <source>Create Launcher</source>
        <translation>Créer un lanceur</translation>
    </message>
    <message>
        <location filename="../desktopentrydialog.ui" line="38"/>
        <source>Name:</source>
        <translation>Nom :</translation>
    </message>
    <message>
        <location filename="../desktopentrydialog.ui" line="55"/>
        <source>Description:</source>
        <translation>Description :</translation>
    </message>
    <message>
        <location filename="../desktopentrydialog.ui" line="69"/>
        <source>Comment:</source>
        <translation>Commentaire :</translation>
    </message>
    <message>
        <location filename="../desktopentrydialog.ui" line="83"/>
        <source>Command:</source>
        <translation>Commande :</translation>
    </message>
    <message>
        <location filename="../desktopentrydialog.ui" line="99"/>
        <location filename="../desktopentrydialog.ui" line="127"/>
        <source>...</source>
        <translation>. . .</translation>
    </message>
    <message>
        <location filename="../desktopentrydialog.ui" line="108"/>
        <source>Icon:</source>
        <translation>Icône :</translation>
    </message>
    <message>
        <location filename="../desktopentrydialog.ui" line="136"/>
        <location filename="../desktopentrydialog.ui" line="146"/>
        <source>Run in terminal?</source>
        <translation>Ouvrir dans le terminal ?</translation>
    </message>
    <message>
        <location filename="../desktopentrydialog.ui" line="139"/>
        <source>Terminal:</source>
        <translation>Terminal :</translation>
    </message>
    <message>
        <location filename="../desktopentrydialog.ui" line="150"/>
        <source>No</source>
        <translation>Non</translation>
    </message>
    <message>
        <location filename="../desktopentrydialog.ui" line="155"/>
        <source>Yes</source>
        <translation>Oui</translation>
    </message>
    <message>
        <location filename="../desktopentrydialog.ui" line="163"/>
        <source>Type:</source>
        <translation>Type :</translation>
    </message>
    <message>
        <location filename="../desktopentrydialog.ui" line="171"/>
        <source>Application</source>
        <translation>Application(s)</translation>
    </message>
    <message>
        <location filename="../desktopentrydialog.ui" line="176"/>
        <source>Link</source>
        <translation>Lien</translation>
    </message>
</context>
<context>
    <name>DesktopFolder</name>
    <message>
        <location filename="../desktop-folder.ui" line="14"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <location filename="../desktop-folder.ui" line="23"/>
        <source>Desktop</source>
        <translation>Bureau</translation>
    </message>
    <message>
        <location filename="../desktop-folder.ui" line="29"/>
        <source>Desktop folder:</source>
        <translation>Répertoire du bureau :</translation>
    </message>
    <message>
        <location filename="../desktop-folder.ui" line="36"/>
        <source>Image file</source>
        <translation>Fichier image</translation>
    </message>
    <message>
        <location filename="../desktop-folder.ui" line="42"/>
        <source>Folder path</source>
        <translation>Chemin du répertoire</translation>
    </message>
    <message>
        <location filename="../desktop-folder.ui" line="49"/>
        <source>&amp;Browse</source>
        <translation>&amp;Parcourir</translation>
    </message>
</context>
<context>
    <name>DesktopPreferencesDialog</name>
    <message>
        <location filename="../desktop-preferences.ui" line="14"/>
        <source>Desktop Preferences</source>
        <translation>Préférences du bureau</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="247"/>
        <location filename="../desktop-preferences.ui" line="253"/>
        <source>Background</source>
        <translation>Arrière-plan</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="294"/>
        <source>Wallpaper mode:</source>
        <translation>Mode du papier-peint :</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="317"/>
        <source>Wallpaper image file:</source>
        <translation>Fichier image du papier peint :</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="259"/>
        <source>Select background color:</source>
        <translation>Sélection de la couleur de l&apos;arrière-plan :</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="326"/>
        <source>Image file</source>
        <translation>Fichier image</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="332"/>
        <source>Image file path</source>
        <translation>Chemin du fichier image</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="339"/>
        <source>&amp;Browse</source>
        <translation>&amp;Parcourir</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="30"/>
        <source>Icons</source>
        <translation>Icônes</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="36"/>
        <source>Icon size:</source>
        <translation>Taille de l&apos;icône :</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="49"/>
        <source>Label Text</source>
        <translation>Texte de l&apos;étiquette</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="113"/>
        <source>Select shadow color:</source>
        <translation>Sélection de la couleur de l&apos;ombre :</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="61"/>
        <source>Select font:</source>
        <translation>Sélection de la police :</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="24"/>
        <source>General</source>
        <translation>Général</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="87"/>
        <source>Select text color:</source>
        <translation>Sélection de la couleur du texte :</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="136"/>
        <source>Spacing</source>
        <translation>Espacement</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="142"/>
        <source>Minimum item margins:</source>
        <translation>Marges minimales entre les éléments :</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="149"/>
        <source>3 px by default.</source>
        <translation>3 px par défaut.</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="152"/>
        <location filename="../desktop-preferences.ui" line="176"/>
        <source> px</source>
        <translation> px</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="165"/>
        <source>x</source>
        <translation>x</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="172"/>
        <source>1 px by default.
A space is also reserved for 3 lines of text.</source>
        <translation>1 px par défaut
Un espace est également réservé pour 3 lignes de texte.</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="189"/>
        <source>Lock</source>
        <translation>Verrouiller</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="212"/>
        <source>By default, desktop folders will be opened in PCManFM-Qt if they
are left clicked, even when it is not the default file manager.</source>
        <translation>Par défaut, les dossiers de bureau s&apos;ouvrent dans PCManFM-Qt 
par un clic gauche, même si aucun gestionnaire de fichiers n&apos;est défini par défaut.</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="216"/>
        <source>Open desktop folders in default file manager by left clicking</source>
        <translation>Ouvrir le dossier du bureau dans le gestionnaire de fichier par défaut avec un clic gauche</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="223"/>
        <source>Make all items stick to their positions</source>
        <translation>Faire en sorte que tous les éléments restent à leur position</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="282"/>
        <source>Wallpaper</source>
        <translation>Fond d&apos;écran</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="348"/>
        <source>Transform image based on EXIF data</source>
        <translation>Transformer l&apos;image en se basant sur les données EXIF</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="355"/>
        <source>Individual wallpaper for each monitor</source>
        <translation>Fond d&apos;écran individuel pour chaque écran</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="382"/>
        <source>Slide Show</source>
        <translation>Diaporama</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="388"/>
        <source>Enable Slide Show</source>
        <translation>Activer le diaporama</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="400"/>
        <source>Wallpaper image folder:</source>
        <translation>Répertoire des fonds d&apos;écran :</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="407"/>
        <source>Browse</source>
        <translation>Parcourir</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="414"/>
        <source> hour(s)</source>
        <translation> heure(s)</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="424"/>
        <source>and</source>
        <translation>et</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="437"/>
        <source>Intervals less than 5min will be ignored</source>
        <translation>Les intervalles inférieurs à 5 minutes seront ignorés</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="440"/>
        <source>Interval:</source>
        <translation>Intervalle :</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="447"/>
        <source> minute(s)</source>
        <translation> minute(s)</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="473"/>
        <source>Wallpaper folder</source>
        <translation>Répertoire des fonds d&apos;écran</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="496"/>
        <source>Randomize the slide show</source>
        <translation>Défilement aléatoire des diapositives</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="529"/>
        <source>Visible Shortcuts</source>
        <translation>Raccourcis visibles</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="535"/>
        <source>Home</source>
        <translation>Accueil</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="546"/>
        <source>Trash</source>
        <translation>Corbeille</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="557"/>
        <source>Computer</source>
        <translation>Ordinateur</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="568"/>
        <source>Network</source>
        <translation>Réseau</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="523"/>
        <source>Advanced</source>
        <translation>Avancé</translation>
    </message>
</context>
<context>
    <name>HiddenShortcutsDialog</name>
    <message>
        <location filename="../shortcuts.ui" line="14"/>
        <source>Hidden Shortcuts</source>
        <translation>Raccourcis cachés</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="21"/>
        <source>Shortcut</source>
        <translation>Raccourci</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="26"/>
        <source>Action</source>
        <translation>Action</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="31"/>
        <source>Esc</source>
        <translation>Échap</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="34"/>
        <source>Focus view, clear filter bar</source>
        <translation>Vue concentrée, nettoyer la bar de filtres</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="39"/>
        <source>Ctrl+Esc</source>
        <translation>Ctrl+Échap</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="42"/>
        <source>Focus side-pane</source>
        <translation>Mise au point sur le volet latéral</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="47"/>
        <source>Ctrl+L</source>
        <translation>Ctrl+L</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="50"/>
        <location filename="../shortcuts.ui" line="58"/>
        <source>Focus path entry</source>
        <translation>Focus sur l&apos;entrée du chemin</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="55"/>
        <source>Alt+D</source>
        <translation>Alt+D</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="63"/>
        <source>Ctrl+Tab</source>
        <translation>Ctrl+Tab</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="66"/>
        <location filename="../shortcuts.ui" line="82"/>
        <source>Next tab</source>
        <translation>Onglet suivant</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="71"/>
        <source>Ctrl+Shift+Tab</source>
        <translation>Ctrl+Maj+Tab</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="74"/>
        <location filename="../shortcuts.ui" line="90"/>
        <source>Previous tab</source>
        <translation>Onglet précédent</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="79"/>
        <source>Ctrl+PageDown</source>
        <translation>Ctrl+↓</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="87"/>
        <source>Ctrl+PageUp</source>
        <translation>Ctrl+↑</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="95"/>
        <source>Ctrl+Number</source>
        <translation>Ctrl+Num</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="98"/>
        <location filename="../shortcuts.ui" line="106"/>
        <source>Jump to tab</source>
        <translation>Aller à l&apos;onglet</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="103"/>
        <source>Alt+Number</source>
        <translation>Alt+Num</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="111"/>
        <source>Backspace</source>
        <translation>Retour arrière</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="114"/>
        <source>Go up</source>
        <translation>Monter</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="119"/>
        <source>Ctrl+F</source>
        <translation>Ctrl+F</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="122"/>
        <source>Search dialog</source>
        <translation>Boîte de dialogue de recherche</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="127"/>
        <source>Shift+Insert</source>
        <translation>Maj+Inser</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="130"/>
        <source>Paste into transient filter bar</source>
        <translation>Coller dans la barre de filtre temporaire</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="135"/>
        <source>Drag+Shift</source>
        <translation>Glisser+Maj</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="138"/>
        <source>Move file on dropping</source>
        <translation>Déplacer le fichier lors du dépôt</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="143"/>
        <source>Drag+Ctrl</source>
        <translation>Ctrl+Glisser</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="146"/>
        <source>Copy file on dropping</source>
        <translation>Copier le fichier lors du dépôt</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="151"/>
        <source>Drag+Shift+Ctrl</source>
        <translation>Maj+Ctrl+Glisser</translation>
    </message>
    <message>
        <location filename="../shortcuts.ui" line="154"/>
        <source>Make a symlink on dropping</source>
        <translation>Créer un lien symbolique lors de la suppression</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../main-win.ui" line="14"/>
        <source>File Manager</source>
        <translation>Gestionnaire de fichiers</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="283"/>
        <source>Go Up</source>
        <translation>Monter</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="286"/>
        <source>Alt+Up</source>
        <translation>Alt+Haut</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="298"/>
        <source>Alt+Home</source>
        <translation>Alt+Accueil (début)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="414"/>
        <source>Reload</source>
        <translation>Recharger</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="310"/>
        <source>F5</source>
        <translation>F5</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="133"/>
        <source>&amp;Toolbars</source>
        <translation>Barre d&apos;ou&amp;tils</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="140"/>
        <source>Path &amp;Bar</source>
        <translation>Barre des &amp;chemins</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="147"/>
        <source>&amp;Filtering</source>
        <translation>&amp;Filtrer</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="221"/>
        <source>&amp;Tools</source>
        <translation>&amp;Outils</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="295"/>
        <source>&amp;Home</source>
        <translation>&amp;Accueil</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="307"/>
        <source>&amp;Reload</source>
        <translation>&amp;Recharger</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="319"/>
        <source>Go</source>
        <translation>Aller à</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="328"/>
        <source>Quit</source>
        <translation>Quitter</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="337"/>
        <source>&amp;About</source>
        <translation>&amp;A propos</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="349"/>
        <source>New Window</source>
        <translation>Nouvelle fenêtre</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="352"/>
        <source>Ctrl+N</source>
        <translation>Ctrl+N</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="360"/>
        <source>Show &amp;Hidden</source>
        <translation>Afficher les éléments cac&amp;hés</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="363"/>
        <source>Ctrl+H</source>
        <translation>Ctrl+H</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="372"/>
        <source>&amp;Computer</source>
        <translation>&amp;Ordinateur</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="381"/>
        <source>&amp;Trash</source>
        <translation>&amp;Corbeille</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="386"/>
        <source>&amp;Network</source>
        <translation>&amp;Réseau</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="395"/>
        <source>&amp;Desktop</source>
        <translation>&amp;Bureau</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="404"/>
        <source>&amp;Add to Bookmarks</source>
        <translation>&amp;Ajouter aux signets</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="409"/>
        <source>&amp;Applications</source>
        <translation>&amp;Logiciels</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="458"/>
        <source>Ctrl+X</source>
        <translation>Ctrl+X</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="467"/>
        <source>&amp;Copy</source>
        <translation>&amp;Copier</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="470"/>
        <source>Ctrl+C</source>
        <translation>Ctrl+C</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="479"/>
        <source>&amp;Paste</source>
        <translation>C&amp;oller</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="482"/>
        <source>Ctrl+V</source>
        <translation>Ctrl+V</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="487"/>
        <source>Select &amp;All</source>
        <translation>Tout sélect&amp;ionner</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="495"/>
        <source>Pr&amp;eferences</source>
        <translation>Préfér&amp;ences</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="503"/>
        <source>&amp;Ascending</source>
        <translation>&amp;Croissant</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="511"/>
        <source>&amp;Descending</source>
        <translation>&amp;Décroissant</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="519"/>
        <source>&amp;By File Name</source>
        <translation>&amp;Par nom de fichier</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="527"/>
        <source>By &amp;Modification Time</source>
        <translation>Par date de &amp;modification</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="535"/>
        <source>By C&amp;reation Time</source>
        <translation>Par date de cr&amp;éation</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="543"/>
        <source>By Deletio&amp;n Time</source>
        <translation>Par date de suppressio&amp;n</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="551"/>
        <source>By File &amp;Type</source>
        <translation>Par &amp;type de fichier</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="559"/>
        <source>By &amp;Owner</source>
        <translation>Par &amp;Propriétaire</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="567"/>
        <source>By &amp;Group</source>
        <translation>Par &amp;groupe</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="575"/>
        <source>&amp;Folder First</source>
        <translation>&amp;Répertoires d&apos;abord</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="583"/>
        <source>&amp;Hidden Last</source>
        <translation>&amp;Cachés en dernier</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="948"/>
        <source>Preserve Settings Recursively from &amp;Here</source>
        <translation>Conserver les paramètres de manière récursive à partir d&apos;&amp;Ici</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="953"/>
        <source>&amp;Go to Source of Inherited Settings</source>
        <translation>&amp;Aller à la source des paramètres hérités</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="958"/>
        <source>&amp;Remove Settings of Nonexistent Folders</source>
        <translation>&amp;Supprimer les paramètres des dossiers inexistants</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="633"/>
        <source>&amp;Invert Selection</source>
        <translation>&amp;Inverser la sélection</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="636"/>
        <source>Ctrl+Shift+A</source>
        <translation>Ctrl+Shift+A</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="645"/>
        <source>&amp;Delete</source>
        <translation>&amp;Supprimer</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="653"/>
        <source>&amp;Rename</source>
        <translation>&amp;Renommer</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="685"/>
        <source>&amp;Case Sensitive</source>
        <translation>Sensible à la &amp;casse</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="693"/>
        <source>By File &amp;Size</source>
        <translation>Par &amp;taille de fichier</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="698"/>
        <source>&amp;Close Window</source>
        <translation>&amp;Fermer la fenêtre</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="719"/>
        <source>Open Tab in &amp;Root Instance</source>
        <translation>Ouvrir l&apos;onglet en tant que &amp;Root</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="832"/>
        <source>Preserve Settings for &amp;This Folder</source>
        <translation>Conserver les paramètres de ce &amp;Dossier</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="869"/>
        <source>&amp;Show/Focus Filter Bar</source>
        <translation>&amp;Montrer/Focus sur la barre de filtre</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="872"/>
        <source>Show Filter Bar</source>
        <translation>Afficher la barre de filtre</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="875"/>
        <source>Ctrl+I</source>
        <translation>Ctrl+I</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="883"/>
        <source>S&amp;plit View</source>
        <translation>Vue &amp;partagée</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="886"/>
        <source>Split View</source>
        <translation>Vue partagée</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="889"/>
        <source>F6</source>
        <translation>F6</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="894"/>
        <source>&amp;Copy Full Path</source>
        <translation>&amp;Copier le chemin d&apos;accès complet</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="897"/>
        <source>Ctrl+Shift+C</source>
        <translation>Ctrl+Shift+C</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="908"/>
        <source>Show Thumb&amp;nails</source>
        <translation>Afficher les mi&amp;niatures</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="911"/>
        <source>Show Thumbnails</source>
        <translation>Afficher les miniatures</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="919"/>
        <source>S&amp;ide Pane</source>
        <translation>vollet &amp;latéral</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="922"/>
        <source>F9</source>
        <translation>F9</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="927"/>
        <source>Hidden &amp;Shortcuts</source>
        <translation>Raccourci&amp;s cachés</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="932"/>
        <source>Open Tab in &amp;Admin Mode</source>
        <translation>Ouvrir un onglet en mode &amp;Admin</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="937"/>
        <location filename="../main-win.ui" line="940"/>
        <source>Create Launcher</source>
        <translation>Créer un lanceur</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="767"/>
        <source>Ctrl+B</source>
        <translation>Ctrl+B</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="772"/>
        <source>&amp;Clear All Filters</source>
        <translation>&amp;Effacer tous les filtres</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="775"/>
        <source>Ctrl+Shift+K</source>
        <translation>Ctrl+Shift+K</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="784"/>
        <source>Close &amp;previous tabs</source>
        <translation>Fermer les onglets &amp;précédents</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="793"/>
        <source>Close &amp;next tabs</source>
        <translation>Fermer les onglets &amp;suivants</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="806"/>
        <source>&amp;Menu bar</source>
        <translation>Barre des &amp;menus</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="837"/>
        <source>Connect to &amp;Server</source>
        <translation>Connecter au &amp;serveur</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="845"/>
        <source>&amp;Location</source>
        <translation>&amp;Emplacement</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="853"/>
        <source>&amp;Path Buttons</source>
        <translation>Bo&amp;utons des chemins</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="858"/>
        <source>&amp;Bulk Rename</source>
        <translation>&amp;Renommer en masse</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="861"/>
        <source>Bulk Rename</source>
        <translation>Renommer en masse</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="864"/>
        <source>Ctrl+F2</source>
        <translation>Ctrl+F2</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="798"/>
        <source>Close &amp;other tabs</source>
        <translation>Fermer les autres &amp;onglets</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="764"/>
        <source>Permanent &amp;filter bar</source>
        <translation>Barre de &amp;filtre permanente</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="809"/>
        <source>Menu bar</source>
        <translation>Barre de menu</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="812"/>
        <source>Ctrl+M</source>
        <translation>Ctrl+M</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="821"/>
        <location filename="../main-win.ui" line="824"/>
        <source>Menu</source>
        <translation>Menu</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="490"/>
        <source>Ctrl+A</source>
        <translation>Ctrl+A</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="154"/>
        <source>&amp;Customized View Settings</source>
        <translation>Paramètres d&apos;a&amp;ffichage personnalisé</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="280"/>
        <source>Go &amp;Up</source>
        <translation>Mon&amp;ter</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="346"/>
        <source>&amp;New Window</source>
        <translation>&amp;Nouvelle fenêtre</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="422"/>
        <source>&amp;Icon View</source>
        <translation>Vue en &amp;icônes</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="430"/>
        <source>&amp;Compact View</source>
        <translation>Vue &amp;compacte</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="438"/>
        <source>&amp;Detailed List</source>
        <translation>Liste &amp;détaillée</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="446"/>
        <source>&amp;Thumbnail View</source>
        <translation>Vue en vigne&amp;ttes</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="455"/>
        <source>Cu&amp;t</source>
        <translation>Cou&amp;per</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="592"/>
        <source>New &amp;Tab</source>
        <translation>Nouvel ongle&amp;t</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="595"/>
        <source>New Tab</source>
        <translation>Nouvel onglet</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="598"/>
        <source>Ctrl+T</source>
        <translation>Ctrl+T</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="607"/>
        <source>Go &amp;Back</source>
        <translation>Reto&amp;ur</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="610"/>
        <source>Go Back</source>
        <translation>Retour</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="613"/>
        <source>Alt+Left</source>
        <translation>Alt+Gauche</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="622"/>
        <source>Go &amp;Forward</source>
        <translation>Ava&amp;ncer</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="625"/>
        <source>Go Forward</source>
        <translation>Avancer</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="628"/>
        <source>Alt+Right</source>
        <translation>Alt+Droite</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="648"/>
        <source>Del</source>
        <translation>Suppr</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="656"/>
        <source>F2</source>
        <translation>F2</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="661"/>
        <source>C&amp;lose Tab</source>
        <translation>Fermer &amp;l&apos;onglet</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="669"/>
        <source>File &amp;Properties</source>
        <translation>&amp;Propriétés du fichier</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="677"/>
        <source>&amp;Folder Properties</source>
        <translation>Propriétés du &amp;dossier</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="701"/>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="736"/>
        <source>Ctrl+Shift+N</source>
        <translation>Ctrl+Maj+N</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="748"/>
        <source>Ctrl+Alt+N</source>
        <translation>Ctrl+Alt+N</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="74"/>
        <source>C&amp;reate New</source>
        <translation>Créer un &amp;nouveau</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="103"/>
        <source>&amp;Sorting</source>
        <translation>Tri des élément&amp;s</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="246"/>
        <source>Main Toolbar</source>
        <translation>Barre d&apos;outils principale</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="664"/>
        <source>Ctrl+W</source>
        <translation>Ctrl+W</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="672"/>
        <source>Alt+Return</source>
        <translation>Alt+Entrée</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="706"/>
        <source>Edit Bookmarks</source>
        <translation>Modifier les signets</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="711"/>
        <source>Open &amp;Terminal</source>
        <translation>Ouvrir le &amp;terminal</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="714"/>
        <source>F4</source>
        <translation>F4</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="724"/>
        <source>&amp;Edit Bookmarks</source>
        <translation>Modifi&amp;er les signets</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="733"/>
        <source>&amp;Folder</source>
        <translation>&amp;Dossier</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="745"/>
        <source>&amp;Blank File</source>
        <translation>&amp;Fichier vide</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="753"/>
        <source>&amp;Find Files</source>
        <translation>&amp;Trouver des fichiers</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="756"/>
        <source>F3</source>
        <translation>F3</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="70"/>
        <source>&amp;File</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="92"/>
        <source>&amp;Help</source>
        <translation>A&amp;ide</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="99"/>
        <location filename="../main-win.ui" line="123"/>
        <source>&amp;View</source>
        <translation>&amp;Vue</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="179"/>
        <source>&amp;Edit</source>
        <translation>&amp;Editer</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="197"/>
        <source>&amp;Bookmarks</source>
        <translation>&amp;Signets</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="204"/>
        <source>&amp;Go</source>
        <translation>A&amp;ller à</translation>
    </message>
</context>
<context>
    <name>PCManFM::Application</name>
    <message>
        <location filename="../application.cpp" line="168"/>
        <source>Name of configuration profile</source>
        <translation>Nom du profil de configuration</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="168"/>
        <source>PROFILE</source>
        <translation>PROFIL</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="177"/>
        <source>Launch desktop manager</source>
        <translation>Lancer le gestionnaire de bureau</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="180"/>
        <source>Turn off desktop manager if it&apos;s running</source>
        <translation>Désactiver le gestionnaire de bureau s&apos;il est en cours d&apos;exécution</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="183"/>
        <source>Open desktop preference dialog on the page with the specified name</source>
        <translation>Ouvrir la boîte de dialogue des préférences du bureau sur la page portant le nom spécifié</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="183"/>
        <location filename="../application.cpp" line="198"/>
        <source>NAME</source>
        <translation>NOM</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="192"/>
        <source>Set desktop wallpaper from image FILE</source>
        <translation>Définir le fond d&apos;écran à partir d&apos;un FICHIER image</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="192"/>
        <source>FILE</source>
        <translation>FICHIER</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="195"/>
        <source>MODE</source>
        <translation>MODE</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="198"/>
        <source>Open Preferences dialog on the page with the specified name</source>
        <translation>Ouvrir la boîte de dialogue des préférences sur la page portant le nom spécifié</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="186"/>
        <source>Open new window</source>
        <translation>Ouvrir une nouvelle fenêtre</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="171"/>
        <source>Run PCManFM-Qt as a daemon</source>
        <translation>Exécutez PCManFM-Qt en tant que démon</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="174"/>
        <source>Quit PCManFM-Qt</source>
        <translation>Quitter PCManFM-Qt</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="189"/>
        <source>Open Find Files utility</source>
        <translation>Ouvrir l&apos;utilitaire de recherche de fichiers</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="195"/>
        <source>Set mode of desktop wallpaper. MODE=(%1)</source>
        <translation>Définir le mode du papier-peint. MODE=(%1)</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="201"/>
        <source>[FILE1, FILE2,...]</source>
        <translation>[FICHIER1, FICHIER2,...]</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="201"/>
        <source>Files or directories to open</source>
        <translation>Fichiers ou dossiers à ouvrir</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="627"/>
        <location filename="../application.cpp" line="632"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="632"/>
        <source>Terminal emulator is not set.</source>
        <translation>L&apos;émulateur de terminal n&apos;est pas défini.</translation>
    </message>
</context>
<context>
    <name>PCManFM::AutoRunDialog</name>
    <message>
        <location filename="../autorundialog.cpp" line="44"/>
        <source>Open in file manager</source>
        <translation>Ouvrir dans le gestionnaire de fichiers</translation>
    </message>
    <message>
        <location filename="../autorundialog.cpp" line="138"/>
        <source>Removable Disk</source>
        <translation>Disque amovible</translation>
    </message>
</context>
<context>
    <name>PCManFM::ConnectServerDialog</name>
    <message>
        <location filename="../connectserverdialog.cpp" line="9"/>
        <source>SSH</source>
        <translation>SSH</translation>
    </message>
    <message>
        <location filename="../connectserverdialog.cpp" line="10"/>
        <source>FTP</source>
        <translation>FTP</translation>
    </message>
    <message>
        <location filename="../connectserverdialog.cpp" line="11"/>
        <source>WebDav</source>
        <translation>Web Dav</translation>
    </message>
    <message>
        <location filename="../connectserverdialog.cpp" line="12"/>
        <source>Secure WebDav</source>
        <translation>WebDav sécurisé</translation>
    </message>
    <message>
        <location filename="../connectserverdialog.cpp" line="13"/>
        <source>HTTP</source>
        <translation>HTTP</translation>
    </message>
    <message>
        <location filename="../connectserverdialog.cpp" line="14"/>
        <source>HTTPS</source>
        <translation>HTTPS</translation>
    </message>
</context>
<context>
    <name>PCManFM::DesktopEntryDialog</name>
    <message>
        <location filename="../desktopentrydialog.cpp" line="29"/>
        <source>Command:</source>
        <translation>Commande :</translation>
    </message>
    <message>
        <location filename="../desktopentrydialog.cpp" line="32"/>
        <source>URL:</source>
        <translation>Lien du site :</translation>
    </message>
    <message>
        <location filename="../desktopentrydialog.cpp" line="57"/>
        <source>Select an icon</source>
        <translation>Sélectionner une icône</translation>
    </message>
    <message>
        <location filename="../desktopentrydialog.cpp" line="59"/>
        <source>Images (*.png *.xpm *.svg *.svgz )</source>
        <translation>Images (*.png *.xpm *.svg *.svgz)</translation>
    </message>
    <message>
        <location filename="../desktopentrydialog.cpp" line="81"/>
        <source>Select an executable file</source>
        <translation>Sélectionner un fichier exécutable</translation>
    </message>
    <message>
        <location filename="../desktopentrydialog.cpp" line="89"/>
        <source>Select a file</source>
        <translation>Sélectionner un fichier</translation>
    </message>
</context>
<context>
    <name>PCManFM::DesktopPreferencesDialog</name>
    <message>
        <location filename="../desktoppreferencesdialog.cpp" line="53"/>
        <source>Fill with background color only</source>
        <translation>Remplir seulement avec la couleur d&apos;arrière-plan</translation>
    </message>
    <message>
        <location filename="../desktoppreferencesdialog.cpp" line="54"/>
        <source>Stretch to fill the entire screen</source>
        <translation>Étirer pour remplir l&apos;écran entier</translation>
    </message>
    <message>
        <location filename="../desktoppreferencesdialog.cpp" line="55"/>
        <source>Stretch to fit the screen</source>
        <translation>Étirer pour remplir l&apos;écran</translation>
    </message>
    <message>
        <location filename="../desktoppreferencesdialog.cpp" line="56"/>
        <source>Center on the screen</source>
        <translation>Centrer sur l’écran</translation>
    </message>
    <message>
        <location filename="../desktoppreferencesdialog.cpp" line="57"/>
        <source>Tile the image to fill the entire screen</source>
        <translation>Faire une mosaïque avec l&apos;image pour remplir l&apos;écran en entier</translation>
    </message>
    <message>
        <location filename="../desktoppreferencesdialog.cpp" line="58"/>
        <source>Zoom the image to fill the entire screen</source>
        <translation>Zoomer l&apos;image pour remplir tout l&apos;écran</translation>
    </message>
    <message>
        <location filename="../desktoppreferencesdialog.cpp" line="255"/>
        <source>Select Wallpaper</source>
        <translation>Sélectionner un fond d&apos;écran</translation>
    </message>
    <message>
        <location filename="../desktoppreferencesdialog.cpp" line="259"/>
        <source>Image Files</source>
        <translation>Fichiers image</translation>
    </message>
    <message>
        <location filename="../desktoppreferencesdialog.cpp" line="301"/>
        <source>Select Wallpaper Folder</source>
        <translation>Sélectionnez le dossier de papier peint</translation>
    </message>
</context>
<context>
    <name>PCManFM::DesktopWindow</name>
    <message>
        <location filename="../desktopwindow.cpp" line="299"/>
        <source>Trash (One item)</source>
        <translation>Corbeille (Un élément)</translation>
    </message>
    <message numerus="yes">
        <location filename="../desktopwindow.cpp" line="302"/>
        <source>Trash (%Ln items)</source>
        <translation>
            <numerusform>Corbeille (%Ln élément)</numerusform>
            <numerusform>Corbeille (%Ln éléments)</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../desktopwindow.cpp" line="306"/>
        <source>Trash (Empty)</source>
        <translation>Corbeille (Vide)</translation>
    </message>
    <message>
        <location filename="../desktopwindow.cpp" line="341"/>
        <source>Computer</source>
        <translation>Ordinateur</translation>
    </message>
    <message>
        <location filename="../desktopwindow.cpp" line="355"/>
        <source>Network</source>
        <translation>Réseau</translation>
    </message>
    <message>
        <location filename="../desktopwindow.cpp" line="963"/>
        <source>Open</source>
        <translation>Ouvrir</translation>
    </message>
    <message>
        <location filename="../desktopwindow.cpp" line="969"/>
        <location filename="../desktopwindow.cpp" line="1009"/>
        <source>Stic&amp;k to Current Position</source>
        <translation>Res&amp;ter à la position actuelle</translation>
    </message>
    <message>
        <location filename="../desktopwindow.cpp" line="977"/>
        <source>Empty Trash</source>
        <translation>Vider la corbeille</translation>
    </message>
    <message>
        <location filename="../desktopwindow.cpp" line="1036"/>
        <source>Hide Desktop Items</source>
        <translation>Masquer les éléments du bureau</translation>
    </message>
    <message>
        <location filename="../desktopwindow.cpp" line="1042"/>
        <source>Create Launcher</source>
        <translation>Créer un lanceur</translation>
    </message>
    <message>
        <location filename="../desktopwindow.cpp" line="1045"/>
        <source>Desktop Preferences</source>
        <translation>Préférences du bureau</translation>
    </message>
</context>
<context>
    <name>PCManFM::FilterBar</name>
    <message>
        <location filename="../tabpage.cpp" line="93"/>
        <source>Filter:</source>
        <translation>Filtre :</translation>
    </message>
</context>
<context>
    <name>PCManFM::FilterEdit</name>
    <message>
        <location filename="../tabpage.cpp" line="63"/>
        <source>Clear text (Ctrl+K or Esc)</source>
        <translation>Effacer le texte (Ctrl+K ou Ech)</translation>
    </message>
</context>
<context>
    <name>PCManFM::MainWindow</name>
    <message>
        <location filename="../mainwindow.cpp" line="141"/>
        <source>Root Instance</source>
        <translation>Instance Root</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="724"/>
        <source>Hide menu bar</source>
        <translation>Cacher la barre des menus</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="725"/>
        <source>This will hide the menu bar completely, use Ctrl+M to show it again.</source>
        <translation>Cela masquera complètement la barre de menus, utiliser Ctrl+M pour l&apos;afficher à nouveau.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1097"/>
        <source>Version: %1</source>
        <translation>Version : %1</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="174"/>
        <location filename="../mainwindow.cpp" line="2057"/>
        <source>&amp;Move to Trash</source>
        <translation>&amp;Mettre à la corbeille</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="174"/>
        <location filename="../mainwindow.cpp" line="2057"/>
        <source>&amp;Delete</source>
        <translation>&amp;Supprimer</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1900"/>
        <source>Customized View Settings</source>
        <translation>Paramètres d&apos;affichage personnalisé</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2125"/>
        <location filename="../mainwindow.cpp" line="2133"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2133"/>
        <source>Switch user command is not set.</source>
        <translation>La commande de changement d&apos;utilisateur n&apos;est pas définie.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2235"/>
        <source>Cleaning Folder Settings</source>
        <translation>Nettoyage des paramètres du dossier</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="2236"/>
        <source>Do you want to remove settings of nonexistent folders?
They might be useful if those folders are created again.</source>
        <translation>Voulez-vous supprimer les paramètres des dossiers inexistants ?
Ils peuvent être utiles si ces dossiers sont à nouveau créés.</translation>
    </message>
</context>
<context>
    <name>PCManFM::PreferencesDialog</name>
    <message>
        <location filename="../preferencesdialog.cpp" line="199"/>
        <source>Icon View</source>
        <translation>Vue en icônes</translation>
    </message>
    <message>
        <location filename="../preferencesdialog.cpp" line="200"/>
        <source>Compact View</source>
        <translation>Vue compacte</translation>
    </message>
    <message>
        <location filename="../preferencesdialog.cpp" line="201"/>
        <source>Thumbnail View</source>
        <translation>Vue en vignettes</translation>
    </message>
    <message>
        <location filename="../preferencesdialog.cpp" line="202"/>
        <source>Detailed List View</source>
        <translation>Vue en liste détaillée</translation>
    </message>
</context>
<context>
    <name>PCManFM::TabPage</name>
    <message>
        <location filename="../tabpage.cpp" line="506"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../tabpage.cpp" line="515"/>
        <source>Free space: %1 (Total: %2)</source>
        <translation>Espace libre : %1 (total : %2)</translation>
    </message>
    <message numerus="yes">
        <location filename="../tabpage.cpp" line="532"/>
        <source>%n item(s)</source>
        <translation>
            <numerusform>%n élément</numerusform>
            <numerusform>%n éléments</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../tabpage.cpp" line="534"/>
        <source> (%n hidden)</source>
        <translation>
            <numerusform> (%n caché)</numerusform>
            <numerusform> (%n cachés)</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../tabpage.cpp" line="539"/>
        <location filename="../tabpage.cpp" line="735"/>
        <location filename="../tabpage.cpp" line="749"/>
        <source>Link to</source>
        <translation>Lien vers</translation>
    </message>
    <message numerus="yes">
        <location filename="../tabpage.cpp" line="761"/>
        <source>%n item(s) selected</source>
        <translation>
            <numerusform>%n élément sélectionné</numerusform>
            <numerusform>%n éléments sélectionnés</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>PCManFM::View</name>
    <message>
        <location filename="../view.cpp" line="59"/>
        <source>Many files</source>
        <translation>Beaucoup de fichiers</translation>
    </message>
    <message numerus="yes">
        <location filename="../view.cpp" line="60"/>
        <source>Do you want to open these %1 files?</source>
        <translation>
            <numerusform>Voulez vous ouvrir ce %1 fichier ?</numerusform>
            <numerusform>Voulez vous ouvrir ces %1 fichiers ?</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../view.cpp" line="122"/>
        <source>Open in New T&amp;ab</source>
        <translation>Ouvrir dans un nouvel ongle&amp;t</translation>
    </message>
    <message>
        <location filename="../view.cpp" line="126"/>
        <source>Open in New Win&amp;dow</source>
        <translation>Ouvrir &amp;dans une nouvelle fenêtre</translation>
    </message>
    <message>
        <location filename="../view.cpp" line="134"/>
        <source>Open in Termina&amp;l</source>
        <translation>Ouvrir dans &amp;le terminal</translation>
    </message>
</context>
<context>
    <name>PreferencesDialog</name>
    <message>
        <location filename="../preferences.ui" line="14"/>
        <source>Preferences</source>
        <translation>Préférences</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="45"/>
        <source>User Interface</source>
        <translation>Interface utilisateur</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="35"/>
        <source>Behavior</source>
        <translation>Comportement</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="50"/>
        <location filename="../preferences.ui" line="605"/>
        <source>Thumbnail</source>
        <translation>Vignette</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="55"/>
        <source>Volume</source>
        <translation>Volume</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="60"/>
        <source>Advanced</source>
        <translation>Avancé</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="222"/>
        <source>Select newly created files</source>
        <translation>Sélectionner les fichiers nouvellement créés</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="259"/>
        <source>Icons</source>
        <translation>Icônes</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="291"/>
        <source>Size of big icons:</source>
        <translation>Taille des grandes icônes :</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="308"/>
        <source>Size of small icons:</source>
        <translation>Taille des petites icônes :</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="325"/>
        <source>Size of thumbnails:</source>
        <translation>Taille des vignettes :</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="339"/>
        <source>Size of side pane icons:</source>
        <translation>Taille des icônes du volet latéral :</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="271"/>
        <source>Icon theme:</source>
        <translation>Thème des icônes :</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="509"/>
        <source>Window</source>
        <translation>Fenêtre</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="550"/>
        <source>Default width of new windows:</source>
        <translation>Largeur par défaut des nouvelles fenêtres :</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="564"/>
        <source>Default height of new windows:</source>
        <translation>Hauteur par défaut des nouvelles fenêtres :</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="522"/>
        <source>Always show the tab bar</source>
        <translation>Toujours afficher la barre des onglets</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="529"/>
        <source>Show &apos;Close&apos; buttons on tabs	</source>
        <translation>Afficher les boutons &apos;Fermer&apos; sur les onglets	</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="543"/>
        <source>Remember the size of the last closed window</source>
        <translation>Mémoriser la taille de la dernière fenêtre fermée</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="81"/>
        <source>Browsing</source>
        <translation>Navigation</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="93"/>
        <source>Open files with single click</source>
        <translation>Ouvrir des fichiers en un simple clic</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="113"/>
        <source>Default view mode:</source>
        <translation>Mode d&apos;affichage par défaut :</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="129"/>
        <source> sec</source>
        <translation> sec</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="175"/>
        <source>File Operations</source>
        <translation>Opérations sur les fichiers</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="181"/>
        <source>Confirm before deleting files</source>
        <translation>Confirmer avant de supprimer les fichiers</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="188"/>
        <source>Move deleted files to &quot;trash bin&quot; instead of erasing from disk.</source>
        <translation>Déplacer les fichiers supprimés vers la corbeille au lieu de les effacer du disque.</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="614"/>
        <source>Show thumbnails of files</source>
        <translation>Montrer les vignettes des fichiers</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="621"/>
        <source>Only show thumbnails for local files</source>
        <translation>Montrer les vignettes seulement pour les fichiers locaux</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="40"/>
        <source>Display</source>
        <translation>Affichage</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="90"/>
        <source>If this is unchecked, the DE setting will be used.</source>
        <translation>Si cette case est décochée, le paramètre de l&apos;environnement de bureau sera utilisé.</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="103"/>
        <location filename="../preferences.ui" line="126"/>
        <source>Set to zero to disable auto-selection.</source>
        <translation>Réglez à zéro pour désactiver la sélection automatique.</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="106"/>
        <source>Delay of auto-selection in single click mode:</source>
        <translation>Délai de sélection automatique avec un simple clic :</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="147"/>
        <source>Open in current tab</source>
        <translation>Ouvrir dans l&apos;onglet actuel</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="152"/>
        <source>Open in new tab</source>
        <translation>Ouvrir dans un nouvel onglet</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="157"/>
        <source>Open in new window</source>
        <translation>Ouvrir dans une nouvelle fenêtre</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="165"/>
        <source>Show folder context menu with Ctrl + right click</source>
        <translation>Afficher le menu contextuel du dossier avec Ctrl + clic droit</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="195"/>
        <source>Erase files on removable media instead of &quot;trash can&quot; creation</source>
        <translation>Effacer les fichiers des supports amovibles au lieu de les mettre dans la corbeille</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="202"/>
        <source>Confirm before moving files into &quot;trash can&quot;</source>
        <translation>Confirmer avant de déplacer des fichiers à la corbeille</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="209"/>
        <location filename="../preferences.ui" line="372"/>
        <location filename="../preferences.ui" line="382"/>
        <source>Requires application restart to take effect completely</source>
        <translation>Requiert le redémarrage de l&apos;application pour prendre complètement effet</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="212"/>
        <source>Launch executable files without prompt</source>
        <translation>Lancer des fichiers exécutables sans invite</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="219"/>
        <source>Renamed files will also be selected</source>
        <translation>Les fichiers renommés seront aussi sélectionnés</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="229"/>
        <source>Open folders in new tabs as far as possible</source>
        <translation>Ouvrez les dossiers dans de nouveaux onglets autant que possible</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="232"/>
        <source>Single window mode</source>
        <translation>Mode fenêtre unique</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="288"/>
        <location filename="../preferences.ui" line="298"/>
        <source>Used by Icon View</source>
        <translation>Utilisé par la vue en icônes</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="305"/>
        <location filename="../preferences.ui" line="315"/>
        <source>Used by Compact View and Detailed List View</source>
        <translation>Utilisé par la vue compacte et la vue en liste détaillée</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="322"/>
        <location filename="../preferences.ui" line="332"/>
        <source>Used by Thumbnail View</source>
        <translation>Utilisé par la vue miniature</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="352"/>
        <source>User interface</source>
        <translation>Interface utilisateur</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="365"/>
        <source>Treat backup files as hidden</source>
        <translation>Traiter les fichiers de sauvegarde comme cachés</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="385"/>
        <source>Show icons of hidden files shadowed</source>
        <translation>Afficher les icônes des fichiers cachés ombrés</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="399"/>
        <source>Disable smooth scrolling in list and compact modes</source>
        <translation>Désactiver le défilement doux dans les listes et en mode compact</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="422"/>
        <source>Minimum item margins in icon view:</source>
        <translation>Marges minimales des éléments dans la vue en icônes :</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="429"/>
        <source>3 px by default.</source>
        <translation>3 px par défaut.</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="432"/>
        <location filename="../preferences.ui" line="456"/>
        <source> px</source>
        <translation> px</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="445"/>
        <source>x</source>
        <translation>x</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="452"/>
        <source>3 px by default.
A space is also reserved for 3 lines of text.</source>
        <translation>3 px par défaut.
Un espace est également réservé pour 3 lignes de texte.</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="469"/>
        <source>Lock</source>
        <translation>Verrouiller</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="518"/>
        <source>When unchecked, the tab bar will be shown
only if there are more than one tab.</source>
        <translation>Lorsqu&apos;elle est décochée, la barre d&apos;onglets sera affichée
seulement s&apos;il y a plus d&apos;un onglet.</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="536"/>
        <source>Switch to newly opened tab</source>
        <translation>Passer à l&apos;onglet nouvellement ouvert</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="578"/>
        <source>Reopen last window tabs in a new window</source>
        <translation>Rouvrir les derniers onglets de la fenêtre dans une nouvelle fenêtre</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="628"/>
        <location filename="../preferences.ui" line="640"/>
        <source>The built-in thumbnailer makes thumbnails of images that are supported by Qt.

Usually, most image types are supported. The default size limit is 4 MiB.</source>
        <translation>La vignette intégrée crée des vignettes d&apos;images prises en charge par Qt.

Habituellement, la plupart des types d&apos;images sont pris en charge. La limite de taille par défaut est de 4 Mio.</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="633"/>
        <source>Image size limit for built-in thumbnailer:</source>
        <translation>Limite de taille d&apos;image pour la vignette intégrée :</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="645"/>
        <location filename="../preferences.ui" line="681"/>
        <source> MiB</source>
        <translation> Mio</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="661"/>
        <location filename="../preferences.ui" line="673"/>
        <source>If existing, external thumbnailers are used for videos, PDF documents, etc.

A value of -1 means that there is no limit for the file size (the default).</source>
        <translation>Le cas échéant, des vignettes externes sont utilisées pour les vidéos, les documents PDF, etc.

Une valeur de -1 signifie qu&apos;il n&apos;y a pas de limite pour la taille du fichier (valeur par défaut).</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="666"/>
        <source>File size limit for external thumbnailers:</source>
        <translation>Limite de taille de fichier pour les miniatures externes :</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="678"/>
        <source>No limit</source>
        <translation>Pas de limite</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="816"/>
        <source>Examples:&lt;br&gt;For terminal: &lt;i&gt;xterm -e %s&lt;/i&gt;&lt;br&gt;For switching user: &lt;i&gt;lxsudo %s&lt;/i&gt; or &lt;i&gt;lxsudo dbus-run-session -- %s&lt;/i&gt;&lt;br&gt;&lt;i&gt;%s&lt;/i&gt; is the command line you want to execute with terminal or su.&lt;br&gt; Important: Please use lxsudo, sudo alone will wreck permissions of the settings file.</source>
        <translation>Exemples :&lt;br&gt;Pour le terminal : &lt;i&gt;xterm -e %s&lt;/i&gt;&lt;br&gt;Pour changer d&apos;utilisateur : &lt;i&gt;lxsudo %s&lt;/i&gt; ou &lt;i&gt;lxsudo dbus-run-session -- %s&lt;/i&gt;&lt;br&gt;&lt;i&gt;%s&lt;/i&gt; est la commande que vous devez exécuter dans le terminal avec accès root.&lt;br&gt; Important : veuillez utilisez lxsudo, car sudo seul peut casser les permission du fichier de configuration.</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="907"/>
        <source>Application restart is needed for changes to take effect.</source>
        <translation>Le redémarrage de l&apos;application est nécessaire pour que les modifications prennent effet.</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="139"/>
        <source>Bookmarks menu:</source>
        <translation>Menu des marques pages :</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="375"/>
        <source>Always show real file names</source>
        <translation>Toujours afficher les noms réels des fichiers</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="392"/>
        <source>Do not show file tooltips</source>
        <translation>Ne pas afficher les info-bulles de fichier</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="611"/>
        <source>Needs ffmpegthumbnailer</source>
        <translation>Nécessite ffmpegthumbnailer</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="717"/>
        <source>Auto Mount</source>
        <translation>Monter automatiquement</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="723"/>
        <source>Mount mountable volumes automatically on program startup</source>
        <translation>Monter automatiquement les volumes au démarrage du programme</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="730"/>
        <source>Mount removable media automatically when they are inserted</source>
        <translation>Monter automatiquement les supports amovibles lorsqu&apos;ils sont insérés</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="737"/>
        <source>Show available options for removable media when they are inserted</source>
        <translation>Afficher les options disponibles pour les supports amovibles lorsqu&apos;ils sont insérés</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="747"/>
        <source>When removable medium unmounted:</source>
        <translation>Lorsque le support amovible est démonté :</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="753"/>
        <source>Close &amp;tab containing removable medium</source>
        <translation>&amp;Fermer les onglets contenant le support amovible</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="760"/>
        <source>Chan&amp;ge folder in the tab to home folder</source>
        <translation>Remplacer le dossier de l&apos;on&amp;glet par le dossier home</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="803"/>
        <source>Switch &amp;user command:</source>
        <translation>Basculer en commande &amp;utilisateur :</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="829"/>
        <source>Archiver in&amp;tegration:</source>
        <translation>In&amp;tégration de l&apos;archiveur :</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="852"/>
        <source>Templates</source>
        <translation>Modèles</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="858"/>
        <source>Show only user defined templates in menu</source>
        <translation>Afficher uniquement les modèles définis par l&apos;utilisateur dans le menu</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="865"/>
        <source>Show only one template for each MIME type</source>
        <translation>Ne montrer qu&apos;un modèle pour chaque type MIME</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="872"/>
        <source>Run default application after creation from template</source>
        <translation>Exécuter l&apos;application par défaut après la création à partir d&apos;un modèle</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="787"/>
        <source>Programs</source>
        <translation>Programmes</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="796"/>
        <source>Terminal emulator:</source>
        <translation>Émulateur de terminal :</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="358"/>
        <source>Use SI decimal prefixes instead of IEC binary prefixes</source>
        <translation>Utiliser les préfixes de décimales du système international au lieu des préfixes binaires CEI</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../bulkrename.cpp" line="73"/>
        <source>Renaming files...</source>
        <translation>Renommage des fichiers...</translation>
    </message>
    <message>
        <location filename="../bulkrename.cpp" line="73"/>
        <source>Abort</source>
        <translation>Interrompre</translation>
    </message>
    <message>
        <location filename="../bulkrename.cpp" line="82"/>
        <source>Warning</source>
        <translation>Attention</translation>
    </message>
    <message>
        <location filename="../bulkrename.cpp" line="82"/>
        <source>Renaming is aborted.</source>
        <translation>Le renommage est interrompu.</translation>
    </message>
    <message>
        <location filename="../bulkrename.cpp" line="108"/>
        <location filename="../bulkrename.cpp" line="111"/>
        <location filename="../tabpage.cpp" line="1077"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../bulkrename.cpp" line="108"/>
        <source>No file could be renamed.</source>
        <translation>Aucun fichier ne peut être renommé.</translation>
    </message>
    <message>
        <location filename="../bulkrename.cpp" line="111"/>
        <source>Some files could not be renamed.</source>
        <translation>Certains fichiers n&apos;ont pas pu être renommés.</translation>
    </message>
    <message>
        <location filename="../tabpage.cpp" line="1077"/>
        <source>Cannot open as Admin.</source>
        <translation>Impossible d&apos;ouvrir en tant qu&apos;administrateur.</translation>
    </message>
</context>
</TS>
