/****************************************************************************
** Meta object code from reading C++ file 'configwidget.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.15.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../../src/core/ui/configwidget.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'configwidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.15.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_ConfigDialog_t {
    QByteArrayData data[31];
    char stringdata0[383];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ConfigDialog_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ConfigDialog_t qt_meta_stringdata_ConfigDialog = {
    {
QT_MOC_LITERAL(0, 0, 12), // "ConfigDialog"
QT_MOC_LITERAL(1, 13, 15), // "collapsTreeKeys"
QT_MOC_LITERAL(2, 29, 0), // ""
QT_MOC_LITERAL(3, 30, 11), // "QModelIndex"
QT_MOC_LITERAL(4, 42, 5), // "index"
QT_MOC_LITERAL(5, 48, 19), // "doubleclickTreeKeys"
QT_MOC_LITERAL(6, 68, 19), // "toggleCheckShowTray"
QT_MOC_LITERAL(7, 88, 7), // "checked"
QT_MOC_LITERAL(8, 96, 18), // "currentItemChanged"
QT_MOC_LITERAL(9, 115, 1), // "c"
QT_MOC_LITERAL(10, 117, 1), // "p"
QT_MOC_LITERAL(11, 119, 14), // "editDateTmeTpl"
QT_MOC_LITERAL(12, 134, 3), // "str"
QT_MOC_LITERAL(13, 138, 21), // "setVisibleDateTplEdit"
QT_MOC_LITERAL(14, 160, 17), // "changeTrayMsgType"
QT_MOC_LITERAL(15, 178, 4), // "type"
QT_MOC_LITERAL(16, 183, 18), // "changeTimeTrayMess"
QT_MOC_LITERAL(17, 202, 3), // "sec"
QT_MOC_LITERAL(18, 206, 23), // "setVisibleAutoSaveFirst"
QT_MOC_LITERAL(19, 230, 6), // "status"
QT_MOC_LITERAL(20, 237, 16), // "changeFormatType"
QT_MOC_LITERAL(21, 254, 22), // "changeImgQualituSlider"
QT_MOC_LITERAL(22, 277, 3), // "pos"
QT_MOC_LITERAL(23, 281, 12), // "saveSettings"
QT_MOC_LITERAL(24, 294, 9), // "selectDir"
QT_MOC_LITERAL(25, 304, 15), // "restoreDefaults"
QT_MOC_LITERAL(26, 320, 14), // "acceptShortcut"
QT_MOC_LITERAL(27, 335, 3), // "seq"
QT_MOC_LITERAL(28, 339, 14), // "changeShortcut"
QT_MOC_LITERAL(29, 354, 12), // "clearShrtcut"
QT_MOC_LITERAL(30, 367, 15) // "keyNotSupported"

    },
    "ConfigDialog\0collapsTreeKeys\0\0QModelIndex\0"
    "index\0doubleclickTreeKeys\0toggleCheckShowTray\0"
    "checked\0currentItemChanged\0c\0p\0"
    "editDateTmeTpl\0str\0setVisibleDateTplEdit\0"
    "changeTrayMsgType\0type\0changeTimeTrayMess\0"
    "sec\0setVisibleAutoSaveFirst\0status\0"
    "changeFormatType\0changeImgQualituSlider\0"
    "pos\0saveSettings\0selectDir\0restoreDefaults\0"
    "acceptShortcut\0seq\0changeShortcut\0"
    "clearShrtcut\0keyNotSupported"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ConfigDialog[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      18,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,  104,    2, 0x08 /* Private */,
       5,    1,  107,    2, 0x08 /* Private */,
       6,    1,  110,    2, 0x08 /* Private */,
       8,    2,  113,    2, 0x08 /* Private */,
      11,    1,  118,    2, 0x08 /* Private */,
      13,    1,  121,    2, 0x08 /* Private */,
      14,    1,  124,    2, 0x08 /* Private */,
      16,    1,  127,    2, 0x08 /* Private */,
      18,    1,  130,    2, 0x08 /* Private */,
      20,    1,  133,    2, 0x08 /* Private */,
      21,    1,  136,    2, 0x08 /* Private */,
      23,    0,  139,    2, 0x08 /* Private */,
      24,    0,  140,    2, 0x08 /* Private */,
      25,    0,  141,    2, 0x08 /* Private */,
      26,    1,  142,    2, 0x08 /* Private */,
      28,    1,  145,    2, 0x08 /* Private */,
      29,    0,  148,    2, 0x08 /* Private */,
      30,    0,  149,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, QMetaType::Bool,    7,
    QMetaType::Void, 0x80000000 | 3, 0x80000000 | 3,    9,   10,
    QMetaType::Void, QMetaType::QString,   12,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, QMetaType::Int,   15,
    QMetaType::Void, QMetaType::Int,   17,
    QMetaType::Void, QMetaType::Bool,   19,
    QMetaType::Void, QMetaType::Int,   15,
    QMetaType::Void, QMetaType::Int,   22,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QKeySequence,   27,
    QMetaType::Void, QMetaType::QKeySequence,   27,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void ConfigDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<ConfigDialog *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->collapsTreeKeys((*reinterpret_cast< QModelIndex(*)>(_a[1]))); break;
        case 1: _t->doubleclickTreeKeys((*reinterpret_cast< QModelIndex(*)>(_a[1]))); break;
        case 2: _t->toggleCheckShowTray((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 3: _t->currentItemChanged((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< const QModelIndex(*)>(_a[2]))); break;
        case 4: _t->editDateTmeTpl((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 5: _t->setVisibleDateTplEdit((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 6: _t->changeTrayMsgType((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: _t->changeTimeTrayMess((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 8: _t->setVisibleAutoSaveFirst((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 9: _t->changeFormatType((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 10: _t->changeImgQualituSlider((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 11: _t->saveSettings(); break;
        case 12: _t->selectDir(); break;
        case 13: _t->restoreDefaults(); break;
        case 14: _t->acceptShortcut((*reinterpret_cast< const QKeySequence(*)>(_a[1]))); break;
        case 15: _t->changeShortcut((*reinterpret_cast< const QKeySequence(*)>(_a[1]))); break;
        case 16: _t->clearShrtcut(); break;
        case 17: _t->keyNotSupported(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject ConfigDialog::staticMetaObject = { {
    QMetaObject::SuperData::link<QDialog::staticMetaObject>(),
    qt_meta_stringdata_ConfigDialog.data,
    qt_meta_data_ConfigDialog,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *ConfigDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ConfigDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_ConfigDialog.stringdata0))
        return static_cast<void*>(this);
    return QDialog::qt_metacast(_clname);
}

int ConfigDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 18)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 18;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 18)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 18;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
