# Install script for directory: /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/plugin-dom

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "RelWithDebInfo")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

# Set default install directory permissions.
if(NOT DEFINED CMAKE_OBJDUMP)
  set(CMAKE_OBJDUMP "/usr/bin/objdump")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xRuntimex" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/share/lxqt/translations/lxqt-panel/dom/dom_ar.qm;/usr/share/lxqt/translations/lxqt-panel/dom/dom_arn.qm;/usr/share/lxqt/translations/lxqt-panel/dom/dom_ast.qm;/usr/share/lxqt/translations/lxqt-panel/dom/dom_bg.qm;/usr/share/lxqt/translations/lxqt-panel/dom/dom_ca.qm;/usr/share/lxqt/translations/lxqt-panel/dom/dom_cs.qm;/usr/share/lxqt/translations/lxqt-panel/dom/dom_cy.qm;/usr/share/lxqt/translations/lxqt-panel/dom/dom_da.qm;/usr/share/lxqt/translations/lxqt-panel/dom/dom_de.qm;/usr/share/lxqt/translations/lxqt-panel/dom/dom_el.qm;/usr/share/lxqt/translations/lxqt-panel/dom/dom_es.qm;/usr/share/lxqt/translations/lxqt-panel/dom/dom_et.qm;/usr/share/lxqt/translations/lxqt-panel/dom/dom_fr.qm;/usr/share/lxqt/translations/lxqt-panel/dom/dom_gl.qm;/usr/share/lxqt/translations/lxqt-panel/dom/dom_he.qm;/usr/share/lxqt/translations/lxqt-panel/dom/dom_hr.qm;/usr/share/lxqt/translations/lxqt-panel/dom/dom_hu.qm;/usr/share/lxqt/translations/lxqt-panel/dom/dom_id.qm;/usr/share/lxqt/translations/lxqt-panel/dom/dom_it.qm;/usr/share/lxqt/translations/lxqt-panel/dom/dom_ja.qm;/usr/share/lxqt/translations/lxqt-panel/dom/dom_lt.qm;/usr/share/lxqt/translations/lxqt-panel/dom/dom_lv.qm;/usr/share/lxqt/translations/lxqt-panel/dom/dom_nb_NO.qm;/usr/share/lxqt/translations/lxqt-panel/dom/dom_nl.qm;/usr/share/lxqt/translations/lxqt-panel/dom/dom_pl.qm;/usr/share/lxqt/translations/lxqt-panel/dom/dom_pt.qm;/usr/share/lxqt/translations/lxqt-panel/dom/dom_pt_BR.qm;/usr/share/lxqt/translations/lxqt-panel/dom/dom_ru.qm;/usr/share/lxqt/translations/lxqt-panel/dom/dom_si.qm;/usr/share/lxqt/translations/lxqt-panel/dom/dom_sk_SK.qm;/usr/share/lxqt/translations/lxqt-panel/dom/dom_sv.qm;/usr/share/lxqt/translations/lxqt-panel/dom/dom_tr.qm;/usr/share/lxqt/translations/lxqt-panel/dom/dom_uk.qm;/usr/share/lxqt/translations/lxqt-panel/dom/dom_zh_CN.qm")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/usr/share/lxqt/translations/lxqt-panel/dom" TYPE FILE FILES
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-dom/dom_ar.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-dom/dom_arn.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-dom/dom_ast.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-dom/dom_bg.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-dom/dom_ca.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-dom/dom_cs.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-dom/dom_cy.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-dom/dom_da.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-dom/dom_de.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-dom/dom_el.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-dom/dom_es.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-dom/dom_et.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-dom/dom_fr.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-dom/dom_gl.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-dom/dom_he.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-dom/dom_hr.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-dom/dom_hu.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-dom/dom_id.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-dom/dom_it.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-dom/dom_ja.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-dom/dom_lt.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-dom/dom_lv.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-dom/dom_nb_NO.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-dom/dom_nl.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-dom/dom_pl.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-dom/dom_pt.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-dom/dom_pt_BR.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-dom/dom_ru.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-dom/dom_si.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-dom/dom_sk_SK.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-dom/dom_sv.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-dom/dom_tr.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-dom/dom_uk.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-dom/dom_zh_CN.qm"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/lib/x86_64-linux-gnu/lxqt-panel/libdom.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/lib/x86_64-linux-gnu/lxqt-panel/libdom.so")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/lib/x86_64-linux-gnu/lxqt-panel/libdom.so"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/lib/x86_64-linux-gnu/lxqt-panel/libdom.so")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/usr/lib/x86_64-linux-gnu/lxqt-panel" TYPE MODULE FILES "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-dom/libdom.so")
  if(EXISTS "$ENV{DESTDIR}/usr/lib/x86_64-linux-gnu/lxqt-panel/libdom.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/lib/x86_64-linux-gnu/lxqt-panel/libdom.so")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/lib/x86_64-linux-gnu/lxqt-panel/libdom.so")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/share/lxqt/lxqt-panel/dom.desktop")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/usr/share/lxqt/lxqt-panel" TYPE FILE FILES "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-dom/dom.desktop")
endif()

