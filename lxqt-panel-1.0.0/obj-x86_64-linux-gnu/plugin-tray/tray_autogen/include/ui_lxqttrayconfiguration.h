/********************************************************************************
** Form generated from reading UI file 'lxqttrayconfiguration.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LXQTTRAYCONFIGURATION_H
#define UI_LXQTTRAYCONFIGURATION_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_LXQtTrayConfiguration
{
public:
    QVBoxLayout *verticalLayout;
    QGroupBox *generalGB;
    QGridLayout *gridLayout;
    QLabel *spacingLabel;
    QSpinBox *spacingSB;
    QCheckBox *sortIconsCB;
    QDialogButtonBox *buttons;

    void setupUi(QDialog *LXQtTrayConfiguration)
    {
        if (LXQtTrayConfiguration->objectName().isEmpty())
            LXQtTrayConfiguration->setObjectName(QString::fromUtf8("LXQtTrayConfiguration"));
        verticalLayout = new QVBoxLayout(LXQtTrayConfiguration);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        generalGB = new QGroupBox(LXQtTrayConfiguration);
        generalGB->setObjectName(QString::fromUtf8("generalGB"));
        gridLayout = new QGridLayout(generalGB);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        spacingLabel = new QLabel(generalGB);
        spacingLabel->setObjectName(QString::fromUtf8("spacingLabel"));

        gridLayout->addWidget(spacingLabel, 3, 0, 1, 1);

        spacingSB = new QSpinBox(generalGB);
        spacingSB->setObjectName(QString::fromUtf8("spacingSB"));

        gridLayout->addWidget(spacingSB, 3, 1, 1, 1);

        sortIconsCB = new QCheckBox(generalGB);
        sortIconsCB->setObjectName(QString::fromUtf8("sortIconsCB"));

        gridLayout->addWidget(sortIconsCB, 2, 0, 1, 2);


        verticalLayout->addWidget(generalGB);

        buttons = new QDialogButtonBox(LXQtTrayConfiguration);
        buttons->setObjectName(QString::fromUtf8("buttons"));
        buttons->setStandardButtons(QDialogButtonBox::Close|QDialogButtonBox::Reset);

        verticalLayout->addWidget(buttons);


        retranslateUi(LXQtTrayConfiguration);

        QMetaObject::connectSlotsByName(LXQtTrayConfiguration);
    } // setupUi

    void retranslateUi(QDialog *LXQtTrayConfiguration)
    {
        LXQtTrayConfiguration->setWindowTitle(QCoreApplication::translate("LXQtTrayConfiguration", "System Tray Settings", nullptr));
        generalGB->setTitle(QCoreApplication::translate("LXQtTrayConfiguration", "General", nullptr));
        spacingLabel->setText(QCoreApplication::translate("LXQtTrayConfiguration", "Spacing between icons:", nullptr));
        spacingSB->setSuffix(QCoreApplication::translate("LXQtTrayConfiguration", " px", nullptr));
        sortIconsCB->setText(QCoreApplication::translate("LXQtTrayConfiguration", "Sort icons by window class", nullptr));
    } // retranslateUi

};

namespace Ui {
    class LXQtTrayConfiguration: public Ui_LXQtTrayConfiguration {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LXQTTRAYCONFIGURATION_H
