# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.22

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Disable VCS-based implicit rules.
% : %,v

# Disable VCS-based implicit rules.
% : RCS/%

# Disable VCS-based implicit rules.
% : RCS/%,v

# Disable VCS-based implicit rules.
% : SCCS/s.%

# Disable VCS-based implicit rules.
% : s.%

.SUFFIXES: .hpux_make_needs_suffix_list

# Produce verbose output by default.
VERBOSE = 1

# Command-line flag to silence nested $(MAKE).
$(VERBOSE)MAKESILENT = -s

#Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E rm -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu

# Include any dependencies generated for this target.
include plugin-tray/CMakeFiles/tray.dir/depend.make
# Include any dependencies generated by the compiler for this target.
include plugin-tray/CMakeFiles/tray.dir/compiler_depend.make

# Include the progress variables for this target.
include plugin-tray/CMakeFiles/tray.dir/progress.make

# Include the compile flags for this target's objects.
include plugin-tray/CMakeFiles/tray.dir/flags.make

plugin-tray/tray_arn.qm: ../plugin-tray/translations/tray_arn.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/CMakeFiles --progress-num=$(CMAKE_PROGRESS_1) "Generating tray_arn.qm"
	cd /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray && /usr/lib/qt5/bin/lrelease /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/plugin-tray/translations/tray_arn.ts -qm /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray_arn.qm

plugin-tray/tray_ast.qm: ../plugin-tray/translations/tray_ast.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/CMakeFiles --progress-num=$(CMAKE_PROGRESS_2) "Generating tray_ast.qm"
	cd /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray && /usr/lib/qt5/bin/lrelease /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/plugin-tray/translations/tray_ast.ts -qm /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray_ast.qm

plugin-tray/tray_bg.qm: ../plugin-tray/translations/tray_bg.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/CMakeFiles --progress-num=$(CMAKE_PROGRESS_3) "Generating tray_bg.qm"
	cd /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray && /usr/lib/qt5/bin/lrelease /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/plugin-tray/translations/tray_bg.ts -qm /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray_bg.qm

plugin-tray/tray_ca.qm: ../plugin-tray/translations/tray_ca.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/CMakeFiles --progress-num=$(CMAKE_PROGRESS_4) "Generating tray_ca.qm"
	cd /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray && /usr/lib/qt5/bin/lrelease /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/plugin-tray/translations/tray_ca.ts -qm /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray_ca.qm

plugin-tray/tray_cs.qm: ../plugin-tray/translations/tray_cs.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/CMakeFiles --progress-num=$(CMAKE_PROGRESS_5) "Generating tray_cs.qm"
	cd /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray && /usr/lib/qt5/bin/lrelease /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/plugin-tray/translations/tray_cs.ts -qm /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray_cs.qm

plugin-tray/tray_cy.qm: ../plugin-tray/translations/tray_cy.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/CMakeFiles --progress-num=$(CMAKE_PROGRESS_6) "Generating tray_cy.qm"
	cd /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray && /usr/lib/qt5/bin/lrelease /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/plugin-tray/translations/tray_cy.ts -qm /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray_cy.qm

plugin-tray/tray_da.qm: ../plugin-tray/translations/tray_da.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/CMakeFiles --progress-num=$(CMAKE_PROGRESS_7) "Generating tray_da.qm"
	cd /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray && /usr/lib/qt5/bin/lrelease /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/plugin-tray/translations/tray_da.ts -qm /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray_da.qm

plugin-tray/tray_de.qm: ../plugin-tray/translations/tray_de.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/CMakeFiles --progress-num=$(CMAKE_PROGRESS_8) "Generating tray_de.qm"
	cd /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray && /usr/lib/qt5/bin/lrelease /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/plugin-tray/translations/tray_de.ts -qm /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray_de.qm

plugin-tray/tray_es.qm: ../plugin-tray/translations/tray_es.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/CMakeFiles --progress-num=$(CMAKE_PROGRESS_9) "Generating tray_es.qm"
	cd /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray && /usr/lib/qt5/bin/lrelease /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/plugin-tray/translations/tray_es.ts -qm /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray_es.qm

plugin-tray/tray_et.qm: ../plugin-tray/translations/tray_et.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/CMakeFiles --progress-num=$(CMAKE_PROGRESS_10) "Generating tray_et.qm"
	cd /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray && /usr/lib/qt5/bin/lrelease /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/plugin-tray/translations/tray_et.ts -qm /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray_et.qm

plugin-tray/tray_fr.qm: ../plugin-tray/translations/tray_fr.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/CMakeFiles --progress-num=$(CMAKE_PROGRESS_11) "Generating tray_fr.qm"
	cd /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray && /usr/lib/qt5/bin/lrelease /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/plugin-tray/translations/tray_fr.ts -qm /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray_fr.qm

plugin-tray/tray_gl.qm: ../plugin-tray/translations/tray_gl.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/CMakeFiles --progress-num=$(CMAKE_PROGRESS_12) "Generating tray_gl.qm"
	cd /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray && /usr/lib/qt5/bin/lrelease /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/plugin-tray/translations/tray_gl.ts -qm /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray_gl.qm

plugin-tray/tray_he.qm: ../plugin-tray/translations/tray_he.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/CMakeFiles --progress-num=$(CMAKE_PROGRESS_13) "Generating tray_he.qm"
	cd /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray && /usr/lib/qt5/bin/lrelease /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/plugin-tray/translations/tray_he.ts -qm /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray_he.qm

plugin-tray/tray_hr.qm: ../plugin-tray/translations/tray_hr.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/CMakeFiles --progress-num=$(CMAKE_PROGRESS_14) "Generating tray_hr.qm"
	cd /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray && /usr/lib/qt5/bin/lrelease /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/plugin-tray/translations/tray_hr.ts -qm /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray_hr.qm

plugin-tray/tray_hu.qm: ../plugin-tray/translations/tray_hu.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/CMakeFiles --progress-num=$(CMAKE_PROGRESS_15) "Generating tray_hu.qm"
	cd /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray && /usr/lib/qt5/bin/lrelease /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/plugin-tray/translations/tray_hu.ts -qm /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray_hu.qm

plugin-tray/tray_id.qm: ../plugin-tray/translations/tray_id.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/CMakeFiles --progress-num=$(CMAKE_PROGRESS_16) "Generating tray_id.qm"
	cd /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray && /usr/lib/qt5/bin/lrelease /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/plugin-tray/translations/tray_id.ts -qm /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray_id.qm

plugin-tray/tray_it.qm: ../plugin-tray/translations/tray_it.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/CMakeFiles --progress-num=$(CMAKE_PROGRESS_17) "Generating tray_it.qm"
	cd /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray && /usr/lib/qt5/bin/lrelease /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/plugin-tray/translations/tray_it.ts -qm /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray_it.qm

plugin-tray/tray_ja.qm: ../plugin-tray/translations/tray_ja.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/CMakeFiles --progress-num=$(CMAKE_PROGRESS_18) "Generating tray_ja.qm"
	cd /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray && /usr/lib/qt5/bin/lrelease /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/plugin-tray/translations/tray_ja.ts -qm /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray_ja.qm

plugin-tray/tray_lt.qm: ../plugin-tray/translations/tray_lt.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/CMakeFiles --progress-num=$(CMAKE_PROGRESS_19) "Generating tray_lt.qm"
	cd /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray && /usr/lib/qt5/bin/lrelease /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/plugin-tray/translations/tray_lt.ts -qm /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray_lt.qm

plugin-tray/tray_lv.qm: ../plugin-tray/translations/tray_lv.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/CMakeFiles --progress-num=$(CMAKE_PROGRESS_20) "Generating tray_lv.qm"
	cd /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray && /usr/lib/qt5/bin/lrelease /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/plugin-tray/translations/tray_lv.ts -qm /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray_lv.qm

plugin-tray/tray_nb_NO.qm: ../plugin-tray/translations/tray_nb_NO.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/CMakeFiles --progress-num=$(CMAKE_PROGRESS_21) "Generating tray_nb_NO.qm"
	cd /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray && /usr/lib/qt5/bin/lrelease /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/plugin-tray/translations/tray_nb_NO.ts -qm /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray_nb_NO.qm

plugin-tray/tray_nl.qm: ../plugin-tray/translations/tray_nl.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/CMakeFiles --progress-num=$(CMAKE_PROGRESS_22) "Generating tray_nl.qm"
	cd /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray && /usr/lib/qt5/bin/lrelease /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/plugin-tray/translations/tray_nl.ts -qm /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray_nl.qm

plugin-tray/tray_pl.qm: ../plugin-tray/translations/tray_pl.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/CMakeFiles --progress-num=$(CMAKE_PROGRESS_23) "Generating tray_pl.qm"
	cd /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray && /usr/lib/qt5/bin/lrelease /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/plugin-tray/translations/tray_pl.ts -qm /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray_pl.qm

plugin-tray/tray_pt.qm: ../plugin-tray/translations/tray_pt.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/CMakeFiles --progress-num=$(CMAKE_PROGRESS_24) "Generating tray_pt.qm"
	cd /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray && /usr/lib/qt5/bin/lrelease /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/plugin-tray/translations/tray_pt.ts -qm /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray_pt.qm

plugin-tray/tray_pt_BR.qm: ../plugin-tray/translations/tray_pt_BR.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/CMakeFiles --progress-num=$(CMAKE_PROGRESS_25) "Generating tray_pt_BR.qm"
	cd /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray && /usr/lib/qt5/bin/lrelease /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/plugin-tray/translations/tray_pt_BR.ts -qm /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray_pt_BR.qm

plugin-tray/tray_ru_RU.qm: ../plugin-tray/translations/tray_ru_RU.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/CMakeFiles --progress-num=$(CMAKE_PROGRESS_26) "Generating tray_ru_RU.qm"
	cd /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray && /usr/lib/qt5/bin/lrelease /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/plugin-tray/translations/tray_ru_RU.ts -qm /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray_ru_RU.qm

plugin-tray/tray_si.qm: ../plugin-tray/translations/tray_si.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/CMakeFiles --progress-num=$(CMAKE_PROGRESS_27) "Generating tray_si.qm"
	cd /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray && /usr/lib/qt5/bin/lrelease /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/plugin-tray/translations/tray_si.ts -qm /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray_si.qm

plugin-tray/tray_sk_SK.qm: ../plugin-tray/translations/tray_sk_SK.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/CMakeFiles --progress-num=$(CMAKE_PROGRESS_28) "Generating tray_sk_SK.qm"
	cd /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray && /usr/lib/qt5/bin/lrelease /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/plugin-tray/translations/tray_sk_SK.ts -qm /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray_sk_SK.qm

plugin-tray/tray_tr.qm: ../plugin-tray/translations/tray_tr.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/CMakeFiles --progress-num=$(CMAKE_PROGRESS_29) "Generating tray_tr.qm"
	cd /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray && /usr/lib/qt5/bin/lrelease /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/plugin-tray/translations/tray_tr.ts -qm /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray_tr.qm

plugin-tray/tray_uk.qm: ../plugin-tray/translations/tray_uk.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/CMakeFiles --progress-num=$(CMAKE_PROGRESS_30) "Generating tray_uk.qm"
	cd /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray && /usr/lib/qt5/bin/lrelease /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/plugin-tray/translations/tray_uk.ts -qm /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray_uk.qm

plugin-tray/tray_zh_Hans.qm: ../plugin-tray/translations/tray_zh_Hans.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/CMakeFiles --progress-num=$(CMAKE_PROGRESS_31) "Generating tray_zh_Hans.qm"
	cd /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray && /usr/lib/qt5/bin/lrelease /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/plugin-tray/translations/tray_zh_Hans.ts -qm /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray_zh_Hans.qm

plugin-tray/tray.desktop:
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/CMakeFiles --progress-num=$(CMAKE_PROGRESS_32) "Generating tray.desktop"
	cd /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray && /usr/bin/perl /usr/share/cmake/lxqt-build-tools/modules//LXQtTranslateDesktopYaml.pl /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/plugin-tray/resources/tray.desktop.in tray "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/plugin-tray/translations/tray[_.]*desktop.yaml" >> /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray.desktop

plugin-tray/CMakeFiles/tray.dir/tray_autogen/mocs_compilation.cpp.o: plugin-tray/CMakeFiles/tray.dir/flags.make
plugin-tray/CMakeFiles/tray.dir/tray_autogen/mocs_compilation.cpp.o: plugin-tray/tray_autogen/mocs_compilation.cpp
plugin-tray/CMakeFiles/tray.dir/tray_autogen/mocs_compilation.cpp.o: plugin-tray/CMakeFiles/tray.dir/compiler_depend.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/CMakeFiles --progress-num=$(CMAKE_PROGRESS_33) "Building CXX object plugin-tray/CMakeFiles/tray.dir/tray_autogen/mocs_compilation.cpp.o"
	cd /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray && /usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -MD -MT plugin-tray/CMakeFiles/tray.dir/tray_autogen/mocs_compilation.cpp.o -MF CMakeFiles/tray.dir/tray_autogen/mocs_compilation.cpp.o.d -o CMakeFiles/tray.dir/tray_autogen/mocs_compilation.cpp.o -c /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray_autogen/mocs_compilation.cpp

plugin-tray/CMakeFiles/tray.dir/tray_autogen/mocs_compilation.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/tray.dir/tray_autogen/mocs_compilation.cpp.i"
	cd /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray && /usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray_autogen/mocs_compilation.cpp > CMakeFiles/tray.dir/tray_autogen/mocs_compilation.cpp.i

plugin-tray/CMakeFiles/tray.dir/tray_autogen/mocs_compilation.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/tray.dir/tray_autogen/mocs_compilation.cpp.s"
	cd /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray && /usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray_autogen/mocs_compilation.cpp -o CMakeFiles/tray.dir/tray_autogen/mocs_compilation.cpp.s

plugin-tray/CMakeFiles/tray.dir/lxqttrayconfiguration.cpp.o: plugin-tray/CMakeFiles/tray.dir/flags.make
plugin-tray/CMakeFiles/tray.dir/lxqttrayconfiguration.cpp.o: ../plugin-tray/lxqttrayconfiguration.cpp
plugin-tray/CMakeFiles/tray.dir/lxqttrayconfiguration.cpp.o: plugin-tray/CMakeFiles/tray.dir/compiler_depend.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/CMakeFiles --progress-num=$(CMAKE_PROGRESS_34) "Building CXX object plugin-tray/CMakeFiles/tray.dir/lxqttrayconfiguration.cpp.o"
	cd /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray && /usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -MD -MT plugin-tray/CMakeFiles/tray.dir/lxqttrayconfiguration.cpp.o -MF CMakeFiles/tray.dir/lxqttrayconfiguration.cpp.o.d -o CMakeFiles/tray.dir/lxqttrayconfiguration.cpp.o -c /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/plugin-tray/lxqttrayconfiguration.cpp

plugin-tray/CMakeFiles/tray.dir/lxqttrayconfiguration.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/tray.dir/lxqttrayconfiguration.cpp.i"
	cd /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray && /usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/plugin-tray/lxqttrayconfiguration.cpp > CMakeFiles/tray.dir/lxqttrayconfiguration.cpp.i

plugin-tray/CMakeFiles/tray.dir/lxqttrayconfiguration.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/tray.dir/lxqttrayconfiguration.cpp.s"
	cd /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray && /usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/plugin-tray/lxqttrayconfiguration.cpp -o CMakeFiles/tray.dir/lxqttrayconfiguration.cpp.s

plugin-tray/CMakeFiles/tray.dir/lxqttrayplugin.cpp.o: plugin-tray/CMakeFiles/tray.dir/flags.make
plugin-tray/CMakeFiles/tray.dir/lxqttrayplugin.cpp.o: ../plugin-tray/lxqttrayplugin.cpp
plugin-tray/CMakeFiles/tray.dir/lxqttrayplugin.cpp.o: plugin-tray/CMakeFiles/tray.dir/compiler_depend.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/CMakeFiles --progress-num=$(CMAKE_PROGRESS_35) "Building CXX object plugin-tray/CMakeFiles/tray.dir/lxqttrayplugin.cpp.o"
	cd /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray && /usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -MD -MT plugin-tray/CMakeFiles/tray.dir/lxqttrayplugin.cpp.o -MF CMakeFiles/tray.dir/lxqttrayplugin.cpp.o.d -o CMakeFiles/tray.dir/lxqttrayplugin.cpp.o -c /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/plugin-tray/lxqttrayplugin.cpp

plugin-tray/CMakeFiles/tray.dir/lxqttrayplugin.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/tray.dir/lxqttrayplugin.cpp.i"
	cd /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray && /usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/plugin-tray/lxqttrayplugin.cpp > CMakeFiles/tray.dir/lxqttrayplugin.cpp.i

plugin-tray/CMakeFiles/tray.dir/lxqttrayplugin.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/tray.dir/lxqttrayplugin.cpp.s"
	cd /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray && /usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/plugin-tray/lxqttrayplugin.cpp -o CMakeFiles/tray.dir/lxqttrayplugin.cpp.s

plugin-tray/CMakeFiles/tray.dir/lxqttray.cpp.o: plugin-tray/CMakeFiles/tray.dir/flags.make
plugin-tray/CMakeFiles/tray.dir/lxqttray.cpp.o: ../plugin-tray/lxqttray.cpp
plugin-tray/CMakeFiles/tray.dir/lxqttray.cpp.o: plugin-tray/CMakeFiles/tray.dir/compiler_depend.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/CMakeFiles --progress-num=$(CMAKE_PROGRESS_36) "Building CXX object plugin-tray/CMakeFiles/tray.dir/lxqttray.cpp.o"
	cd /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray && /usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -MD -MT plugin-tray/CMakeFiles/tray.dir/lxqttray.cpp.o -MF CMakeFiles/tray.dir/lxqttray.cpp.o.d -o CMakeFiles/tray.dir/lxqttray.cpp.o -c /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/plugin-tray/lxqttray.cpp

plugin-tray/CMakeFiles/tray.dir/lxqttray.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/tray.dir/lxqttray.cpp.i"
	cd /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray && /usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/plugin-tray/lxqttray.cpp > CMakeFiles/tray.dir/lxqttray.cpp.i

plugin-tray/CMakeFiles/tray.dir/lxqttray.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/tray.dir/lxqttray.cpp.s"
	cd /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray && /usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/plugin-tray/lxqttray.cpp -o CMakeFiles/tray.dir/lxqttray.cpp.s

plugin-tray/CMakeFiles/tray.dir/trayicon.cpp.o: plugin-tray/CMakeFiles/tray.dir/flags.make
plugin-tray/CMakeFiles/tray.dir/trayicon.cpp.o: ../plugin-tray/trayicon.cpp
plugin-tray/CMakeFiles/tray.dir/trayicon.cpp.o: plugin-tray/CMakeFiles/tray.dir/compiler_depend.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/CMakeFiles --progress-num=$(CMAKE_PROGRESS_37) "Building CXX object plugin-tray/CMakeFiles/tray.dir/trayicon.cpp.o"
	cd /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray && /usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -MD -MT plugin-tray/CMakeFiles/tray.dir/trayicon.cpp.o -MF CMakeFiles/tray.dir/trayicon.cpp.o.d -o CMakeFiles/tray.dir/trayicon.cpp.o -c /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/plugin-tray/trayicon.cpp

plugin-tray/CMakeFiles/tray.dir/trayicon.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/tray.dir/trayicon.cpp.i"
	cd /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray && /usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/plugin-tray/trayicon.cpp > CMakeFiles/tray.dir/trayicon.cpp.i

plugin-tray/CMakeFiles/tray.dir/trayicon.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/tray.dir/trayicon.cpp.s"
	cd /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray && /usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/plugin-tray/trayicon.cpp -o CMakeFiles/tray.dir/trayicon.cpp.s

plugin-tray/CMakeFiles/tray.dir/xfitman.cpp.o: plugin-tray/CMakeFiles/tray.dir/flags.make
plugin-tray/CMakeFiles/tray.dir/xfitman.cpp.o: ../plugin-tray/xfitman.cpp
plugin-tray/CMakeFiles/tray.dir/xfitman.cpp.o: plugin-tray/CMakeFiles/tray.dir/compiler_depend.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/CMakeFiles --progress-num=$(CMAKE_PROGRESS_38) "Building CXX object plugin-tray/CMakeFiles/tray.dir/xfitman.cpp.o"
	cd /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray && /usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -MD -MT plugin-tray/CMakeFiles/tray.dir/xfitman.cpp.o -MF CMakeFiles/tray.dir/xfitman.cpp.o.d -o CMakeFiles/tray.dir/xfitman.cpp.o -c /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/plugin-tray/xfitman.cpp

plugin-tray/CMakeFiles/tray.dir/xfitman.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/tray.dir/xfitman.cpp.i"
	cd /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray && /usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/plugin-tray/xfitman.cpp > CMakeFiles/tray.dir/xfitman.cpp.i

plugin-tray/CMakeFiles/tray.dir/xfitman.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/tray.dir/xfitman.cpp.s"
	cd /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray && /usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/plugin-tray/xfitman.cpp -o CMakeFiles/tray.dir/xfitman.cpp.s

plugin-tray/CMakeFiles/tray.dir/LXQtPluginTranslationLoader.cpp.o: plugin-tray/CMakeFiles/tray.dir/flags.make
plugin-tray/CMakeFiles/tray.dir/LXQtPluginTranslationLoader.cpp.o: plugin-tray/LXQtPluginTranslationLoader.cpp
plugin-tray/CMakeFiles/tray.dir/LXQtPluginTranslationLoader.cpp.o: plugin-tray/CMakeFiles/tray.dir/compiler_depend.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/CMakeFiles --progress-num=$(CMAKE_PROGRESS_39) "Building CXX object plugin-tray/CMakeFiles/tray.dir/LXQtPluginTranslationLoader.cpp.o"
	cd /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray && /usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -MD -MT plugin-tray/CMakeFiles/tray.dir/LXQtPluginTranslationLoader.cpp.o -MF CMakeFiles/tray.dir/LXQtPluginTranslationLoader.cpp.o.d -o CMakeFiles/tray.dir/LXQtPluginTranslationLoader.cpp.o -c /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/LXQtPluginTranslationLoader.cpp

plugin-tray/CMakeFiles/tray.dir/LXQtPluginTranslationLoader.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/tray.dir/LXQtPluginTranslationLoader.cpp.i"
	cd /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray && /usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/LXQtPluginTranslationLoader.cpp > CMakeFiles/tray.dir/LXQtPluginTranslationLoader.cpp.i

plugin-tray/CMakeFiles/tray.dir/LXQtPluginTranslationLoader.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/tray.dir/LXQtPluginTranslationLoader.cpp.s"
	cd /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray && /usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/LXQtPluginTranslationLoader.cpp -o CMakeFiles/tray.dir/LXQtPluginTranslationLoader.cpp.s

# Object files for target tray
tray_OBJECTS = \
"CMakeFiles/tray.dir/tray_autogen/mocs_compilation.cpp.o" \
"CMakeFiles/tray.dir/lxqttrayconfiguration.cpp.o" \
"CMakeFiles/tray.dir/lxqttrayplugin.cpp.o" \
"CMakeFiles/tray.dir/lxqttray.cpp.o" \
"CMakeFiles/tray.dir/trayicon.cpp.o" \
"CMakeFiles/tray.dir/xfitman.cpp.o" \
"CMakeFiles/tray.dir/LXQtPluginTranslationLoader.cpp.o"

# External object files for target tray
tray_EXTERNAL_OBJECTS =

plugin-tray/libtray.a: plugin-tray/CMakeFiles/tray.dir/tray_autogen/mocs_compilation.cpp.o
plugin-tray/libtray.a: plugin-tray/CMakeFiles/tray.dir/lxqttrayconfiguration.cpp.o
plugin-tray/libtray.a: plugin-tray/CMakeFiles/tray.dir/lxqttrayplugin.cpp.o
plugin-tray/libtray.a: plugin-tray/CMakeFiles/tray.dir/lxqttray.cpp.o
plugin-tray/libtray.a: plugin-tray/CMakeFiles/tray.dir/trayicon.cpp.o
plugin-tray/libtray.a: plugin-tray/CMakeFiles/tray.dir/xfitman.cpp.o
plugin-tray/libtray.a: plugin-tray/CMakeFiles/tray.dir/LXQtPluginTranslationLoader.cpp.o
plugin-tray/libtray.a: plugin-tray/CMakeFiles/tray.dir/build.make
plugin-tray/libtray.a: plugin-tray/CMakeFiles/tray.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --bold --progress-dir=/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/CMakeFiles --progress-num=$(CMAKE_PROGRESS_40) "Linking CXX static library libtray.a"
	cd /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray && $(CMAKE_COMMAND) -P CMakeFiles/tray.dir/cmake_clean_target.cmake
	cd /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray && $(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/tray.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
plugin-tray/CMakeFiles/tray.dir/build: plugin-tray/libtray.a
.PHONY : plugin-tray/CMakeFiles/tray.dir/build

plugin-tray/CMakeFiles/tray.dir/clean:
	cd /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray && $(CMAKE_COMMAND) -P CMakeFiles/tray.dir/cmake_clean.cmake
.PHONY : plugin-tray/CMakeFiles/tray.dir/clean

plugin-tray/CMakeFiles/tray.dir/depend: plugin-tray/tray.desktop
plugin-tray/CMakeFiles/tray.dir/depend: plugin-tray/tray_arn.qm
plugin-tray/CMakeFiles/tray.dir/depend: plugin-tray/tray_ast.qm
plugin-tray/CMakeFiles/tray.dir/depend: plugin-tray/tray_bg.qm
plugin-tray/CMakeFiles/tray.dir/depend: plugin-tray/tray_ca.qm
plugin-tray/CMakeFiles/tray.dir/depend: plugin-tray/tray_cs.qm
plugin-tray/CMakeFiles/tray.dir/depend: plugin-tray/tray_cy.qm
plugin-tray/CMakeFiles/tray.dir/depend: plugin-tray/tray_da.qm
plugin-tray/CMakeFiles/tray.dir/depend: plugin-tray/tray_de.qm
plugin-tray/CMakeFiles/tray.dir/depend: plugin-tray/tray_es.qm
plugin-tray/CMakeFiles/tray.dir/depend: plugin-tray/tray_et.qm
plugin-tray/CMakeFiles/tray.dir/depend: plugin-tray/tray_fr.qm
plugin-tray/CMakeFiles/tray.dir/depend: plugin-tray/tray_gl.qm
plugin-tray/CMakeFiles/tray.dir/depend: plugin-tray/tray_he.qm
plugin-tray/CMakeFiles/tray.dir/depend: plugin-tray/tray_hr.qm
plugin-tray/CMakeFiles/tray.dir/depend: plugin-tray/tray_hu.qm
plugin-tray/CMakeFiles/tray.dir/depend: plugin-tray/tray_id.qm
plugin-tray/CMakeFiles/tray.dir/depend: plugin-tray/tray_it.qm
plugin-tray/CMakeFiles/tray.dir/depend: plugin-tray/tray_ja.qm
plugin-tray/CMakeFiles/tray.dir/depend: plugin-tray/tray_lt.qm
plugin-tray/CMakeFiles/tray.dir/depend: plugin-tray/tray_lv.qm
plugin-tray/CMakeFiles/tray.dir/depend: plugin-tray/tray_nb_NO.qm
plugin-tray/CMakeFiles/tray.dir/depend: plugin-tray/tray_nl.qm
plugin-tray/CMakeFiles/tray.dir/depend: plugin-tray/tray_pl.qm
plugin-tray/CMakeFiles/tray.dir/depend: plugin-tray/tray_pt.qm
plugin-tray/CMakeFiles/tray.dir/depend: plugin-tray/tray_pt_BR.qm
plugin-tray/CMakeFiles/tray.dir/depend: plugin-tray/tray_ru_RU.qm
plugin-tray/CMakeFiles/tray.dir/depend: plugin-tray/tray_si.qm
plugin-tray/CMakeFiles/tray.dir/depend: plugin-tray/tray_sk_SK.qm
plugin-tray/CMakeFiles/tray.dir/depend: plugin-tray/tray_tr.qm
plugin-tray/CMakeFiles/tray.dir/depend: plugin-tray/tray_uk.qm
plugin-tray/CMakeFiles/tray.dir/depend: plugin-tray/tray_zh_Hans.qm
	cd /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0 /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/plugin-tray /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/CMakeFiles/tray.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : plugin-tray/CMakeFiles/tray.dir/depend

