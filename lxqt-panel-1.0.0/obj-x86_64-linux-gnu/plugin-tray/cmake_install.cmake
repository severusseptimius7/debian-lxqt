# Install script for directory: /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/plugin-tray

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "RelWithDebInfo")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

# Set default install directory permissions.
if(NOT DEFINED CMAKE_OBJDUMP)
  set(CMAKE_OBJDUMP "/usr/bin/objdump")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xRuntimex" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/share/lxqt/translations/lxqt-panel/tray/tray_arn.qm;/usr/share/lxqt/translations/lxqt-panel/tray/tray_ast.qm;/usr/share/lxqt/translations/lxqt-panel/tray/tray_bg.qm;/usr/share/lxqt/translations/lxqt-panel/tray/tray_ca.qm;/usr/share/lxqt/translations/lxqt-panel/tray/tray_cs.qm;/usr/share/lxqt/translations/lxqt-panel/tray/tray_cy.qm;/usr/share/lxqt/translations/lxqt-panel/tray/tray_da.qm;/usr/share/lxqt/translations/lxqt-panel/tray/tray_de.qm;/usr/share/lxqt/translations/lxqt-panel/tray/tray_es.qm;/usr/share/lxqt/translations/lxqt-panel/tray/tray_et.qm;/usr/share/lxqt/translations/lxqt-panel/tray/tray_fr.qm;/usr/share/lxqt/translations/lxqt-panel/tray/tray_gl.qm;/usr/share/lxqt/translations/lxqt-panel/tray/tray_he.qm;/usr/share/lxqt/translations/lxqt-panel/tray/tray_hr.qm;/usr/share/lxqt/translations/lxqt-panel/tray/tray_hu.qm;/usr/share/lxqt/translations/lxqt-panel/tray/tray_id.qm;/usr/share/lxqt/translations/lxqt-panel/tray/tray_it.qm;/usr/share/lxqt/translations/lxqt-panel/tray/tray_ja.qm;/usr/share/lxqt/translations/lxqt-panel/tray/tray_lt.qm;/usr/share/lxqt/translations/lxqt-panel/tray/tray_lv.qm;/usr/share/lxqt/translations/lxqt-panel/tray/tray_nb_NO.qm;/usr/share/lxqt/translations/lxqt-panel/tray/tray_nl.qm;/usr/share/lxqt/translations/lxqt-panel/tray/tray_pl.qm;/usr/share/lxqt/translations/lxqt-panel/tray/tray_pt.qm;/usr/share/lxqt/translations/lxqt-panel/tray/tray_pt_BR.qm;/usr/share/lxqt/translations/lxqt-panel/tray/tray_ru_RU.qm;/usr/share/lxqt/translations/lxqt-panel/tray/tray_si.qm;/usr/share/lxqt/translations/lxqt-panel/tray/tray_sk_SK.qm;/usr/share/lxqt/translations/lxqt-panel/tray/tray_tr.qm;/usr/share/lxqt/translations/lxqt-panel/tray/tray_uk.qm;/usr/share/lxqt/translations/lxqt-panel/tray/tray_zh_Hans.qm")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/usr/share/lxqt/translations/lxqt-panel/tray" TYPE FILE FILES
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray_arn.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray_ast.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray_bg.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray_ca.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray_cs.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray_cy.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray_da.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray_de.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray_es.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray_et.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray_fr.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray_gl.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray_he.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray_hr.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray_hu.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray_id.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray_it.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray_ja.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray_lt.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray_lv.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray_nb_NO.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray_nl.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray_pl.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray_pt.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray_pt_BR.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray_ru_RU.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray_si.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray_sk_SK.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray_tr.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray_uk.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray_zh_Hans.qm"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/share/lxqt/lxqt-panel/tray.desktop")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/usr/share/lxqt/lxqt-panel" TYPE FILE FILES "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-tray/tray.desktop")
endif()

