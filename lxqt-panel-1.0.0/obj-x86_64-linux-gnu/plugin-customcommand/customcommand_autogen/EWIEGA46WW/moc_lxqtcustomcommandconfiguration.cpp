/****************************************************************************
** Meta object code from reading C++ file 'lxqtcustomcommandconfiguration.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.15.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../../../plugin-customcommand/lxqtcustomcommandconfiguration.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'lxqtcustomcommandconfiguration.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.15.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_LXQtCustomCommandConfiguration_t {
    QByteArrayData data[32];
    char stringdata0[498];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_LXQtCustomCommandConfiguration_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_LXQtCustomCommandConfiguration_t qt_meta_stringdata_LXQtCustomCommandConfiguration = {
    {
QT_MOC_LITERAL(0, 0, 30), // "LXQtCustomCommandConfiguration"
QT_MOC_LITERAL(1, 31, 16), // "buttonBoxClicked"
QT_MOC_LITERAL(2, 48, 0), // ""
QT_MOC_LITERAL(3, 49, 16), // "QAbstractButton*"
QT_MOC_LITERAL(4, 66, 3), // "btn"
QT_MOC_LITERAL(5, 70, 11), // "setUiValues"
QT_MOC_LITERAL(6, 82, 17), // "autoRotateChanged"
QT_MOC_LITERAL(7, 100, 10), // "autoRotate"
QT_MOC_LITERAL(8, 111, 17), // "fontButtonClicked"
QT_MOC_LITERAL(9, 129, 11), // "fontChanged"
QT_MOC_LITERAL(10, 141, 10), // "fontString"
QT_MOC_LITERAL(11, 152, 27), // "commandPlainTextEditChanged"
QT_MOC_LITERAL(12, 180, 26), // "runWithBashCheckBoxChanged"
QT_MOC_LITERAL(13, 207, 11), // "runWithBash"
QT_MOC_LITERAL(14, 219, 21), // "repeatCheckBoxChanged"
QT_MOC_LITERAL(15, 241, 6), // "repeat"
QT_MOC_LITERAL(16, 248, 25), // "repeatTimerSpinBoxChanged"
QT_MOC_LITERAL(17, 274, 11), // "repeatTimer"
QT_MOC_LITERAL(18, 286, 19), // "iconLineEditChanged"
QT_MOC_LITERAL(19, 306, 4), // "icon"
QT_MOC_LITERAL(20, 311, 23), // "iconBrowseButtonClicked"
QT_MOC_LITERAL(21, 335, 19), // "textLineEditChanged"
QT_MOC_LITERAL(22, 355, 4), // "text"
QT_MOC_LITERAL(23, 360, 22), // "maxWidthSpinBoxChanged"
QT_MOC_LITERAL(24, 383, 8), // "maxWidth"
QT_MOC_LITERAL(25, 392, 20), // "clickLineEditChanged"
QT_MOC_LITERAL(26, 413, 5), // "click"
QT_MOC_LITERAL(27, 419, 22), // "wheelUpLineEditChanged"
QT_MOC_LITERAL(28, 442, 7), // "wheelUp"
QT_MOC_LITERAL(29, 450, 24), // "wheelDownLineEditChanged"
QT_MOC_LITERAL(30, 475, 9), // "wheelDown"
QT_MOC_LITERAL(31, 485, 12) // "loadSettings"

    },
    "LXQtCustomCommandConfiguration\0"
    "buttonBoxClicked\0\0QAbstractButton*\0"
    "btn\0setUiValues\0autoRotateChanged\0"
    "autoRotate\0fontButtonClicked\0fontChanged\0"
    "fontString\0commandPlainTextEditChanged\0"
    "runWithBashCheckBoxChanged\0runWithBash\0"
    "repeatCheckBoxChanged\0repeat\0"
    "repeatTimerSpinBoxChanged\0repeatTimer\0"
    "iconLineEditChanged\0icon\0"
    "iconBrowseButtonClicked\0textLineEditChanged\0"
    "text\0maxWidthSpinBoxChanged\0maxWidth\0"
    "clickLineEditChanged\0click\0"
    "wheelUpLineEditChanged\0wheelUp\0"
    "wheelDownLineEditChanged\0wheelDown\0"
    "loadSettings"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_LXQtCustomCommandConfiguration[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      17,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   99,    2, 0x08 /* Private */,
       5,    0,  102,    2, 0x08 /* Private */,
       6,    1,  103,    2, 0x08 /* Private */,
       8,    0,  106,    2, 0x08 /* Private */,
       9,    1,  107,    2, 0x08 /* Private */,
      11,    0,  110,    2, 0x08 /* Private */,
      12,    1,  111,    2, 0x08 /* Private */,
      14,    1,  114,    2, 0x08 /* Private */,
      16,    1,  117,    2, 0x08 /* Private */,
      18,    1,  120,    2, 0x08 /* Private */,
      20,    0,  123,    2, 0x08 /* Private */,
      21,    1,  124,    2, 0x08 /* Private */,
      23,    1,  127,    2, 0x08 /* Private */,
      25,    1,  130,    2, 0x08 /* Private */,
      27,    1,  133,    2, 0x08 /* Private */,
      29,    1,  136,    2, 0x08 /* Private */,
      31,    0,  139,    2, 0x09 /* Protected */,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,    7,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   10,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   13,
    QMetaType::Void, QMetaType::Bool,   15,
    QMetaType::Void, QMetaType::Int,   17,
    QMetaType::Void, QMetaType::QString,   19,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   22,
    QMetaType::Void, QMetaType::Int,   24,
    QMetaType::Void, QMetaType::QString,   26,
    QMetaType::Void, QMetaType::QString,   28,
    QMetaType::Void, QMetaType::QString,   30,
    QMetaType::Void,

       0        // eod
};

void LXQtCustomCommandConfiguration::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<LXQtCustomCommandConfiguration *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->buttonBoxClicked((*reinterpret_cast< QAbstractButton*(*)>(_a[1]))); break;
        case 1: _t->setUiValues(); break;
        case 2: _t->autoRotateChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 3: _t->fontButtonClicked(); break;
        case 4: _t->fontChanged((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 5: _t->commandPlainTextEditChanged(); break;
        case 6: _t->runWithBashCheckBoxChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 7: _t->repeatCheckBoxChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 8: _t->repeatTimerSpinBoxChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 9: _t->iconLineEditChanged((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 10: _t->iconBrowseButtonClicked(); break;
        case 11: _t->textLineEditChanged((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 12: _t->maxWidthSpinBoxChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 13: _t->clickLineEditChanged((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 14: _t->wheelUpLineEditChanged((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 15: _t->wheelDownLineEditChanged((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 16: _t->loadSettings(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 0:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QAbstractButton* >(); break;
            }
            break;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject LXQtCustomCommandConfiguration::staticMetaObject = { {
    QMetaObject::SuperData::link<LXQtPanelPluginConfigDialog::staticMetaObject>(),
    qt_meta_stringdata_LXQtCustomCommandConfiguration.data,
    qt_meta_data_LXQtCustomCommandConfiguration,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *LXQtCustomCommandConfiguration::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *LXQtCustomCommandConfiguration::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_LXQtCustomCommandConfiguration.stringdata0))
        return static_cast<void*>(this);
    return LXQtPanelPluginConfigDialog::qt_metacast(_clname);
}

int LXQtCustomCommandConfiguration::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = LXQtPanelPluginConfigDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 17)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 17;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 17)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 17;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
