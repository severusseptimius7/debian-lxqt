# Install script for directory: /home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/plugin-customcommand

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "RelWithDebInfo")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

# Set default install directory permissions.
if(NOT DEFINED CMAKE_OBJDUMP)
  set(CMAKE_OBJDUMP "/usr/bin/objdump")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xRuntimex" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/share/lxqt/translations/lxqt-panel/customcommand/customcommand_bg.qm;/usr/share/lxqt/translations/lxqt-panel/customcommand/customcommand_ca.qm;/usr/share/lxqt/translations/lxqt-panel/customcommand/customcommand_cs.qm;/usr/share/lxqt/translations/lxqt-panel/customcommand/customcommand_da.qm;/usr/share/lxqt/translations/lxqt-panel/customcommand/customcommand_de.qm;/usr/share/lxqt/translations/lxqt-panel/customcommand/customcommand_el.qm;/usr/share/lxqt/translations/lxqt-panel/customcommand/customcommand_es.qm;/usr/share/lxqt/translations/lxqt-panel/customcommand/customcommand_et.qm;/usr/share/lxqt/translations/lxqt-panel/customcommand/customcommand_fi.qm;/usr/share/lxqt/translations/lxqt-panel/customcommand/customcommand_fr.qm;/usr/share/lxqt/translations/lxqt-panel/customcommand/customcommand_hr.qm;/usr/share/lxqt/translations/lxqt-panel/customcommand/customcommand_hu.qm;/usr/share/lxqt/translations/lxqt-panel/customcommand/customcommand_it.qm;/usr/share/lxqt/translations/lxqt-panel/customcommand/customcommand_ja.qm;/usr/share/lxqt/translations/lxqt-panel/customcommand/customcommand_lt.qm;/usr/share/lxqt/translations/lxqt-panel/customcommand/customcommand_lv.qm;/usr/share/lxqt/translations/lxqt-panel/customcommand/customcommand_nb_NO.qm;/usr/share/lxqt/translations/lxqt-panel/customcommand/customcommand_nl.qm;/usr/share/lxqt/translations/lxqt-panel/customcommand/customcommand_pl.qm;/usr/share/lxqt/translations/lxqt-panel/customcommand/customcommand_pt.qm;/usr/share/lxqt/translations/lxqt-panel/customcommand/customcommand_pt_BR.qm;/usr/share/lxqt/translations/lxqt-panel/customcommand/customcommand_ru.qm;/usr/share/lxqt/translations/lxqt-panel/customcommand/customcommand_sk.qm;/usr/share/lxqt/translations/lxqt-panel/customcommand/customcommand_sr.qm;/usr/share/lxqt/translations/lxqt-panel/customcommand/customcommand_sv.qm;/usr/share/lxqt/translations/lxqt-panel/customcommand/customcommand_tr.qm;/usr/share/lxqt/translations/lxqt-panel/customcommand/customcommand_uk.qm;/usr/share/lxqt/translations/lxqt-panel/customcommand/customcommand_zh_CN.qm;/usr/share/lxqt/translations/lxqt-panel/customcommand/customcommand_zh_TW.qm")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/usr/share/lxqt/translations/lxqt-panel/customcommand" TYPE FILE FILES
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-customcommand/customcommand_bg.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-customcommand/customcommand_ca.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-customcommand/customcommand_cs.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-customcommand/customcommand_da.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-customcommand/customcommand_de.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-customcommand/customcommand_el.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-customcommand/customcommand_es.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-customcommand/customcommand_et.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-customcommand/customcommand_fi.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-customcommand/customcommand_fr.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-customcommand/customcommand_hr.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-customcommand/customcommand_hu.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-customcommand/customcommand_it.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-customcommand/customcommand_ja.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-customcommand/customcommand_lt.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-customcommand/customcommand_lv.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-customcommand/customcommand_nb_NO.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-customcommand/customcommand_nl.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-customcommand/customcommand_pl.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-customcommand/customcommand_pt.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-customcommand/customcommand_pt_BR.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-customcommand/customcommand_ru.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-customcommand/customcommand_sk.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-customcommand/customcommand_sr.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-customcommand/customcommand_sv.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-customcommand/customcommand_tr.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-customcommand/customcommand_uk.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-customcommand/customcommand_zh_CN.qm"
    "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-customcommand/customcommand_zh_TW.qm"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/lib/x86_64-linux-gnu/lxqt-panel/libcustomcommand.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/lib/x86_64-linux-gnu/lxqt-panel/libcustomcommand.so")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/lib/x86_64-linux-gnu/lxqt-panel/libcustomcommand.so"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/lib/x86_64-linux-gnu/lxqt-panel/libcustomcommand.so")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/usr/lib/x86_64-linux-gnu/lxqt-panel" TYPE MODULE FILES "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-customcommand/libcustomcommand.so")
  if(EXISTS "$ENV{DESTDIR}/usr/lib/x86_64-linux-gnu/lxqt-panel/libcustomcommand.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/lib/x86_64-linux-gnu/lxqt-panel/libcustomcommand.so")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/lib/x86_64-linux-gnu/lxqt-panel/libcustomcommand.so")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/share/lxqt/lxqt-panel/customcommand.desktop")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/usr/share/lxqt/lxqt-panel" TYPE FILE FILES "/home/debian/lxqt/lxqt-panel/lxqt-panel-1.0.0/obj-x86_64-linux-gnu/plugin-customcommand/customcommand.desktop")
endif()

