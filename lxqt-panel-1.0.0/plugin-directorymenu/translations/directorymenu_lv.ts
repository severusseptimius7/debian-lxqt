<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="lv">
<context>
    <name>DirectoryMenu</name>
    <message>
        <location filename="../directorymenu.cpp" line="125"/>
        <source>Open</source>
        <translation>Atvērt</translation>
    </message>
    <message>
        <location filename="../directorymenu.cpp" line="129"/>
        <source>Open in terminal</source>
        <translation>Atvērt konsolē (terminālī)</translation>
    </message>
</context>
<context>
    <name>DirectoryMenuConfiguration</name>
    <message>
        <location filename="../directorymenuconfiguration.ui" line="14"/>
        <source>Directory Menu Settings</source>
        <translation>Mapju izvēlnes iestatījumi</translation>
    </message>
    <message>
        <location filename="../directorymenuconfiguration.ui" line="20"/>
        <source>Appearance</source>
        <translation>Izskats</translation>
    </message>
    <message>
        <location filename="../directorymenuconfiguration.ui" line="26"/>
        <source>Base directory:</source>
        <translation>Bāzes katalogs/mape/direktorija:</translation>
    </message>
    <message>
        <location filename="../directorymenuconfiguration.ui" line="39"/>
        <source>BaseDirectoryName</source>
        <translation>BāzesKatalogaNosaukums</translation>
    </message>
    <message>
        <location filename="../directorymenuconfiguration.ui" line="46"/>
        <source>Icon:</source>
        <translation>Ikona:</translation>
    </message>
    <message>
        <location filename="../directorymenuconfiguration.ui" line="66"/>
        <source>Terminal</source>
        <translation>Termināls</translation>
    </message>
    <message>
        <location filename="../directorymenuconfiguration.ui" line="73"/>
        <location filename="../directorymenuconfiguration.cpp" line="112"/>
        <source>Choose Default Terminal</source>
        <translation>Izvēlieties noklusējuma termināli</translation>
    </message>
    <message>
        <location filename="../directorymenuconfiguration.cpp" line="96"/>
        <source>Choose Base Directory</source>
        <translation>Izvēlieties bāzes mapi</translation>
    </message>
    <message>
        <location filename="../directorymenuconfiguration.cpp" line="126"/>
        <source>Choose Icon</source>
        <translation>Izvēlieties ikonu</translation>
    </message>
    <message>
        <location filename="../directorymenuconfiguration.cpp" line="126"/>
        <source>Icons (*.png *.xpm *.jpg)</source>
        <translation>Ikonas (*.png *.xpm *.jpg)</translation>
    </message>
    <message>
        <location filename="../directorymenuconfiguration.cpp" line="135"/>
        <source>Directory Menu</source>
        <translation>Mapju izvēlne</translation>
    </message>
    <message>
        <location filename="../directorymenuconfiguration.cpp" line="135"/>
        <source>An error occurred while loading the icon.</source>
        <translation>Ielādējot ikonu atgadījās kļūme.</translation>
    </message>
</context>
</TS>
