<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="lv">
<context>
    <name>LXQtTrayConfiguration</name>
    <message>
        <location filename="../lxqttrayconfiguration.ui" line="6"/>
        <source>System Tray Settings</source>
        <translation>Sistēmas teknes (tray) iestatījumi</translation>
    </message>
    <message>
        <location filename="../lxqttrayconfiguration.ui" line="12"/>
        <source>General</source>
        <translation>Visparīgie</translation>
    </message>
    <message>
        <location filename="../lxqttrayconfiguration.ui" line="18"/>
        <source>Spacing between icons:</source>
        <translation>Atstatums starp ikonām:</translation>
    </message>
    <message>
        <location filename="../lxqttrayconfiguration.ui" line="25"/>
        <source> px</source>
        <translation> pikseļi</translation>
    </message>
    <message>
        <location filename="../lxqttrayconfiguration.ui" line="32"/>
        <source>Sort icons by window class</source>
        <translation>Kārtot/grupēt ikonas pēc logu klasēm</translation>
    </message>
</context>
</TS>
