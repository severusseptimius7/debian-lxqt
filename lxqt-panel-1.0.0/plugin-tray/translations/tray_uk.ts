<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="uk">
<context>
    <name>LXQtTrayConfiguration</name>
    <message>
        <location filename="../lxqttrayconfiguration.ui" line="6"/>
        <source>System Tray Settings</source>
        <translation>Налаштування системного лотка</translation>
    </message>
    <message>
        <location filename="../lxqttrayconfiguration.ui" line="12"/>
        <source>General</source>
        <translation>Загальні</translation>
    </message>
    <message>
        <location filename="../lxqttrayconfiguration.ui" line="18"/>
        <source>Spacing between icons:</source>
        <translation>Відстань між піктограмами:</translation>
    </message>
    <message>
        <location filename="../lxqttrayconfiguration.ui" line="25"/>
        <source> px</source>
        <translation> px</translation>
    </message>
    <message>
        <location filename="../lxqttrayconfiguration.ui" line="32"/>
        <source>Sort icons by window class</source>
        <translation>Сортувати піктограми за класом вікна</translation>
    </message>
</context>
</TS>
