<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ca">
<context>
    <name>LXQtTrayConfiguration</name>
    <message>
        <location filename="../lxqttrayconfiguration.ui" line="6"/>
        <source>System Tray Settings</source>
        <translation>Arranjament de la safata del sistema</translation>
    </message>
    <message>
        <location filename="../lxqttrayconfiguration.ui" line="12"/>
        <source>General</source>
        <translation>General</translation>
    </message>
    <message>
        <location filename="../lxqttrayconfiguration.ui" line="18"/>
        <source>Spacing between icons:</source>
        <translation>Espai entre icones:</translation>
    </message>
    <message>
        <location filename="../lxqttrayconfiguration.ui" line="25"/>
        <source> px</source>
        <translation> px</translation>
    </message>
    <message>
        <location filename="../lxqttrayconfiguration.ui" line="32"/>
        <source>Sort icons by window class</source>
        <translation>Ordena les icones per classe de finestra</translation>
    </message>
</context>
</TS>
